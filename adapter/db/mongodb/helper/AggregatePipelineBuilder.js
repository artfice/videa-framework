"use strict";
var MongodbQueryHelper_1 = require("./MongodbQueryHelper");
var AggregatePipelineBuilder;
(function (AggregatePipelineBuilder) {
    /**
     * NOTE the order of the pipeline parameters matters, for the current implemented stages in the pipline
     * the order should be LookUp, Match, Project, Sort, Group, Project.
     *
     * Restrictions found:
     * - LookUp should be done at the beginning in case we need to query fields of foreign collection
     * - Match is where the query is stored and should be before Sort to preserve the correct order of the results
     * - The using Group is being use to store the results (actual results) and total (number of results), so this
     * should be done before the last Project that's currently limiting and skipping the results.
     *
     * More about the pipeline: https://docs.mongodb.com/manual/core/aggregation-pipeline/
     */
    /**
     * This function builds the MongoDB pipeline in aggregate
     * More about the pipeline: https://docs.mongodb.com/manual/core/aggregation-pipeline/
     * @param options aggregate params for the pipeline
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param defaultProjectFields default projected fields
     */
    function buildPipeline(options, docPrefix, defaultProjectFields) {
        var pipeline = [];
        pipeline.push({ $lookup: buildLookup(options.lookUp, docPrefix) });
        var buildQueryOptions = {
            includeDeleted: false
        };
        var query = buildQuery(options.query, docPrefix, buildQueryOptions, options.lookUp.output);
        query = query || {};
        pipeline.push({ $match: query });
        // set the default projection
        var projectedFields = options.projection;
        projectedFields = projectedFields || defaultProjectFields;
        if (projectedFields) {
            pipeline.push({ $project: buildProjection(options, projectedFields, docPrefix) });
        }
        if (options.sort) {
            pipeline.push(buildSort(options, docPrefix));
        }
        pipeline.push({ $group: buildGroupToCount() });
        pipeline.push({ $project: buildProjectWithLimitter(options) });
        return pipeline;
    }
    AggregatePipelineBuilder.buildPipeline = buildPipeline;
    /**
     * This function prepares the MongoDB sort for pipeline in aggregate
     * @param options aggregate params for the pipeline
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     */
    function buildSort(options, docPrefix) {
        var lookUpField = options.lookUp ? options.lookUp.output : undefined;
        var asc = 1;
        if (options.ascending !== undefined) {
            asc = options.ascending ? 1 : -1;
        }
        if (lookUpField) {
            options.sort = replaceLookupField(options.sort, lookUpField, docPrefix);
        }
        var sortObj = {};
        sortObj[docPrefix + '.' + options.sort] = asc;
        return { $sort: sortObj };
    }
    /**
     * This function builds the MongoDB look up for pipeline in aggregate
     * @param lookUp lookup params for the aggregate pipeline
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     */
    function buildLookup(lookUp, docPrefix) {
        lookUp.output = lookUp.output || lookUp.fromCollection;
        return {
            from: lookUp.fromCollection,
            localField: docPrefix + '.' + lookUp.localField,
            foreignField: docPrefix + '.' + lookUp.foreignField,
            as: docPrefix + '.' + lookUp.output
        };
    }
    /**
     * This function builds the MongoDB project for pipeline in aggregate
     * @param lookUp lookup params for the aggregate pipeline
     * @param projectedFields object with the projected fields
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     */
    function buildProjection(options, projectedFields, docPrefix) {
        var lookUpField = options.lookUp ? options.lookUp.output : undefined;
        if (!projectedFields) {
            return undefined;
        }
        var projection = {};
        var keys = Object.keys(projectedFields);
        for (var i = 0; i < keys.length; i++) {
            var field = projectedFields[i];
            if (lookUpField) {
                field = replaceLookupField(field, lookUpField, docPrefix);
            }
            projection[docPrefix + '.' + field] = projectedFields[field];
        }
        return projection;
    }
    function buildQuery(paramQuery, docPrefix, options, lookUpOutputField) {
        var query = MongodbQueryHelper_1.MongodbQueryHelper.buildQuery(paramQuery, docPrefix, lookUpOutputField);
        if (options && !options.includeDeleted) {
            var deleteQuery = { markForDelete: { $exists: false } };
            query = query ? { $and: [query, deleteQuery] } : deleteQuery;
        }
        return query;
    }
    /**
     * This function builds group for the pipeline aggregate, to be able to count the users in a single aggregate call.
     * This will make the results of the aggregate stored in the results attributte and total will represent the total of
     * elements from the aggregate
     */
    function buildGroupToCount() {
        return { _id: null, total: { $sum: 1 }, results: { $push: '$$ROOT' } };
    }
    /**
     * This function will build a projection with the actual limit and skip of the results
     * @param options aggregate params for the pipeline
     */
    function buildProjectWithLimitter(options) {
        var project = { total: 1, results: 1 };
        if (options.offset && options.setSize) {
            project.results = { $slice: ['$results', options.offset, options.setSize] };
        }
        else if (!options.offset && options.setSize) {
            project.results = { $slice: ['$results', options.setSize] };
        }
        return project;
    }
    function replaceLookupField(fieldToReplace, lookUpField, docPrefix) {
        var lookUpFieldString = lookUpField + '.';
        if (fieldToReplace.indexOf(lookUpField) > -1) {
            fieldToReplace = fieldToReplace.replace(lookUpFieldString, lookUpFieldString + docPrefix + '.');
        }
        return fieldToReplace;
    }
})(AggregatePipelineBuilder = exports.AggregatePipelineBuilder || (exports.AggregatePipelineBuilder = {}));
