"use strict";
var _ = require("lodash");
var Digi_1 = require("../../../../Digi");
var UpdateOperators_1 = require("../../../../domain/db/UpdateOperators");
var MongoDbUpdateOperators_1 = require("../enum/MongoDbUpdateOperators");
/**
 * This modules is to build the MongoDb update parameter specific here:
 * https://docs.mongodb.com/manual/reference/method/db.collection.update/#update-parameter
 */
var MongodbDocumentUpdateBuilder;
(function (MongodbDocumentUpdateBuilder) {
    /**
     * Builds the update parameters to be send to mongodb update
     * @param dto with the fields that will be updated
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param options videa update options
     * @return mongodb update document
     */
    function buildUpdateParameter(dto, docPrefix, options) {
        var fixedOptions = buildOptionsWithPrefix(docPrefix, options);
        var item = {};
        item[docPrefix] = dto;
        var updateSelectedFields = Digi_1.Obj.flattenObject(item);
        // safety check
        if (!updateSelectedFields) {
            return {};
        }
        return buildUpdateDocument(updateSelectedFields, docPrefix, fixedOptions, options);
    }
    MongodbDocumentUpdateBuilder.buildUpdateParameter = buildUpdateParameter;
    /**
     * This function will add fix the options by adding the doc prefix to the field
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param options videa update options
     * @return options with the correct fields
     */
    function buildOptionsWithPrefix(docPrefix, options) {
        var fixedOptions = {};
        if (options && options.fieldOptions) {
            var prefix_1 = docPrefix + '.';
            _.forOwn(options.fieldOptions, function (value, key) {
                fixedOptions[prefix_1 + key] = value;
            });
        }
        return fixedOptions;
    }
    /**
     * Builds the actual mongodb update document, according to the supported params
     * @param updateSelectedFields document to update with the normalized fields(with docPrefix)
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param fieldOptions with the normalize fields(with docPrefix)
     * @param options videa update options
     * @return mongodb update document
     */
    function buildUpdateDocument(updateSelectedFields, docPrefix, fieldOptions, options) {
        var updateFieldsDictionary = buildUpdateFieldsDictionary(updateSelectedFields, fieldOptions);
        var updateDocument = {};
        _.forOwn(updateFieldsDictionary, function (value, mongoOperator) {
            addNonEmptyFieldsToUpdateDocument(value, mongoOperator, updateDocument);
        });
        if (!_.isEmpty(options) && options.upsert) {
            var createdDateUpdate = {};
            createdDateUpdate[docPrefix + '.createdDate'] = new Date();
            addNonEmptyFieldsToUpdateDocument(createdDateUpdate, MongoDbUpdateOperators_1.MongoDbUpdateOperators.SET_ON_INSERT, updateDocument);
        }
        return updateDocument;
    }
    function buildUpdateFieldsDictionary(updateSelectedFields, fieldOptions) {
        var operatorsList = _.values(MongoDbUpdateOperators_1.MongoDbUpdateOperators);
        operatorsList.splice(operatorsList.indexOf(MongoDbUpdateOperators_1.MongoDbUpdateOperators.SET_ON_INSERT), 1); // remove this property $setOnInsert
        // dic<MongoDbUpdateOperators, any> initialize with empty object
        var updateFieldsDictionary = _.zipObject(operatorsList, _.map(operatorsList, function () { return {}; }));
        _.forOwn(updateSelectedFields, function (fieldValue, field) {
            if (_.isNil(fieldValue)) {
                updateFieldsDictionary[MongoDbUpdateOperators_1.MongoDbUpdateOperators.UNSET][field] = 1;
            }
            else if (fieldOptions[field]) {
                switch (fieldOptions[field].operator) {
                    case UpdateOperators_1.UpdateOperators.push:
                        updateFieldsDictionary[MongoDbUpdateOperators_1.MongoDbUpdateOperators.PUSH][field] = { $each: fieldValue };
                        break;
                    case UpdateOperators_1.UpdateOperators.addToSet:
                        updateFieldsDictionary[MongoDbUpdateOperators_1.MongoDbUpdateOperators.ADD_TO_SET][field] = { $each: fieldValue };
                        break;
                    case UpdateOperators_1.UpdateOperators.pullFromElement:
                        var updateFieldObject = updateFieldsDictionary[MongoDbUpdateOperators_1.MongoDbUpdateOperators.PULL_ALL];
                        updateFieldObject[field + '.$.' + fieldOptions[field].elementField] = fieldValue;
                        break;
                    case UpdateOperators_1.UpdateOperators.pull:
                        updateFieldsDictionary[MongoDbUpdateOperators_1.MongoDbUpdateOperators.PULL][field] = { $in: fieldValue };
                        break;
                    case UpdateOperators_1.UpdateOperators.unset:
                        updateFieldsDictionary[MongoDbUpdateOperators_1.MongoDbUpdateOperators.UNSET][field] = 1;
                        break;
                    default:
                        updateFieldsDictionary[MongoDbUpdateOperators_1.MongoDbUpdateOperators.SET][field] = fieldValue;
                }
            }
            else {
                updateFieldsDictionary[MongoDbUpdateOperators_1.MongoDbUpdateOperators.SET][field] = fieldValue;
            }
        });
        return updateFieldsDictionary;
    }
    function addNonEmptyFieldsToUpdateDocument(fields, instructionName, updateDocument) {
        if (!_.isEmpty(fields)) {
            updateDocument[instructionName] = fields;
        }
    }
})(MongodbDocumentUpdateBuilder = exports.MongodbDocumentUpdateBuilder || (exports.MongodbDocumentUpdateBuilder = {}));
