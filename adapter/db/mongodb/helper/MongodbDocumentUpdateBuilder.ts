import * as _ from 'lodash';

import { Obj } from '../../../../Digi';
import { UpdateOperators } from '../../../../domain/db/UpdateOperators';
import { UpdateOptions } from '../../../../domain/db/UpdateOptions';

import { MongoDbUpdateOperators } from '../enum/MongoDbUpdateOperators';
/**
 * This modules is to build the MongoDb update parameter specific here:
 * https://docs.mongodb.com/manual/reference/method/db.collection.update/#update-parameter
 */
export module MongodbDocumentUpdateBuilder {

    /**
     * Builds the update parameters to be send to mongodb update
     * @param dto with the fields that will be updated
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param options videa update options
     * @return mongodb update document
     */
    export function buildUpdateParameter<T>(dto: T,
                                            docPrefix: string,
                                            options?: UpdateOptions): any {

        const fixedOptions = buildOptionsWithPrefix(docPrefix, options);
        const item = {};
        item[docPrefix] = dto;

        const updateSelectedFields = Obj.flattenObject(item);
        // safety check
        if (!updateSelectedFields) {
            return {};
        }

        return buildUpdateDocument(updateSelectedFields, docPrefix, fixedOptions, options);
    }

    /**
     * This function will add fix the options by adding the doc prefix to the field
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param options videa update options
     * @return options with the correct fields
     */
    function buildOptionsWithPrefix(docPrefix: string, options?: UpdateOptions) : any {
        const fixedOptions = {};

        if (options && options.fieldOptions) {
            const prefix = docPrefix + '.';
            _.forOwn(options.fieldOptions, (value, key) => {
                fixedOptions[prefix + key] = value;
            });
        }

        return fixedOptions;
    }

    /**
     * Builds the actual mongodb update document, according to the supported params
     * @param updateSelectedFields document to update with the normalized fields(with docPrefix)
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param fieldOptions with the normalize fields(with docPrefix)
     * @param options videa update options
     * @return mongodb update document
     */
    function buildUpdateDocument(updateSelectedFields: any, docPrefix: string,  fieldOptions: any, options: UpdateOptions) : any {
        const  updateFieldsDictionary = buildUpdateFieldsDictionary(updateSelectedFields, fieldOptions);
        const updateDocument = <any>{};

        _.forOwn(updateFieldsDictionary, (value, mongoOperator) => {
            addNonEmptyFieldsToUpdateDocument(value, <MongoDbUpdateOperators>mongoOperator, updateDocument);
        });

        if (!_.isEmpty(options) && options.upsert) {
            const createdDateUpdate = {};
            createdDateUpdate[docPrefix + '.createdDate'] = new Date();
            addNonEmptyFieldsToUpdateDocument(createdDateUpdate, MongoDbUpdateOperators.SET_ON_INSERT, updateDocument);
        }

        return updateDocument;
    }

    function buildUpdateFieldsDictionary(updateSelectedFields: any, fieldOptions: any) : any {
        const operatorsList = _.values(MongoDbUpdateOperators);
        operatorsList.splice(operatorsList.indexOf(MongoDbUpdateOperators.SET_ON_INSERT), 1); // remove this property $setOnInsert

        // dic<MongoDbUpdateOperators, any> initialize with empty object
        const updateFieldsDictionary =  _.zipObject(operatorsList, _.map(operatorsList, () => { return {}; }));

        _.forOwn(updateSelectedFields, (fieldValue, field) => {
            if (_.isNil(fieldValue)) {
                updateFieldsDictionary[MongoDbUpdateOperators.UNSET][field] = 1;
            } else if (fieldOptions[field]) {
                switch (fieldOptions[field].operator) {
                    case UpdateOperators.push:
                        updateFieldsDictionary[MongoDbUpdateOperators.PUSH][field] = {$each: fieldValue};
                        break;
                    case UpdateOperators.addToSet:
                        updateFieldsDictionary[MongoDbUpdateOperators.ADD_TO_SET][field] = {$each: fieldValue};
                        break;
                    case UpdateOperators.pullFromElement:
                        const updateFieldObject = updateFieldsDictionary[MongoDbUpdateOperators.PULL_ALL];
                        updateFieldObject[field + '.$.' + fieldOptions[field].elementField] = fieldValue;
                        break;
                    case UpdateOperators.pull:
                        updateFieldsDictionary[MongoDbUpdateOperators.PULL][field] = {$in: fieldValue};
                        break;
                    case UpdateOperators.unset:
                        updateFieldsDictionary[MongoDbUpdateOperators.UNSET][field] = 1;
                        break;
                    default:
                        updateFieldsDictionary[MongoDbUpdateOperators.SET][field] = fieldValue;
                }
            } else {
                updateFieldsDictionary[MongoDbUpdateOperators.SET][field] = fieldValue;
            }
        });

        return updateFieldsDictionary;
    }

    function addNonEmptyFieldsToUpdateDocument(fields: any, instructionName: MongoDbUpdateOperators, updateDocument: any): void {
        if (!_.isEmpty(fields)) {
            updateDocument[instructionName] = fields;
        }
    }
}
