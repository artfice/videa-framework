import * as _ from 'lodash';

import {SearchOperators} from '../../../../domain/db/SearchOperators';
import {SearchQuery} from '../../../../domain/db/SearchQuery';

export module MongodbQueryHelper {
    /**
     * This function prepares the MongoDB query
     * @param paramQuery the videa search query to be converted
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param lookUpField is the field where the foreign document from the different collection will be inserted
     */
    export function buildQuery(paramQuery: SearchQuery, docPrefix?: string, lookUpField?: string): any {

        if (!paramQuery) {
            return;
        }

        let query = {};
        let fieldName;

        if (paramQuery.field) {
            fieldName = constructField(paramQuery.field, docPrefix, lookUpField);
        }

        if (paramQuery.operator === SearchOperators.NE) {
            query[fieldName] = {$ne: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.NIn) {
            query[fieldName] = {$nin: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.In) {
            query[fieldName] = {$in: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.Equal) {
            query[fieldName] = paramQuery.value;
        } else if (paramQuery.operator === SearchOperators.Regex) {
            query[fieldName] = { $regex: paramQuery.value, $options: 'i' };
        } else if (paramQuery.operator === SearchOperators.And) {
            const conditions = [];
            if (paramQuery.value) {
                const keys = Object.keys(paramQuery.value);
                for (let c = 0; c < keys.length; c++) {
                    conditions.push(buildQuery(paramQuery.value[keys[c]], docPrefix, lookUpField));
                }
            }
            query = {$and: conditions};
        } else if (paramQuery.operator === SearchOperators.Or) {
            const conditions = [];
            if (paramQuery.value) {
                const keys = Object.keys(paramQuery.value);
                for (let c = 0; c < keys.length; c++) {
                    conditions.push(buildQuery(paramQuery.value[keys[c]], docPrefix, lookUpField));
                }
            }
            query = {$or: conditions};
        } else if (paramQuery.operator === SearchOperators.Ids) {
            fieldName = '_id';
            query[fieldName] = {$in: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.Exists) {
            query[fieldName] = {$exists: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.Lt) {
            query[fieldName] = {$lt: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.Lte) {
            query[fieldName] = {$lte: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.Gt) {
            query[fieldName] = {$gt: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.Gte) {
            query[fieldName] = {$gte: paramQuery.value};
        } else if (paramQuery.operator === SearchOperators.ElementMatch) {
            query[fieldName] = {$elemMatch: buildElementMatchQuery(paramQuery)};
        }

        return query;
    }

    /**
     * This function prepares the fields to the raw format, with the docPrefix and lookField in case this exists
     * @param field is the name of the field to be updated
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param lookUpField is the field where the foreign document from the different collection will be inserted
     */
    function constructField(field: string, docPrefix?: string, lookUpField?: string) : string {
        if (!docPrefix) {
            return field;
        }

        let fieldName = field === 'id' ? '_id' : docPrefix + '.' + field;
        if (!lookUpField) {
            return fieldName;
        }
        const lookUpFieldString = lookUpField + '.';
        if (fieldName.indexOf(lookUpFieldString) > -1) {
            fieldName = fieldName.replace(lookUpFieldString, lookUpFieldString + docPrefix + '.');
        }
        return fieldName;
    }

    /**
     * This functions builds a mongodb element match query, to be able to query specific properties in an array
     * @param paramQuery the videa search query to be converted
     * @return returns a element match query
     */
    function buildElementMatchQuery(paramQuery: SearchQuery) : any {
        /** NOTE Element match is a special case, it receives a object of query
         * https://docs.mongodb.com/v3.2/reference/operator/query/elemMatch/
         * the encapsulated fields on the should start from the field which the $elementMatch is being applied
         */
        const elementMatchQuery = {};
        paramQuery.value.forEach((query) => {
            const elementQuery = buildQuery(query);
            const firstKey = _.head(_.keys(elementQuery));
            if (firstKey) {
                elementMatchQuery[firstKey] = elementQuery[firstKey];
            }
        });

        return elementMatchQuery;
    }
}
