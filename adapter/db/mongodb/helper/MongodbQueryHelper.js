"use strict";
var _ = require("lodash");
var SearchOperators_1 = require("../../../../domain/db/SearchOperators");
var MongodbQueryHelper;
(function (MongodbQueryHelper) {
    /**
     * This function prepares the MongoDB query
     * @param paramQuery the videa search query to be converted
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param lookUpField is the field where the foreign document from the different collection will be inserted
     */
    function buildQuery(paramQuery, docPrefix, lookUpField) {
        if (!paramQuery) {
            return;
        }
        var query = {};
        var fieldName;
        if (paramQuery.field) {
            fieldName = constructField(paramQuery.field, docPrefix, lookUpField);
        }
        if (paramQuery.operator === SearchOperators_1.SearchOperators.NE) {
            query[fieldName] = { $ne: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.NIn) {
            query[fieldName] = { $nin: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.In) {
            query[fieldName] = { $in: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Equal) {
            query[fieldName] = paramQuery.value;
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Regex) {
            query[fieldName] = { $regex: paramQuery.value, $options: 'i' };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.And) {
            var conditions = [];
            if (paramQuery.value) {
                var keys = Object.keys(paramQuery.value);
                for (var c = 0; c < keys.length; c++) {
                    conditions.push(buildQuery(paramQuery.value[keys[c]], docPrefix, lookUpField));
                }
            }
            query = { $and: conditions };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Or) {
            var conditions = [];
            if (paramQuery.value) {
                var keys = Object.keys(paramQuery.value);
                for (var c = 0; c < keys.length; c++) {
                    conditions.push(buildQuery(paramQuery.value[keys[c]], docPrefix, lookUpField));
                }
            }
            query = { $or: conditions };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Ids) {
            fieldName = '_id';
            query[fieldName] = { $in: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Exists) {
            query[fieldName] = { $exists: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Lt) {
            query[fieldName] = { $lt: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Lte) {
            query[fieldName] = { $lte: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Gt) {
            query[fieldName] = { $gt: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.Gte) {
            query[fieldName] = { $gte: paramQuery.value };
        }
        else if (paramQuery.operator === SearchOperators_1.SearchOperators.ElementMatch) {
            query[fieldName] = { $elemMatch: buildElementMatchQuery(paramQuery) };
        }
        return query;
    }
    MongodbQueryHelper.buildQuery = buildQuery;
    /**
     * This function prepares the fields to the raw format, with the docPrefix and lookField in case this exists
     * @param field is the name of the field to be updated
     * @param docPrefix is the prefix version where the document will be inserted in the MongoDB
     * @param lookUpField is the field where the foreign document from the different collection will be inserted
     */
    function constructField(field, docPrefix, lookUpField) {
        if (!docPrefix) {
            return field;
        }
        var fieldName = field === 'id' ? '_id' : docPrefix + '.' + field;
        if (!lookUpField) {
            return fieldName;
        }
        var lookUpFieldString = lookUpField + '.';
        if (fieldName.indexOf(lookUpFieldString) > -1) {
            fieldName = fieldName.replace(lookUpFieldString, lookUpFieldString + docPrefix + '.');
        }
        return fieldName;
    }
    /**
     * This functions builds a mongodb element match query, to be able to query specific properties in an array
     * @param paramQuery the videa search query to be converted
     * @return returns a element match query
     */
    function buildElementMatchQuery(paramQuery) {
        /** NOTE Element match is a special case, it receives a object of query
         * https://docs.mongodb.com/v3.2/reference/operator/query/elemMatch/
         * the encapsulated fields on the should start from the field which the $elementMatch is being applied
         */
        var elementMatchQuery = {};
        paramQuery.value.forEach(function (query) {
            var elementQuery = buildQuery(query);
            var firstKey = _.head(_.keys(elementQuery));
            if (firstKey) {
                elementMatchQuery[firstKey] = elementQuery[firstKey];
            }
        });
        return elementMatchQuery;
    }
})(MongodbQueryHelper = exports.MongodbQueryHelper || (exports.MongodbQueryHelper = {}));
