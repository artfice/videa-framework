
import {DtoMongoDbAdapterConfig} from '../config/DtoMongoDbAdapterConfig';

export module MongodbAdapterConfigHelper {
    export function getDtoMongoDbAdapterConfig(dtoName?: string): DtoMongoDbAdapterConfig {
        return <DtoMongoDbAdapterConfig>{
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS !== 'test') ? process.env.EXTRA_OPTIONS : false,
            dtoName: dtoName
        };
    }
}