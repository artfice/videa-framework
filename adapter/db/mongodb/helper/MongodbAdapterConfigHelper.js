"use strict";
var MongodbAdapterConfigHelper;
(function (MongodbAdapterConfigHelper) {
    function getDtoMongoDbAdapterConfig(dtoName) {
        return {
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS !== 'test') ? process.env.EXTRA_OPTIONS : false,
            dtoName: dtoName
        };
    }
    MongodbAdapterConfigHelper.getDtoMongoDbAdapterConfig = getDtoMongoDbAdapterConfig;
})(MongodbAdapterConfigHelper = exports.MongodbAdapterConfigHelper || (exports.MongodbAdapterConfigHelper = {}));
