"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var uuid = require("node-uuid");
var Promise = require("bluebird");
var _ = require("lodash");
var VideaError_1 = require("../../../VideaError");
var MongoDbAdapter_1 = require("./MongoDbAdapter");
var db_1 = require("../../../domain/db");
var EventService_1 = require("../../../EventService");
var Logger_1 = require("../../../Logger");
var AggregatePipelineBuilder_1 = require("./helper/AggregatePipelineBuilder");
var MongodbDocumentUpdateBuilder_1 = require("./helper/MongodbDocumentUpdateBuilder");
/**
 * Base generic service to manage CRUD operations of DTOs on the mongoDB repository
 */
var DtoMongoDbAdapter = (function (_super) {
    __extends(DtoMongoDbAdapter, _super);
    function DtoMongoDbAdapter(config) {
        var _this = _super.call(this, config) || this;
        _this._dtoName = config.dtoName;
        if (!_this._collectionName) {
            _this._collectionName = config.dtoName + 's';
        }
        return _this;
    }
    DtoMongoDbAdapter.prototype.addDtoToCollection = function (dto, dbName, collectionName, eventData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                dto.id = dto.id || _this.generateIdForDto(dto);
                if (!dto.id) {
                    return reject(new VideaError_1.InternalServerError('Failed to generate a new ID while adding a new DTO of type ' + _this._dtoName +
                        ' to database ' + _this.getDbName(dbName)));
                }
                dto.createdDate = new Date();
                dto.modifiedDate = dto.createdDate;
                var item = {
                    _id: _this.getMongoIdFromDto(dto)
                };
                item[_this._documentVersion] = dto;
                return collection.insert(item, function (err, result) {
                    if (err) {
                        if (err.message.indexOf('E11000 duplicate key error') === 0) {
                            return reject(new VideaError_1.DuplicatedItemIdError('Duplicate id error while adding a new DTO of type ' +
                                _this._dtoName + ' with id ' + item._id + ' to database ' + _this.getDbName(dbName)));
                        }
                        return reject(new VideaError_1.InternalServerError('Internal error while adding a new DTO of type ' + _this._dtoName +
                            ' with id ' + item._id +
                            ' to database ' + _this.getDbName(dbName), err.message));
                    }
                    else {
                        var eventName_1 = 'videa-dto-add-' + _this._dtoName;
                        return EventService_1.EventService.emit(eventName_1, {
                            dto: dto,
                            data: eventData,
                            dbName: dbName
                        }, function (err) {
                            if (err) {
                                Logger_1.Logger.error('Caught error while processing ' + eventName_1 +
                                    ' when adding a new DTO of type ' +
                                    ' with id ' + item._id +
                                    ' to database ' + _this.getDbName(dbName) +
                                    ' caused by ' + err.message, err);
                            }
                            // This is run after all of the event listeners are done
                            return resolve(dto);
                        });
                    }
                });
            }).catch(function (err) {
                return reject(new VideaError_1.InternalServerError(err.message));
            });
        });
    };
    /**
     * @deprecated since version 0.5, please updateDtoOnCollection.
     */
    DtoMongoDbAdapter.prototype.updateManyDtosOnCollection = function (searchQuery, dto, dbName, collectionName, options) {
        options.upsert = false;
        options.multi = true;
        return this.updateDtoOnCollection(undefined, dto, dbName, collectionName, options, searchQuery);
    };
    DtoMongoDbAdapter.prototype.updateDtoOnCollection = function (compoundId, dto, dbName, collectionName, options, searchQuery) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var updateOptions = _this.normalizeUpdateOptions(options);
            _this.validateUpdateArguments(compoundId, dto, dbName, updateOptions, searchQuery);
            searchQuery = searchQuery ? _this.buildQuery(searchQuery) : undefined;
            var mongoIdQuery = compoundId ? _this.getMongoIdQuery(compoundId) : undefined;
            var mongoQuery = _this.mergeMongoQueriesIfExists(searchQuery, mongoIdQuery);
            // TODO: check only "updatable" fields. Use validators or something similar to get that information
            dto = _this.normalizeDatesInDto(dto);
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var updateParameters = MongodbDocumentUpdateBuilder_1.MongodbDocumentUpdateBuilder.buildUpdateParameter(dto, _this._documentVersion, options);
                return collection.update(mongoQuery, updateParameters, updateOptions, function (err, updateResult) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while updating multiple DTOs of type ' + _this._dtoName +
                            ' running query ' + JSON.stringify(mongoQuery) +
                            ' in database ' + _this.getDbName(dbName), err.message));
                    }
                    if (updateResult.result.nModified === 0 && (!updateOptions.upsert || updateResult.result.nUpserted === 0)) {
                        return reject(new VideaError_1.ItemNotFoundError('Not found object with id ' + compoundId + ' while updating DTO of type ' +
                            _this._dtoName + ' to database ' + _this.getDbName(dbName)));
                    }
                    else {
                        return resolve({
                            id: compoundId,
                            modifiedDate: dto.modifiedDate,
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.replaceDtoOnCollection = function (compoundId, dto, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (compoundId === undefined) {
                return reject(new VideaError_1.InvalidArgumentError('Id is required for replacing the DTO of type ' +
                    _this._dtoName + ' to database ' + _this.getDbName(dbName)));
            }
            // make sure we include the same id in the replace
            dto.id = compoundId;
            var nowDate = new Date();
            if (!dto.createdDate) {
                dto.createdDate = nowDate;
            }
            // update the modified date
            dto.modifiedDate = nowDate;
            _this.getCollection(dbName, collectionName).then(function (collection) {
                var item = {};
                item[_this._documentVersion] = dto;
                collection.replaceOne(_this.getMongoIdQuery(compoundId), item, function (err, result) {
                    if (err) {
                        reject(new VideaError_1.InternalServerError('An error was thrown while replacing DTO of type ' + _this._dtoName +
                            ' with id ' + _this.getMongoId(compoundId) +
                            ' to database ' + _this.getDbName(dbName), err.message));
                        return;
                    }
                    else {
                        resolve({
                            modifiedDate: dto.modifiedDate,
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            });
        });
    };
    DtoMongoDbAdapter.prototype.removeDtoFromCollection = function (compoundId, dbName, collectionName, eventData, mongoQuery) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var mongoIdQuery = _this.getMongoIdQuery(compoundId);
                var query = mongoQuery ? { $and: [mongoQuery, mongoIdQuery] } : mongoIdQuery;
                return collection.remove(query, { justOne: true }, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while removing DTO of type ' + _this._dtoName +
                            ' width id ' + _this.getMongoId(compoundId) +
                            ' from database ' + _this.getDbName(dbName), err.message));
                    }
                    // check if we found the dto to remove
                    if (result.result.n === 0) {
                        return reject(new VideaError_1.ItemNotFoundError('Not found object with id ' + _this.getMongoId(compoundId) +
                            ' while updating DTO of type ' + _this._dtoName + ' to database ' + _this.getDbName(dbName)));
                    }
                    var eventName = 'videa-dto-remove-' + _this._dtoName;
                    EventService_1.EventService.emit(eventName, {
                        id: compoundId,
                        data: eventData
                    }, function (err) {
                        if (err) {
                            Logger_1.Logger.error('Caught error while processing ' + eventName +
                                ' when removing a DTO of type ' + _this._dtoName +
                                ' width id ' + _this.getMongoId(compoundId) +
                                ' from database ' + _this.getDbName(dbName) + ' caused by ', err);
                        }
                        // This is run after all of the event listeners are done
                        resolve(undefined);
                    });
                });
            });
        });
    };
    DtoMongoDbAdapter.prototype.removeDtosFromCollection = function (compoundsIds, dbName, collectionName, eventData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var items = _this.buildQuery({
                    operator: db_1.SearchOperators.Ids,
                    value: compoundsIds
                });
                return collection.remove(items, { justOne: false }, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while removing DTO of type ' + _this._dtoName +
                            ' from database ' + _this.getDbName(dbName), err.message));
                    }
                    // check if we found the dto to remove
                    if (result.result.n === 0) {
                        return reject(new VideaError_1.ItemNotFoundError('Not found objects with ids ' + JSON.stringify(compoundsIds) +
                            ' while removing DTOs of type ' + _this._dtoName + ' from database ' + _this.getDbName(dbName)));
                    }
                    var eventName = 'videa-dto-remove-' + _this._dtoName;
                    EventService_1.EventService.emit(eventName, {
                        ids: compoundsIds,
                        data: eventData
                    }, function (err) {
                        if (err) {
                            Logger_1.Logger.error('Caught error while processing ' + eventName +
                                ' when removing a DTO of type ' + _this._dtoName +
                                ' from database ' + _this.getDbName(dbName) + ' caused by ', err);
                        }
                        // This is run after all of the event listeners are done
                        resolve(undefined);
                    });
                    resolve(undefined);
                });
            });
        });
    };
    DtoMongoDbAdapter.prototype.getDtoByIdFromCollection = function (compoundId, options, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.innerGetObjByIdFromCollection(compoundId, options, dbName, collectionName).then(function (result) {
                return resolve(result[_this._documentVersion]);
            }).catch(function (err) {
                return reject(err);
            });
        });
    };
    DtoMongoDbAdapter.prototype.getRelationFromCollection = function (compoundIdA, compoundIdB, relationName, options, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getRelationsFromCollections(compoundIdA, relationName, options, dbName, collectionName).then(function (relation) {
                if (relation) {
                    return resolve(relation[compoundIdB]);
                }
                else {
                    return resolve(undefined);
                }
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.getRelationsFromCollections = function (compoundId, relationName, options, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.innerGetObjByIdFromCollection(compoundId, options, dbName, collectionName).then(function (result) {
                var relations = result[_this._relationsVersion];
                if (!relations) {
                    return resolve(undefined);
                }
                var relation = relations[relationName];
                if (!relation) {
                    return reject(new VideaError_1.InternalServerError('Relation ' + relationName + ' not found in DTO of type ' + _this._dtoName +
                        ' with id ' + compoundId +
                        ' in database ' + _this.getDbName(dbName)));
                }
                return resolve(relation);
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.getDtoByInfoFromCollection = function (info, options, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var item = _this.getMongoInfoQuery(info);
                var findOptions = {};
                // set the default projection
                var projectedFields = options ? options.projection : undefined;
                projectedFields = projectedFields || _this._projectFields;
                if (projectedFields) {
                    findOptions.fields = _this.getProjection(projectedFields);
                }
                return collection.find(item, findOptions, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while getting by info DTO of type ' +
                            _this._dtoName + ' from database ' + _this.getDbName(dbName), err.message));
                    }
                    return result.toArray(function (err, results) {
                        if (results.length === 0) {
                            return reject(new VideaError_1.ItemNotFoundError('Not found object with id ' + _this.getMongoId(info) +
                                ' while getting by info DTO of type ' + _this._dtoName + ' from database ' + _this.getDbName(dbName)));
                        }
                        else {
                            return resolve(results[0][_this._documentVersion]);
                        }
                    });
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.batchGetDtoFromCollection = function (ids, dbName, collectionName, convertFunc) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var searchParams = {
                offset: 0,
                setSize: ids.length,
                query: {
                    field: 'id',
                    operator: db_1.SearchOperators.In,
                    value: ids
                }
            };
            return _this.searchDtoOnCollection(searchParams, dbName, collectionName, convertFunc).then(function (resultSet) {
                var results = resultSet.data;
                var orderedResult = [];
                var currentId = '';
                for (var i = 0; i < ids.length; i++) {
                    currentId = ids[i];
                    for (var j = 0; j < results.length; j++) {
                        if (results[j].id === currentId) {
                            orderedResult.push(results[j]);
                        }
                    }
                }
                resolve(orderedResult);
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.searchDtoOnCollection = function (options, dbName, collectionName, convertFunc) {
        return this.innerSearchOnCollection(options, undefined, dbName, collectionName, convertFunc);
    };
    DtoMongoDbAdapter.prototype.searchRelationOnCollection = function (dtoOptions, relationParams, dbName, collectionName, convertFunc) {
        return this.innerSearchOnCollection(dtoOptions, relationParams, dbName, collectionName, convertFunc);
    };
    DtoMongoDbAdapter.prototype.bulkRemoveDtoFromCollection = function (options, dbName, collectionName, eventData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var markForDelete = uuid.v1();
                var updateOptions = { multi: true };
                var removeOptions = { justOne: false };
                var query = {};
                if (options.query !== undefined) {
                    query = _this.buildQuery(options.query);
                }
                return collection.update(query, { $set: { markForDelete: markForDelete } }, updateOptions, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while bulk removing DTO of type ' + _this._dtoName +
                            ' from database ' + _this.getDbName(dbName), err.message));
                    }
                    // build the projected document based on the compound Id
                    var idFields = _this._compoundIdFields ? _this._compoundIdFields : ['id'];
                    var projectedCId = {};
                    var idFieldsKeys = Object.keys(idFields);
                    for (var i = 0; i < idFieldsKeys.length; i++) {
                        var key = idFieldsKeys[i];
                        projectedCId[_this._documentVersion + '.' + idFields[key]] = true;
                    }
                    return collection.find({ markForDelete: markForDelete }, projectedCId, function (err, result) {
                        if (err) {
                            return reject(new VideaError_1.InternalServerError('An error was thrown while bulk removing DTO of type ' + _this._dtoName +
                                ' from database ' + _this.getDbName(dbName), err.message));
                        }
                        return result.toArray(function (err, results) {
                            if (err) {
                                return reject(new VideaError_1.InternalServerError('An error was thrown while bulk removing DTO of type ' +
                                    _this._dtoName + ' from database ' + _this.getDbName(dbName), err.message));
                            }
                            var _loop_1 = function (r) {
                                var eventName = 'videa-dto-remove-' + _this._dtoName;
                                EventService_1.EventService.emit(eventName, {
                                    id: _this.getCompoundId(results[r][_this._documentVersion]),
                                    data: eventData
                                }, function (err) {
                                    if (err) {
                                        Logger_1.Logger.error('Caught error while processing ' + eventName +
                                            ' when bulk removing a DTO of type ' + _this._dtoName +
                                            ' from database ' + _this.getDbName(dbName) + ' caused by ', err.message);
                                    }
                                });
                            };
                            for (var r = 0; r < results.length; r++) {
                                _loop_1(r);
                            }
                            return collection.remove({ markForDelete: markForDelete }, removeOptions, function (err, result) {
                                if (err) {
                                    return reject(new VideaError_1.InternalServerError('An error was thrown while bulk removing DTO of type ' +
                                        _this._dtoName + ' to database ' + _this.getDbName(dbName), err.message));
                                }
                                return resolve(undefined);
                            });
                        });
                    });
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.bulkAddDtoFromCollection = function (dtos, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var newDate = new Date();
                var items = [];
                // generate new ids and creation dates
                _.each(dtos, function (dto) {
                    dto.id = dto.id ? dto.id : _this.generateIdForDto(dto);
                    dto.createdDate = newDate;
                    dto.modifiedDate = newDate;
                    var item = {
                        _id: dto.id ? dto.id : _this.getMongoIdFromDto(dto)
                    };
                    item[_this._documentVersion] = dto;
                    items.push(item);
                });
                return collection.insert(items, function (err, result) {
                    if (err) {
                        if (err.message.indexOf('E11000 duplicate key error index') === 0) {
                            return new reject(new VideaError_1.DuplicatedItemIdError('Duplicate id error while adding a new batch of DTOs of type ' +
                                _this._dtoName + ' to database ' + _this.getDbName(dbName)));
                        }
                        return reject(new VideaError_1.InternalServerError('An error was thrown while bulk adding DTOs of type ' +
                            _this._dtoName + ' to database ' + _this.getDbName(dbName), err.message));
                    }
                    else {
                        return resolve(dtos);
                    }
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.addElemToFieldOnCollection = function (compoundId, fieldName, elem, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (compoundId === undefined) {
                return reject(new VideaError_1.InvalidArgumentError('Id is required for adding an element to the field ' + fieldName +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!fieldName) {
                return reject(new VideaError_1.InvalidArgumentError('Field name is required for adding an element to the provided field name ' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!elem) {
                return reject(new VideaError_1.InvalidArgumentError('Element to push is required for adding an element to the field ' + fieldName +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            var modifiedDate = new Date();
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var updateSelectedFields = {};
                var setFields = {};
                setFields[_this._documentVersion + '.modifiedDate'] = modifiedDate;
                updateSelectedFields[_this._documentVersion + '.' + fieldName] = elem;
                var mongoQuery = {};
                mongoQuery['$set'] = setFields;
                mongoQuery['$push'] = updateSelectedFields;
                collection.update(_this.getMongoIdQuery(compoundId), mongoQuery, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while adding element to field ' + fieldName +
                            ' of a DTO of type ' + _this._dtoName +
                            ' to database ' + _this.getDbName(dbName), err.message));
                    }
                    else {
                        return resolve({
                            modifiedDate: modifiedDate,
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.addManyToOneRelationToDtoOnCollection = function (compoundId, relationParams, relationObj, updateExisting, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (compoundId === undefined) {
                return reject(new VideaError_1.InvalidArgumentError('Id required for adding a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!relationParams) {
                return reject(new VideaError_1.InvalidArgumentError('Relation params are required for adding a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!relationParams.relation) {
                return reject(new VideaError_1.InvalidArgumentError('Relation properties are required for adding a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!relationParams.foreignKeyValue) {
                return reject(new VideaError_1.InvalidArgumentError('Foreign key value is required for adding a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!relationObj) {
                return reject(new VideaError_1.InvalidArgumentError('Relation object is required for adding a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            var relation = relationParams.relation;
            var foreignKeyValue = relationParams.foreignKeyValue;
            var relationName = _this._relationsVersion + '.' + relation.name + '.' + foreignKeyValue;
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var updateRelation = {};
                updateRelation[relationName] = relationObj;
                var q = _this.getMongoIdQuery(compoundId);
                var u = { $set: updateRelation };
                // extra condition to check if the key doesn't exist yet
                q[relationName] = { $exists: updateExisting };
                collection.update(q, u, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while adding a many to one relation' +
                            ' of a DTO of type ' + _this._dtoName +
                            ' with id ' + _this.getMongoId(compoundId) +
                            ' to database ' + _this.getDbName(dbName), err.message));
                    }
                    else {
                        return resolve({
                            modifiedDate: new Date(),
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.removeManyToOneRelationToDtoOnCollection = function (compoundId, relationParams, updateExisting, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (compoundId === undefined) {
                return reject(new VideaError_1.InvalidArgumentError('Id required for removing a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!relationParams) {
                return reject(new VideaError_1.InvalidArgumentError('Relation params are required for removing a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!relationParams.relation) {
                return reject(new VideaError_1.InvalidArgumentError('Relation properties are required for removing a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            if (!relationParams.foreignKeyValue) {
                return reject(new VideaError_1.InvalidArgumentError('Foreign key value is required for removing a many to one relation' +
                    ' on a DTO of type ' + _this._dtoName +
                    ' with id ' + _this.getMongoId(compoundId) +
                    ' from database ' + _this.getDbName(dbName)));
            }
            var relation = relationParams.relation;
            var foreignKeyValue = relationParams.foreignKeyValue;
            var relationName = _this._relationsVersion + '.' + relation.name + '.' + foreignKeyValue;
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var updateRelation = {};
                updateRelation[relationName] = 1;
                var q = _this.getMongoIdQuery(compoundId);
                var u = { $unset: updateRelation };
                // extra condition to check if the key doesn't exist yet
                q[relationName] = { $exists: updateExisting };
                collection.update(q, u, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while removing a many to one relation' +
                            ' of a DTO of type ' + _this._dtoName +
                            ' with id ' + _this.getMongoId(compoundId) +
                            ' to database ' + _this.getDbName(dbName), err.message));
                    }
                    else {
                        resolve({
                            modifiedDate: new Date(),
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.aggregateDtoOnCollection = function (dtoOptions, dbName, collectionName, convertFunc) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                dtoOptions = _this.validateAggregationParams(dtoOptions);
                if (_this.isLookUpInValid(dtoOptions.lookUp)) {
                    return reject(new VideaError_1.InvalidArgumentError('An error was thrown while aggregation DTO of type ' + _this._dtoName +
                        ' with the collection ' + dtoOptions.lookUp.foreignField +
                        ' in database ' + _this.getDbName(dbName), 'the parameter lookup is not valid ' + JSON.stringify(dtoOptions.lookUp)));
                }
                if (convertFunc === undefined) {
                    convertFunc = function (d) {
                        return d;
                    };
                }
                var pipeline = AggregatePipelineBuilder_1.AggregatePipelineBuilder.buildPipeline(dtoOptions, _this._documentVersion, _this._projectFields);
                collection.aggregate(pipeline, function (err, results) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while aggregation DTO of type ' + _this._dtoName +
                            ' with the collection ' + dtoOptions.lookUp.foreignField +
                            ' in database ' + _this.getDbName(dbName), err.message));
                    }
                    // NOTE extract the MongoDB results
                    results = results && results[0] ? results[0] : { results: [], total: 0 };
                    // NOTE extract the items results
                    var dtos = _this.extractDocuments(results.results || [], dtoOptions, convertFunc);
                    var resultSet = {
                        data: dtos,
                        total: results.total || 0
                    };
                    resolve(resultSet);
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.countDtoInCollection = function (options, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                options = _this.validateSearchParams(options);
                var findOptions = _this.buildCountFindOptions(options);
                var query = _this.buildCountQuery(options);
                collection.count(query, findOptions, function (err, nodesCount) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('Error while counting documents ' +
                            ' on database ' + dbName, err.message));
                    }
                    return resolve(nodesCount);
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.validateSearchParams = function (options) {
        options = options || {};
        options.offset = options.offset || 0;
        options.setSize = options.setSize || 0;
        return options;
    };
    DtoMongoDbAdapter.prototype.validateAggregationParams = function (options) {
        options = options || {};
        options.offset = options.offset || 0;
        return options;
    };
    /**
     * Method responsible for generating the DTO id.
     * Subclasses may override it to generate a different kind of Id (e.g.: based on the dto name).
     */
    DtoMongoDbAdapter.prototype.generateIdForDto = function (dto) {
        return uuid.v1();
    };
    DtoMongoDbAdapter.prototype.getCompoundId = function (dto) {
        if (!this._compoundIdFields) {
            return dto.id;
        }
        var cid = {};
        for (var i = 0; i < this._compoundIdFields.length; i++) {
            cid[this._compoundIdFields[i]] = dto[this._compoundIdFields[i]];
        }
        return cid;
    };
    DtoMongoDbAdapter.prototype.getMongoId = function (compoundId) {
        if (!compoundId) {
            return compoundId;
        }
        if (!this._compoundIdFields || this._compoundIdFields.length === 0) {
            return compoundId;
        }
        var mid = compoundId[this._compoundIdFields[0]];
        for (var i = 1; i < this._compoundIdFields.length; i++) {
            var fid = compoundId[this._compoundIdFields[i]];
            fid.replace('_', '__');
            mid = mid + '_' + fid;
        }
        return mid;
    };
    DtoMongoDbAdapter.prototype.getMongoIdFromDto = function (dto) {
        return this.getMongoId(this.getCompoundId(dto));
    };
    DtoMongoDbAdapter.prototype.getMongoIdQuery = function (compoundId) {
        return { _id: this.getMongoId(compoundId) };
    };
    DtoMongoDbAdapter.prototype.getMongoInfoQuery = function (info) {
        var query = {};
        var fields = Object.keys(info);
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            query[this._documentVersion + '.' + field] = info[field];
        }
        return query;
    };
    DtoMongoDbAdapter.prototype.isLookUpInValid = function (lookUp) {
        return ((_.isEmpty(lookUp)) ||
            (_.isEmpty(lookUp.fromCollection)) ||
            (_.isEmpty(lookUp.foreignField)) ||
            (_.isEmpty(lookUp.localField)));
    };
    DtoMongoDbAdapter.prototype.innerGetObjByIdFromCollection = function (compoundId, options, dbName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                var item = _this.getMongoIdQuery(compoundId);
                var findOptions = {};
                // set the default projection
                var projectedFields = options ? options.projection : undefined;
                projectedFields = projectedFields || _this._projectFields;
                if (projectedFields) {
                    findOptions = _this.getProjection(projectedFields);
                }
                return collection.findOne(item, findOptions, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while getting DTO of type ' + _this._dtoName +
                            ' with id ' + compoundId +
                            ' from database ' + _this.getDbName(dbName), err.message));
                    }
                    if (!result) {
                        return reject(new VideaError_1.ItemNotFoundError('Not found object with id ' + _this.getMongoId(compoundId) +
                            ' while getting by id DTO of type ' + _this._dtoName + ' from database ' + _this.getDbName(dbName)));
                    }
                    return resolve(result);
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.innerSearchOnCollection = function (dtoOptions, relationParams, dbName, collectionName, convertFunc) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getCollection(dbName, collectionName).then(function (collection) {
                dtoOptions = _this.validateSearchParams(dtoOptions);
                var findOptions = {
                    skip: dtoOptions.offset,
                    limit: dtoOptions.setSize
                };
                if (dtoOptions.sort) {
                    var asc = 1;
                    if (dtoOptions.ascending !== undefined) {
                        asc = dtoOptions.ascending ? 1 : -1;
                    }
                    var sortObj = {};
                    sortObj[_this._documentVersion + '.' + dtoOptions.sort] = asc;
                    findOptions.sort = sortObj;
                }
                // set the default projection
                var projectedFields = dtoOptions.projection;
                projectedFields = projectedFields || _this._projectFields;
                if (projectedFields) {
                    findOptions.fields = _this.getProjection(projectedFields);
                }
                var buildQueryOptions = {
                    includeDeleted: false
                };
                if (relationParams) {
                    buildQueryOptions.relationParams = relationParams;
                }
                var query = _this.buildQuery(dtoOptions.query, buildQueryOptions);
                query = query || {};
                collection.find(query, findOptions, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('An error was thrown while searching DTO of type ' + _this._dtoName +
                            ' in database ' + _this.getDbName(dbName), err.message));
                    }
                    if (convertFunc === undefined) {
                        convertFunc = relationParams ? function (d, u) {
                            return d;
                        } : function (d) {
                            return d;
                        };
                    }
                    var dtos = [];
                    collection.find(query, { skip: 0 }).count(function (err, total) {
                        if (err) {
                            return reject(new VideaError_1.InternalServerError('An error was thrown while counting DTO of type ' + _this._dtoName +
                                ' in database ' + _this.getDbName(dbName), err.message));
                        }
                        if (relationParams) {
                            result.forEach(function (x) {
                                dtos.push(convertFunc(x[_this._documentVersion], x[_this._relationsVersion][relationParams.relation.name][relationParams.foreignKeyValue]));
                            }, function () {
                                var resultSet = {
                                    data: dtos,
                                    total: total
                                };
                                resolve(resultSet);
                                return;
                            });
                        }
                        else {
                            result.forEach(function (x) {
                                dtos.push(convertFunc(x[_this._documentVersion]));
                            }, function () {
                                var resultSet = {
                                    data: dtos,
                                    total: total
                                };
                                resolve(resultSet);
                            });
                        }
                    });
                });
            }).catch(reject);
        });
    };
    DtoMongoDbAdapter.prototype.buildCountFindOptions = function (options) {
        return {
            skip: options.offset,
            limit: options.setSize
        };
    };
    DtoMongoDbAdapter.prototype.buildCountQuery = function (options) {
        var buildQueryOptions = {
            includeDeleted: false
        };
        return this.buildQuery(options.query, buildQueryOptions) || {};
    };
    DtoMongoDbAdapter.prototype.extractDocuments = function (items, dtoOptions, convertFunc) {
        var dtos = [];
        for (var i = 0; i < items.length; i++) {
            var x = items[i];
            // remove document version from foreign document
            x[this._documentVersion][dtoOptions.lookUp.output] =
                x[this._documentVersion][dtoOptions.lookUp.output][0][this._documentVersion];
            // convert the object according to the convert function
            dtos.push(convertFunc(x[this._documentVersion]));
        }
        return dtos;
    };
    DtoMongoDbAdapter.prototype.normalizeUpdateOptions = function (options) {
        var updateOptions = {
            upsert: false
        };
        if (options) {
            if (options.upsert) {
                updateOptions.upsert = true;
            }
            if (options.multi) {
                updateOptions.multi = true;
            }
        }
        return updateOptions;
    };
    /**
     * Validates the update arguments and throws an error if not valid
     * @param compoundId
     * @param dto
     * @param dbName
     * @param updateOptions
     * @param searchQuery
     */
    DtoMongoDbAdapter.prototype.validateUpdateArguments = function (compoundId, dto, dbName, updateOptions, searchQuery) {
        if (updateOptions.upsert) {
            compoundId = compoundId || dto.id;
            compoundId = compoundId || this.generateIdForDto(dto);
            if (!compoundId && !searchQuery) {
                throw new VideaError_1.InternalServerError('Failed to generate a new ID while upserting DTO of type ' + this._dtoName +
                    ' into database ' + this.getDbName(dbName));
            }
            // make sure the dto.id is set
            dto.id = compoundId;
        }
        if (_.isNil(compoundId) && !searchQuery) {
            throw new VideaError_1.InvalidArgumentError('Id is required for updating the DTO of type ' + this._dtoName +
                ' into database ' + this.getDbName(dbName));
        }
        var id;
        if (compoundId) {
            id = this.getMongoId(compoundId);
        }
        // check if the user is trying to modify the id
        if ((dto.hasOwnProperty('id') || dto.id) && id !== this.getMongoIdFromDto(dto)) {
            throw new VideaError_1.InvalidArgumentError('The id cannot be modified while updating a DTO of type ' + this._dtoName +
                ' into database ' + this.getDbName(dbName));
        }
    };
    DtoMongoDbAdapter.prototype.normalizeDatesInDto = function (dto) {
        // check if the user is trying to modify the create date
        if (dto.createdDate) {
            dto = _.clone(dto);
            delete dto.createdDate;
        }
        // update the modified date
        dto.modifiedDate = new Date();
        return dto;
    };
    DtoMongoDbAdapter.prototype.mergeMongoQueriesIfExists = function (queryA, queryB) {
        var mergeQuery;
        if (queryA && queryB) {
            mergeQuery = { $and: [queryA, queryB] };
        }
        else if (queryA) {
            mergeQuery = queryA;
        }
        else {
            mergeQuery = queryB;
        }
        return mergeQuery;
    };
    return DtoMongoDbAdapter;
}(MongoDbAdapter_1.MongoDbAdapter));
exports.DtoMongoDbAdapter = DtoMongoDbAdapter;
