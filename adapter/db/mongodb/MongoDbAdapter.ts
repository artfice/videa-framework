import mongodb = require('mongodb');
import Promise = require('bluebird');
import MongoClient = mongodb.MongoClient;

import {SearchQuery} from '../../../domain/db/SearchQuery';
import {BuildQueryOptions} from '../BuildQueryOptions';
import {MongoDbAdapterConfig} from './config/MongoDbAdapterConfig';

import {InternalServerError} from '../../../VideaError';
import {MongodbQueryHelper as QueryHelper} from './helper/MongodbQueryHelper';

const connectOptions = {
    db: {},
    server: {
        poolSize: 5,
        // socketOptions: {
        //     connectTimeoutMS: 30000,
        //     keepAlive: 1
        // },
        auto_reconnect: true
    }
    // replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
    // mongos: {}
};

export class MongoDbAdapter {

    private static _connectionsPool: any;
    protected _collectionName: string;
    protected _documentVersion: string;
    protected _relationsVersion: string;
    protected _config: MongoDbAdapterConfig;

    constructor(config: MongoDbAdapterConfig) {
        this._config = config;
        this._documentVersion = 'document_v1';
        this._relationsVersion = 'relations_v1';
        this._collectionName = config.collectionName;

        if (!MongoDbAdapter._connectionsPool) {
            MongoDbAdapter._connectionsPool = {};
        }
    }

    public getDbName(name?: string) {
        return name || this._config.database;
    }

    public getDatabase(name?: string, ignorePool?: boolean) : Promise<any> {
        return new Promise<any> ((resolve, reject) => {
            const dbName = name || this._config.database;
            if (!ignorePool && MongoDbAdapter._connectionsPool[dbName]) {
                resolve(MongoDbAdapter._connectionsPool[dbName]);
                return;
            } else {
                let url = this._config.host + '/' + dbName;

                if (this._config.extraOptions || this._config.authSource) {
                    url = url + '?';

                    if (this._config.authSource) {
                        url = url + 'authSource=' + this._config.authSource;
                    }
                    if (this._config.extraOptions) {
                        if (this._config.authSource) {
                            url = url + '&';
                        }
                        url = url + this._config.extraOptions;
                    }
                }

                MongoClient.connect(url, connectOptions, (err, db) => {
                    if (err) {
                        return reject(new InternalServerError('Problem connecting to the mongodb repository ' + url, err.message));
                    }

                    if (ignorePool) {
                        return resolve(db);
                    }

                    // check if another "thread" got the connection first
                    if (MongoDbAdapter._connectionsPool[dbName]) {
                        db.close();
                        return resolve(MongoDbAdapter._connectionsPool[dbName]);
                    } else {
                        MongoDbAdapter._connectionsPool[dbName] = db;
                        return resolve(db);
                    }
                });
            }
        });
    }

    public closeDatabase(databaseName?: string): Promise<any> {
        return new Promise<any> ((resolve, reject) => {
            return this.getDatabase(databaseName).then((db) => {
                const dbName = databaseName || this._config.database;
                db.close();

                if (MongoDbAdapter._connectionsPool[dbName]) {
                    delete MongoDbAdapter._connectionsPool[dbName];
                }

                return  resolve(undefined);
            }).catch(reject);
        });
    }

    public getCollection(databaseName?: string, collectionName?: string) : Promise<any> {
        return new Promise<any> ((resolve, reject) => {
            return this.getDatabase(databaseName).then((db) => {
                db.collection(collectionName || this._collectionName, (err, collection) => {
                    if (err) {
                        return reject(new InternalServerError('Error getting collection ' + collectionName +
                            ' from database ' + databaseName, err.message));
                    }
                    return resolve(collection);
                });
            }).catch(reject);
        });
    }

    public dropDatabase(databaseName?: string): Promise<any> {
        return new Promise<any> ((resolve, reject) => {
            return this.getDatabase(databaseName).then((db) => {
                const dbName = databaseName || this._config.database;
                db.dropDatabase((err, result) => {
                    if (err) {
                        return reject(new InternalServerError('Error dropping database ' + databaseName, err.message));
                    }
                    db.close();

                    if (MongoDbAdapter._connectionsPool[dbName]) {
                        delete MongoDbAdapter._connectionsPool[dbName];
                    }

                    return  resolve(undefined);
                });
            }).catch(reject);
        });
    }

    public dropCollection(databaseName?: string, collectionName?: string): Promise<any> {
        return new Promise<any> ((resolve, reject) => {
            return this.getDatabase(databaseName).then((db) => {
                db.dropCollection(collectionName || this._collectionName, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError('Error dropping collection ' + collectionName +
                            ' from database ' + databaseName, err.message));
                    }
                    return resolve(undefined);
                });
            }).catch(reject);
        });
    }

    public getProjection(projectedFields: any) : any {
        if (!projectedFields) {
            return undefined;
        }

        const projection = {};
        if (projectedFields) {
            const keys = Object.keys(projectedFields);
            for (let i = 0; i < keys.length; i++) {
                const field = projectedFields[i];
                projection[this._documentVersion + '.' + field] = projectedFields[field];
            }
        }

        return projection;
    }

    public buildQuery(paramQuery: SearchQuery, options?: BuildQueryOptions, lookUpOutputField?: string): any {
        let query = QueryHelper.buildQuery(paramQuery, this._documentVersion, lookUpOutputField);

        if (options) {
            if (options.relationParams) {
                const relationQuery = { };
                relationQuery[this._relationsVersion + '.' + options.relationParams.relation.name + '.' +
                options.relationParams.foreignKeyValue] = { $exists: true };
                query = query ? { $and: [ query, relationQuery ] } : relationQuery;
            }

            if (!options.includeDeleted) {
                const deleteQuery = { markForDelete: { $exists: false } };
                query = query ? { $and: [ query, deleteQuery ] } : deleteQuery;
            }
        }

        return query;
    }
}
