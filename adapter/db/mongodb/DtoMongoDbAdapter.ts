import uuid = require('node-uuid');
import * as Promise from 'bluebird';
import * as _ from 'lodash';
import {DuplicatedItemIdError, InternalServerError, InvalidArgumentError, ItemNotFoundError} from '../../../VideaError';

import {BuildQueryOptions} from '../BuildQueryOptions';
import {ManyToOneRelationParams} from '../ManyToOneRelationParams';
import {MongoDbAdapter} from './MongoDbAdapter';

import {BaseDto} from '../../../domain';
import {
    AggregationLookup,
    AggregationParams,
    ResultSet,
    SearchOperators,
    SearchOptions,
    SearchParams,
    SearchQuery,
    UpdateOptions,
    UpdateResult} from '../../../domain/db';

import {EventService} from '../../../EventService';
import {Logger} from '../../../Logger';
import {DtoMongoDbAdapterConfig} from './config/DtoMongoDbAdapterConfig';
import { AggregatePipelineBuilder } from './helper/AggregatePipelineBuilder';
import { MongodbDocumentUpdateBuilder } from './helper/MongodbDocumentUpdateBuilder';

/**
 * Base generic service to manage CRUD operations of DTOs on the mongoDB repository
 */
export class DtoMongoDbAdapter<T extends BaseDto> extends MongoDbAdapter {

    protected _dtoName: string;
    protected _compoundIdFields: string[];
    /**
     * If set, the projection will be used to filter the fields retrieved in the queries.
     * It follows the same structure of MongoDB. E.g.: { field1: 1, field2: 0 }
     */
    protected _projectFields: any;

    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);

        this._dtoName = config.dtoName;
        if (!this._collectionName) {
            this._collectionName = config.dtoName + 's';
        }
    }

    public addDtoToCollection(dto: T, dbName?: string, collectionName?: string, eventData?: any): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {

                dto.id = dto.id || this.generateIdForDto(dto);
                if (!dto.id) {
                    return reject(
                        new InternalServerError('Failed to generate a new ID while adding a new DTO of type ' + this._dtoName +
                                                ' to database ' + this.getDbName(dbName)));
                }

                dto.createdDate = new Date();
                dto.modifiedDate = dto.createdDate;

                const item = {
                    _id: this.getMongoIdFromDto(dto)
                };
                item[this._documentVersion] = dto;
                return collection.insert(item, (err, result) => {
                    if (err) {
                        if (err.message.indexOf('E11000 duplicate key error') === 0) {
                            return reject(
                                new DuplicatedItemIdError('Duplicate id error while adding a new DTO of type ' +
                                                          this._dtoName + ' with id ' + item._id + ' to database ' + this.getDbName(dbName)));
                        }
                        return reject(new InternalServerError(
                            'Internal error while adding a new DTO of type ' + this._dtoName +
                            ' with id ' + item._id +
                            ' to database ' + this.getDbName(dbName),
                            err.message));
                    } else {
                        const eventName = 'videa-dto-add-' + this._dtoName;
                        return EventService.emit(
                            eventName,
                            {
                                dto: dto,
                                data: eventData,
                                dbName: dbName
                            },
                            (err) => {
                                if (err) {
                                    Logger.error(
                                        'Caught error while processing ' + eventName +
                                        ' when adding a new DTO of type ' +
                                        ' with id ' + item._id +
                                        ' to database ' + this.getDbName(dbName) +
                                        ' caused by ' + err.message,
                                        err);
                                }
                                // This is run after all of the event listeners are done
                                return resolve(<any>dto);
                            }
                        );
                    }
                });
            }).catch((err) => {
                return reject(new InternalServerError(err.message));
            });
        });
    }

    /**
     * @deprecated since version 0.5, please updateDtoOnCollection.
     */
    public updateManyDtosOnCollection(searchQuery: SearchQuery,
                                      dto: T,
                                      dbName?: string,
                                      collectionName?: string,
                                      options?: UpdateOptions): Promise<UpdateResult> {
        options.upsert = false;
        options.multi = true;
        return this.updateDtoOnCollection(undefined, dto, dbName, collectionName, options, searchQuery);
    }

    public updateDtoOnCollection(compoundId: any,
                                 dto: T,
                                 dbName?: string,
                                 collectionName?: string,
                                 options?: UpdateOptions,
                                 searchQuery?: SearchQuery): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            const updateOptions = this.normalizeUpdateOptions(options);
            this.validateUpdateArguments(compoundId, dto, dbName, updateOptions, searchQuery);
            searchQuery = searchQuery ? this.buildQuery(searchQuery) : undefined;
            const mongoIdQuery = compoundId ? this.getMongoIdQuery(compoundId) : undefined;
            const mongoQuery = this.mergeMongoQueriesIfExists(searchQuery, mongoIdQuery);

            // TODO: check only "updatable" fields. Use validators or something similar to get that information
            dto = this.normalizeDatesInDto(dto);
            return this.getCollection(dbName, collectionName).then((collection) => {
                const updateParameters = MongodbDocumentUpdateBuilder.buildUpdateParameter<T>(dto, this._documentVersion, options);
                return collection.update(mongoQuery, updateParameters, updateOptions, (err, updateResult) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while updating multiple DTOs of type ' + this._dtoName +
                            ' running query ' + JSON.stringify(mongoQuery) +
                            ' in database ' + this.getDbName(dbName),
                            err.message));
                    }

                    if (updateResult.result.nModified === 0 && (!updateOptions.upsert || updateResult.result.nUpserted === 0)) {
                        return reject(new ItemNotFoundError('Not found object with id ' + compoundId + ' while updating DTO of type ' +
                                                            this._dtoName + ' to database ' + this.getDbName(dbName)));
                    } else {
                        return resolve(<any><UpdateResult>{
                            id: compoundId,
                            modifiedDate: dto.modifiedDate,
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            }).catch(reject);
        });
    }

    public replaceDtoOnCollection(compoundId: any, dto: T, dbName?: string, collectionName?: string): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            if (compoundId === undefined) {
                return reject(new InvalidArgumentError('Id is required for replacing the DTO of type ' +
                                                       this._dtoName + ' to database ' + this.getDbName(dbName)));
            }

            // make sure we include the same id in the replace
            dto.id = compoundId;

            const nowDate = new Date();

            if (!dto.createdDate) {
                dto.createdDate = nowDate;
            }

            // update the modified date
            dto.modifiedDate = nowDate;

            this.getCollection(dbName, collectionName).then((collection) => {
                const item = {};
                item[this._documentVersion] = dto;

                collection.replaceOne(this.getMongoIdQuery(compoundId), item, (err, result) => {
                    if (err) {
                        reject(new InternalServerError(
                            'An error was thrown while replacing DTO of type ' + this._dtoName +
                            ' with id ' + this.getMongoId(compoundId) +
                            ' to database ' + this.getDbName(dbName),
                            err.message));
                        return;
                    } else {
                        resolve(<any><UpdateResult>{
                            modifiedDate: dto.modifiedDate,
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            });
        });
    }

    public removeDtoFromCollection(compoundId: any,
                                   dbName?: string,
                                   collectionName?: string,
                                   eventData?: any,
                                   mongoQuery?: any): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {

                const mongoIdQuery = this.getMongoIdQuery(compoundId);
                const query = mongoQuery ? { $and: [ mongoQuery, mongoIdQuery ] } : mongoIdQuery;

                return collection.remove(query, {justOne: true}, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while removing DTO of type ' + this._dtoName +
                            ' width id ' + this.getMongoId(compoundId) +
                            ' from database ' + this.getDbName(dbName),
                            err.message));
                    }

                    // check if we found the dto to remove
                    if (result.result.n === 0) {
                        return reject(new ItemNotFoundError('Not found object with id ' + this.getMongoId(compoundId) +
                                                            ' while updating DTO of type ' + this._dtoName + ' to database ' + this.getDbName(dbName)));
                    }

                    const eventName = 'videa-dto-remove-' + this._dtoName;
                    EventService.emit(
                        eventName,
                        {
                            id: compoundId,
                            data: eventData
                        },
                        (err) => {
                            if (err) {
                                Logger.error(
                                    'Caught error while processing ' + eventName +
                                    ' when removing a DTO of type ' + this._dtoName +
                                    ' width id ' + this.getMongoId(compoundId) +
                                    ' from database ' + this.getDbName(dbName) + ' caused by ',
                                    err);
                            }

                            // This is run after all of the event listeners are done
                            resolve(undefined);
                        });
                });
            });
        });
    }

    public removeDtosFromCollection(compoundsIds: string[], dbName?: string, collectionName?: string, eventData?: any): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {
                const items = this.buildQuery(<SearchQuery>{
                    operator: SearchOperators.Ids,
                    value: compoundsIds
                });

                return collection.remove(items, {justOne: false}, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while removing DTO of type ' + this._dtoName +
                            ' from database ' + this.getDbName(dbName),
                            err.message));
                    }

                    // check if we found the dto to remove
                    if (result.result.n === 0) {
                        return reject(new ItemNotFoundError('Not found objects with ids ' + JSON.stringify(compoundsIds) +
                                                            ' while removing DTOs of type ' + this._dtoName + ' from database ' + this.getDbName(dbName)));
                    }

                    const eventName = 'videa-dto-remove-' + this._dtoName;
                    EventService.emit(
                        eventName,
                        {
                            ids: compoundsIds,
                            data: eventData
                        },
                        (err) => {
                            if (err) {
                                Logger.error(
                                    'Caught error while processing ' + eventName +
                                    ' when removing a DTO of type ' + this._dtoName +
                                    ' from database ' + this.getDbName(dbName) + ' caused by ',
                                    err);
                            }
                            // This is run after all of the event listeners are done
                            resolve(undefined);
                        });
                    resolve(undefined);
                });
            });
        });
    }

    public getDtoByIdFromCollection(compoundId: any, options?: SearchOptions, dbName?: string, collectionName?: string): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            return this.innerGetObjByIdFromCollection(compoundId, options, dbName, collectionName).then((result) => {
                return resolve(result[this._documentVersion]);
            }).catch((err) => {
                return reject(err);
            });
        });
    }

    public getRelationFromCollection(
        compoundIdA: any,
        compoundIdB: any,
        relationName: string,
        options?: SearchOptions,
        dbName?: string,
        collectionName?: string): Promise<T> {

        return new Promise<T>((resolve, reject) => {
            return this.getRelationsFromCollections(compoundIdA, relationName, options, dbName, collectionName).then((relation) => {
                if (relation) {
                    return resolve(relation[compoundIdB]);
                } else {
                    return resolve(undefined);
                }
            }).catch(reject);
        });
    }

    public getRelationsFromCollections(
        compoundId: any,
        relationName: string,
        options?: SearchOptions,
        dbName?: string,
        collectionName?: string): Promise<T> {

        return new Promise<T>((resolve, reject) => {
            return this.innerGetObjByIdFromCollection(compoundId, options, dbName, collectionName).then((result) => {
                const relations = result[this._relationsVersion];
                if (!relations) {
                    return resolve(undefined);
                }
                const relation = relations[relationName];
                if (!relation) {
                    return reject(new InternalServerError(
                        'Relation ' + relationName + ' not found in DTO of type ' + this._dtoName +
                        ' with id ' + compoundId +
                        ' in database ' + this.getDbName(dbName)));
                }

                return resolve(relation);
            }).catch(reject);
        });
    }

    public getDtoByInfoFromCollection(info: any, options?: SearchOptions, dbName?: string, collectionName?: string): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {
                const item = this.getMongoInfoQuery(info);
                const findOptions = <any>{};

                // set the default projection
                let projectedFields = options ? options.projection : undefined;
                projectedFields = projectedFields || this._projectFields;
                if (projectedFields) {
                    findOptions.fields = this.getProjection(projectedFields);
                }

                return collection.find(item, findOptions, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while getting by info DTO of type ' +
                            this._dtoName + ' from database ' + this.getDbName(dbName),
                            err.message));
                    }

                    return result.toArray((err, results) => {
                        if (results.length === 0) {
                            return reject(new ItemNotFoundError('Not found object with id ' + this.getMongoId(info) +
                                                                ' while getting by info DTO of type ' + this._dtoName + ' from database ' + this.getDbName(dbName)));
                        } else {
                            return resolve(results[0][this._documentVersion]);
                        }
                    });
                });
            }).catch(reject);
        });
    }

    public batchGetDtoFromCollection(ids: any[], dbName?: string, collectionName?: string, convertFunc?: (dto: T) => T): Promise<T[]> {
        return new Promise<T[]>((resolve, reject) => {

            const searchParams = <SearchParams> {
                offset: 0,
                setSize: ids.length,
                query: <SearchQuery>{
                    field: 'id',
                    operator: SearchOperators.In,
                    value: ids
                }
            };

            return this.searchDtoOnCollection(searchParams, dbName, collectionName, convertFunc).then((resultSet) => {
                const results = <T[]>resultSet.data;
                const orderedResult = [];
                let currentId = '';

                for (let i = 0; i < ids.length; i++) {
                    currentId = ids[i];
                    for (let j = 0; j < results.length; j++) {
                        if (results[j].id === currentId) {
                            orderedResult.push(results[j]);
                        }
                    }
                }
                resolve(<any>orderedResult);
            }).catch(reject);
        });
    }

    public searchDtoOnCollection(
        options: SearchParams,
        dbName?: string,
        collectionName?: string,
        convertFunc?: (dto: T) => T): Promise<ResultSet> {
        return this.innerSearchOnCollection(options, undefined, dbName, collectionName, convertFunc);
    }

    public searchRelationOnCollection(
        dtoOptions: SearchParams,
        relationParams?: ManyToOneRelationParams,
        dbName?: string,
        collectionName?: string,
        convertFunc?: (dto: T, rel: any) => any): Promise<ResultSet> {
        return this.innerSearchOnCollection(dtoOptions, relationParams, dbName, collectionName, convertFunc);
    }

    public bulkRemoveDtoFromCollection(options: SearchParams, dbName?: string, collectionName?: string, eventData?: any): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {
                const markForDelete = uuid.v1();
                const updateOptions = {multi: true};
                const removeOptions = {justOne: false};

                let query = {};
                if (options.query !== undefined) {
                    query = this.buildQuery(options.query);
                }

                return collection.update(query, {$set: {markForDelete: markForDelete}}, updateOptions, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while bulk removing DTO of type ' + this._dtoName +
                            ' from database ' + this.getDbName(dbName),
                            err.message));
                    }

                    // build the projected document based on the compound Id
                    const idFields = this._compoundIdFields ? this._compoundIdFields : ['id'];
                    const projectedCId = {};

                    const idFieldsKeys = Object.keys(idFields);
                    for (let i = 0; i < idFieldsKeys.length; i++) {
                        const key = idFieldsKeys[i];
                        projectedCId[this._documentVersion + '.' + idFields[key]] = true;
                    }

                    return collection.find({markForDelete: markForDelete}, projectedCId, (err, result) => {
                        if (err) {
                            return reject(new InternalServerError(
                                'An error was thrown while bulk removing DTO of type ' + this._dtoName +
                                ' from database ' + this.getDbName(dbName),
                                err.message));
                        }
                        return result.toArray((err, results) => {
                            if (err) {
                                return reject(new InternalServerError(
                                    'An error was thrown while bulk removing DTO of type ' +
                                    this._dtoName + ' from database ' + this.getDbName(dbName),
                                    err.message));
                            }

                            for (let r = 0; r < results.length; r++) {
                                const eventName = 'videa-dto-remove-' + this._dtoName;
                                EventService.emit(
                                    eventName, {
                                        id: this.getCompoundId(results[r][this._documentVersion]),
                                        data: eventData
                                    },
                                    (err) => {
                                        if (err) {
                                            Logger.error(
                                                'Caught error while processing ' + eventName +
                                                ' when bulk removing a DTO of type ' + this._dtoName +
                                                ' from database ' + this.getDbName(dbName) + ' caused by ',
                                                err.message);
                                        }
                                    });
                            }
                            return collection.remove({markForDelete: markForDelete}, removeOptions, (err, result) => {
                                if (err) {
                                    return reject(new InternalServerError(
                                        'An error was thrown while bulk removing DTO of type ' +
                                        this._dtoName + ' to database ' + this.getDbName(dbName),
                                        err.message));
                                }
                                return resolve(undefined);
                            });
                        });
                    });
                });
            }).catch(reject);
        });
    }

    public bulkAddDtoFromCollection(dtos: T[], dbName?: string, collectionName?: string): Promise<Array<T>> {
        return new Promise<T[]>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {

                const newDate = new Date();
                const items = [];

                // generate new ids and creation dates
                _.each(dtos, (dto) => {
                    dto.id = dto.id ? dto.id : this.generateIdForDto(dto);
                    dto.createdDate = newDate
                    dto.modifiedDate = newDate;
                    const item = {
                        _id: dto.id ? dto.id : this.getMongoIdFromDto(dto)
                    };
                    item[this._documentVersion] = dto;

                    items.push(item);
                });

                return collection.insert(items, (err, result) => {
                    if (err) {
                        if (err.message.indexOf('E11000 duplicate key error index') === 0) {
                            return new reject(new DuplicatedItemIdError('Duplicate id error while adding a new batch of DTOs of type ' +
                                                                        this._dtoName + ' to database ' + this.getDbName(dbName)));
                        }
                        return reject(new InternalServerError(
                            'An error was thrown while bulk adding DTOs of type ' +
                            this._dtoName + ' to database ' + this.getDbName(dbName),
                            err.message));
                    } else {
                        return resolve(dtos);
                    }
                });
            }).catch(reject);
        });
    }

    public addElemToFieldOnCollection(
        compoundId: any,
        fieldName: string,
        elem: any,
        dbName?: string,
        collectionName?: string): Promise<UpdateResult> {

        return new Promise<UpdateResult>((resolve, reject) => {

            if (compoundId === undefined) {
                return reject(new InvalidArgumentError('Id is required for adding an element to the field ' + fieldName +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!fieldName) {
                return reject(new InvalidArgumentError('Field name is required for adding an element to the provided field name ' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!elem) {
                return reject(new InvalidArgumentError('Element to push is required for adding an element to the field ' + fieldName +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            const modifiedDate = new Date();

            return this.getCollection(dbName, collectionName).then((collection) => {
                const updateSelectedFields = {};
                const setFields = {};

                setFields[this._documentVersion + '.modifiedDate'] = modifiedDate;
                updateSelectedFields[this._documentVersion + '.' + fieldName] = elem;

                const mongoQuery = {};
                mongoQuery['$set'] = setFields;
                mongoQuery['$push'] = updateSelectedFields;

                collection.update(this.getMongoIdQuery(compoundId), mongoQuery, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while adding element to field ' + fieldName +
                            ' of a DTO of type ' + this._dtoName +
                            ' to database ' + this.getDbName(dbName),
                            err.message));
                    } else {
                        return resolve(<any><UpdateResult>{
                            modifiedDate: modifiedDate,
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            }).catch(reject);
        });
    }

    public addManyToOneRelationToDtoOnCollection(
        compoundId: any,
        relationParams: ManyToOneRelationParams,
        relationObj: any,
        updateExisting: boolean,
        dbName?: string,
        collectionName?: string): Promise<UpdateResult> {

        return new Promise<UpdateResult>((resolve, reject) => {

            if (compoundId === undefined) {
                return reject(new InvalidArgumentError('Id required for adding a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!relationParams) {
                return reject(new InvalidArgumentError('Relation params are required for adding a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!relationParams.relation) {
                return reject(new InvalidArgumentError('Relation properties are required for adding a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!relationParams.foreignKeyValue) {
                return reject(new InvalidArgumentError('Foreign key value is required for adding a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!relationObj) {
                return reject(new InvalidArgumentError('Relation object is required for adding a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            const relation = relationParams.relation;
            const foreignKeyValue = relationParams.foreignKeyValue;
            const relationName = this._relationsVersion + '.' + relation.name + '.' + foreignKeyValue;

            return this.getCollection(dbName, collectionName).then((collection) => {
                const updateRelation = {};
                updateRelation[relationName] = relationObj;
                const q = this.getMongoIdQuery(compoundId);
                const u = {$set: updateRelation};

                // extra condition to check if the key doesn't exist yet
                q[relationName] = {$exists: updateExisting};

                collection.update(q, u, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while adding a many to one relation' +
                            ' of a DTO of type ' + this._dtoName +
                            ' with id ' + this.getMongoId(compoundId) +
                            ' to database ' + this.getDbName(dbName),
                            err.message));
                    } else {
                        return resolve(<any><UpdateResult>{
                            modifiedDate: new Date(),
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            }).catch(reject);
        });
    }

    public removeManyToOneRelationToDtoOnCollection(
        compoundId: any,
        relationParams: ManyToOneRelationParams,
        updateExisting: boolean,
        dbName?: string,
        collectionName?: string): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {

            if (compoundId === undefined) {
                return reject(new InvalidArgumentError('Id required for removing a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!relationParams) {
                return reject(new InvalidArgumentError('Relation params are required for removing a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!relationParams.relation) {
                return reject(new InvalidArgumentError('Relation properties are required for removing a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            if (!relationParams.foreignKeyValue) {
                return reject(new InvalidArgumentError('Foreign key value is required for removing a many to one relation' +
                                                       ' on a DTO of type ' + this._dtoName +
                                                       ' with id ' + this.getMongoId(compoundId) +
                                                       ' from database ' + this.getDbName(dbName)));
            }

            const relation = relationParams.relation;
            const foreignKeyValue = relationParams.foreignKeyValue;
            const relationName = this._relationsVersion + '.' + relation.name + '.' + foreignKeyValue;

            return this.getCollection(dbName, collectionName).then((collection) => {
                const updateRelation = {};
                updateRelation[relationName] = 1;
                const q = this.getMongoIdQuery(compoundId);
                const u = {$unset: updateRelation};

                // extra condition to check if the key doesn't exist yet
                q[relationName] = {$exists: updateExisting};

                collection.update(q, u, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while removing a many to one relation' +
                            ' of a DTO of type ' + this._dtoName +
                            ' with id ' + this.getMongoId(compoundId) +
                            ' to database ' + this.getDbName(dbName),
                            err.message));
                    } else {
                        resolve(<any><UpdateResult>{
                            modifiedDate: new Date(),
                            success: true,
                            modifiedItems: 1
                        });
                    }
                });
            }).catch(reject);
        });
    }

    public aggregateDtoOnCollection(
        dtoOptions: AggregationParams,
        dbName?: string,
        collectionName?: string,
        convertFunc?: (dto: T) => T): Promise<ResultSet> {

        return new Promise<ResultSet>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {
                dtoOptions = this.validateAggregationParams(dtoOptions);

                if (this.isLookUpInValid(dtoOptions.lookUp)) {
                    return reject(new InvalidArgumentError(
                        'An error was thrown while aggregation DTO of type ' + this._dtoName +
                        ' with the collection ' + dtoOptions.lookUp.foreignField +
                        ' in database ' + this.getDbName(dbName),
                        'the parameter lookup is not valid ' + JSON.stringify(dtoOptions.lookUp)));
                }

                if (convertFunc === undefined) {
                    convertFunc = (d) => {
                        return d;
                    };
                }

                const pipeline = AggregatePipelineBuilder.buildPipeline(dtoOptions, this._documentVersion, this._projectFields);
                collection.aggregate(pipeline,  (err, results) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while aggregation DTO of type ' + this._dtoName +
                            ' with the collection ' + dtoOptions.lookUp.foreignField +
                            ' in database ' + this.getDbName(dbName),
                            err.message));
                    }

                    // NOTE extract the MongoDB results
                    results = results && results[0] ? results[0] : { results: [], total: 0};

                    // NOTE extract the items results
                    const dtos = this.extractDocuments(results.results || [], dtoOptions, convertFunc);

                    const resultSet = <ResultSet> {
                        data: dtos,
                        total : results.total || 0
                    };
                    resolve(resultSet);
                });
            }).catch(reject);
        });
    }

    public countDtoInCollection(options?: SearchParams,
                                dbName?: string,
                                collectionName?: string): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {
                options = this.validateSearchParams(options);
                const findOptions = this.buildCountFindOptions(options);
                const query = this.buildCountQuery(options);
                collection.count(query, findOptions, (err, nodesCount) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'Error while counting documents ' +
                            ' on database ' + dbName,
                            err.message));
                    }

                    return resolve(nodesCount);
                });
            }).catch(reject);
        });
    }

    protected validateSearchParams(options: SearchParams) : SearchParams {
        options = options || <SearchParams>{};
        options.offset = options.offset || 0;
        options.setSize = options.setSize || 0;

        return options;
    }

    protected validateAggregationParams(options: AggregationParams) : AggregationParams {
        options = options || <AggregationParams>{};
        options.offset = options.offset || 0;

        return options;
    }

    /**
     * Method responsible for generating the DTO id.
     * Subclasses may override it to generate a different kind of Id (e.g.: based on the dto name).
     */
    protected generateIdForDto(dto: T): string {
        return uuid.v1();
    }

    protected getCompoundId(dto: T): any {
        if (!this._compoundIdFields) {
            return dto.id;
        }

        const cid = {};
        for (let i = 0; i < this._compoundIdFields.length; i++) {
            cid[this._compoundIdFields[i]] = dto[this._compoundIdFields[i]];
        }
        return cid;
    }

    protected getMongoId(compoundId: any): string {
        if (!compoundId) {
            return compoundId;
        }

        if (!this._compoundIdFields || this._compoundIdFields.length === 0) {
            return compoundId;
        }

        let mid = compoundId[this._compoundIdFields[0]];
        for (let i = 1; i < this._compoundIdFields.length; i++) {
            const fid = compoundId[this._compoundIdFields[i]];
            fid.replace('_', '__');
            mid = mid + '_' + fid;
        }
        return mid;
    }

    protected getMongoIdFromDto(dto: T): string {
        return this.getMongoId(this.getCompoundId(dto));
    }

    protected getMongoIdQuery(compoundId: any): any {
        return {_id: this.getMongoId(compoundId)};
    }

    protected getMongoInfoQuery(info: any): any {
        const query = {};

        const fields = Object.keys(info);
        for (let i = 0; i < fields.length; i++) {
            const field = fields[i];
            query[this._documentVersion + '.' + field] = info[field];
        }

        return query;
    }

    private isLookUpInValid(lookUp: AggregationLookup) : boolean {
        return (
            (_.isEmpty(lookUp)) ||
            (_.isEmpty(lookUp.fromCollection)) ||
            ( _.isEmpty(lookUp.foreignField)) ||
            ( _.isEmpty(lookUp.localField))
        );
    }

    private innerGetObjByIdFromCollection(compoundId: any,
                                          options?: SearchOptions,
                                          dbName?: string,
                                          collectionName?: string): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {
                const item = this.getMongoIdQuery(compoundId);
                let findOptions = <any>{};

                // set the default projection
                let projectedFields = options ? options.projection : undefined;
                projectedFields = projectedFields || this._projectFields;
                if (projectedFields) {
                    findOptions = this.getProjection(projectedFields);
                }

                return collection.findOne(item, findOptions, (err, result) => {
                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while getting DTO of type ' + this._dtoName +
                            ' with id ' + compoundId +
                            ' from database ' + this.getDbName(dbName),
                            err.message));
                    }

                    if (!result) {
                        return reject(new ItemNotFoundError('Not found object with id ' + this.getMongoId(compoundId) +
                                                            ' while getting by id DTO of type ' + this._dtoName + ' from database ' + this.getDbName(dbName)));
                    }

                    return resolve(result);
                });
            }).catch(reject);
        });
    }

    private innerSearchOnCollection(
        dtoOptions: SearchParams,
        relationParams?: ManyToOneRelationParams,
        dbName?: string,
        collectionName?: string,
        convertFunc?: (dto: T, rel?: any) => any): Promise<ResultSet> {

        return new Promise<ResultSet>((resolve, reject) => {
            return this.getCollection(dbName, collectionName).then((collection) => {
                dtoOptions = this.validateSearchParams(dtoOptions);
                const findOptions: any = {
                    skip: dtoOptions.offset,
                    limit: dtoOptions.setSize
                };

                if (dtoOptions.sort) {
                    let asc = 1;
                    if (dtoOptions.ascending !== undefined) {
                        asc = dtoOptions.ascending ? 1 : -1;
                    }

                    const sortObj = {};
                    sortObj[this._documentVersion + '.' + dtoOptions.sort] = asc;
                    findOptions.sort = sortObj;
                }

                // set the default projection
                let projectedFields = dtoOptions.projection;
                projectedFields = projectedFields || this._projectFields;
                if (projectedFields) {
                    findOptions.fields = this.getProjection(projectedFields);
                }

                const buildQueryOptions = <BuildQueryOptions>{
                    includeDeleted: false
                };
                if (relationParams) {
                    buildQueryOptions.relationParams = relationParams;
                }

                let query = this.buildQuery(dtoOptions.query, buildQueryOptions);
                query = query || {};

                collection.find(query, findOptions, (err, result) => {

                    if (err) {
                        return reject(new InternalServerError(
                            'An error was thrown while searching DTO of type ' + this._dtoName +
                            ' in database ' + this.getDbName(dbName),
                            err.message));
                    }

                    if (convertFunc === undefined) {
                        convertFunc = relationParams ? (d, u) => {
                            return d;
                        } : (d) => {
                            return d;
                        };
                    }

                    const dtos = <any>[];
                    collection.find(query, {skip : 0}).count((err, total) => {
                        if (err) {
                            return reject(new InternalServerError(
                                'An error was thrown while counting DTO of type ' + this._dtoName +
                                ' in database ' + this.getDbName(dbName),
                                err.message));
                        }

                        if (relationParams) {
                            result.forEach(
                                (x) => {
                                    dtos.push(convertFunc(
                                        x[this._documentVersion],
                                        x[this._relationsVersion][relationParams.relation.name][relationParams.foreignKeyValue]));
                                },
                                () => {
                                    const resultSet = <ResultSet> {
                                        data: dtos,
                                        total : total
                                    };
                                    resolve(resultSet);
                                    return;
                                });
                        } else {
                            result.forEach(
                                (x) => {
                                    dtos.push(convertFunc(x[this._documentVersion]));
                                },
                                () => {
                                    const resultSet = <ResultSet> {
                                        data: dtos,
                                        total : total
                                        // nextOffset: dtoOptions.offset += dtos.length
                                    };
                                    resolve(resultSet);
                                });
                        }
                    });
                });
            }).catch(reject);
        });
    }

    private buildCountFindOptions(options: SearchParams): any {
        return {
            skip: options.offset,
            limit: options.setSize
        };
    }

    private buildCountQuery(options: SearchParams) {
        const buildQueryOptions = <BuildQueryOptions>{
            includeDeleted: false
        };

        return this.buildQuery(options.query, buildQueryOptions) || {};
    }

    private extractDocuments(items: any[], dtoOptions: AggregationParams, convertFunc?: (dto: T) => T) : any[] {
        const dtos = [];

        for (let i = 0 ; i < items.length; i++) {
            const x = items[i];

            // remove document version from foreign document
            x[this._documentVersion][dtoOptions.lookUp.output] =
                x[this._documentVersion][dtoOptions.lookUp.output][0][this._documentVersion];

            // convert the object according to the convert function
            dtos.push(convertFunc(x[this._documentVersion]));
        }
        return dtos;
    }

    private normalizeUpdateOptions(options: UpdateOptions) : any {
        const updateOptions = <any>{
            upsert: false
        };

        if (options) {
            if (options.upsert) {
                updateOptions.upsert = true;
            }
            if (options.multi) {
                updateOptions.multi = true;
            }
        }

        return updateOptions;
    }

    /**
     * Validates the update arguments and throws an error if not valid
     * @param compoundId
     * @param dto
     * @param dbName
     * @param updateOptions
     * @param searchQuery
     */
    private validateUpdateArguments(compoundId: any,
                                    dto: T,
                                    dbName?: string,
                                    updateOptions?: UpdateOptions,
                                    searchQuery?: SearchQuery) : void {
        if (updateOptions.upsert) {
            compoundId = compoundId || dto.id;
            compoundId = compoundId || this.generateIdForDto(dto);
            if (!compoundId && !searchQuery) {
                throw new InternalServerError('Failed to generate a new ID while upserting DTO of type ' + this._dtoName +
                                              ' into database ' + this.getDbName(dbName));
            }
            // make sure the dto.id is set
            dto.id = compoundId;
        }

        if (_.isNil(compoundId) && !searchQuery) {
            throw new InvalidArgumentError('Id is required for updating the DTO of type ' + this._dtoName +
                                           ' into database ' + this.getDbName(dbName));
        }

        let id;
        if (compoundId) {
            id = this.getMongoId(compoundId);
        }

        // check if the user is trying to modify the id
        if ((dto.hasOwnProperty('id') || dto.id) && id !== this.getMongoIdFromDto(dto)) {
            throw new InvalidArgumentError('The id cannot be modified while updating a DTO of type ' + this._dtoName +
                                           ' into database ' + this.getDbName(dbName));
        }
    }

    private normalizeDatesInDto(dto: T) : T {
        // check if the user is trying to modify the create date
        if (dto.createdDate) {
            dto = _.clone(dto);
            delete dto.createdDate;
        }

        // update the modified date
        dto.modifiedDate = new Date();

        return dto;
    }

    private mergeMongoQueriesIfExists(queryA: any, queryB: any) : any {
        let mergeQuery;
        if (queryA && queryB) {
            mergeQuery = { $and: [queryA, queryB]};
        } else if (queryA) {
            mergeQuery = queryA;
        } else {
            mergeQuery = queryB;
        }
        return mergeQuery;
    }
}
