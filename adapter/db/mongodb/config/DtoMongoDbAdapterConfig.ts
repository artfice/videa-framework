import {MongoDbAdapterConfig} from './MongoDbAdapterConfig';

export interface DtoMongoDbAdapterConfig extends MongoDbAdapterConfig {
    /**
     * DTO name
     */
    dtoName: string;
}