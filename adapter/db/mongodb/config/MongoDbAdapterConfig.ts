export interface MongoDbAdapterConfig {
    host: string;
    port: number;
    database: string;
    /**
     * Name of the database to authenticate against.
     * If null it will connect to the current database.
     * DEPRECATED: You can use extraOptions for that.
     */
    authSource: string;
    /**
     * Extra options that will be added to the connection string.
     * E.g.: replicaSet=rs-ds133035
     */
    extraOptions: string;
    /**
     * Default colleciton name
     */
    collectionName: string;
}