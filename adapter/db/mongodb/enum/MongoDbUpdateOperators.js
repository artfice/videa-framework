"use strict";
exports.MongoDbUpdateOperators = {
    SET: '$set',
    PUSH: '$push',
    PULL: '$pull',
    UNSET: '$unset',
    ADD_TO_SET: '$addToSet',
    SET_ON_INSERT: '$setOnInsert',
    PULL_ALL: '$pullAll'
};
