export const MongoDbUpdateOperators = {
    SET          : '$set' as 'SET',
    PUSH         : '$push' as 'PUSH',
    PULL         : '$pull' as 'PULL',
    UNSET        : '$unset' as 'UNSET',
    ADD_TO_SET   : '$addToSet' as 'ADD_TO_SET',
    SET_ON_INSERT: '$setOnInsert' as 'SET_ON_INSERT',
    PULL_ALL     : '$pullAll' as 'PULL_ALL'
};

export type MongoDbUpdateOperators = keyof typeof MongoDbUpdateOperators;
