"use strict";
var mongodb = require("mongodb");
var Promise = require("bluebird");
var MongoClient = mongodb.MongoClient;
var VideaError_1 = require("../../../VideaError");
var MongodbQueryHelper_1 = require("./helper/MongodbQueryHelper");
var connectOptions = {
    db: {},
    server: {
        poolSize: 5,
        // socketOptions: {
        //     connectTimeoutMS: 30000,
        //     keepAlive: 1
        // },
        auto_reconnect: true
    }
};
var MongoDbAdapter = (function () {
    function MongoDbAdapter(config) {
        this._config = config;
        this._documentVersion = 'document_v1';
        this._relationsVersion = 'relations_v1';
        this._collectionName = config.collectionName;
        if (!MongoDbAdapter._connectionsPool) {
            MongoDbAdapter._connectionsPool = {};
        }
    }
    MongoDbAdapter.prototype.getDbName = function (name) {
        return name || this._config.database;
    };
    MongoDbAdapter.prototype.getDatabase = function (name, ignorePool) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var dbName = name || _this._config.database;
            if (!ignorePool && MongoDbAdapter._connectionsPool[dbName]) {
                resolve(MongoDbAdapter._connectionsPool[dbName]);
                return;
            }
            else {
                var url_1 = _this._config.host + '/' + dbName;
                if (_this._config.extraOptions || _this._config.authSource) {
                    url_1 = url_1 + '?';
                    if (_this._config.authSource) {
                        url_1 = url_1 + 'authSource=' + _this._config.authSource;
                    }
                    if (_this._config.extraOptions) {
                        if (_this._config.authSource) {
                            url_1 = url_1 + '&';
                        }
                        url_1 = url_1 + _this._config.extraOptions;
                    }
                }
                MongoClient.connect(url_1, connectOptions, function (err, db) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('Problem connecting to the mongodb repository ' + url_1, err.message));
                    }
                    if (ignorePool) {
                        return resolve(db);
                    }
                    // check if another "thread" got the connection first
                    if (MongoDbAdapter._connectionsPool[dbName]) {
                        db.close();
                        return resolve(MongoDbAdapter._connectionsPool[dbName]);
                    }
                    else {
                        MongoDbAdapter._connectionsPool[dbName] = db;
                        return resolve(db);
                    }
                });
            }
        });
    };
    MongoDbAdapter.prototype.closeDatabase = function (databaseName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getDatabase(databaseName).then(function (db) {
                var dbName = databaseName || _this._config.database;
                db.close();
                if (MongoDbAdapter._connectionsPool[dbName]) {
                    delete MongoDbAdapter._connectionsPool[dbName];
                }
                return resolve(undefined);
            }).catch(reject);
        });
    };
    MongoDbAdapter.prototype.getCollection = function (databaseName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getDatabase(databaseName).then(function (db) {
                db.collection(collectionName || _this._collectionName, function (err, collection) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('Error getting collection ' + collectionName +
                            ' from database ' + databaseName, err.message));
                    }
                    return resolve(collection);
                });
            }).catch(reject);
        });
    };
    MongoDbAdapter.prototype.dropDatabase = function (databaseName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getDatabase(databaseName).then(function (db) {
                var dbName = databaseName || _this._config.database;
                db.dropDatabase(function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('Error dropping database ' + databaseName, err.message));
                    }
                    db.close();
                    if (MongoDbAdapter._connectionsPool[dbName]) {
                        delete MongoDbAdapter._connectionsPool[dbName];
                    }
                    return resolve(undefined);
                });
            }).catch(reject);
        });
    };
    MongoDbAdapter.prototype.dropCollection = function (databaseName, collectionName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getDatabase(databaseName).then(function (db) {
                db.dropCollection(collectionName || _this._collectionName, function (err, result) {
                    if (err) {
                        return reject(new VideaError_1.InternalServerError('Error dropping collection ' + collectionName +
                            ' from database ' + databaseName, err.message));
                    }
                    return resolve(undefined);
                });
            }).catch(reject);
        });
    };
    MongoDbAdapter.prototype.getProjection = function (projectedFields) {
        if (!projectedFields) {
            return undefined;
        }
        var projection = {};
        if (projectedFields) {
            var keys = Object.keys(projectedFields);
            for (var i = 0; i < keys.length; i++) {
                var field = projectedFields[i];
                projection[this._documentVersion + '.' + field] = projectedFields[field];
            }
        }
        return projection;
    };
    MongoDbAdapter.prototype.buildQuery = function (paramQuery, options, lookUpOutputField) {
        var query = MongodbQueryHelper_1.MongodbQueryHelper.buildQuery(paramQuery, this._documentVersion, lookUpOutputField);
        if (options) {
            if (options.relationParams) {
                var relationQuery = {};
                relationQuery[this._relationsVersion + '.' + options.relationParams.relation.name + '.' +
                    options.relationParams.foreignKeyValue] = { $exists: true };
                query = query ? { $and: [query, relationQuery] } : relationQuery;
            }
            if (!options.includeDeleted) {
                var deleteQuery = { markForDelete: { $exists: false } };
                query = query ? { $and: [query, deleteQuery] } : deleteQuery;
            }
        }
        return query;
    };
    return MongoDbAdapter;
}());
exports.MongoDbAdapter = MongoDbAdapter;
