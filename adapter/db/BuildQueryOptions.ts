/**
 * Created by bardiakhosravi on 2016-10-14.
 */
import {ManyToOneRelationParams} from './ManyToOneRelationParams';

export interface BuildQueryOptions {
    includeDeleted: boolean;
    relationParams: ManyToOneRelationParams;
}