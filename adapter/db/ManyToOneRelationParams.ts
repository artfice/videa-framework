/**
 * Created by bardiakhosravi on 2016-10-14.
 */
import {RelationDesc} from './RelationDesc';

export interface ManyToOneRelationParams {
    relation: RelationDesc;
    foreignKeyValue: string;
}