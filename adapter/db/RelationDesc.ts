/**
 * Created by bardiakhosravi on 2016-10-14.
 */

export interface RelationDesc {
    name: string;
    foreignKey: string;
}