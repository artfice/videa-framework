import * as AWS from 'aws-sdk';
import * as S3Uploader from 's3-upload-stream';

import {AwsS3AdapterConfig} from './config/AwsS3AdapterConfig';

export class AwsS3Adapter {
    private _s3: AWS.S3;
    private _s3Stream: any;
    private _bucket: string;

    constructor(config: AwsS3AdapterConfig) {
        this._s3 = new AWS.S3({
            region: config.region,
            credentials: config.credentials,
            apiVersion: '2006-03-01'
        });
        this._s3Stream = S3Uploader(this._s3);
        this._bucket = config.bucket;
    }

    public getClient(): AWS.S3 {
        return this._s3;
    }

    public getUploader(): any {
        return this._s3Stream;
    }

    public getBucket(): string {
        return this._bucket;
    }
}
