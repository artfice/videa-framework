import * as AWS from 'aws-sdk';

import {AwsSqsAdapterConfig} from './config/AwsSqsAdapterConfig';

export class AwsSqsAdapter {
    private _sqs: any;
    private _queueUrl;

    constructor(config: AwsSqsAdapterConfig) {
        this._sqs = new AWS.SQS({
            region: config.region,
            credentials: config.credentials,
            apiVersion: '2012-11-05'
        });
        this._queueUrl = config.queueUrl;
    }

    public getClient(): any {
        return this._sqs;
    }

    public getQueueUrl(): string {
        return this._queueUrl;
    }
}
