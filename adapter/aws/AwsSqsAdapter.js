"use strict";
var AWS = require("aws-sdk");
var AwsSqsAdapter = (function () {
    function AwsSqsAdapter(config) {
        this._sqs = new AWS.SQS({
            region: config.region,
            credentials: config.credentials,
            apiVersion: '2012-11-05'
        });
        this._queueUrl = config.queueUrl;
    }
    AwsSqsAdapter.prototype.getClient = function () {
        return this._sqs;
    };
    AwsSqsAdapter.prototype.getQueueUrl = function () {
        return this._queueUrl;
    };
    return AwsSqsAdapter;
}());
exports.AwsSqsAdapter = AwsSqsAdapter;
