import {AwsOptions} from './AwsOptions';

export interface AwsSqsAdapterConfig extends AwsOptions {
    queueUrl: string;
}
