import * as AWS from 'aws-sdk';

export interface AwsOptions {
    credentials: AWS.Credentials;
    region: string;
}
