import {AwsOptions} from './AwsOptions';

export interface AwsS3AdapterConfig extends AwsOptions {
    bucket: string;
}
