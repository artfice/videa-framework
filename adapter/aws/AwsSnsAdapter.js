"use strict";
var AWS = require("aws-sdk");
var Logger_1 = require("../../Logger");
var AwsSnsAdapter = (function () {
    function AwsSnsAdapter(region, awsAccountId) {
        this._region = region;
        this._awsAccountId = awsAccountId;
        this._sns = new AWS.SNS({ apiVersion: '2010-03-31' });
    }
    AwsSnsAdapter.prototype.publishEvent = function (eventFunction, payload, cb) {
        var params = {
            Message: JSON.stringify(payload),
            TopicArn: 'arn:aws:sns:' + this._region + ':' + this._awsAccountId + ':' + eventFunction
        };
        this._sns.publish(params, function (error, data) {
            if (error) {
                Logger_1.Logger.critical('SNS EVENT ERROR ' + eventFunction, error);
                cb(error);
            }
            cb(null, data);
        });
    };
    AwsSnsAdapter.prototype.isValidEvent = function (eventSource) {
        return eventSource === 'aws:sns';
    };
    AwsSnsAdapter.prototype.retrievePayload = function (payload) {
        var snsPayload = payload.Sns ? payload.Sns : undefined;
        var result = {};
        if (!snsPayload) {
            return result;
        }
        var messagePayload = snsPayload.Message ? JSON.parse(snsPayload.Message) : undefined;
        if (!messagePayload) {
            return result;
        }
        else {
            return messagePayload;
        }
    };
    return AwsSnsAdapter;
}());
exports.AwsSnsAdapter = AwsSnsAdapter;
