import * as AWS from 'aws-sdk';
import {Logger} from '../../Logger';

export class AwsSnsAdapter {
    private _region: string;
    private _awsAccountId: string;
    private _sns: any;

    constructor(region: string, awsAccountId: string) {
        this._region = region;
        this._awsAccountId = awsAccountId;
        this._sns = new AWS.SNS({ apiVersion: '2010-03-31' });
    }

    public publishEvent(eventFunction: string, payload: any, cb: (...args: any[]) => void) {
        const params = {
            Message: JSON.stringify(payload),
            TopicArn: 'arn:aws:sns:' + this._region + ':' + this._awsAccountId + ':' + eventFunction
        };

        this._sns.publish(params, (error, data) => {
            if (error) {
                Logger.critical('SNS EVENT ERROR ' + eventFunction, error);
                cb(error);
            }

            cb(null, data);
        });
    }

    public isValidEvent(eventSource: string): boolean {
        return eventSource === 'aws:sns';
    }

    public retrievePayload(payload: any): any {
        const snsPayload = payload.Sns ? payload.Sns : undefined;
        let result = {};

        if (!snsPayload) {
            return result;
        }

        const messagePayload = snsPayload.Message ? JSON.parse(snsPayload.Message) : undefined;

        if (!messagePayload) {
            return result;
        } else {
            return messagePayload;
        }
    }
}
