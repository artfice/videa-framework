"use strict";
var AWS = require("aws-sdk");
var S3Uploader = require("s3-upload-stream");
var AwsS3Adapter = (function () {
    function AwsS3Adapter(config) {
        this._s3 = new AWS.S3({
            region: config.region,
            credentials: config.credentials,
            apiVersion: '2006-03-01'
        });
        this._s3Stream = S3Uploader(this._s3);
        this._bucket = config.bucket;
    }
    AwsS3Adapter.prototype.getClient = function () {
        return this._s3;
    };
    AwsS3Adapter.prototype.getUploader = function () {
        return this._s3Stream;
    };
    AwsS3Adapter.prototype.getBucket = function () {
        return this._bucket;
    };
    return AwsS3Adapter;
}());
exports.AwsS3Adapter = AwsS3Adapter;
