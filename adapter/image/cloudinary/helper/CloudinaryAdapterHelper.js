"use strict";
var CloudinaryAdapterHelper;
(function (CloudinaryAdapterHelper) {
    function getTransformName(tenantId, imageTypeId) {
        return removeAllSpaceCharacters(tenantId) + '_' + imageTypeId;
    }
    CloudinaryAdapterHelper.getTransformName = getTransformName;
    function removeAllSpaceCharacters(value) {
        return value.replace(/\s+/g, '').toLowerCase();
    }
})(CloudinaryAdapterHelper = exports.CloudinaryAdapterHelper || (exports.CloudinaryAdapterHelper = {}));
