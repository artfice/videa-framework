import {CloudinaryAdapterConfig} from '../config/CloudinaryAdapterConfig';

export module CloudinaryAdapterConfigHelper {
    export function getCloudinaryAdapterConfig(): CloudinaryAdapterConfig {
        return <CloudinaryAdapterConfig>{
            cloudName: process.env.CLOUDINARY_CLOUD_NAME,
            apiKey: process.env.CLOUDINARY_API_KEY,
            apiSecret: process.env.CLOUDINARY_API_SECRET
        };
    }
}
