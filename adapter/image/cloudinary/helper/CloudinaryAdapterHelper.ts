export module CloudinaryAdapterHelper {
    export function getTransformName(tenantId: string, imageTypeId: string): string {
        return removeAllSpaceCharacters(tenantId) + '_' + imageTypeId;
    }

    function removeAllSpaceCharacters(value: string): string {
        return value.replace(/\s+/g, '').toLowerCase();
    }
}
