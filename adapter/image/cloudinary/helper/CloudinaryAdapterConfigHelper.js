"use strict";
var CloudinaryAdapterConfigHelper;
(function (CloudinaryAdapterConfigHelper) {
    function getCloudinaryAdapterConfig() {
        return {
            cloudName: process.env.CLOUDINARY_CLOUD_NAME,
            apiKey: process.env.CLOUDINARY_API_KEY,
            apiSecret: process.env.CLOUDINARY_API_SECRET
        };
    }
    CloudinaryAdapterConfigHelper.getCloudinaryAdapterConfig = getCloudinaryAdapterConfig;
})(CloudinaryAdapterConfigHelper = exports.CloudinaryAdapterConfigHelper || (exports.CloudinaryAdapterConfigHelper = {}));
