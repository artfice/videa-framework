export interface CloudinaryAdapterConfig {
    cloudName: string;
    apiKey: string;
    apiSecret: string;
}
