"use strict";
var cloudinary = require("cloudinary");
var uuid = require("node-uuid");
var CloudinaryAdapter = (function () {
    function CloudinaryAdapter(config) {
        cloudinary.config({
            cloud_name: config.cloudName,
            api_key: config.apiKey,
            api_secret: config.apiSecret
        });
    }
    CloudinaryAdapter.prototype.getClient = function () {
        return cloudinary;
    };
    CloudinaryAdapter.prototype.generateIdForDto = function (dto) {
        return uuid.v1();
    };
    return CloudinaryAdapter;
}());
exports.CloudinaryAdapter = CloudinaryAdapter;
