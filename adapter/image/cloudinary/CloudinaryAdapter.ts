import cloudinary = require('cloudinary');
import uuid = require('node-uuid');

import {Image} from '../../../domain/image/Image';
import {CloudinaryAdapterConfig} from './config/CloudinaryAdapterConfig';

export class CloudinaryAdapter {

    constructor(config: CloudinaryAdapterConfig) {
        cloudinary.config({
            cloud_name: config.cloudName,
            api_key: config.apiKey,
            api_secret: config.apiSecret
        });
    }

    public getClient(): any {
        return cloudinary;
    }

    public generateIdForDto(dto: Image): string {
        return uuid.v1();
    }
}
