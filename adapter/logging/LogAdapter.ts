import winston = require('winston');

import * as Digi from '../../Digi';
import {ILogger} from './ILogger';

// Transports used with winston - tslint disabled due to the way winston transports work
// tslint:disable-next-line

// TODO: This class needs refactoring https://digiflare.atlassian.net/browse/VAS-1152
export class LogAdapter implements ILogger {
    private _winstonLogger: any;
    private _loggingLevels: any = {
        trace: 5,
        debug: 4,
        info: 3,
        warn: 2,
        error: 1,
        critical: 0
    };
    private _colorLevels: any = {
        trace: 'cyan',
        debug: 'blue',
        info: 'green',
        warn: 'yellow',
        error: 'red',
        critical: 'red'
    };

    constructor(transportList: any[]) {
        const transports = this.configureTransports(transportList);
        winston.addColors(this._colorLevels);
        this._winstonLogger = new (winston.Logger)({
            transports: transports,
            levels: this._loggingLevels
        });
    }

    public log(level: string, message: string, obj?: any) {
        if (obj) {
            this._winstonLogger.log(level, message, obj);
        } else {
            this._winstonLogger.log(level, message);
        }
    }

    public trace(message: string, obj?: any) {
        this.log('trace', message, obj);
    }

    public info(message: string, obj?: any) {
        this.log('info', message, obj);
    }

    public debug(message: string, obj?: any) {
        this.log('debug', message, obj);
    }

    public warn(message: string, obj?: any) {
        this.log('warn', message, obj);
    }

    public error(message: string, obj?: any) {
        this.log('error', message, obj);
    }

    public critical(message: string, obj?: any) {
        this.log('critical', message, obj);
    }

    private configureTransports(configTransports: any): any {
        const transports = [];

        for (let i = 0; i < configTransports.length; i++) {
            const transport = this.addTransport(configTransports[i]);
            if (transport) {
                transports.push(transport);
            }
        }

        return transports;
    }

    private addTransport(config: any): winston.TransportInstance {
        // TODO: if config has no keys then this will crash. Please fix.
        const configKey = Object.keys(config)[0];

        if (!Digi.Obj.hasKeys(config[configKey])) {
            return undefined;
        }

        switch (configKey) {
            case 'console':
                return new (winston.transports.Console)(config[configKey]);
            case 'file':
                return new (winston.transports.File)(config[configKey]);
            case 'logentries':
                return new (winston.transports.Logentries)(config[configKey]);
            default:
                return undefined;
        }
    }
}
