/**
 * Created by bardiakhosravi on 2016-10-14.
 */
export interface ILogger {
    log(level: string, message: string, obj?: any);
    debug(message: string, obj?: any);
    trace(message: string, obj?: any);
    info(message: string, obj?: any);
    warn(message: string, obj?: any);
    error(message: string, obj?: any);
    critical(message: string, obj?: any);
}