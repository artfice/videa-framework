"use strict";
var winston = require("winston");
var Digi = require("../../Digi");
// Transports used with winston - tslint disabled due to the way winston transports work
// tslint:disable-next-line
// TODO: This class needs refactoring https://digiflare.atlassian.net/browse/VAS-1152
var LogAdapter = (function () {
    function LogAdapter(transportList) {
        this._loggingLevels = {
            trace: 5,
            debug: 4,
            info: 3,
            warn: 2,
            error: 1,
            critical: 0
        };
        this._colorLevels = {
            trace: 'cyan',
            debug: 'blue',
            info: 'green',
            warn: 'yellow',
            error: 'red',
            critical: 'red'
        };
        var transports = this.configureTransports(transportList);
        winston.addColors(this._colorLevels);
        this._winstonLogger = new (winston.Logger)({
            transports: transports,
            levels: this._loggingLevels
        });
    }
    LogAdapter.prototype.log = function (level, message, obj) {
        if (obj) {
            this._winstonLogger.log(level, message, obj);
        }
        else {
            this._winstonLogger.log(level, message);
        }
    };
    LogAdapter.prototype.trace = function (message, obj) {
        this.log('trace', message, obj);
    };
    LogAdapter.prototype.info = function (message, obj) {
        this.log('info', message, obj);
    };
    LogAdapter.prototype.debug = function (message, obj) {
        this.log('debug', message, obj);
    };
    LogAdapter.prototype.warn = function (message, obj) {
        this.log('warn', message, obj);
    };
    LogAdapter.prototype.error = function (message, obj) {
        this.log('error', message, obj);
    };
    LogAdapter.prototype.critical = function (message, obj) {
        this.log('critical', message, obj);
    };
    LogAdapter.prototype.configureTransports = function (configTransports) {
        var transports = [];
        for (var i = 0; i < configTransports.length; i++) {
            var transport = this.addTransport(configTransports[i]);
            if (transport) {
                transports.push(transport);
            }
        }
        return transports;
    };
    LogAdapter.prototype.addTransport = function (config) {
        // TODO: if config has no keys then this will crash. Please fix.
        var configKey = Object.keys(config)[0];
        if (!Digi.Obj.hasKeys(config[configKey])) {
            return undefined;
        }
        switch (configKey) {
            case 'console':
                return new (winston.transports.Console)(config[configKey]);
            case 'file':
                return new (winston.transports.File)(config[configKey]);
            case 'logentries':
                return new (winston.transports.Logentries)(config[configKey]);
            default:
                return undefined;
        }
    };
    return LogAdapter;
}());
exports.LogAdapter = LogAdapter;
