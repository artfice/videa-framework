"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var mongodb_1 = require("../adapter/db/mongodb");
var Logger_1 = require("../Logger");
var BaseHandler_1 = require("./BaseHandler");
var ErrorTranslatorHelper_1 = require("./helper/ErrorTranslatorHelper");
var RequestParamsHelper_1 = require("./helper/RequestParamsHelper");
var BaseDtoHandler = (function (_super) {
    __extends(BaseDtoHandler, _super);
    function BaseDtoHandler(config) {
        var _this = _super.call(this) || this;
        // set default params
        var tempConfig = config || {};
        _this._config = {
            idParamName: tempConfig.idParamName || RequestParamsHelper_1.RequestParamsHelper.ID_PARAM_NAME
        };
        return _this;
    }
    BaseDtoHandler.prototype.setBaseService = function (service) {
        this._baseService = service;
    };
    BaseDtoHandler.prototype.getBaseService = function () {
        return this._baseService;
    };
    BaseDtoHandler.prototype.getDtoMongoDbAdapterConfig = function (dtoName) {
        return mongodb_1.MongodbAdapterConfigHelper.getDtoMongoDbAdapterConfig(dtoName);
    };
    BaseDtoHandler.prototype.add = function (event, context, callback) {
        var _this = this;
        context.callbackWaitsForEmptyEventLoop = false;
        this.getRequest(event).then(function (request) {
            var dto = request.body;
            _this._baseService.add(dto).then(function (result) {
                callback(null, result);
            }).catch(callback);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call add', error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    BaseDtoHandler.prototype.getById = function (event, context, callback) {
        context.callbackWaitsForEmptyEventLoop = false;
        var id = this.extractIdParam(event);
        this._baseService.getById(id).then(function (result) {
            callback(null, result);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call getById', error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    BaseDtoHandler.prototype.search = function (event, context, callback) {
        context.callbackWaitsForEmptyEventLoop = false;
        var searchParams = this.extractSearchParams(event);
        this._baseService.search(searchParams).then(function (result) {
            callback(null, result);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call search', error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    BaseDtoHandler.prototype.update = function (event, context, callback) {
        var _this = this;
        context.callbackWaitsForEmptyEventLoop = false;
        var id = this.extractIdParam(event);
        this.getRequest(event).then(function (request) {
            var dto = request.body;
            _this.getBaseService().update(id, dto).then(function (result) {
                callback(null, result);
            }).catch(callback);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call update', error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    BaseDtoHandler.prototype.remove = function (event, context, callback) {
        context.callbackWaitsForEmptyEventLoop = false;
        var id = this.extractIdParam(event);
        this._baseService.remove(id).then(function (result) {
            callback(null, result);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call remove  ', error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    BaseDtoHandler.prototype.extractIdParam = function (event) {
        return RequestParamsHelper_1.RequestParamsHelper.extractIdParam(event, this._config.idParamName);
    };
    return BaseDtoHandler;
}(BaseHandler_1.BaseHandler));
exports.BaseDtoHandler = BaseDtoHandler;
