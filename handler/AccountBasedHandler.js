"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var mongodb_1 = require("../adapter/db/mongodb");
var Logger_1 = require("../Logger");
var BaseHandler_1 = require("./BaseHandler");
var ErrorTranslatorHelper_1 = require("./helper/ErrorTranslatorHelper");
var RequestParamsHelper_1 = require("./helper/RequestParamsHelper");
var AccountBasedHandler = (function (_super) {
    __extends(AccountBasedHandler, _super);
    function AccountBasedHandler(config) {
        var _this = _super.call(this) || this;
        // set default params
        var tempConfig = config || {};
        _this._config = {
            accountIdParamName: tempConfig.accountIdParamName || RequestParamsHelper_1.RequestParamsHelper.TENANT_ID_PARAM_NAME,
            idParamName: tempConfig.idParamName || RequestParamsHelper_1.RequestParamsHelper.ID_PARAM_NAME
        };
        return _this;
    }
    AccountBasedHandler.prototype.setBaseService = function (service) {
        this._baseService = service;
    };
    AccountBasedHandler.prototype.getBaseService = function () {
        return this._baseService;
    };
    AccountBasedHandler.prototype.getDtoMongoDbAdapterConfig = function (dtoName) {
        return mongodb_1.MongodbAdapterConfigHelper.getDtoMongoDbAdapterConfig(dtoName);
    };
    AccountBasedHandler.prototype.add = function (event, context, callback) {
        var _this = this;
        context.callbackWaitsForEmptyEventLoop = false;
        var accountId = this.extractAccountIdParam(event);
        this.getRequest(event).then(function (request) {
            var dto = request.body;
            return _this.getBaseService().add(accountId, dto);
        }).then(function (result) {
            callback(null, result);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call add on account ' + accountId, error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    AccountBasedHandler.prototype.update = function (event, context, callback) {
        var _this = this;
        context.callbackWaitsForEmptyEventLoop = false;
        var accountId = this.extractAccountIdParam(event);
        var id = this.extractIdParam(event);
        this.getRequest(event).then(function (request) {
            var dto = request.body;
            return _this.getBaseService().update(accountId, id, dto);
        }).then(function (result) {
            callback(null, result);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call update on account ' + accountId, error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    AccountBasedHandler.prototype.remove = function (event, context, callback) {
        context.callbackWaitsForEmptyEventLoop = false;
        var accountId = this.extractAccountIdParam(event);
        var id = this.extractIdParam(event);
        this.getBaseService().remove(accountId, id).then(function (result) {
            callback(null, result);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call remove on account ' + accountId, error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    AccountBasedHandler.prototype.getById = function (event, context, callback) {
        context.callbackWaitsForEmptyEventLoop = false;
        var accountId = this.extractAccountIdParam(event);
        var id = this.extractIdParam(event);
        this.getBaseService().getById(accountId, id).then(function (result) {
            callback(null, result);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call getById on account ' + accountId, error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    AccountBasedHandler.prototype.search = function (event, context, callback) {
        context.callbackWaitsForEmptyEventLoop = false;
        var accountId = this.extractAccountIdParam(event);
        var searchParams = this.extractSearchParams(event);
        this.getBaseService().search(accountId, searchParams).then(function (result) {
            callback(null, result);
        }).catch(function (error) {
            Logger_1.Logger.error('Failed to call search on account ' + accountId, error);
            callback(ErrorTranslatorHelper_1.ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    };
    AccountBasedHandler.prototype.extractAccountIdParam = function (event) {
        return RequestParamsHelper_1.RequestParamsHelper.extractTenantIdParam(event, this._config.accountIdParamName);
    };
    AccountBasedHandler.prototype.extractIdParam = function (event) {
        return RequestParamsHelper_1.RequestParamsHelper.extractIdParam(event, this._config.idParamName);
    };
    return AccountBasedHandler;
}(BaseHandler_1.BaseHandler));
exports.AccountBasedHandler = AccountBasedHandler;
