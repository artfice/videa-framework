"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var BaseBodyParser_1 = require("./BaseBodyParser");
var Logger_1 = require("./../../../Logger");
var streamifier = require("streamifier");
var Busboy = require("busboy");
var MultipartBodyParser = (function (_super) {
    __extends(MultipartBodyParser, _super);
    function MultipartBodyParser() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.CONTENT_TYPE = 'multipart/form-data';
        return _this;
    }
    MultipartBodyParser.prototype.parse = function () {
        if (this._contentType.indexOf(this.CONTENT_TYPE) !== -1) {
            return this._parse();
        }
        else {
            return this._nextParser.parse();
        }
    };
    MultipartBodyParser.prototype._parse = function () {
        return this._parseMultipartForm();
    };
    MultipartBodyParser.prototype._parseMultipartForm = function () {
        var _this = this;
        var newBody = {};
        var busboy;
        var bodyBuffer = new Buffer(this._body, 'binary');
        var bufferStream = streamifier.createReadStream(bodyBuffer);
        //Busboy expects lowercase headers
        this._headers['content-type'] = this._contentType;
        busboy = new Busboy({ headers: this._headers });
        Logger_1.Logger.debug('Ready to parse with Busboy: Event Body');
        return new Promise(function (resolve, reject) {
            //parsed files from the form. This is not used if a file is uploaded withe base64 encoding.
            busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
                file.on('data', function (data) {
                });
                file.on('end', function () {
                });
            });
            //Parsed fields from the form.
            busboy.on('field', function (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
                newBody[fieldname] = _this._getBase64Buffer(val);
            });
            //Called when busboy is finished parsing the form.
            busboy.on('finish', function () {
                Logger_1.Logger.debug('Finished parsing body');
                return resolve(newBody);
            });
            bufferStream.pipe(busboy);
        });
    };
    MultipartBodyParser.prototype._getBase64Buffer = function (value) {
        var base64Index = value.indexOf(';base64,');
        if (base64Index > -1) {
            return new Buffer(value.split(',')[1], 'base64');
        }
        return value;
    };
    return MultipartBodyParser;
}(BaseBodyParser_1.BaseBodyParser));
exports.MultipartBodyParser = MultipartBodyParser;
