"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var BaseBodyParser_1 = require("./BaseBodyParser");
var GenericBodyParser = (function (_super) {
    __extends(GenericBodyParser, _super);
    function GenericBodyParser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GenericBodyParser.prototype.parse = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return resolve(_this._body);
        });
    };
    return GenericBodyParser;
}(BaseBodyParser_1.BaseBodyParser));
exports.GenericBodyParser = GenericBodyParser;
