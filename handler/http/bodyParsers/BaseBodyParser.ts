import * as Promise from 'bluebird';
import * as _ from 'lodash';
import {InvalidArgumentError} from './../../../VideaError';

export abstract class BaseBodyParser {
    protected _nextParser: BaseBodyParser;
    protected _headers: any;
    protected _body: any;
    protected _contentType: string;
    
    constructor(headers: any, body: any) {

        if(_.isUndefined(headers)){
            throw new InvalidArgumentError("Headers are not defined in this request: " + headers);
        }

        this._headers = headers;
        this._body = body;
        this._getContentType();
    }

    private _getContentType(): string {
        this._contentType = this._headers['content-type'] || this._headers['Content-Type'] || 'application/json';
        return this._contentType;
    }

    public addParser(nextParser: BaseBodyParser) {
        this._nextParser = nextParser;
    }

    public abstract parse(): Promise<any>;
}