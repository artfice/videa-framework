import * as Promise from 'bluebird';
import {BaseBodyParser} from './BaseBodyParser';
import {Logger} from './../../../Logger';
import streamifier = require('streamifier');
import Busboy = require('busboy');

export class MultipartBodyParser extends BaseBodyParser{
    private readonly CONTENT_TYPE = 'multipart/form-data';

    public parse(): Promise<any> {

        if(this._contentType.indexOf(this.CONTENT_TYPE) !== -1) {
           return this._parse();
        } else {
           return this._nextParser.parse(); 
        }
    }

    private _parse(): Promise<any> {
        return this._parseMultipartForm();
    }

    private _parseMultipartForm(): Promise<any> {
        let newBody: any = {};
        let busboy: any;
        const bodyBuffer: Buffer = new Buffer(this._body, 'binary');
        const bufferStream: any = streamifier.createReadStream(bodyBuffer);

        //Busboy expects lowercase headers
        this._headers['content-type'] = this._contentType;
        busboy = new Busboy({ headers: this._headers });

        Logger.debug('Ready to parse with Busboy: Event Body');

        return new Promise<any>((resolve, reject) => {

            //parsed files from the form. This is not used if a file is uploaded withe base64 encoding.
            busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {

                file.on('data', (data) => {
                });

                file.on('end', () => {
                });

            });

            //Parsed fields from the form.
            busboy.on('field', (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) => {
                newBody[fieldname] = this._getBase64Buffer(val);
            });

            //Called when busboy is finished parsing the form.
            busboy.on('finish', () => {
                Logger.debug('Finished parsing body');
                return resolve(newBody);
            });

            bufferStream.pipe(busboy);
        });
    }

    private _getBase64Buffer(value : string) : any{
        let base64Index = value.indexOf(';base64,');

        if(base64Index > -1) {
            return new Buffer(value.split(',')[1], 'base64');
        }
        
        return value;
    }
}