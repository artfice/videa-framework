import * as Promise from 'bluebird';

import {BaseBodyParser} from './BaseBodyParser';

export class GenericBodyParser extends BaseBodyParser{

    public parse(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return resolve(this._body);
        });
    }
}
