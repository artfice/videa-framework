"use strict";
var _ = require("lodash");
var VideaError_1 = require("./../../../VideaError");
var BaseBodyParser = (function () {
    function BaseBodyParser(headers, body) {
        if (_.isUndefined(headers)) {
            throw new VideaError_1.InvalidArgumentError("Headers are not defined in this request: " + headers);
        }
        this._headers = headers;
        this._body = body;
        this._getContentType();
    }
    BaseBodyParser.prototype._getContentType = function () {
        this._contentType = this._headers['content-type'] || this._headers['Content-Type'] || 'application/json';
        return this._contentType;
    };
    BaseBodyParser.prototype.addParser = function (nextParser) {
        this._nextParser = nextParser;
    };
    return BaseBodyParser;
}());
exports.BaseBodyParser = BaseBodyParser;
