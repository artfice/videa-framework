"use strict";
var Promise = require("bluebird");
var Request_1 = require("./Request");
var GenericBodyParser_1 = require("./bodyParsers/GenericBodyParser");
var MultipartBodyParser_1 = require("./bodyParsers/MultipartBodyParser");
var RequestBuilder = (function () {
    function RequestBuilder(method, headers, query, body, path) {
        this._method = method;
        this._headers = headers;
        this._path = path;
        this._query = query;
        this._body = body;
    }
    Object.defineProperty(RequestBuilder.prototype, "method", {
        get: function () {
            return this._method;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestBuilder.prototype, "headers", {
        get: function () {
            return this._headers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestBuilder.prototype, "query", {
        get: function () {
            return this._query;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestBuilder.prototype, "body", {
        get: function () {
            return this._body;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestBuilder.prototype, "path", {
        get: function () {
            return this._path;
        },
        enumerable: true,
        configurable: true
    });
    RequestBuilder.prototype.buildRequest = function () {
        var _this = this;
        var parser = this.setupBodyParsers();
        return parser.parse().then(function (parsedBody) {
            return _this.createRequest(parsedBody);
        });
    };
    RequestBuilder.prototype.createRequest = function (parsedBody) {
        var _this = this;
        var request;
        return new Promise(function (resolve, reject) {
            _this._body = parsedBody;
            request = new Request_1.Request(_this);
            return resolve(request);
        });
    };
    RequestBuilder.prototype.setupBodyParsers = function () {
        //Chain of responsibilty - add more parsers here
        var multipartParser = new MultipartBodyParser_1.MultipartBodyParser(this._headers, this._body);
        var genricBodyParser = new GenericBodyParser_1.GenericBodyParser(this._headers, this._body);
        multipartParser.addParser(genricBodyParser);
        return multipartParser;
    };
    return RequestBuilder;
}());
exports.RequestBuilder = RequestBuilder;
