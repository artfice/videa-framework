import * as Promise from 'bluebird';

import {Request} from './Request';
import {GenericBodyParser} from './bodyParsers/GenericBodyParser';
import {MultipartBodyParser} from './bodyParsers/MultipartBodyParser';
import {BaseBodyParser} from './bodyParsers/BaseBodyParser';

export class RequestBuilder {
    private _method: any;
    private _headers: any;
    private _query: any;
    private _body: any;
    private _path: any;

    public get method(): any {
        return this._method;
    }

    public get headers(): any {
        return this._headers;
    }

    public get query(): any {
        return this._query;
    }

    public get body(): any {
        return this._body;
    }

    public get path(): any {
        return this._path;
    }

    constructor(method: string, headers: any, query: any, body: any, path: any) {
        this._method = method;
        this._headers = headers;
        this._path = path;
        this._query = query;
        this._body = body;
    }

    public buildRequest(): Promise<Request>{
        const parser: BaseBodyParser = this.setupBodyParsers();

        return parser.parse().then((parsedBody) => {
            return this.createRequest(parsedBody);
        });
    }

    private createRequest(parsedBody: any): Promise<Request> {
        let request: Request;

        return new Promise<Request>((resolve, reject) => {
            this._body = parsedBody;
            request = new Request(this);
            return resolve(request);
        });
    }

    private setupBodyParsers(): BaseBodyParser {
        //Chain of responsibilty - add more parsers here
        const multipartParser = new MultipartBodyParser(this._headers, this._body);
        const genricBodyParser = new GenericBodyParser(this._headers, this._body);

        multipartParser.addParser(genricBodyParser);

        return multipartParser;
    }
}