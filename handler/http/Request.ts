import {RequestBuilder} from './RequestBuilder';

export class Request {
    private _method: string;
    private _headers: any;
    private _query: any;
    private _body: any;
    private _path: any;

    public get method(): string {
        return this._method;
    }

    public get headers(): any {
        return this._headers;
    }

    public get query(): any {
        return this._query;
    }

    public get body(): any {
        return this._body;
    }

    public get path(): any {
        return this._path;
    }
   
    constructor(builder: RequestBuilder) {
        this._method = builder.method;
        this._headers = builder.headers;
        this._query = builder.query;
        this._body = builder.body;
        this._path = builder.path;
    }
}