import * as Promise from 'bluebird';

import {SchemaApplicationServiceConfig} from '../application/config/SchemaApplicationServiceConfig';
import {SchemaApplicationService} from '../application/SchemaApplicationService';
import {SearchParams} from '../domain/db/SearchParams';
import {SearchParamsHelper} from './helper/SearchParamsHelper';
import {Request} from './http/Request';
import {RequestBuilder} from './http/RequestBuilder';
import {RequestParamsHelper} from './helper/RequestParamsHelper';

export class BaseHandler {

    protected _context;
    protected _isOffline: boolean;
    private _schemaService: SchemaApplicationService;

    constructor() {
        this._schemaService = this.createSchemaService();
    }

    public getSchemaService(): SchemaApplicationService {
        return this._schemaService;
    }

    public handler(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        this._context = context;
        this._isOffline = event && event.isOffline;
    }

    protected getRequest(event: any): Promise<Request> {
        const requestBuilder = new RequestBuilder(event.method, event.headers, event.query, event.body, event.path);

        return requestBuilder.buildRequest();
    }

    protected extractSearchParams(event: any): SearchParams {
        return SearchParamsHelper.extractSearchParams(event);
    }

    protected extractParamByName(event: any, paramName: string): string {
        return RequestParamsHelper.extractParamByName(event, paramName);
    }

    protected createSchemaService(): SchemaApplicationService {
        const config = <SchemaApplicationServiceConfig>{
            schemaLocations: [
                __dirname + '/../../../' + process.env.MODULE_NAME, // add module schema folder
                __dirname + '/..' // add framework schema folder
            ]
        };
        return new SchemaApplicationService(config);
    }
}
