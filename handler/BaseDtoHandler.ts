import {DtoMongoDbAdapterConfig, MongodbAdapterConfigHelper as AdapterConfigHelper} from '../adapter/db/mongodb';
import {BaseService} from '../application/BaseService';
import {BaseDto} from '../domain';
import {SearchParams} from '../domain/db';
import {Request} from '../handler/http/Request';
import {Logger} from '../Logger';
import {BaseHandler} from './BaseHandler';
import {BaseDTOHandlerConfig} from './config/BaseDTOHandlerConfig';
import {ErrorTranslatorHelper} from './helper/ErrorTranslatorHelper';
import {RequestParamsHelper} from './helper/RequestParamsHelper';

export class BaseDtoHandler<T extends BaseDto> extends BaseHandler {

    private _baseService: BaseService<T>;
    private _config: BaseDTOHandlerConfig;

    constructor(config?: BaseDTOHandlerConfig) {
        super();

        // set default params
        const tempConfig: BaseDTOHandlerConfig = config || <BaseDTOHandlerConfig>{};
        this._config = <BaseDTOHandlerConfig>{
            idParamName: tempConfig.idParamName || RequestParamsHelper.ID_PARAM_NAME
        };
    }

    public setBaseService(service: BaseService<T>) {
        this._baseService = service;
    }

    public getBaseService(): BaseService<T> {
        return this._baseService;
    }

    public getDtoMongoDbAdapterConfig(dtoName?: string): DtoMongoDbAdapterConfig {
        return AdapterConfigHelper.getDtoMongoDbAdapterConfig(dtoName);
    }

    public add(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        this.getRequest(event).then((request: Request) => {
            const dto = request.body;
            this._baseService.add(dto).then((result) => {
                callback(null, result);
            }).catch(callback);
        }).catch((error) => {
            Logger.error('Failed to call add', error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    public getById(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const id: string = this.extractIdParam(event);
        this._baseService.getById(id).then((result) => {
            callback(null, result);
        }).catch((error) => {
            Logger.error('Failed to call getById', error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    public search(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const searchParams: SearchParams = this.extractSearchParams(event);
        this._baseService.search(searchParams).then((result) => {
            callback(null, result);

        }).catch((error) => {
            Logger.error('Failed to call search', error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    public update(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const id: string = this.extractIdParam(event);
        this.getRequest(event).then((request: Request) => {
            const dto = request.body;
            this.getBaseService().update(id, dto).then((result) => {
                callback(null, result);
            }).catch(callback);
        }).catch((error) => {
            Logger.error('Failed to call update', error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    public remove(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const id: string = this.extractIdParam(event);
        this._baseService.remove(id).then((result) => {
            callback(null, result);
        }).catch((error) => {
            Logger.error('Failed to call remove  ', error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    protected extractIdParam(event: any): string {
        return RequestParamsHelper.extractIdParam(event, this._config.idParamName);
    }
}
