"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseHandler_1 = require("./BaseHandler");
var RequestBuilder_1 = require("./http/RequestBuilder");
var HttpHandler = (function (_super) {
    __extends(HttpHandler, _super);
    function HttpHandler() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HttpHandler.prototype.handler = function (event, context, callback) {
        _super.prototype.handler.call(this, event, context, callback);
        this._method = event.method;
        this._headers = event.headers;
        this._query = event.query;
        this._body = event.body;
        this._path = event.path;
    };
    HttpHandler.prototype.getRequest = function () {
        var requestBuilder = new RequestBuilder_1.RequestBuilder(this._method, this._headers, this._query, this._body, this._path);
        return requestBuilder.buildRequest();
    };
    return HttpHandler;
}(BaseHandler_1.BaseHandler));
exports.HttpHandler = HttpHandler;
