import {AccountBasedHandlerConfig} from './config/AccountBasedHandlerConfig';
import {DtoMongoDbAdapterConfig, MongodbAdapterConfigHelper as AdapterConfigHelper} from '../adapter/db/mongodb';
import {AccountBasedService} from '../application/AccountBasedService';
import {BaseDto} from '../domain';
import {SearchParams} from '../domain/db';
import {Request} from '../handler/http/Request';
import {Logger} from '../Logger';
import {BaseHandler} from './BaseHandler';
import {ErrorTranslatorHelper} from './helper/ErrorTranslatorHelper';
import {RequestParamsHelper} from './helper/RequestParamsHelper';

export class AccountBasedHandler<T extends BaseDto> extends BaseHandler {

    private _baseService: AccountBasedService<T>;
    private _config: AccountBasedHandlerConfig;

    constructor(config?: AccountBasedHandlerConfig) {
        super();

        // set default params
        const tempConfig: AccountBasedHandlerConfig = config || <AccountBasedHandlerConfig>{};
        this._config = <AccountBasedHandlerConfig>{
            accountIdParamName: tempConfig.accountIdParamName || RequestParamsHelper.TENANT_ID_PARAM_NAME,
            idParamName: tempConfig.idParamName || RequestParamsHelper.ID_PARAM_NAME
        };
    }

    public setBaseService(service: AccountBasedService<T>) {
        this._baseService = service;
    }

    public getBaseService(): AccountBasedService<T> {
        return this._baseService;
    }

    public getDtoMongoDbAdapterConfig(dtoName?: string): DtoMongoDbAdapterConfig {
        return AdapterConfigHelper.getDtoMongoDbAdapterConfig(dtoName);
    }

    public add(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const accountId: string = this.extractAccountIdParam(event);
        this.getRequest(event).then((request: Request) => {
            const dto = request.body;
            return this.getBaseService().add(accountId, dto);
        }).then((result) => {
            callback(null, result);
        }).catch((error) => {
            Logger.error('Failed to call add on account ' + accountId, error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    public update(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const accountId: string = this.extractAccountIdParam(event);
        const id: string = this.extractIdParam(event);
        this.getRequest(event).then((request: Request) => {
            const dto = request.body;
            return this.getBaseService().update(accountId, id, dto);
        }).then((result) => {
            callback(null, result);
        }).catch((error) => {
            Logger.error('Failed to call update on account ' + accountId, error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    public remove(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const accountId: string = this.extractAccountIdParam(event);
        const id: string = this.extractIdParam(event);
        this.getBaseService().remove(accountId, id).then((result) => {
            callback(null, result);
        }).catch((error) => {
            Logger.error('Failed to call remove on account ' + accountId, error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    public getById(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const accountId: string = this.extractAccountIdParam(event);
        const id: string = this.extractIdParam(event);
        this.getBaseService().getById(accountId, id).then((result) => {
            callback(null, result);
        }).catch((error) => {
            Logger.error('Failed to call getById on account ' + accountId, error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    public search(event: any, context: any, callback: any): void {
        context.callbackWaitsForEmptyEventLoop = false;
        const accountId: string = this.extractAccountIdParam(event);
        const searchParams: SearchParams = this.extractSearchParams(event);
        this.getBaseService().search(accountId, searchParams).then((result) => {
            callback(null, result);

        }).catch((error) => {
            Logger.error('Failed to call search on account ' + accountId, error);
            callback(ErrorTranslatorHelper.translateErrorToResponse(error));
        });
    }

    protected extractAccountIdParam(event: any): string {
        return RequestParamsHelper.extractTenantIdParam(event, this._config.accountIdParamName);
    }

    protected extractIdParam(event: any): string {
        return RequestParamsHelper.extractIdParam(event, this._config.idParamName);
    }
}
