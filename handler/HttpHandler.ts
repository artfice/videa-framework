import * as Promise from 'bluebird';
import {BaseHandler} from './BaseHandler';
import {RequestBuilder} from './http/RequestBuilder';
import {Request} from './http/Request';

export class HttpHandler extends BaseHandler {

    protected _method: string;
    protected _headers: any;
    protected _query: any;
    protected _body: any;
    protected _path: any;

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._method = event.method;
        this._headers = event.headers;
        this._query = event.query;
        this._body = event.body;
        this._path = event.path;
    }

    protected getRequest(): Promise<Request> {
        const requestBuilder = new RequestBuilder(this._method, this._headers, this._query, this._body, this._path);

        return requestBuilder.buildRequest();
    }
}
