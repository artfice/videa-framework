export interface AccountBasedHandlerConfig {
    accountIdParamName: string;
    idParamName: string;
}
