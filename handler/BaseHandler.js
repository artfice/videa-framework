"use strict";
var SchemaApplicationService_1 = require("../application/SchemaApplicationService");
var SearchParamsHelper_1 = require("./helper/SearchParamsHelper");
var RequestBuilder_1 = require("./http/RequestBuilder");
var RequestParamsHelper_1 = require("./helper/RequestParamsHelper");
var BaseHandler = (function () {
    function BaseHandler() {
        this._schemaService = this.createSchemaService();
    }
    BaseHandler.prototype.getSchemaService = function () {
        return this._schemaService;
    };
    BaseHandler.prototype.handler = function (event, context, callback) {
        context.callbackWaitsForEmptyEventLoop = false;
        this._context = context;
        this._isOffline = event && event.isOffline;
    };
    BaseHandler.prototype.getRequest = function (event) {
        var requestBuilder = new RequestBuilder_1.RequestBuilder(event.method, event.headers, event.query, event.body, event.path);
        return requestBuilder.buildRequest();
    };
    BaseHandler.prototype.extractSearchParams = function (event) {
        return SearchParamsHelper_1.SearchParamsHelper.extractSearchParams(event);
    };
    BaseHandler.prototype.extractParamByName = function (event, paramName) {
        return RequestParamsHelper_1.RequestParamsHelper.extractParamByName(event, paramName);
    };
    BaseHandler.prototype.createSchemaService = function () {
        var config = {
            schemaLocations: [
                __dirname + '/../../../' + process.env.MODULE_NAME,
                __dirname + '/..' // add framework schema folder
            ]
        };
        return new SchemaApplicationService_1.SchemaApplicationService(config);
    };
    return BaseHandler;
}());
exports.BaseHandler = BaseHandler;
