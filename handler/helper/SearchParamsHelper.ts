import * as _ from 'lodash';

import {SearchOperators} from '../../domain/db/SearchOperators';
import {SearchParams} from '../../domain/db/SearchParams';
import {SearchQuery} from '../../domain/db/SearchQuery';
import {Logger} from '../../Logger';

export module SearchParamsHelper {

    export function isUnsorted(searchParams: SearchParams): boolean {
        return _.isEmpty(_.get(searchParams, 'sort', ''));
    }

    export function extractSearchParams(event: any): SearchParams {
        const params = event.query;
        const searchParams: SearchParams = <SearchParams> {};

        if (params) {
            searchParams.offset = parseParam(params.offset);
            searchParams.setSize = parseParam(params.setSize);

            searchParams.sort = parseParam(params.sort);
            searchParams.ascending = parseParam(params.ascending);

            let queryParam = params.query;
            if (_.isUndefined(queryParam) && event.body) {
                searchParams.query = event.body;
            } else {
                searchParams.query = getQuery(queryParam);
            }
        }

        return searchParams;
    }

    export function getQuery(param: string): SearchQuery {
        try {
            if (_.startsWith(param, '{')) {
                return <SearchQuery> JSON.parse(param);
            }

            return <SearchQuery> {
                operator: SearchOperators.Match,
                value: JSON.parse(param)
            };
        } catch (error) {
            Logger.info('Invalid Query param: "' + param + '". It will be ignored.');
        }
        return null;
    }

    export function parseParam(jsonParam: any): any {
        try {
            return JSON.parse(jsonParam);
        } catch (error) {
            // ignore error
        }
        return undefined;
    }
}
