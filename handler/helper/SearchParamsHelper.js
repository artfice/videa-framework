"use strict";
var _ = require("lodash");
var SearchOperators_1 = require("../../domain/db/SearchOperators");
var Logger_1 = require("../../Logger");
var SearchParamsHelper;
(function (SearchParamsHelper) {
    function isUnsorted(searchParams) {
        return _.isEmpty(_.get(searchParams, 'sort', ''));
    }
    SearchParamsHelper.isUnsorted = isUnsorted;
    function extractSearchParams(event) {
        var params = event.query;
        var searchParams = {};
        if (params) {
            searchParams.offset = parseParam(params.offset);
            searchParams.setSize = parseParam(params.setSize);
            searchParams.sort = parseParam(params.sort);
            searchParams.ascending = parseParam(params.ascending);
            var queryParam = params.query;
            if (_.isUndefined(queryParam) && event.body) {
                searchParams.query = event.body;
            }
            else {
                searchParams.query = getQuery(queryParam);
            }
        }
        return searchParams;
    }
    SearchParamsHelper.extractSearchParams = extractSearchParams;
    function getQuery(param) {
        try {
            if (_.startsWith(param, '{')) {
                return JSON.parse(param);
            }
            return {
                operator: SearchOperators_1.SearchOperators.Match,
                value: JSON.parse(param)
            };
        }
        catch (error) {
            Logger_1.Logger.info('Invalid Query param: "' + param + '". It will be ignored.');
        }
        return null;
    }
    SearchParamsHelper.getQuery = getQuery;
    function parseParam(jsonParam) {
        try {
            return JSON.parse(jsonParam);
        }
        catch (error) {
        }
        return undefined;
    }
    SearchParamsHelper.parseParam = parseParam;
})(SearchParamsHelper = exports.SearchParamsHelper || (exports.SearchParamsHelper = {}));
