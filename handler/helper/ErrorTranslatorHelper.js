"use strict";
var VideaError_1 = require("../../VideaError");
var ErrorTranslatorHelper;
(function (ErrorTranslatorHelper) {
    function translateErrorToResponse(error) {
        var response = {
            statusCode: 500,
            message: error.message
        };
        if (error instanceof VideaError_1.SecurityError) {
            response.statusCode = 401; // Unauthorized
        }
        else if (error instanceof VideaError_1.ItemNotFoundError) {
            response.statusCode = 404; // Not Found
        }
        return JSON.stringify(response);
    }
    ErrorTranslatorHelper.translateErrorToResponse = translateErrorToResponse;
})(ErrorTranslatorHelper = exports.ErrorTranslatorHelper || (exports.ErrorTranslatorHelper = {}));
