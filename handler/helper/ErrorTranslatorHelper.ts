import { ItemNotFoundError, SecurityError } from '../../VideaError';

export module ErrorTranslatorHelper {
    export function translateErrorToResponse(error: any) : any {
        const response = {
            statusCode : 500, // Default is internal server error
            message : error.message
        };

        if (error instanceof SecurityError) {
            response.statusCode = 401; // Unauthorized
        } else if (error instanceof ItemNotFoundError) {
            response.statusCode = 404; // Not Found
        }

        return JSON.stringify(response);
    }
}
