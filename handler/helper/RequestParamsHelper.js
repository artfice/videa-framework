"use strict";
var RequestParamsHelper;
(function (RequestParamsHelper) {
    RequestParamsHelper.TENANT_ID_PARAM_NAME = 'tenantId';
    RequestParamsHelper.ID_PARAM_NAME = 'id';
    function extractParamByName(event, paramName) {
        var value = event.path[paramName];
        if (!value) {
            value = event.query[paramName];
        }
        return value;
    }
    RequestParamsHelper.extractParamByName = extractParamByName;
    function extractTenantIdParam(event, tenantIdParamName) {
        return this.extractParamByName(event, tenantIdParamName || RequestParamsHelper.TENANT_ID_PARAM_NAME);
    }
    RequestParamsHelper.extractTenantIdParam = extractTenantIdParam;
    function extractIdParam(event, idParamName) {
        return this.extractParamByName(event, idParamName || RequestParamsHelper.ID_PARAM_NAME);
    }
    RequestParamsHelper.extractIdParam = extractIdParam;
})(RequestParamsHelper = exports.RequestParamsHelper || (exports.RequestParamsHelper = {}));
