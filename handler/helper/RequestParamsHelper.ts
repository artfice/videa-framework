export module RequestParamsHelper {

    export const TENANT_ID_PARAM_NAME: string = 'tenantId';
    export const ID_PARAM_NAME: string = 'id';

    export function extractParamByName(event: any, paramName: string): string {
        let value = event.path[paramName];
        if (!value) {
            value = event.query[paramName];
        }
        return value;
    }

    export function extractTenantIdParam(event: any, tenantIdParamName?: string): string {
        return this.extractParamByName(event, tenantIdParamName || TENANT_ID_PARAM_NAME);
    }

    export function extractIdParam(event: any, idParamName?: string): string {
        return this.extractParamByName(event, idParamName || ID_PARAM_NAME);
    }
}
