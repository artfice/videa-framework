import {BaseDto} from '../BaseDto';
import {MessageAttribute} from './MessageAttribute';

export interface Message extends BaseDto {
    delaySeconds: number;
    attributes: MessageAttribute[];
    body: string;
}
