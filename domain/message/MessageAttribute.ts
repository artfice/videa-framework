export interface MessageAttribute {
    name: string;
    dataType: string;
    stringValue: string;
}
