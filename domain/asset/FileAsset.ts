export interface FileAsset {
    buffer: any;
    name: string;
}
