export interface RemoteAsset {
    containerId: string;
    assetId: string;
    contentType: string;
    url: string;
}
