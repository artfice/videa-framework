export interface BaseDto {
    id: string;
    createdDate: Date;
    modifiedDate: Date;
    /**
     * Is this object a system object? If affirmative it can't be modified or deleted.
     */
    systemObject: boolean;
}
