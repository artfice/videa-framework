export interface ValidatorResult {
    valid: boolean;
    errorsText: string;
}
