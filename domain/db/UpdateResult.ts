export interface UpdateResult {
    id: string;
    success: boolean;
    modifiedDate: Date;
    modifiedItems: number;
}
