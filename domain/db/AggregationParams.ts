import { AggregationLookup } from './AggregationLookup';
import { SearchParams } from './SearchParams';

export interface AggregationParams extends SearchParams {
    lookUp : AggregationLookup;
}
