import {SearchOperators} from './SearchOperators';

export interface SearchQuery {
    field: string; // field name to perform the query. Some operators support _all to specify a search in all available fields
    operator: SearchOperators;
    value: any; // depends on the selected SearchOperators
}
