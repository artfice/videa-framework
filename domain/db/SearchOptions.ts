/**
 * Options that can be used to filter or customize the results of a search service.
 * E.g.: listing devices of a given brand sorted by name
 */
export interface SearchOptions {
    offset: number;
    setSize: number;
    sort: string;
    ascending: boolean;
    /**
     * The projection property is a map of { <fieldName>: 0 or 1 } that includes or excludes fields from the search results.
     */
    projection: any;
}
