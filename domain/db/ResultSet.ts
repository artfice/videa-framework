export interface ResultSet {
    data: any[];
    nextOffset: number;
    total: number;
}
