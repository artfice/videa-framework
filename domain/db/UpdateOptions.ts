/**
 * Options that can be used to customize the update operation.
 */
export interface UpdateOptions {
    /**
     * If true object will be added if it doesn't exist.
     */
    upsert: boolean;
    /**
     * If true query will update more than one document
     */
    multi : boolean;
    /**
     * Dictionary with the name of the field and the UpdateFieldOptions for that particular field.
     */
    fieldOptions: any;
}
