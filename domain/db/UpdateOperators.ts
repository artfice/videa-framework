
export enum UpdateOperators {
    set,    // modifies the value of the given field
    push,   // adds the value or values at the end of the array. Fails if the given field isn't an array
    pull,   // deletes all occurrences of the given value or values from the array. Fails if the given field isn't an array
    addToSet,    // adds the value or values at the end of the array unless it already exists. Fails if the given field isn't an array
    pullFromElement,   /* deletes all occurrences in the elements of array property of the given value or values from
                        the array. Fails if the given field isn't an array */
    unset
}
