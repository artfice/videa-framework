"use strict";
var SearchOperators;
(function (SearchOperators) {
    SearchOperators[SearchOperators["In"] = 0] = "In";
    SearchOperators[SearchOperators["Match"] = 1] = "Match";
    SearchOperators[SearchOperators["MatchAll"] = 2] = "MatchAll";
    SearchOperators[SearchOperators["Equal"] = 3] = "Equal";
    SearchOperators[SearchOperators["And"] = 4] = "And";
    SearchOperators[SearchOperators["NIn"] = 5] = "NIn";
    SearchOperators[SearchOperators["Or"] = 6] = "Or";
    SearchOperators[SearchOperators["Ids"] = 7] = "Ids";
    SearchOperators[SearchOperators["NE"] = 8] = "NE";
    SearchOperators[SearchOperators["MultiMatch"] = 9] = "MultiMatch";
    SearchOperators[SearchOperators["Exists"] = 10] = "Exists";
    SearchOperators[SearchOperators["Lt"] = 11] = "Lt";
    SearchOperators[SearchOperators["Lte"] = 12] = "Lte";
    SearchOperators[SearchOperators["Gt"] = 13] = "Gt";
    SearchOperators[SearchOperators["Gte"] = 14] = "Gte";
    SearchOperators[SearchOperators["Regex"] = 15] = "Regex";
    SearchOperators[SearchOperators["ElementMatch"] = 16] = "ElementMatch"; // matches documents that contain an array field with at least one element that matches all the specified query criteria
})(SearchOperators = exports.SearchOperators || (exports.SearchOperators = {}));
