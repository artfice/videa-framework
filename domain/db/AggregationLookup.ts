export interface AggregationLookup {
    fromCollection: string;  // collection to join
    localField: string; // field from the input documents
    foreignField: string; // field from the documents of the "from" collection
    output : string; // output array field
}
