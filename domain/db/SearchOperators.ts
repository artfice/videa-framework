
export enum SearchOperators {
    In, // check if the current field is equal to any of the given values
    Match, // Performs a full text search in the current field or fields
    MatchAll, // Retrieves all results
    Equal, // check if the field is equal to the given value
    And, // value: [ SearchQuery, SearchQuery, ... ]
    NIn, // check if the current field is different to any of the given values
    Or,
    Ids, // specific operator to get objects through ids
    NE, // not equal
    MultiMatch, // Performs a full text search in the current fields
    Exists, // does the property is not undefined
    Lt, // Less than
    Lte, // Less than equal
    Gt, // Less than
    Gte, // Less than equal
    Regex, // to evaluate regular expression
    ElementMatch // matches documents that contain an array field with at least one element that matches all the specified query criteria
}
