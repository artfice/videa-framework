import {UpdateOperators} from './UpdateOperators';

/**
 * Options that can be used to customize a specific field update operation.
 */
export interface UpdateFieldOptions {
    operator: UpdateOperators;
    elementField?: string;
}
