import {SearchOptions} from './SearchOptions';
import {SearchQuery} from './SearchQuery';

/**
 * Object that defines a search query that filters results on search services.
 */
export interface SearchParams extends SearchOptions {
    query: SearchQuery;
}
