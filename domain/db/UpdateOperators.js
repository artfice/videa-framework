"use strict";
var UpdateOperators;
(function (UpdateOperators) {
    UpdateOperators[UpdateOperators["set"] = 0] = "set";
    UpdateOperators[UpdateOperators["push"] = 1] = "push";
    UpdateOperators[UpdateOperators["pull"] = 2] = "pull";
    UpdateOperators[UpdateOperators["addToSet"] = 3] = "addToSet";
    UpdateOperators[UpdateOperators["pullFromElement"] = 4] = "pullFromElement";
    UpdateOperators[UpdateOperators["unset"] = 5] = "unset";
})(UpdateOperators = exports.UpdateOperators || (exports.UpdateOperators = {}));
