import {WorkerAction} from './WorkerAction';
export interface WorkerEvent {
    workerId: string;
    action: WorkerAction;
}
