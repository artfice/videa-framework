export const WorkerState = {
    Pending: 'Pending' as 'Pending',
    Running: 'Running' as 'Running',
    Completed: 'Completed' as 'Completed',
    Failed: 'Failed' as 'Failed',
    Paused: 'Paused' as 'Paused'
};

export type WorkerState = keyof typeof WorkerState;
