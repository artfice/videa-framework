import * as Promise from 'bluebird';
import {UpdateResult} from '../db/UpdateResult';
import {Worker} from './Worker';

export interface WorkerFunction {
    (worker: Worker, updateCallback: (state: Worker) => Promise<UpdateResult>): Promise<void>;
};
