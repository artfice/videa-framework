export const WorkerAction = {
    Start: 'Start' as 'Start',
    Stop: 'Stop' as 'Stop',
    Pause: 'Pause' as 'Pause',
    Resume: 'Resume' as 'Resume'
};

export type WorkerAction = keyof typeof WorkerAction;
