import {BaseDto} from '../BaseDto';
import { WorkerState } from './WorkerState';

export interface Worker extends BaseDto {
    progress: number;
    internalData: any;
    parameters: any;
    description: string;
    taskName: string;
    state: WorkerState;
    userId: string;
    error: any;
}
