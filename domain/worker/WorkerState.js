"use strict";
exports.WorkerState = {
    Pending: 'Pending',
    Running: 'Running',
    Completed: 'Completed',
    Failed: 'Failed',
    Paused: 'Paused'
};
