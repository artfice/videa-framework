"use strict";
exports.WorkerAction = {
    Start: 'Start',
    Stop: 'Stop',
    Pause: 'Pause',
    Resume: 'Resume'
};
