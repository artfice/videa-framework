import {BaseDto} from '../BaseDto';

export interface ImageType extends BaseDto {
    /**
     * Name that identifies the current image type. The name must be unique.
     */
    name: string;
    /**
     * Image width in pixels
     */
    width: number;
    /**
     * Image height in pixels
     */
    height: number;
}
