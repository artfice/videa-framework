import {BaseDto} from '../BaseDto';

export interface Image extends BaseDto {
    /**
     * Name of the asset
     */
    name: string;
    /**
     * Name of the asset in the form of /path/assetname.extension
     * The name is unique
     */
    fullName: string;
    /**
     * Name of the associated image type
     */
    imageType: string;
    /**
     * Image width in pixels
     */
    width: number;
    /**
     * Image height in pixels
     */
    height: number;
    /**
     * Direct Url for getting the asset using HTTP
     */
    url: string;
    rawUrl: string;
    /**
     * Secure direct Url for getting the asset using HTTPS
     */
    secure_url: string;

    bytes: number;

    /**
     * Is the url self hosted by a 3rd party?
     */
    externalHost: boolean;
}
