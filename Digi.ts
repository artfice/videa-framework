import * as Promise from 'bluebird';
import check = require('check-types');
import _ = require('lodash');

export module String {
    export function capitalize(s: string) {
        return s.charAt(0).toUpperCase() + s.slice(1);
    }
}

export module Obj {

    export function flattenObject(ob: any): any {
        const toReturn = {};

        for (let i in ob) {
            if (!ob.hasOwnProperty(i)) {
                continue;
            }

            if (ob[i] != null && check.object(ob[i])) {
                const flatObject = Obj.flattenObject(ob[i]);
                for (let x in flatObject) {
                    if (!flatObject.hasOwnProperty(x)) {
                        continue;
                    }
                    toReturn[i + '.' + x] = flatObject[x];
                }
            } else {
                toReturn[i] = ob[i];
            }
        }
        return toReturn;
    }

    export function hasKeys(obj: any) {
        return Object.keys(obj).length > 0;
    }

    export function changeNestedObjectValue(properties: string[], value: any, original: any) {
        if (!properties || properties.length === 0) {
            return;
        }

        for (let i = 1; i < properties.length; i++) {
            const property = properties[i - 1];

            if (!original[property]) {
                original[property] = {};
            }
            original = original[property];
        }
        original[properties[properties.length - 1]] = value;
    }

    export function getNestedObjectValue(properties: string[], original: any): any {
        if (!properties || properties.length === 0) {
            return undefined;
        }

        for (let i = 1; i < properties.length; i++) {
            const property = properties[i - 1];

            if (!original[property]) {
                return undefined;
            }
            original = original[property];
        }
        return original[properties[properties.length - 1]];
    }
    
    export function deepOmit(obj, keysToOmit) : any {
        const keysToOmitIndex =  _.keyBy(Array.isArray(keysToOmit) ? keysToOmit : [keysToOmit] ); // create an index object of the keys that should be omitted

        return _.transform(obj, function(result, value, key) { // transform to a new object
            if (key in keysToOmitIndex) { // if the key is in the index skip it
                return;
            }

            result[key] = _.isObject(value) ? deepOmit(value, keysToOmit) : value; // if the key is an object run it through the inner function - omitFromObject
        });
    }    
}

export module Function {
    /**
     * This function lets execute functions with different scopes
     * @param fn the function you want to execute
     * @param args are the arguments you past into the function
     * @param  scope is the scope this function is running from
     */
    export function pass(fn: any, args: any[], scope: any) {
        return function () {
            const fnArgs = args.slice();
            fnArgs.push.apply(fnArgs, arguments);
            return fn.apply(scope || this, fnArgs);
        };
    }

}

export module List {
    export function exist(list: any[], needle: any) {
        return list.indexOf(needle) > -1;
    }
}

export module Promises {
    export function promiseWhile(condition: any, action: any) {
        return new Promise<void>((resolve, reject) => {
            const loop = (): Promise<void> => {
                if (!condition()) {
                    resolve(undefined);
                    return;
                }
                return Promise.cast(action())
                    .then(loop)
                    .catch(reject);
            };

            process.nextTick(loop);
        });
    }
}
