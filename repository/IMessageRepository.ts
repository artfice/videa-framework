import * as Promise from 'bluebird';
import {Message} from '../domain/message/Message';

export interface IMessageRepository {
    sendMessage(message: Message): Promise<Message>;
    pullMessage(): Promise<Message>;
}
