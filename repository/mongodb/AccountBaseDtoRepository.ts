import Promise = require('bluebird');

import { EventService } from '../../EventService';
import { DtoMongoDbAdapter, DtoMongoDbAdapterConfig } from '../../adapter/db/mongodb';
import { BaseDto } from '../../domain';
import { Logger } from '../../Logger';
import { ResultSet, SearchOptions, SearchParams, UpdateResult, UpdateOptions, AggregationParams, SearchQuery } from '../../domain/db';
import { IAccountBasedRepository } from '../IAccountBasedRepository';

import { InvalidArgumentError, VideaError } from '../../VideaError';

export class AccountBaseDtoRepository<T extends BaseDto> extends DtoMongoDbAdapter<T> implements IAccountBasedRepository<T> {

    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
    }

    public add(accountId: string, dto: T): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }

            // check if the account id is valid
            return this.checkAccountId(accountId).then(
                (result) => {
                    return this.addDtoToCollection(
                        dto,
                        this.getAccountDBName(accountId),
                        this._collectionName,
                        { accountId: accountId }
                    ).then(resolve).catch(
                        (err) => {
                            return reject(this.getAccountError(accountId, err));
                        });
                }).catch((err) => {
                    return reject(this.getAccountError(accountId, err));
                });
        });
    }

    public bulkAdd(accountId: string, dtos: T[], versionId?: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }
            // check if the account id is valid
            return this.checkAccountId(accountId).then(
                (result) => {
                    return this.bulkAddDtoFromCollection(dtos, this.getAccountDBName(accountId),
                        this._collectionName).then((newDtos) => {
                            return resolve(newDtos);
                        }).catch((err) => {
                            return reject(this.getAccountError(accountId, err));
                        });
                }).catch((err) => {
                    return reject(this.getAccountError(accountId, err));
                });
        });
    }

    public update(accountId: string, compoundId: any, dto: T, options?: UpdateOptions, query?: SearchQuery): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }

            return this.updateDtoOnCollection(compoundId, dto, this.getAccountDBName(accountId), this._collectionName,
                options, query).then(resolve).catch((err) => {
                    return reject(this.getAccountError(accountId, err));
                });
        });
    }

    public replace(accountId: string, compoundId: any, dto: T): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }
            return this.replaceDtoOnCollection(compoundId, dto, this.getAccountDBName(accountId),
                this._collectionName).then(resolve).catch((err) => {
                    return reject(this.getAccountError(accountId, err));
                });
        });
    }

    public remove(accountId: string, compoundId: any, query?: SearchQuery): Promise<void> {
        const self = this;
        return new Promise<void>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }

            EventService.emit('videa-dto-pre-remove-' + this._dtoName,
                {
                    id: compoundId,
                    accountId: accountId
                },
                // TODO: https://digiflare.atlassian.net/browse/VH-1063
                (err: any) => {
                    if (err) {
                        return reject(self.getAccountError(accountId, err));
                    }

                    // TODO: https://digiflare.atlassian.net/browse/VH-1064 Discuss with Giulherme
                    self.removeDtoFromCollection(compoundId, self.getAccountDBName(accountId),
                        self._collectionName, { accountId: accountId }, this.buildQuery(query)).then(() => {
                            return resolve(undefined);
                        }).catch((err) => {
                            return reject(self.getAccountError(accountId, err));
                        });
                });
        });
    }

    public getById(accountId: string, compoundId: any, options?: SearchOptions): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }
            return this.getDtoByIdFromCollection(compoundId, options, this.getAccountDBName(accountId), this._collectionName)
                .then(resolve)
                .catch((err) => {
                    return reject(this.getAccountError(accountId, err));
                });
        });
    }

    public batchGet(accountId: string, ids: string[]): Promise<T[]> {
        return new Promise<T[]>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }
            return this.batchGetDtoFromCollection(ids, this.getAccountDBName(accountId), this._collectionName)
                .then(resolve)
                .catch((err) => {
                    return reject(this.getAccountError(accountId, err));
                });
        });
    }

    public search(accountId: string, options: SearchParams, convertFunc?: (T) => T): Promise<ResultSet> {
        return new Promise<ResultSet>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }

            return this.searchDtoOnCollection(options, this.getAccountDBName(accountId), this._collectionName, convertFunc)
                .then(resolve)
                .catch((err) => {
                    return reject(this.getAccountError(accountId, err));
                });
        });
    }

    public resetData(accountId: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            if (!accountId) {
                return reject(new InvalidArgumentError('accountId must be valid to search DTOs of type ' + this._dtoName));
            }
            return this.dropCollection(this.getAccountDBName(accountId), this._collectionName).then(() => {
                return resolve(undefined);
            }).catch((err) => {
                // ignore the case when we already don't have a collection
                if (err && err.name === 'InternalServerError' && err.message.endsWith('ns not found')) {
                    return resolve(undefined);
                }
                return reject(this.getAccountError(accountId, err));
            });
        });
    }

    public aggregate(accountId: string, options: AggregationParams, convertFunc?: (T) => T): Promise<ResultSet> {
        return new Promise<ResultSet>((resolve, reject) => {
            return this.aggregateDtoOnCollection(options, this.getAccountDBName(accountId), undefined, convertFunc)
                       .then(resolve)
                       .catch((err) => {
                           return reject(this.getAccountError(accountId, err));
                       });
        });
    }

    public count(accountId: string, options?: SearchParams): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            return this.countDtoInCollection(options, this.getAccountDBName(accountId))
                .then(resolve)
                .catch((err) => {
                    return reject(this.getAccountError(accountId, err));
                });
        });
    }

    protected getAccountDBName(accountId: string): string {

        return this.getDBName(accountId);
    }

    protected checkAccountId(accountId: string) {
        // TODO: https://digiflare.atlassian.net/browse/VAS-1153
        return Promise.resolve(true);
    }

    protected getDBName(accountId: string) {
        if (!accountId) {
            Logger.error('getDBName: ACCOUNT ID IS NULL');
        }

        return accountId.replace(/\s+/g, '').toLowerCase();
    }

    private getAccountError(accountId: string, err: VideaError): VideaError {
        err.message = 'Error on account ' + accountId + ' caused by: ' + err.message;
        return err;
    }
}
