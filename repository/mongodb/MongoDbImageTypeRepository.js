"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AccountBaseDtoRepository_1 = require("./AccountBaseDtoRepository");
var MongoDbImageTypeRepository = (function (_super) {
    __extends(MongoDbImageTypeRepository, _super);
    function MongoDbImageTypeRepository(config) {
        return _super.call(this, config) || this;
    }
    MongoDbImageTypeRepository.prototype.generateIdForDto = function (dto) {
        return dto.name.toLowerCase().replace(' ', '-');
    };
    return MongoDbImageTypeRepository;
}(AccountBaseDtoRepository_1.AccountBaseDtoRepository));
exports.MongoDbImageTypeRepository = MongoDbImageTypeRepository;
