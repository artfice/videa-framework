import Promise = require('bluebird');

import { DtoMongoDbAdapter, DtoMongoDbAdapterConfig } from '../../adapter/db/mongodb';
import { BaseDto } from '../../domain';
import { ResultSet, SearchOptions, SearchParams, UpdateOptions, UpdateResult } from '../../domain/db';

import {AggregationParams} from '../../domain/db/AggregationParams';
import {SearchQuery} from '../../domain/db/SearchQuery';
import { VideaError } from '../../VideaError';

export class BaseDtoRepository<T extends BaseDto> extends DtoMongoDbAdapter<T> {

    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
    }

    public add(dto: T): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            return this.addDtoToCollection(dto).then(resolve).catch((err) => {
                return reject(this.innerGetError(err));
            });
        });
    }

    public bulkAdd(dtos: T[], versionId?: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.bulkAddDtoFromCollection(dtos).then((newDtos) => {
                return resolve(newDtos);
            }).catch((err) => {
                return reject(this.innerGetError(err));
            });
        });
    }

    public update(compoundId: any, dto: T, options?: UpdateOptions, query?: SearchQuery): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            return this.updateDtoOnCollection(
                compoundId,
                dto,
                undefined,
                undefined,
                options,
                query).then(resolve).catch((err) => {
                return reject(this.innerGetError(err));
            });
        });
    }

    public replace(compoundId: any, dto: T): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            return this.replaceDtoOnCollection(compoundId, dto).then(resolve).catch((err) => {
                return reject(this.innerGetError(err));
            });
        });
    }

    public remove(compoundId: any, query?: SearchQuery): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.removeDtoFromCollection(
                compoundId,
                undefined,
                undefined,
                undefined,
                this.buildQuery(query)).then(resolve).catch((err) => {
                return reject(this.innerGetError(err));
            });
        });
    }

    public getById(compoundId: any, options?: SearchOptions): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            return this.getDtoByIdFromCollection(compoundId, options)
                .then(resolve)
                .catch((err) => {
                    return reject(this.innerGetError(err));
                });
        });
    }

    public getByField(fieldName: string, fieldValue: any, options?: SearchOptions) : Promise<T> {
        return new Promise<T>((resolve, reject) => {
            const info = {};
            info[fieldName] = fieldValue;
            return this.getDtoByInfoFromCollection(info, options)
                .then(resolve)
                .catch((err) => {
                    return reject(this.innerGetError(err));
                });
        });
    }

    public batchGet(ids: string[]): Promise<T[]> {
        return new Promise<T[]>((resolve, reject) => {
            return this.batchGetDtoFromCollection(ids)
                .then(resolve)
                .catch((err) => {
                    return reject(this.innerGetError(err));
                });
        });
    }

    public search(options: SearchParams, convertFunc?: (T) => T): Promise<ResultSet> {
        return new Promise<ResultSet>((resolve, reject) => {
            return this.searchDtoOnCollection(options, undefined, undefined, convertFunc)
                .then(resolve)
                .catch((err) => {
                    return reject(this.innerGetError(err));
                });
        });
    }

    public resetData(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            return this.dropCollection().then(resolve).catch((err) => {
                // ignore the case when we already don't have a collection
                if (err && err.name === 'InternalServerError' && err.message.endsWith('ns not found')) {
                    return resolve(undefined);
                }
                return reject(this.innerGetError(err));
            });
        });
    }

    public aggregate(options: AggregationParams, convertFunc?: (T) => T): Promise<ResultSet> {
        return new Promise<ResultSet>((resolve, reject) => {
            return this.aggregateDtoOnCollection(options, undefined, undefined, convertFunc)
                       .then(resolve)
                       .catch((err) => {
                           return reject(this.innerGetError(err));
                       });
        });
    }

    public count(options?: SearchParams): Promise<number> {
        return this.countDtoInCollection(options).catch((err) => {
            return Promise.reject(this.innerGetError(err));
        });
    }

    private innerGetError(err: VideaError): VideaError {
        err.message = 'Error caused by: ' + err.message;
        return err;
    }
}
