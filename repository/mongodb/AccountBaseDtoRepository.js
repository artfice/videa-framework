"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var EventService_1 = require("../../EventService");
var mongodb_1 = require("../../adapter/db/mongodb");
var Logger_1 = require("../../Logger");
var VideaError_1 = require("../../VideaError");
var AccountBaseDtoRepository = (function (_super) {
    __extends(AccountBaseDtoRepository, _super);
    function AccountBaseDtoRepository(config) {
        return _super.call(this, config) || this;
    }
    AccountBaseDtoRepository.prototype.add = function (accountId, dto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            // check if the account id is valid
            return _this.checkAccountId(accountId).then(function (result) {
                return _this.addDtoToCollection(dto, _this.getAccountDBName(accountId), _this._collectionName, { accountId: accountId }).then(resolve).catch(function (err) {
                    return reject(_this.getAccountError(accountId, err));
                });
            }).catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.bulkAdd = function (accountId, dtos, versionId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            // check if the account id is valid
            return _this.checkAccountId(accountId).then(function (result) {
                return _this.bulkAddDtoFromCollection(dtos, _this.getAccountDBName(accountId), _this._collectionName).then(function (newDtos) {
                    return resolve(newDtos);
                }).catch(function (err) {
                    return reject(_this.getAccountError(accountId, err));
                });
            }).catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.update = function (accountId, compoundId, dto, options, query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            return _this.updateDtoOnCollection(compoundId, dto, _this.getAccountDBName(accountId), _this._collectionName, options, query).then(resolve).catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.replace = function (accountId, compoundId, dto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            return _this.replaceDtoOnCollection(compoundId, dto, _this.getAccountDBName(accountId), _this._collectionName).then(resolve).catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.remove = function (accountId, compoundId, query) {
        var _this = this;
        var self = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            EventService_1.EventService.emit('videa-dto-pre-remove-' + _this._dtoName, {
                id: compoundId,
                accountId: accountId
            }, 
            // TODO: https://digiflare.atlassian.net/browse/VH-1063
            function (err) {
                if (err) {
                    return reject(self.getAccountError(accountId, err));
                }
                // TODO: https://digiflare.atlassian.net/browse/VH-1064 Discuss with Giulherme
                self.removeDtoFromCollection(compoundId, self.getAccountDBName(accountId), self._collectionName, { accountId: accountId }, _this.buildQuery(query)).then(function () {
                    return resolve(undefined);
                }).catch(function (err) {
                    return reject(self.getAccountError(accountId, err));
                });
            });
        });
    };
    AccountBaseDtoRepository.prototype.getById = function (accountId, compoundId, options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            return _this.getDtoByIdFromCollection(compoundId, options, _this.getAccountDBName(accountId), _this._collectionName)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.batchGet = function (accountId, ids) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            return _this.batchGetDtoFromCollection(ids, _this.getAccountDBName(accountId), _this._collectionName)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.search = function (accountId, options, convertFunc) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            return _this.searchDtoOnCollection(options, _this.getAccountDBName(accountId), _this._collectionName, convertFunc)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.resetData = function (accountId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!accountId) {
                return reject(new VideaError_1.InvalidArgumentError('accountId must be valid to search DTOs of type ' + _this._dtoName));
            }
            return _this.dropCollection(_this.getAccountDBName(accountId), _this._collectionName).then(function () {
                return resolve(undefined);
            }).catch(function (err) {
                // ignore the case when we already don't have a collection
                if (err && err.name === 'InternalServerError' && err.message.endsWith('ns not found')) {
                    return resolve(undefined);
                }
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.aggregate = function (accountId, options, convertFunc) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.aggregateDtoOnCollection(options, _this.getAccountDBName(accountId), undefined, convertFunc)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.count = function (accountId, options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.countDtoInCollection(options, _this.getAccountDBName(accountId))
                .then(resolve)
                .catch(function (err) {
                return reject(_this.getAccountError(accountId, err));
            });
        });
    };
    AccountBaseDtoRepository.prototype.getAccountDBName = function (accountId) {
        return this.getDBName(accountId);
    };
    AccountBaseDtoRepository.prototype.checkAccountId = function (accountId) {
        // TODO: https://digiflare.atlassian.net/browse/VAS-1153
        return Promise.resolve(true);
    };
    AccountBaseDtoRepository.prototype.getDBName = function (accountId) {
        if (!accountId) {
            Logger_1.Logger.error('getDBName: ACCOUNT ID IS NULL');
        }
        return accountId.replace(/\s+/g, '').toLowerCase();
    };
    AccountBaseDtoRepository.prototype.getAccountError = function (accountId, err) {
        err.message = 'Error on account ' + accountId + ' caused by: ' + err.message;
        return err;
    };
    return AccountBaseDtoRepository;
}(mongodb_1.DtoMongoDbAdapter));
exports.AccountBaseDtoRepository = AccountBaseDtoRepository;
