"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var mongodb_1 = require("../../adapter/db/mongodb");
var BaseDtoRepository = (function (_super) {
    __extends(BaseDtoRepository, _super);
    function BaseDtoRepository(config) {
        return _super.call(this, config) || this;
    }
    BaseDtoRepository.prototype.add = function (dto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.addDtoToCollection(dto).then(resolve).catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.bulkAdd = function (dtos, versionId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.bulkAddDtoFromCollection(dtos).then(function (newDtos) {
                return resolve(newDtos);
            }).catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.update = function (compoundId, dto, options, query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.updateDtoOnCollection(compoundId, dto, undefined, undefined, options, query).then(resolve).catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.replace = function (compoundId, dto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.replaceDtoOnCollection(compoundId, dto).then(resolve).catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.remove = function (compoundId, query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.removeDtoFromCollection(compoundId, undefined, undefined, undefined, _this.buildQuery(query)).then(resolve).catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.getById = function (compoundId, options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getDtoByIdFromCollection(compoundId, options)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.getByField = function (fieldName, fieldValue, options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var info = {};
            info[fieldName] = fieldValue;
            return _this.getDtoByInfoFromCollection(info, options)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.batchGet = function (ids) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.batchGetDtoFromCollection(ids)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.search = function (options, convertFunc) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.searchDtoOnCollection(options, undefined, undefined, convertFunc)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.resetData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.dropCollection().then(resolve).catch(function (err) {
                // ignore the case when we already don't have a collection
                if (err && err.name === 'InternalServerError' && err.message.endsWith('ns not found')) {
                    return resolve(undefined);
                }
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.aggregate = function (options, convertFunc) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.aggregateDtoOnCollection(options, undefined, undefined, convertFunc)
                .then(resolve)
                .catch(function (err) {
                return reject(_this.innerGetError(err));
            });
        });
    };
    BaseDtoRepository.prototype.count = function (options) {
        var _this = this;
        return this.countDtoInCollection(options).catch(function (err) {
            return Promise.reject(_this.innerGetError(err));
        });
    };
    BaseDtoRepository.prototype.innerGetError = function (err) {
        err.message = 'Error caused by: ' + err.message;
        return err;
    };
    return BaseDtoRepository;
}(mongodb_1.DtoMongoDbAdapter));
exports.BaseDtoRepository = BaseDtoRepository;
