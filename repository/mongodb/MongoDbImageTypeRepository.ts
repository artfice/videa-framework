import {AccountBaseDtoRepository} from './AccountBaseDtoRepository';

import {DtoMongoDbAdapterConfig} from '../../adapter/db/mongodb';
import {ImageType} from '../../domain';

export class MongoDbImageTypeRepository extends AccountBaseDtoRepository<ImageType> {

    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
    }

    protected generateIdForDto(dto: ImageType): string {
        return dto.name.toLowerCase().replace(' ', '-');
    }
}
