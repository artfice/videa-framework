"use strict";
var MockEventRepository = (function () {
    function MockEventRepository(adapter) {
        this._adapter = adapter;
    }
    MockEventRepository.prototype.publishEvent = function (eventFunction, payload, cb) {
        //Do Something
    };
    MockEventRepository.prototype.isValidEvent = function (eventSource) {
        return true;
    };
    MockEventRepository.prototype.retrievePayload = function (payload) {
        return payload;
    };
    return MockEventRepository;
}());
exports.MockEventRepository = MockEventRepository;
