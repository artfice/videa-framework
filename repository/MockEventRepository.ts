import {IEventRepository} from './IEventRepository';

export class MockEventRepository implements IEventRepository {

    private _adapter: any;

    constructor(adapter: any) {
        this._adapter = adapter;
    }

    public publishEvent(eventFunction: string, payload: any, cb: (...args: any[]) => void) {
        //Do Something
    }

    public isValidEvent(eventSource: string): boolean {
        return true;
    }

    public retrievePayload(payload: any): any {
        return payload;
    }
}