import {IEventRepository} from './IEventRepository';

export class EventRepository implements IEventRepository {

    private _adapter: any;

    constructor(adapter: any) {
        this._adapter = adapter;
    }

    public publishEvent(eventFunction: string, payload: any, cb: (...args: any[]) => void) {
        this._adapter.publishEvent(eventFunction, payload, cb);
    }

    public isValidEvent(eventSource: string): boolean {
        return this._adapter.isValidEvent(eventSource);
    }

    public retrievePayload(payload: any): any {
        return this._adapter.retrievePayload(payload);
    }
}