export interface IEventRepository {
    publishEvent(eventFunction: string, payload: any, cb: (...args: any[]) => void);
    retrievePayload(payload: any): any;
    isValidEvent(eventSource: string): boolean;
}