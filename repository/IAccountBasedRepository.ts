import Promise = require('bluebird');

import {BaseDto} from '../domain';
import {AggregationParams} from '../domain/db/AggregationParams';
import { SearchParams } from '../domain/db/SearchParams';
import {IAccountCRUDRepository} from './IAccountCRUDRepository';
import {UpdateResult} from '../domain/db/UpdateResult';
import {ResultSet} from '../domain/db/ResultSet';

export interface IAccountBasedRepository<T extends BaseDto> extends IAccountCRUDRepository<T> {
    replace(accountId: string, id: string, dto: T): Promise<UpdateResult>;
    batchGet(accountId: string, ids: string[]): Promise<T[]>;
    search(accountId: string, options: SearchParams): Promise<ResultSet>;
    bulkAdd(accountId: string, dtos: T[]): Promise<any>;
    resetData(accountId: string): Promise<void>;
    aggregate(accountId: string, options: AggregationParams, convertFunc?: (dto: T) => T): Promise<ResultSet>;
    count(accountId: string, options?: SearchParams): Promise<number>;
}
