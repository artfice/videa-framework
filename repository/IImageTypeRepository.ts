import * as Promise from 'bluebird';

import { ImageType } from '../domain/image/ImageType';
import { IAccountCRUDRepository } from './IAccountCRUDRepository';

export interface IImageTypeRepository extends  IAccountCRUDRepository<ImageType> {
    createTransformation(tenantId: string, imageTypeId: string, imageType: ImageType): Promise<void>;
}
