"use strict";
var Promise = require("bluebird");
var MockImageRepository = (function () {
    function MockImageRepository(imageTypeRepository, adapter) {
    }
    MockImageRepository.prototype.upload = function (accountId, image, stream) {
        return new Promise(function (resolve, reject) {
            resolve({
                id: "zutiijbf86acssh6owpm",
                createdDate: "2015-10-27T21:00:03Z",
                modifiedDate: "2015-10-27T21:00:03Z",
                name: "zutiijbf86acssh6owpm",
                fullName: "zutiijbf86acssh6owpm.png",
                imageType: "40bebec0-9db1-49cc-baf8-eec62e0709f8",
                width: 41,
                height: 70,
                url: "http://res.cloudinary.com/videa-staging/image/upload/v1445979603/zutiijbf86acssh6owpm.png"
            });
        });
    };
    MockImageRepository.prototype.uploadByUrl = function (accountId, image, link) {
        return Promise.resolve({
            id: "zutiijbf86acssh6owpm",
            createdDate: "2015-10-27T21:00:03Z",
            modifiedDate: "2015-10-27T21:00:03Z",
            name: "zutiijbf86acssh6owpm",
            fullName: "zutiijbf86acssh6owpm.png",
            imageType: "40bebec0-9db1-49cc-baf8-eec62e0709f8",
            width: 41,
            height: 70,
            url: "http://res.cloudinary.com/videa-staging/image/upload/v1445979603/zutiijbf86acssh6owpm.png"
        });
    };
    MockImageRepository.prototype.remove = function (accountId, imageId) {
        return Promise.resolve(undefined);
    };
    MockImageRepository.prototype.createExternalHostedImg = function (tenantId, image) {
        throw new Error('Not implemented yet.');
    };
    MockImageRepository.prototype.createTransformation = function (accountIdSimplified, imageTypeId, image) {
        return Promise.resolve(undefined);
    };
    MockImageRepository.prototype.deleteTransformation = function (accountIdSimplified, imageTypeId) {
        return Promise.resolve(undefined);
    };
    MockImageRepository.prototype.getNewUrlWithImageTransformation = function (accountId, img) {
        return '';
    };
    return MockImageRepository;
}());
exports.MockImageRepository = MockImageRepository;
