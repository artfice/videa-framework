import Promise = require('bluebird');

import {BaseDto} from '../domain';
import {SearchQuery, SearchOptions, UpdateOptions, UpdateResult} from '../domain/db';

export interface IAccountCRUDRepository<T extends BaseDto> {
    add(accountId: string, dto: T): Promise<T>;
    update(accountId: string, id: string, dto: T, options?: UpdateOptions, query?: SearchQuery): Promise<UpdateResult>;
    remove(accountId: string, id: string, query?: SearchQuery): Promise<void>;
    getById(accountId: string, id: string, options?: SearchOptions): Promise<T>;
}
