import * as Promise from 'bluebird';
import {RemoteAsset} from '../domain/asset/RemoteAsset';

export interface IRemoteAssetRepository {
    /**
     * Uploads a new asset from the given stream to the remote repository.
     * @param dto should have set the containerId that identifies the container where the asset will be stored
     * (example, an unique container for each account)
     * and the assetId that identifies the asset in the container
     * @param stream
     */
    upload(dto: RemoteAsset, stream: any) : Promise<RemoteAsset>;
    remove(containerKey: string, assetKey: string) : Promise<void>;

    /**
     * Copies an existing object inside the same container
     * @param containerKey
     * @param sourceAssetKey
     * @param targetAssetKey
     */
    copy(sourceContainerKey: string, sourceAssetKey: string, targetContainerKey: string, targetAssetKey: string) : Promise<RemoteAsset>;

    /**
     * Updates the asset url based on the container id and asset id.
     * @param asset
     */
    updateAssetUrl(asset: RemoteAsset) : RemoteAsset;

    get(containerKey: string, assetKey: string) : any;
}
