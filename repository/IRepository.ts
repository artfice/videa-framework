import Promise = require('bluebird');

import {BaseDto} from '../domain';
import {AggregationParams, ResultSet, SearchParams, SearchOptions, UpdateResult, SearchQuery, UpdateOptions} from '../domain/db';

export interface IRepository<T extends BaseDto> {
    add(dto: T): Promise<T>;
    update(compoundId: any, dto: T, options?: UpdateOptions, query?: SearchQuery): Promise<UpdateResult>;
    replace(id: string, dto: T): Promise<UpdateResult>;
    remove(id: string, query?: SearchQuery): Promise<void>;
    getById(id: string): Promise<T>;
    batchGet(uiConfigs: string[]): Promise<T[]>;
    search(options: SearchParams): Promise<ResultSet>;
    bulkAdd(dtos: T[]): Promise<any>;
    resetData(): Promise<void>;
    getByField(fieldName: string, fieldValue: any, options?: SearchOptions) : Promise<T>;
    aggregate(options: AggregationParams, convertFunc?: (dto: T) => T): Promise<ResultSet>;
    count(options?: SearchParams): Promise<number>;
}
