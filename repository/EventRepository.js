"use strict";
var EventRepository = (function () {
    function EventRepository(adapter) {
        this._adapter = adapter;
    }
    EventRepository.prototype.publishEvent = function (eventFunction, payload, cb) {
        this._adapter.publishEvent(eventFunction, payload, cb);
    };
    EventRepository.prototype.isValidEvent = function (eventSource) {
        return this._adapter.isValidEvent(eventSource);
    };
    EventRepository.prototype.retrievePayload = function (payload) {
        return this._adapter.retrievePayload(payload);
    };
    return EventRepository;
}());
exports.EventRepository = EventRepository;
