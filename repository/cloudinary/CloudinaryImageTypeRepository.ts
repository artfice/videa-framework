import * as Promise from 'bluebird';

import { CloudinaryAdapter } from '../../adapter/image/cloudinary/CloudinaryAdapter';
import { CloudinaryAdapterConfig } from '../../adapter/image/cloudinary/config/CloudinaryAdapterConfig';
import { CloudinaryAdapterHelper } from '../../adapter/image/cloudinary/helper/CloudinaryAdapterHelper';
import { SearchOptions } from '../../domain/db/SearchOptions';
import { SearchQuery } from '../../domain/db/SearchQuery';
import { UpdateOptions } from '../../domain/db/UpdateOptions';
import { ImageType } from '../../domain/image/ImageType';
import { IImageTypeRepository } from '../IImageTypeRepository';

import { UpdateResult } from '../../domain/db/UpdateResult';
import { InternalServerError, ItemNotFoundError, NotImplementedError } from '../../VideaError';

import { Logger } from '../../Logger';

export class CloudinaryImageTypeRepository extends CloudinaryAdapter implements IImageTypeRepository {

    constructor(config: CloudinaryAdapterConfig) {
        super(config);
    }

    public add(tenantId: string, imageType: ImageType): Promise<ImageType> {
        return new Promise<ImageType>((resolve, reject) => {
            const imageTypeId = imageType.id;
            let imageTransformName = CloudinaryAdapterHelper.getTransformName(tenantId, imageTypeId);
            this.getClient().api.create_transformation(imageTransformName, {
                with: imageType.width,
                height: imageType.height
            }, (result) => {
                if (result.error) {
                    reject(new InternalServerError('ERROR creating image type' +
                                                   ' with id ' + imageTypeId + ' for tenant ' + tenantId,
                                                   result.error));
                    return;
                }
                resolve(imageType);
            });
        });
    }

    public remove(tenantId: string, imageTypeId: string, query?: SearchQuery): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            let imageTransformName = CloudinaryAdapterHelper.getTransformName(tenantId, imageTypeId);
            this.getClient().api.delete_transformation(
                imageTransformName,
                (result) => {
                    if (result.error) {
                        if (result.error.http_code === 404) {
                            reject(new ItemNotFoundError('Image type with id ' + imageTypeId +
                                                         ' cannot be found for tenant ' + tenantId, result.error.message));
                            return;
                        }
                        reject(new InternalServerError('An error occurred while deleting image type' +
                                                       ' with id ' + imageTypeId +
                                                       ' on tenant ' + tenantId, result.error.message));
                        return;
                    }
                    resolve(undefined);
                });
        });
    }

    public update(tenantId: string, id: string, dto: ImageType, options?: UpdateOptions, query?: SearchQuery): Promise<UpdateResult> {
        throw new NotImplementedError();
    }

    public getById(tenantId: string, id: string, options?: SearchOptions): Promise<ImageType> {
        throw new NotImplementedError();
    }

    public createTransformation(tenantId: string, imageTypeId: string, imageType: ImageType): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            return this.deleteTransformation(tenantId, imageTypeId).then(() => {
                const imageTransformName = this.buildTransformName(tenantId, imageTypeId);
                return this.getClient().api.create_transformation(imageTransformName, {
                    with: imageType.width,
                    height: imageType.height
                }, (result) => {
                    if (result.error) {
                        const message = 'Error occurred while creating transformation for organization ' + tenantId +
                            ' and image type ' + imageTypeId + ', error:' + JSON.stringify(result.error);
                        Logger.error(message);
                        return reject(new InternalServerError(message));
                    }

                    Logger.info('Created image transform ' + imageTransformName);
                    return resolve(undefined);
                });
            }).catch((err) => {
                Logger.error('Error occurred while  deleting transform' + tenantId + ' imageType id: ' + imageTypeId);
                reject(err);
            });
        });    }

    public deleteTransformation(tenantId: string, imageTypeId: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const imageTransformName = this.buildTransformName(tenantId, imageTypeId);
            return this.getClient().api.delete_transformation(
                imageTransformName,
                (result) => {
                    if (result.error) {
                        if (result.error.http_code === 404) {
                            Logger.info('Does not exist image transform ' + imageTransformName);
                            return resolve(undefined);
                        }

                        const message = 'While trying to delete transformation ' + tenantId +
                            ' image type id ' + imageTypeId + JSON.stringify(result.error);
                        Logger.error(message);
                        return reject(new InternalServerError(message));
                    }
                    Logger.info('Deleted image transform ' + imageTransformName);
                    return resolve(undefined);
                });
        });
    }

    private buildTransformName(tenantId: string, imageTypeId: string) : string {
        return this.removeUnwantedCharacters(tenantId) + '_' + imageTypeId;
    }

    private removeUnwantedCharacters(value: string) : string {
        return value.replace(/\s+/g, '').toLowerCase();
    }
}
