import * as Promise from 'bluebird';

import { CloudinaryAdapter } from '../../adapter/image/cloudinary/CloudinaryAdapter';
import { CloudinaryAdapterConfig } from '../../adapter/image/cloudinary/config/CloudinaryAdapterConfig';
import { CloudinaryAdapterHelper } from '../../adapter/image/cloudinary/helper/CloudinaryAdapterHelper';
import { Image } from '../../domain/image/Image';
import { IImageRepository } from '../IImageRepository';

import { Logger } from '../../Logger';
import { InternalServerError, InvalidArgumentError, ItemNotFoundError } from '../../VideaError';

export class CloudinaryImageRepository extends CloudinaryAdapter implements IImageRepository {

    constructor(config: CloudinaryAdapterConfig) {
        super(config);
    }

    public createExternalHostedImg(tenantId: string, image: Image): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            if (!image.url || !image.rawUrl) {
                return reject(new InvalidArgumentError('URL and RawURL must be specified'));
            }
            image.id = image.id || this.generateIdForDto(image);
            if (!image.id) {
                return reject(
                    new InternalServerError('Failed to generate a new ID while creating an external hosted image on tenant ' + tenantId));
            }

            image.id = image.id || this.generateIdForDto(image);
            image.createdDate = new Date();
            image.modifiedDate = image.createdDate;
            image.externalHost = true;
            return resolve(image);
        });
    }

    public upload(accountId: string, image: Image, stream: any): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            const options = <any>{
                eager: [{
                    transformation: CloudinaryAdapterHelper.getTransformName(accountId, image.imageType)
                },
                    {
                        allowed_for_strict: false
                    }]
            };

            options.public_id = image.id;
            options.invalidate = true;
            options.overwrite = true;

            const couldinaryStream = this.getClient().uploader.upload_stream(
                (result) => {
                    Logger.log('Cloudinary upload stream', JSON.stringify(result));
                    if (result.error) {
                        throw new InternalServerError('Error uploading image to repository', result.error.message);
                    }

                    const img = <Image>{
                        id: result.public_id,
                        createdDate: result.created_at,
                        modifiedDate: result.created_at,
                        name: (image.name) ? image.name : result.public_id,
                        fullName: result.public_id + '.' + result.format,
                        imageType: image.imageType,
                        width: result.width,
                        height: result.height,
                        rawUrl: result.url,
                        url: result.eager[0].url, // to fetch the transformed url
                        bytes: result.bytes,
                        externalHost: false
                    };

                    return resolve(<any>img);
                },
                options);
            stream.pipe(couldinaryStream);
        });
    }

    public uploadByUrl(accountId: string, image: Image, link: string): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            let options = <any>{
                eager: [{
                    transformation: CloudinaryAdapterHelper.getTransformName(accountId, image.imageType)
                },
                    {
                        allowed_for_strict: false
                    }]
            };

            options.public_id = image.id;
            options.invalidate = true;
            options.overwrite = true;

            this.getClient().uploader.upload(link, (result) => {
                Logger.log('Cloudinary upload link ', JSON.stringify(result));
                if (result.error) {
                    return reject(new InternalServerError('Error uploading image to repository'));
                }
                const img = <Image>{
                    id: result.public_id,
                    createdDate: result.created_at,
                    modifiedDate: result.created_at,
                    name: (image.name) ? image.name : result.public_id,
                    fullName: result.public_id + '.' + result.format,
                    imageType: image.imageType,
                    width: result.width,
                    height: result.height,
                    rawUrl: result.url,
                    url: result.eager[0].url, // to fetch the transformed url
                    bytes: result.bytes,
                    externalHost: false
                };
                return resolve(<any>img);
            }, options);
        });
    }

    public remove(tenantId: string, imageId: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.getClient().uploader.destroy(imageId, (result) => {
                if (result.error) {
                    throw new InternalServerError('Error uploading image to repository');
                } else if (result.result === 'not found') {
                    throw new ItemNotFoundError('Not found image with id ' + imageId);
                }
                resolve(undefined);
            });
        });
    }
}
