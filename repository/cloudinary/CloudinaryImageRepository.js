"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var CloudinaryAdapter_1 = require("../../adapter/image/cloudinary/CloudinaryAdapter");
var CloudinaryAdapterHelper_1 = require("../../adapter/image/cloudinary/helper/CloudinaryAdapterHelper");
var Logger_1 = require("../../Logger");
var VideaError_1 = require("../../VideaError");
var CloudinaryImageRepository = (function (_super) {
    __extends(CloudinaryImageRepository, _super);
    function CloudinaryImageRepository(config) {
        return _super.call(this, config) || this;
    }
    CloudinaryImageRepository.prototype.createExternalHostedImg = function (tenantId, image) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!image.url || !image.rawUrl) {
                return reject(new VideaError_1.InvalidArgumentError('URL and RawURL must be specified'));
            }
            image.id = image.id || _this.generateIdForDto(image);
            if (!image.id) {
                return reject(new VideaError_1.InternalServerError('Failed to generate a new ID while creating an external hosted image on tenant ' + tenantId));
            }
            image.id = image.id || _this.generateIdForDto(image);
            image.createdDate = new Date();
            image.modifiedDate = image.createdDate;
            image.externalHost = true;
            return resolve(image);
        });
    };
    CloudinaryImageRepository.prototype.upload = function (accountId, image, stream) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var options = {
                eager: [{
                        transformation: CloudinaryAdapterHelper_1.CloudinaryAdapterHelper.getTransformName(accountId, image.imageType)
                    },
                    {
                        allowed_for_strict: false
                    }]
            };
            options.public_id = image.id;
            options.invalidate = true;
            options.overwrite = true;
            var couldinaryStream = _this.getClient().uploader.upload_stream(function (result) {
                Logger_1.Logger.log('Cloudinary upload stream', JSON.stringify(result));
                if (result.error) {
                    throw new VideaError_1.InternalServerError('Error uploading image to repository', result.error.message);
                }
                var img = {
                    id: result.public_id,
                    createdDate: result.created_at,
                    modifiedDate: result.created_at,
                    name: (image.name) ? image.name : result.public_id,
                    fullName: result.public_id + '.' + result.format,
                    imageType: image.imageType,
                    width: result.width,
                    height: result.height,
                    rawUrl: result.url,
                    url: result.eager[0].url,
                    bytes: result.bytes,
                    externalHost: false
                };
                return resolve(img);
            }, options);
            stream.pipe(couldinaryStream);
        });
    };
    CloudinaryImageRepository.prototype.uploadByUrl = function (accountId, image, link) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var options = {
                eager: [{
                        transformation: CloudinaryAdapterHelper_1.CloudinaryAdapterHelper.getTransformName(accountId, image.imageType)
                    },
                    {
                        allowed_for_strict: false
                    }]
            };
            options.public_id = image.id;
            options.invalidate = true;
            options.overwrite = true;
            _this.getClient().uploader.upload(link, function (result) {
                Logger_1.Logger.log('Cloudinary upload link ', JSON.stringify(result));
                if (result.error) {
                    return reject(new VideaError_1.InternalServerError('Error uploading image to repository'));
                }
                var img = {
                    id: result.public_id,
                    createdDate: result.created_at,
                    modifiedDate: result.created_at,
                    name: (image.name) ? image.name : result.public_id,
                    fullName: result.public_id + '.' + result.format,
                    imageType: image.imageType,
                    width: result.width,
                    height: result.height,
                    rawUrl: result.url,
                    url: result.eager[0].url,
                    bytes: result.bytes,
                    externalHost: false
                };
                return resolve(img);
            }, options);
        });
    };
    CloudinaryImageRepository.prototype.remove = function (tenantId, imageId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getClient().uploader.destroy(imageId, function (result) {
                if (result.error) {
                    throw new VideaError_1.InternalServerError('Error uploading image to repository');
                }
                else if (result.result === 'not found') {
                    throw new VideaError_1.ItemNotFoundError('Not found image with id ' + imageId);
                }
                resolve(undefined);
            });
        });
    };
    return CloudinaryImageRepository;
}(CloudinaryAdapter_1.CloudinaryAdapter));
exports.CloudinaryImageRepository = CloudinaryImageRepository;
