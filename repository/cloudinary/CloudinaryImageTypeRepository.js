"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var CloudinaryAdapter_1 = require("../../adapter/image/cloudinary/CloudinaryAdapter");
var CloudinaryAdapterHelper_1 = require("../../adapter/image/cloudinary/helper/CloudinaryAdapterHelper");
var VideaError_1 = require("../../VideaError");
var Logger_1 = require("../../Logger");
var CloudinaryImageTypeRepository = (function (_super) {
    __extends(CloudinaryImageTypeRepository, _super);
    function CloudinaryImageTypeRepository(config) {
        return _super.call(this, config) || this;
    }
    CloudinaryImageTypeRepository.prototype.add = function (tenantId, imageType) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var imageTypeId = imageType.id;
            var imageTransformName = CloudinaryAdapterHelper_1.CloudinaryAdapterHelper.getTransformName(tenantId, imageTypeId);
            _this.getClient().api.create_transformation(imageTransformName, {
                with: imageType.width,
                height: imageType.height
            }, function (result) {
                if (result.error) {
                    reject(new VideaError_1.InternalServerError('ERROR creating image type' +
                        ' with id ' + imageTypeId + ' for tenant ' + tenantId, result.error));
                    return;
                }
                resolve(imageType);
            });
        });
    };
    CloudinaryImageTypeRepository.prototype.remove = function (tenantId, imageTypeId, query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var imageTransformName = CloudinaryAdapterHelper_1.CloudinaryAdapterHelper.getTransformName(tenantId, imageTypeId);
            _this.getClient().api.delete_transformation(imageTransformName, function (result) {
                if (result.error) {
                    if (result.error.http_code === 404) {
                        reject(new VideaError_1.ItemNotFoundError('Image type with id ' + imageTypeId +
                            ' cannot be found for tenant ' + tenantId, result.error.message));
                        return;
                    }
                    reject(new VideaError_1.InternalServerError('An error occurred while deleting image type' +
                        ' with id ' + imageTypeId +
                        ' on tenant ' + tenantId, result.error.message));
                    return;
                }
                resolve(undefined);
            });
        });
    };
    CloudinaryImageTypeRepository.prototype.update = function (tenantId, id, dto, options, query) {
        throw new VideaError_1.NotImplementedError();
    };
    CloudinaryImageTypeRepository.prototype.getById = function (tenantId, id, options) {
        throw new VideaError_1.NotImplementedError();
    };
    CloudinaryImageTypeRepository.prototype.createTransformation = function (tenantId, imageTypeId, imageType) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.deleteTransformation(tenantId, imageTypeId).then(function () {
                var imageTransformName = _this.buildTransformName(tenantId, imageTypeId);
                return _this.getClient().api.create_transformation(imageTransformName, {
                    with: imageType.width,
                    height: imageType.height
                }, function (result) {
                    if (result.error) {
                        var message = 'Error occurred while creating transformation for organization ' + tenantId +
                            ' and image type ' + imageTypeId + ', error:' + JSON.stringify(result.error);
                        Logger_1.Logger.error(message);
                        return reject(new VideaError_1.InternalServerError(message));
                    }
                    Logger_1.Logger.info('Created image transform ' + imageTransformName);
                    return resolve(undefined);
                });
            }).catch(function (err) {
                Logger_1.Logger.error('Error occurred while  deleting transform' + tenantId + ' imageType id: ' + imageTypeId);
                reject(err);
            });
        });
    };
    CloudinaryImageTypeRepository.prototype.deleteTransformation = function (tenantId, imageTypeId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var imageTransformName = _this.buildTransformName(tenantId, imageTypeId);
            return _this.getClient().api.delete_transformation(imageTransformName, function (result) {
                if (result.error) {
                    if (result.error.http_code === 404) {
                        Logger_1.Logger.info('Does not exist image transform ' + imageTransformName);
                        return resolve(undefined);
                    }
                    var message = 'While trying to delete transformation ' + tenantId +
                        ' image type id ' + imageTypeId + JSON.stringify(result.error);
                    Logger_1.Logger.error(message);
                    return reject(new VideaError_1.InternalServerError(message));
                }
                Logger_1.Logger.info('Deleted image transform ' + imageTransformName);
                return resolve(undefined);
            });
        });
    };
    CloudinaryImageTypeRepository.prototype.buildTransformName = function (tenantId, imageTypeId) {
        return this.removeUnwantedCharacters(tenantId) + '_' + imageTypeId;
    };
    CloudinaryImageTypeRepository.prototype.removeUnwantedCharacters = function (value) {
        return value.replace(/\s+/g, '').toLowerCase();
    };
    return CloudinaryImageTypeRepository;
}(CloudinaryAdapter_1.CloudinaryAdapter));
exports.CloudinaryImageTypeRepository = CloudinaryImageTypeRepository;
