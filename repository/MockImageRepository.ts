import * as Promise from 'bluebird';

import {Image} from './../domain/image/Image';

import {IImageRepository} from './IImageRepository';
import {MongoDbImageTypeRepository} from './mongodb/MongoDbImageTypeRepository';

export class MockImageRepository implements IImageRepository {
    protected _imageTypeRepository: MongoDbImageTypeRepository;
    protected _adapter: any;

    constructor(imageTypeRepository: MongoDbImageTypeRepository, adapter: any) {
    }

    upload(accountId: string, image: Image, stream: any): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            resolve(<any>{
                id: "zutiijbf86acssh6owpm",
                createdDate: "2015-10-27T21:00:03Z",
                modifiedDate: "2015-10-27T21:00:03Z",
                name: "zutiijbf86acssh6owpm",
                fullName: "zutiijbf86acssh6owpm.png",
                imageType: "40bebec0-9db1-49cc-baf8-eec62e0709f8",
                width: 41,
                height: 70,
                url: "http://res.cloudinary.com/videa-staging/image/upload/v1445979603/zutiijbf86acssh6owpm.png"
            });
        });
    }

    uploadByUrl(accountId: string, image: Image, link: string): Promise<Image> {
        return Promise.resolve(<any>{
            id: "zutiijbf86acssh6owpm",
            createdDate: "2015-10-27T21:00:03Z",
            modifiedDate: "2015-10-27T21:00:03Z",
            name: "zutiijbf86acssh6owpm",
            fullName: "zutiijbf86acssh6owpm.png",
            imageType: "40bebec0-9db1-49cc-baf8-eec62e0709f8",
            width: 41,
            height: 70,
            url: "http://res.cloudinary.com/videa-staging/image/upload/v1445979603/zutiijbf86acssh6owpm.png"
        });
    }

    remove(accountId: string, imageId: string): Promise<void> {
        return Promise.resolve(undefined);
    }

    createExternalHostedImg(tenantId: string, image: Image): Promise<Image> {
        throw new Error('Not implemented yet.');
    }

    createTransformation(accountIdSimplified: string, imageTypeId: string, image: Image): Promise<void> {
        return Promise.resolve(undefined);
    }
    deleteTransformation(accountIdSimplified: string, imageTypeId: string): Promise<void> {
        return Promise.resolve(undefined);
    }

    getNewUrlWithImageTransformation(accountId: string, img: Image): string {
        return '';
    }
}
