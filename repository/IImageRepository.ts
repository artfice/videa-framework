import * as Promise from 'bluebird';
import {Image} from '../domain/image/Image';

export interface IImageRepository {
    createExternalHostedImg(tenantId: string, image: Image): Promise<Image>;
    upload(accountId: string, image: Image, stream: any): Promise<Image>;
    uploadByUrl(accountId: string, image: Image, link: string): Promise<Image>;
    remove(accountId: string, imageId: string): Promise<void>;
}
