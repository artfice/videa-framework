"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var _ = require("lodash");
var aws_1 = require("../../adapter/aws");
var VideaError_1 = require("../../VideaError");
var AwsS3RemoteAssetRepository = (function (_super) {
    __extends(AwsS3RemoteAssetRepository, _super);
    function AwsS3RemoteAssetRepository(config) {
        return _super.call(this, config) || this;
    }
    AwsS3RemoteAssetRepository.prototype.upload = function (dto, stream) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var containerKey = dto.containerId;
            var assetKey = dto.assetId;
            if (!containerKey) {
                return reject(new VideaError_1.InvalidArgumentError('Container key is required.'));
            }
            if (!assetKey) {
                return reject(new VideaError_1.InvalidArgumentError('Asset key is required.'));
            }
            var contentType = dto.contentType || 'application/octet-stream';
            var upload = _this.getUploader().upload({
                Bucket: _this.getBucket(),
                Key: _this.getConfigKey(containerKey, assetKey),
                ContentType: contentType
            });
            // Optional configuration
            upload.maxPartSize(20971520); // 20 MB
            upload.concurrentParts(5);
            // Handle errors.
            upload.on('error', function (error) {
                console.error(JSON.stringify(error));
                return reject(error);
            });
            /* Handle progress. Example details object:
             { ETag: ''f9ef956c83756a80ad62f54ae5e7d34b'',
             PartNumber: 5,
             receivedSize: 29671068,
             uploadedSize: 29671068 }
             */
            upload.on('part', function (details) {
                // do nothing
            });
            /* Handle upload completion. Example details object:
             { Location: 'https://bucketName.s3.amazonaws.com/filename.ext',
             Bucket: 'bucketName',
             Key: 'filename.ext',
             ETag: ''bf2acbedf84207d696c8da7dbb205b9f-5'' }
             */
            upload.on('uploaded', function (details) {
                dto.url = details.Location;
                resolve(dto);
            });
            stream.pipe(upload);
        });
    };
    AwsS3RemoteAssetRepository.prototype.remove = function (containerKey, assetKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var params = {
                Bucket: _this.getBucket(),
                Key: _this.getConfigKey(containerKey, assetKey)
            };
            _this.getClient().deleteObject(params, function (err, data) {
                if (err) {
                    // an error occurred
                    reject(new VideaError_1.InternalServerError('Error deleting asset "' + params.Key + '"', err.name + ': ' + err.message));
                }
                else {
                    resolve(undefined);
                }
            });
        });
    };
    AwsS3RemoteAssetRepository.prototype.copy = function (sourceContainerKey, sourceAssetKey, targetContainerKey, targetAssetKey) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var params = {
                Bucket: _this.getBucket(),
                CopySource: _this.getBucket() + '/' + _this.getConfigKey(sourceContainerKey, sourceAssetKey),
                Key: _this.getConfigKey(targetContainerKey, targetAssetKey)
            };
            _this.getClient().copyObject(params, function (err, data) {
                if (err) {
                    // an error occurred
                    reject(new VideaError_1.InternalServerError('Error copying source asset "' +
                        _this.getConfigKey(sourceContainerKey, sourceAssetKey) + '" ' +
                        'to target asset "' +
                        _this.getConfigKey(targetContainerKey, targetAssetKey) + '"', err.name + ': ' + err.message));
                }
                else {
                    resolve({
                        containerId: targetContainerKey,
                        assetId: targetAssetKey
                    });
                }
            });
        });
    };
    AwsS3RemoteAssetRepository.prototype.updateAssetUrl = function (asset) {
        asset.url = this.getClient().endpoint.host +
            '/' + this.getBucket() + '/' + this.getConfigKey(asset.containerId, asset.assetId);
        return asset;
    };
    AwsS3RemoteAssetRepository.prototype.get = function (containerKey, assetKey) {
        var options = {
            Bucket: this.getBucket(),
            Key: this.getConfigKey(containerKey, assetKey)
        };
        return this.getClient().getObject(options).createReadStream();
    };
    AwsS3RemoteAssetRepository.prototype.getConfigKey = function (containerKey, assetKey) {
        return _.isEmpty(containerKey) ? assetKey : (containerKey + '/' + assetKey);
    };
    return AwsS3RemoteAssetRepository;
}(aws_1.AwsS3Adapter));
exports.AwsS3RemoteAssetRepository = AwsS3RemoteAssetRepository;
