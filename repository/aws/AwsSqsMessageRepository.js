"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var _ = require("lodash");
var aws_1 = require("../../adapter/aws");
var VideaError_1 = require("../../VideaError");
var MessageHelper_1 = require("./helper/MessageHelper");
var AwsSqsMessageRepository = (function (_super) {
    __extends(AwsSqsMessageRepository, _super);
    function AwsSqsMessageRepository(config) {
        return _super.call(this, config) || this;
    }
    AwsSqsMessageRepository.prototype.sendMessage = function (message) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var params = MessageHelper_1.MessageHelper.createSqsMessage(message);
            params.QueueUrl = _this.getQueueUrl();
            _this.getClient().sendMessage(params, function (err, data) {
                if (err) {
                    reject(new VideaError_1.InternalServerError('Error sending message to ' + _this.getQueueUrl(), err));
                }
                else {
                    message.id = data.MessageId;
                    message.createdDate = new Date();
                    message.modifiedDate = message.createdDate;
                    resolve(message);
                }
            });
        });
    };
    AwsSqsMessageRepository.prototype.pullMessage = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var params = {
                AttributeNames: [
                    'SentTimestamp'
                ],
                MaxNumberOfMessages: 1,
                MessageAttributeNames: [
                    'All'
                ],
                QueueUrl: _this.getQueueUrl(),
                VisibilityTimeout: 0,
                WaitTimeSeconds: 0
            };
            _this.getClient().receiveMessage(params, function (err, data) {
                if (err) {
                    reject(new VideaError_1.InternalServerError('Error receiving message from ' + _this.getQueueUrl(), err));
                }
                else {
                    if (_.isUndefined(data.Messages) || data.Messages.length === 0) {
                        resolve(undefined);
                        return;
                    }
                    var awsMessage = data.Messages[0];
                    var message_1 = MessageHelper_1.MessageHelper.parseSqsMessage(awsMessage);
                    var deleteParams = {
                        QueueUrl: _this.getQueueUrl(),
                        ReceiptHandle: awsMessage.ReceiptHandle
                    };
                    _this.getClient().deleteMessage(deleteParams, function (err, data) {
                        if (err) {
                            reject(new VideaError_1.InternalServerError('Error deleting message from ' + _this.getQueueUrl(), err));
                        }
                        else {
                            resolve(message_1);
                        }
                    });
                }
            });
        });
    };
    return AwsSqsMessageRepository;
}(aws_1.AwsSqsAdapter));
exports.AwsSqsMessageRepository = AwsSqsMessageRepository;
