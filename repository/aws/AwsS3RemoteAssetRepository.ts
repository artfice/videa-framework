import * as Promise from 'bluebird';
import * as _ from 'lodash';
import {AwsS3Adapter, AwsS3AdapterConfig} from '../../adapter/aws';
import {RemoteAsset} from '../../domain/asset/RemoteAsset';
import {InternalServerError, InvalidArgumentError} from '../../VideaError';
import {IRemoteAssetRepository} from '../IRemoteAssetRepository';

export class AwsS3RemoteAssetRepository extends AwsS3Adapter implements IRemoteAssetRepository {

    constructor(config: AwsS3AdapterConfig) {
        super(config);
    }

    public upload(dto: RemoteAsset, stream: any) : Promise<RemoteAsset> {
        return new Promise<RemoteAsset>((resolve, reject) => {

            const containerKey = dto.containerId;
            const assetKey = dto.assetId;

            if (!containerKey) {
                return reject(new InvalidArgumentError('Container key is required.'));
            }
            if (!assetKey) {
                return reject(new InvalidArgumentError('Asset key is required.'));
            }

            const contentType = dto.contentType || 'application/octet-stream';

            const upload = this.getUploader().upload({
                Bucket: this.getBucket(),
                Key: this.getConfigKey(containerKey, assetKey),
                ContentType: contentType
            });

            // Optional configuration
            upload.maxPartSize(20971520); // 20 MB
            upload.concurrentParts(5);

            // Handle errors.
            upload.on('error', (error) => {
                console.error(JSON.stringify(error));
                return reject(error);
            });

            /* Handle progress. Example details object:
             { ETag: ''f9ef956c83756a80ad62f54ae5e7d34b'',
             PartNumber: 5,
             receivedSize: 29671068,
             uploadedSize: 29671068 }
             */
            upload.on('part', (details) => {
                // do nothing
            });

            /* Handle upload completion. Example details object:
             { Location: 'https://bucketName.s3.amazonaws.com/filename.ext',
             Bucket: 'bucketName',
             Key: 'filename.ext',
             ETag: ''bf2acbedf84207d696c8da7dbb205b9f-5'' }
             */
            upload.on('uploaded', (details) => {
                dto.url = details.Location;
                resolve(<any>dto);
            });

            stream.pipe(upload);
        });
    }

    public remove(containerKey: string, assetKey: string) : Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const params = {
                Bucket: this.getBucket(),
                Key: this.getConfigKey(containerKey, assetKey)
            };
            this.getClient().deleteObject(params, (err, data) => {
                if (err) {
                    // an error occurred
                    reject(new InternalServerError('Error deleting asset "' + params.Key + '"',
                        err.name + ': ' + err.message));
                } else {
                    resolve(undefined);
                }
            });
        });
    }

    public copy(sourceContainerKey: string, sourceAssetKey: string, targetContainerKey: string, targetAssetKey: string):
    Promise<RemoteAsset> {
        return new Promise<RemoteAsset>((resolve, reject) => {

            const params = {
                Bucket: this.getBucket(),
                CopySource: this.getBucket() + '/' + this.getConfigKey(sourceContainerKey, sourceAssetKey),
                Key: this.getConfigKey(targetContainerKey, targetAssetKey)
            };

            this.getClient().copyObject(params, (err, data) => {
                if (err) {
                    // an error occurred
                    reject(new InternalServerError('Error copying source asset "' +
                        this.getConfigKey(sourceContainerKey, sourceAssetKey) + '" ' +
                        'to target asset "' +
                        this.getConfigKey(targetContainerKey, targetAssetKey) + '"',
                        err.name + ': ' + err.message));
                } else {
                    resolve(<any><RemoteAsset>{
                        containerId: targetContainerKey,
                        assetId: targetAssetKey
                    });
                }
            });
        });
    }

    public updateAssetUrl(asset: RemoteAsset) : RemoteAsset {
        asset.url = (<any>this.getClient()).endpoint.host +
            '/' + this.getBucket() + '/' + this.getConfigKey(asset.containerId, asset.assetId);
        return asset;
    }

    public get(containerKey: string, assetKey: string): any {
        const options = {
            Bucket: this.getBucket(),
            Key: this.getConfigKey(containerKey, assetKey)
        };
        return this.getClient().getObject(options).createReadStream();
    }

    private getConfigKey(containerKey: string, assetKey: string): string {
        return _.isEmpty(containerKey) ? assetKey : (containerKey + '/' + assetKey);
    }
}
