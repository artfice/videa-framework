import * as Promise from 'bluebird';
import * as _ from 'lodash';

import {AwsSqsAdapter, AwsSqsAdapterConfig} from '../../adapter/aws';
import {Message} from '../../domain/message/Message';
import {InternalServerError} from '../../VideaError';
import {MessageHelper} from './helper/MessageHelper';

export class AwsSqsMessageRepository extends AwsSqsAdapter {

    constructor(config: AwsSqsAdapterConfig) {
        super(config);
    }

    public sendMessage(message: Message): Promise<Message> {
        return new Promise<any>((resolve, reject) => {
            const params = MessageHelper.createSqsMessage(message);
            params.QueueUrl = this.getQueueUrl();
            this.getClient().sendMessage(params, (err, data) => {
                if (err) {
                    reject(new InternalServerError('Error sending message to ' + this.getQueueUrl(), err));
                } else {
                    message.id = data.MessageId;
                    message.createdDate = new Date();
                    message.modifiedDate = message.createdDate;
                    resolve(message);
                }
            });
        });
    }

    public pullMessage(): Promise<Message> {
        return new Promise<any>((resolve, reject) => {
            const params = {
                AttributeNames: [
                    'SentTimestamp'
                ],
                MaxNumberOfMessages: 1,
                MessageAttributeNames: [
                    'All'
                ],
                QueueUrl: this.getQueueUrl(),
                VisibilityTimeout: 0,
                WaitTimeSeconds: 0
            };
            this.getClient().receiveMessage(params, (err, data) => {
                if (err) {
                    reject(new InternalServerError('Error receiving message from ' + this.getQueueUrl(), err));
                } else {
                    if (_.isUndefined(data.Messages) || data.Messages.length === 0) {
                        resolve(undefined);
                        return;
                    }
                    const awsMessage = data.Messages[0];
                    const message = MessageHelper.parseSqsMessage(awsMessage);
                    const deleteParams = {
                        QueueUrl: this.getQueueUrl(),
                        ReceiptHandle: awsMessage.ReceiptHandle
                    };
                    this.getClient().deleteMessage(deleteParams, (err, data) => {
                        if (err) {
                            reject(new InternalServerError('Error deleting message from ' + this.getQueueUrl(), err));
                        } else {
                            resolve(message);
                        }
                    });
                }
            });
        });
    }
}
