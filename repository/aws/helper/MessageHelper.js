"use strict";
var _ = require("lodash");
var MessageHelper;
(function (MessageHelper) {
    function createSqsMessage(message) {
        var awsMessage = {
            DelaySeconds: message.delaySeconds,
            MessageBody: message.body
        };
        if (!_.isUndefined(message.attributes)) {
            awsMessage.MessageAttributes = {};
            for (var i = 0; i < message.attributes.length; i++) {
                awsMessage.MessageAttributes[message.attributes[i].name] = {
                    DataType: message.attributes[i].dataType,
                    StringValue: message.attributes[i].stringValue
                };
            }
        }
        return awsMessage;
    }
    MessageHelper.createSqsMessage = createSqsMessage;
    ;
    function parseSqsMessage(awsMessage) {
        var message = {
            id: awsMessage.MessageId,
            body: awsMessage.Body
        };
        if (!_.isUndefined(awsMessage.MessageAttributes)) {
            message.attributes = [];
            var keys = Object.keys(awsMessage.MessageAttributes);
            for (var i = 0; i < keys.length; i++) {
                var awsAttr = awsMessage.MessageAttributes[keys[i]];
                message.attributes.push({
                    dataType: awsAttr.DataType,
                    stringValue: awsAttr.StringValue
                });
            }
        }
        return message;
    }
    MessageHelper.parseSqsMessage = parseSqsMessage;
    ;
})(MessageHelper = exports.MessageHelper || (exports.MessageHelper = {}));
