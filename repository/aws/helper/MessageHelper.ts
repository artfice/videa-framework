import * as _ from 'lodash';
import {Message} from '../../../domain/message/Message';
import {MessageAttribute} from '../../../domain/message/MessageAttribute';

export module MessageHelper {
    export function createSqsMessage(message: Message): any {
        const awsMessage = <any>{
            DelaySeconds: message.delaySeconds,
            MessageBody: message.body
        };
        if (!_.isUndefined(message.attributes)) {
            awsMessage.MessageAttributes = {};
            for (let i = 0; i < message.attributes.length; i++) {
                awsMessage.MessageAttributes[message.attributes[i].name] = {
                    DataType: message.attributes[i].dataType,
                    StringValue: message.attributes[i].stringValue
                };
            }
        }
        return awsMessage;
    };

    export function parseSqsMessage(awsMessage: any): Message {
        const message = <Message>{
            id: awsMessage.MessageId,
            body: awsMessage.Body
        };
        if (!_.isUndefined(awsMessage.MessageAttributes)) {
            message.attributes = [];
            const keys = Object.keys(awsMessage.MessageAttributes);
            for (let i = 0; i < keys.length; i++) {
                const awsAttr = awsMessage.MessageAttributes[keys[i]];
                message.attributes.push(<MessageAttribute>{
                    dataType: awsAttr.DataType,
                    stringValue: awsAttr.StringValue
                });
            }
        }
        return message;
    };
}
