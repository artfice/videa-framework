module.exports = {
    tsSrc: [
        './**/*.ts',
        '!./node_modules/**/*',
        '!./configuration/**/*',
        '!./gulpfile.js'
    ],
    tsDocSrc: [
        './**/*.ts',
        '!./**/index.ts',
        '!./test/**/*.ts',
        '!./node_modules/**/*'
    ],
    tsDoc: './docs',
    testTsSrc: [
        './test/**/*.js'
    ],
    compiledFixtureJsSrc: './test/**/*.js',
    compiledFiles: [
        './**/*.js',
        './**/*.js.map',
        '!./node_modules/**/*',
        '!./configuration/**/*',
        '!./gulpfile.js'
    ]
};
