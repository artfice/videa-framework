var gulp = require('gulp');
var paths = require('../paths');
var istanbul = require('gulp-istanbul');
var mocha = require('gulp-mocha');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');

gulp.task('test:setup', function () {
    return gulp.src(paths.testTsSrc)
    // Covering files
        .pipe(istanbul())
        // Force `require` to return covered files
        .pipe(istanbul.hookRequire());
});

gulp.task('test:coverage', [], function () {
    return gulp.src(paths.testTsSrc)
        .pipe(mocha())
        // Creating the reports after tests ran
        .pipe(istanbul.writeReports())
        // Enforce a coverage of at least 90%
        .pipe(istanbul.enforceThresholds({ thresholds: { global: 90 } }));
});

gulp.task('test:clean', function() {
    return gulp.src([paths.compiledFixtureJsSrc], { base: '.', read: false })
        .pipe(clean());
});

gulp.task('test:run',  function() {
    return gulp.src(paths.testTsSrc, { read: false })
        .pipe(mocha({
            require: [],
            globals: {
                recursive: true,
                timeout: 15000
            },
            reporter: 'dot'
        })).once('end', function () {
            process.exit();
        });
});

gulp.task('test:all',  function() {
    runSequence('test:clean', 'typescript:all', 'test:run');
});