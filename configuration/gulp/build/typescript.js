var gulp = require('gulp');
var ts = require('gulp-typescript');
var paths = require('../paths');
var tslint = require('gulp-tslint');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var sourcemaps = require('gulp-sourcemaps');
var typedoc = require('gulp-typedoc');

var tsProject = ts.createProject('tsconfig.json');

const tslintOptions = {
    configuration: 'tslint.json',
    formatter: 'prose',
    rulesDirectory: 'node_modules/tslint-microsoft-contrib'
};

gulp.task('typescript:lint', function() {
    gulp.src(paths.tsSrc)
        .pipe(tslint(tslintOptions))
        .pipe(tslint.report({
            summarizeFailureOutput: true
        }));
});

gulp.task('typescript:clean', function() {
    return gulp.src(paths.compiledFiles, { base: '.', read: false })
        .pipe(clean());
});

gulp.task('typescript:clean-doc', function() {
    return gulp.src(paths.tsDoc, { base: '.', read: false })
        .pipe(clean());
});


gulp.task('typescript:framework', function() {
    var stream = tsProject.src()
        .pipe(tsProject())
        .on('error', function() {
            console.error('Error to compile typescript.');
            process.exit(1);
        });

    return stream.pipe(gulp.dest('.'));
});


gulp.task('typescript:recompile', function() {
    runSequence('typescript:clean', 'typescript:all');
});

gulp.task('typescript:all', ['typescript:framework']);

gulp.task('typescript:doc', ['typescript:clean-doc'], function() {
    return gulp.src(paths.tsDocSrc)
        .pipe(typedoc({
            module: 'commonjs',
            target: 'es5',
            out: paths.tsDoc,
            name: 'videa-framework',
            ignoreCompilerErrors: true
        }))
        ;
});
