import * as _ from 'lodash';

import {NotImplementedError} from '../VideaError';
import {DataTypes} from './DataTypes';
import {JsonSchema} from './JsonSchema';
import {
    ArraySchemaProperty,
    DynamicEnumRefSchemaProperty,
    ObjectSchemaProperty,
    PrimitiveSchemaProperty,
    RefSchemaProperty,
    StringSchemaProperty
} from './SchemaProperties';

//TODO: Move BaseSchema to core package
export class BaseSchema extends JsonSchema {
    //TODO: add ID to all other schema objects
    public static ID = 'schema.BaseSchema';

    // TODO: is this really needed?
    protected _values;

    constructor() {
        super();

        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'id',
                typeName: DataTypes.STRING,
                title: 'Id'
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: '_metadata',
                typeName: DataTypes.STRING,
                title: 'Metadata'
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'displayField',
                typeName: DataTypes.STRING,
                title: 'Display Field',
                defaultValue: this._getDefaultDisplayField()
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'createdDate',
                typeName: DataTypes.STRING,
                title: 'Created Date',
                format: 'date-time'
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty> {
                name: 'modifiedDate',
                typeName: DataTypes.STRING,
                title: 'Modified Date',
                format: 'date-time'
            });
        this.addPrimitiveProperty(
            <PrimitiveSchemaProperty>{
                name: 'systemObject',
                typeName: DataTypes.BOOLEAN,
                title: 'systemObject'
            });
    }

    protected _getTitle(): string {
        return 'Default Title';
    }

    protected _getDefaultDisplayField(): string {
        return '';
    }

    protected _addPrimitiveProperty(
        name: string,
        typeName: string,
        title: string,
        defaultValue?: any,
        help?: string): void {
        this.addPrimitiveProperty(<PrimitiveSchemaProperty>{
            name: name,
            typeName: typeName,
            title: title,
            defaultValue: defaultValue,
            help: help
        });
    }

    protected _addEnumProperty(name: string, typeName: string, title: string, values: any[], defaultValue?: any, help?: string): void {
        this.addPrimitiveProperty(<PrimitiveSchemaProperty>{
            name: name,
            typeName: typeName,
            title: title,
            defaultValue: defaultValue,
            enumValues: values,
            help: help
        });
    }

    protected _addRefProperty(name: string, title: string, value: string, help?: string, required?: boolean): void {
        this.addRefProperty(<RefSchemaProperty>{
            name: name,
            title: title,
            refSchemaId: value,
            help: help,
            required: required
        });
    }

    protected addReferenceArrayProperty(name: string, title: string, referenceType: string, help?: string) {
        this.addPrimitiveProperty(<ArraySchemaProperty>{
            name: name,
            typeName: DataTypes.ARRAY,
            title: title,
            items: <StringSchemaProperty> {
                typeName: DataTypes.STRING,
                title: title,
                help: help
            },
            refSchema: <RefSchemaProperty> {
                refSchemaId: referenceType
            }
        });
    }

    protected _addDynamicRefProperty(name: string, title: string, value: string, dynamicEnumUrl: string, help?: string): void {
        this.addDynamicEnumRefProperty(<DynamicEnumRefSchemaProperty>{
            name: name,
            title: title,
            help: help,
            refSchemaId: value,
            dynamicEnumUrl: dynamicEnumUrl
        });
    }

    protected addReferenceArrayOneOfProperty(name: string, title: string, referenceTypes: string[], help?: string) {
        const typeRefs = [];
        for (let i = 0; i < referenceTypes.length; i++) {
            const typeRef = <RefSchemaProperty>{
                refSchemaId: referenceTypes[i]
            };
            typeRefs.push(typeRef);
        }

        this.addPrimitiveProperty(<ArraySchemaProperty>{
            name: name,
            typeName: DataTypes.ARRAY,
            title: title,
            help: help,
            items: <StringSchemaProperty> {
                typeName: DataTypes.STRING
            },
            oneOf: typeRefs
        });
    }

    /**
     * This method is used for adding an array property which has specific types of items in it.
     * @param name
     * @param title
     * @param itemsType Specifies what type of items are in this array
     * @protected
     */
    protected _addArrayProperty(name: string, title: string, itemsType: string, help?: string) {
        this.addPrimitiveProperty(<ArraySchemaProperty>{
            name: name,
            typeName: DataTypes.ARRAY,
            title: title,
            help: help,
            items: <RefSchemaProperty> {
                title: title,
                refSchemaId: itemsType
            }
        });
    }

    /**
     * This method adds an array of any of property
     * @param name array field name
     * @param title array title name
     * @param itemTitle  The title inside of item ie. items: { title: 'Item Title'}
     * @param valueType  The $ref inside of item ie. items: {  $ref: 'ref'}
     * @param values The $ref for anyof "anyOf":[{"$ref":"some ref"}]
     * @param valueTitles The title for anyOf "anyOf":[{"title":"Some title"}]
     * @param valueHelp The Help text for anyOf "anyOf": [{"help":"Some help"}]
     * @param itemHelp The Help text for item
     * @protected
     */
    protected _addArrayAnyOfProperty(
        name: string,
        title: string,
        itemTitle: string,
        valueType: string,
        values: string[],
        valueTitles: string[],
        valueHelp?: string[],
        itemHelp?: string) {

        if (!values || values.length < 1) {
            throw new Error('BaseSchema: _addArrayAnyOfProperty, values cant be undefined or empty');
        }

        const typeRefs = [];
        for (let i = 0; i < values.length; i++) {
            const typeRef = <RefSchemaProperty>{
                refSchemaId: values[i],
                title: valueTitles[i]
            };
            if (valueHelp) {
                typeRef.help = valueHelp[i];
            }
            typeRefs.push(typeRef);
        }

        this.addPrimitiveProperty(<ArraySchemaProperty>{
            name: name,
            typeName: DataTypes.ARRAY,
            title: title,
            items: <RefSchemaProperty> {
                title: itemTitle,
                refSchemaId: valueType,
                help: itemHelp
            },
            anyOf: typeRefs
        });
    }

    protected _addPatternProperty(name: string, typeName: string, title: string, pattern: string) {
        this.addPrimitiveProperty(<PrimitiveSchemaProperty>{
            name: name,
            typeName: typeName,
            title: title,
            pattern: pattern
        });
        //this._properties[name] = SchemaHelper.buildProperty(name, type, title).pattern(pattern);
    }

    /**
     *
     * @param name
     * @param title
     * @param itemTypes
     * @private
     */
    protected _addMultiTypeArrayProperty(name: string, title: string, itemTypes: string[]) {
        throw new NotImplementedError('_addMultiTypeArrayProperty not Implemented');
    }

    protected _addOneOfRefProperty(
        name: string,
        title: string,
        values: string[],
        valueTitles?: string[],
        ref?: string,
        defaultValue?: string): void {

        if (!valueTitles) {
            valueTitles = [];
        }

        const typeRefs : RefSchemaProperty[] = [];
        if (!_.isEmpty(values)) {
            for (let i = 0; i < values.length; i++) {
                const typeRef = <RefSchemaProperty>{
                    refSchemaId: values[i],
                    title: valueTitles[i]
                };
                typeRefs.push(typeRef);
            }
        }

        this.addPrimitiveProperty(<ObjectSchemaProperty>{
            name: name,
            typeName: DataTypes.OBJECT,
            title: title,
            oneOf: typeRefs,
            defaultValue: defaultValue
        });
    }
}
