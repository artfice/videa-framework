import * as _ from 'lodash';

import {Schema} from './Schema';
import * as Digi from '../Digi';
import {SchemaHelper} from './SchemaHelper';
import {
    DynamicEnumRefSchemaProperty,
    PrimitiveSchemaProperty,
    RefSchemaProperty,
    SchemaProperty, JsonSchemaProperty
} from './SchemaProperties';
import {SchemaError, InvalidArgumentError} from '../VideaError';

export class JsonSchema {
    public static ID = 'schema.JsonSchema';

    private _id: string;
    private _name: string;
    private _title: string;
    private _description: string;
    protected _properties = {};
    protected _required: string[] = [];
    protected _schemaRefs: string[] = [];
    private _additionalProperties: boolean;
    private _compiledSchema: Schema;

    private static DEFAULT_TYPE_VALUES = {
        string: '',
        integer: 0,
        number: 0,
        boolean: false,
        array: [],
        object: null
    };

    private static TYPESCRIPT_TYPE_MAPPING = {
        string: ['string'],
        number: ['number', 'integer'],
        boolean: ['boolean'],
        array: ['array'],
        object: ['object', 'null']
    };

    public setId(id: string) {
        this._id = id;
    }

    public getId(): string {
        return this._id;
    }

    public setName(name: string) {
        this._name = name;
    }

    public getName(): string {
        return this._name;
    }

    public setTitle(title: string) {
        this._title = title;
    }

    public getTitle(): string {
        return this._title;
    }

    public setDescription(description: string) {
        this._description = description;
    }

    public getDescription(): string {
        return this._description;
    }

    public getSchemaRefs(): string[] {
        return this._schemaRefs;
    }

    public addSchemaRefs(refs: string[]) {
        this._schemaRefs.concat(refs);
    }

    public getSchema(): Schema {
        if (!this._compiledSchema) {
            this._compiledSchema = SchemaHelper.buildSchema(this._properties, this._required, this._additionalProperties);
        }
        return this._compiledSchema;
    }

    public setAdditionalProperties(additionalProperties: boolean): void {
        this._additionalProperties = additionalProperties;
    }

    public addPrimitiveProperty(property: PrimitiveSchemaProperty): void {
        this._addProperty(property, SchemaHelper.buildPropertyExtended);
    }

    public addRefProperty(property: RefSchemaProperty): void {
        this._addProperty(property, SchemaHelper.buildRefProperty);
    }

    public addDynamicEnumRefProperty(property: DynamicEnumRefSchemaProperty): void {
        this._addProperty(property, SchemaHelper.buildDynamicEnumRefProperty);
    }

    public addSchemaProperty(property: JsonSchemaProperty): void {
        if (_.isUndefined(property.schema)) {
            throw new InvalidArgumentError('Schema must be valid')
        }
        this._addProperty(property, SchemaHelper.buildSchemaProperty);
    }

    public getProperties(): any {
        return this._properties;
    }

    public getInstance(config: any): any {
        const instance = {};
        const schema = this.getSchema();
        const schemaProperties = schema.properties;
        const propertyNames = Object.keys(schemaProperties);
        const nProperties = propertyNames.length;
        let property;
        let propertyType;
        let propertyValue;
        let validType;
        let defaultPropertyGetter;

        for (let i = 0; i < nProperties; i++) {

            property = schemaProperties[propertyNames[i]];
            propertyType = property.type;
            propertyValue = JsonSchema.DEFAULT_TYPE_VALUES[property.type];

            if (config.hasOwnProperty(propertyNames[i]) && config[propertyNames[i]] !== undefined) {

                if (!this._isTypeValid(config[propertyNames[i]], propertyType)) {
                    throw new SchemaError('invalid type ' + (typeof config[propertyNames[i]]) + ' in ' + config);
                }

                propertyValue = config[propertyNames[i]];
            }

            defaultPropertyGetter = this['getDefault' + Digi.String.capitalize(propertyNames[i])];

            if (typeof defaultPropertyGetter === 'function') {
                propertyValue = defaultPropertyGetter.apply(this, []);
                validType = this._isTypeValid(property, propertyType);
                if (!validType) {
                    throw new SchemaError('Default Property Value not matching type from ' +
                        'schema. Property: ' + propertyNames[i] +
                        ' Type: ' + propertyType +
                        ' Default Value: ' + property);
                }
            }

            instance[propertyNames[i]] = propertyValue;

        }

        return instance;
    }

    private _addProperty(
        property: SchemaProperty,
        createPropFunc: (property: SchemaProperty, foundRefCallback?: (refSchema: RefSchemaProperty) => void) => any) : any {
        // clean to force schema recompilation
        this._compiledSchema = null;

        if (this._properties.hasOwnProperty(property.name)) {
            throw new SchemaError('Property ' + property.name + ' already exists');
        }

        const self = this;
        const prop = createPropFunc(property, (refSchema: RefSchemaProperty) => {
            self._cacheSchemaRef(refSchema);
        });
        this._properties[property.name] = prop;

        if (property.required) {
            this._required.push(property.name);
        }

        return prop;
    }

    private _isTypeValid(propertyValue: any, typeName: any) {
        return JsonSchema.TYPESCRIPT_TYPE_MAPPING[typeof propertyValue].indexOf(typeName) > -1;
    }

    private _cacheSchemaRef(property: RefSchemaProperty) {
        const refTypeName = property.refSchemaId;
        if (this._schemaRefs.indexOf(refTypeName) < 0) {
            this._schemaRefs.push(refTypeName);
        }
    }
}
