"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var JsonSchema_1 = require("../JsonSchema");
var DataTypes_1 = require("../DataTypes");
var UpdateOptionsSchema_1 = require("../domain/db/UpdateOptionsSchema");
var SearchQuerySchema_1 = require("../domain/db/SearchQuerySchema");
var SearchParamsSchema_1 = require("../domain/db/SearchParamsSchema");
var SearchOptionsSchema_1 = require("../domain/db/SearchOptionsSchema");
var AccountBasedServiceSchema = (function (_super) {
    __extends(AccountBasedServiceSchema, _super);
    function AccountBasedServiceSchema(baseSchema) {
        var _this = _super.call(this) || this;
        _this.setId(AccountBasedServiceSchema.ID + '.' + baseSchema.getName());
        _this.setName('AccountBasedServiceSchema' + baseSchema.getName());
        _this.addSchemaProperty({
            name: 'add',
            schema: _this.createAddSchema(baseSchema)
        });
        _this.addSchemaProperty({
            name: 'update',
            schema: _this.createUpdateSchema(baseSchema)
        });
        _this.addSchemaProperty({
            name: 'search',
            schema: _this.createSearchSchema(baseSchema)
        });
        _this.addSchemaProperty({
            name: 'replace',
            schema: _this.createReplaceSchema(baseSchema)
        });
        _this.addSchemaProperty({
            name: 'remove',
            schema: _this.createRemoveSchema(baseSchema)
        });
        _this.addSchemaProperty({
            name: 'getById',
            schema: _this.createGetByIdSchema(baseSchema)
        });
        _this.addSchemaProperty({
            name: 'batchGet',
            schema: _this.createBatchGetSchema(baseSchema)
        });
        _this.addSchemaProperty({
            name: 'resetData',
            schema: _this.createResetDataSchema(baseSchema)
        });
        return _this;
    }
    AccountBasedServiceSchema.prototype.createAddSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.add');
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addRefProperty({
            name: 'dto',
            refSchemaId: baseSchema.getId(),
            required: true
        });
        return schema;
    };
    AccountBasedServiceSchema.prototype.createUpdateSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.update.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty({
            name: 'id',
            typeName: DataTypes_1.DataTypes.STRING
        });
        schema.addRefProperty({
            name: 'dto',
            refSchemaId: baseSchema.getId(),
            required: true
        });
        schema.addRefProperty({
            name: 'updateOptions',
            refSchemaId: UpdateOptionsSchema_1.UpdateOptionsSchema.ID
        });
        schema.addRefProperty({
            name: 'searchQuery',
            refSchemaId: SearchQuerySchema_1.SearchQuerySchema.ID
        });
        return schema;
    };
    AccountBasedServiceSchema.prototype.createSearchSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.search.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addRefProperty({
            name: 'searchParams',
            refSchemaId: SearchParamsSchema_1.SearchParamsSchema.ID
        });
        return schema;
    };
    AccountBasedServiceSchema.prototype.createReplaceSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.replace.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty({
            name: 'id',
            typeName: DataTypes_1.DataTypes.STRING,
            required: true
        });
        schema.addRefProperty({
            name: 'dto',
            refSchemaId: baseSchema.getId(),
            required: true
        });
        return schema;
    };
    AccountBasedServiceSchema.prototype.createRemoveSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.remove.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty({
            name: 'id',
            typeName: DataTypes_1.DataTypes.STRING,
            required: true
        });
        schema.addRefProperty({
            name: 'searchQuery',
            refSchemaId: SearchQuerySchema_1.SearchQuerySchema.ID
        });
        return schema;
    };
    AccountBasedServiceSchema.prototype.createGetByIdSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.getById.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty({
            name: 'id',
            typeName: DataTypes_1.DataTypes.STRING,
            required: true
        });
        schema.addRefProperty({
            name: 'searchOptions',
            refSchemaId: SearchOptionsSchema_1.SearchOptionsSchema.ID
        });
        return schema;
    };
    AccountBasedServiceSchema.prototype.createBatchGetSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.batchGet.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty({
            name: 'ids',
            typeName: DataTypes_1.DataTypes.ARRAY
        });
        return schema;
    };
    AccountBasedServiceSchema.prototype.createResetDataSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.resetData.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        return schema;
    };
    AccountBasedServiceSchema.prototype.createBulkAddSchema = function (baseSchema) {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId('AccountBasedService.bulkAdd.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        this.addPrimitiveProperty({
            name: 'dtos',
            typeName: DataTypes_1.DataTypes.ARRAY,
            items: {
                refSchemaId: baseSchema.getId()
            },
            required: true
        });
        return schema;
    };
    AccountBasedServiceSchema.prototype.getTenantIdProperty = function () {
        return {
            name: 'tenantId',
            typeName: DataTypes_1.DataTypes.STRING,
            required: true
        };
    };
    return AccountBasedServiceSchema;
}(JsonSchema_1.JsonSchema));
AccountBasedServiceSchema.ID = 'schema.application.AccountBasedServiceSchema';
exports.AccountBasedServiceSchema = AccountBasedServiceSchema;
