import {JsonSchema} from '../JsonSchema';
import {StringSchemaProperty, RefSchemaProperty, JsonSchemaProperty, ArraySchemaProperty} from '../SchemaProperties';
import {DataTypes} from '../DataTypes';
import {UpdateOptionsSchema} from '../domain/db/UpdateOptionsSchema';
import {SearchQuerySchema} from '../domain/db/SearchQuerySchema';
import {SearchParamsSchema} from '../domain/db/SearchParamsSchema';
import {SearchOptionsSchema} from '../domain/db/SearchOptionsSchema';

export class AccountBasedServiceSchema extends JsonSchema {
    public static ID: string = 'schema.application.AccountBasedServiceSchema';

    constructor(baseSchema: JsonSchema) {
        super();
        this.setId(AccountBasedServiceSchema.ID + '.' + baseSchema.getName());
        this.setName('AccountBasedServiceSchema' + baseSchema.getName());

        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'add',
                schema: this.createAddSchema(baseSchema)
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'update',
                schema: this.createUpdateSchema(baseSchema)
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'search',
                schema: this.createSearchSchema(baseSchema)
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'replace',
                schema: this.createReplaceSchema(baseSchema)
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'remove',
                schema: this.createRemoveSchema(baseSchema)
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'getById',
                schema: this.createGetByIdSchema(baseSchema)
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'batchGet',
                schema: this.createBatchGetSchema(baseSchema)
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'resetData',
                schema: this.createResetDataSchema(baseSchema)
            });
    }

    public createAddSchema(baseSchema: JsonSchema) : JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.add');
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'dto',
            refSchemaId: baseSchema.getId(),
            required: true
        });
        return schema;
    }

    public createUpdateSchema(baseSchema: JsonSchema): JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.update.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty(<StringSchemaProperty>{
            name: 'id',
            typeName: DataTypes.STRING
        });
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'dto',
            refSchemaId: baseSchema.getId(),
            required: true
        });
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'updateOptions',
            refSchemaId: UpdateOptionsSchema.ID
        });
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'searchQuery',
            refSchemaId: SearchQuerySchema.ID
        });
        return schema;
    }

    public createSearchSchema(baseSchema: JsonSchema): JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.search.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'searchParams',
            refSchemaId: SearchParamsSchema.ID
        });
        return schema;
    }

    public createReplaceSchema(baseSchema: JsonSchema): JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.replace.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty(<StringSchemaProperty>{
            name: 'id',
            typeName: DataTypes.STRING,
            required: true
        });
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'dto',
            refSchemaId: baseSchema.getId(),
            required: true
        });
        return schema;
    }

    public createRemoveSchema(baseSchema: JsonSchema): JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.remove.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty(<StringSchemaProperty>{
            name: 'id',
            typeName: DataTypes.STRING,
            required: true
        });
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'searchQuery',
            refSchemaId: SearchQuerySchema.ID
        });
        return schema;
    }

    public createGetByIdSchema(baseSchema: JsonSchema): JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.getById.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty(<StringSchemaProperty>{
            name: 'id',
            typeName: DataTypes.STRING,
            required: true
        });
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'searchOptions',
            refSchemaId: SearchOptionsSchema.ID
        });
        return schema;
    }

    public createBatchGetSchema(baseSchema: JsonSchema): JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.batchGet.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        schema.addPrimitiveProperty(<ArraySchemaProperty>{
            name: 'ids',
            typeName: DataTypes.ARRAY
        });
        return schema;
    }

    public createResetDataSchema(baseSchema: JsonSchema): JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.resetData.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        return schema;
    }

    public createBulkAddSchema(baseSchema: JsonSchema): JsonSchema {
        const schema = new JsonSchema();
        schema.setId('AccountBasedService.bulkAdd.' + baseSchema.getName());
        schema.addPrimitiveProperty(this.getTenantIdProperty());
        this.addPrimitiveProperty(
            <ArraySchemaProperty>{
                name: 'dtos',
                typeName: DataTypes.ARRAY,
                items: <RefSchemaProperty> {
                    refSchemaId: baseSchema.getId()
                },
                required: true
            });
        return schema;
    }

    protected getTenantIdProperty(): StringSchemaProperty {
        return <StringSchemaProperty>{
            name: 'tenantId',
            typeName: DataTypes.STRING,
            required: true
        };
    }
}
