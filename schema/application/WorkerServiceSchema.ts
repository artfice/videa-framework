import {DataTypes} from '../DataTypes';
import {JsonSchema} from '../JsonSchema';
import {JsonSchemaProperty, RefSchemaProperty, StringSchemaProperty} from '../SchemaProperties';

import {SearchOptionsSchema} from '../domain/db/SearchOptionsSchema';
import {SearchParamsSchema} from '../domain/db/SearchParamsSchema';
import {WorkerSchema} from '../domain/worker/WorkerSchema';

export class WorkerServiceSchema extends JsonSchema {
    public static ID: string = 'schema.application.WorkerServiceSchema';

    constructor() {
        super();
        this.setId(WorkerServiceSchema.ID);
        this.setName('WorkerServiceSchema');

        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'start',
                schema: this.createStartSchema()
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'getById',
                schema: this.createGetByIdSchema()
            });
        this.addSchemaProperty(
            <JsonSchemaProperty> {
                name: 'search',
                schema: this.createSearchSchema()
            });
    }

    public createStartSchema() : JsonSchema {
        const schema = new JsonSchema();
        schema.setId(WorkerServiceSchema.ID + '.add');
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'worker',
            refSchemaId: WorkerSchema.ID,
            required: true
        });
        return schema;
    }

    public createSearchSchema(): JsonSchema {
        const schema = new JsonSchema();
        schema.setId(WorkerServiceSchema.ID + '.search');
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'searchParams',
            refSchemaId: SearchParamsSchema.ID
        });
        return schema;
    }

    public createUpdateSchema(): JsonSchema {
        const schema = new JsonSchema();
        schema.setId(WorkerServiceSchema.ID + '.updateState');
        schema.addPrimitiveProperty(<StringSchemaProperty>{
            name: 'id',
            typeName: DataTypes.STRING,
            required: true
        });
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'worker',
            refSchemaId: WorkerSchema.ID,
            required: true
        });
        return schema;
    }

    public createGetByIdSchema(): JsonSchema {
        const schema = new JsonSchema();
        schema.setId(WorkerServiceSchema.ID + '.getById');
        schema.addPrimitiveProperty(<StringSchemaProperty>{
            name: 'id',
            typeName: DataTypes.STRING,
            required: true
        });
        schema.addRefProperty(<RefSchemaProperty>{
            name: 'searchOptions',
            refSchemaId: SearchOptionsSchema.ID
        });
        return schema;
    }
}
