"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DataTypes_1 = require("../DataTypes");
var JsonSchema_1 = require("../JsonSchema");
var SearchOptionsSchema_1 = require("../domain/db/SearchOptionsSchema");
var SearchParamsSchema_1 = require("../domain/db/SearchParamsSchema");
var WorkerSchema_1 = require("../domain/worker/WorkerSchema");
var WorkerServiceSchema = (function (_super) {
    __extends(WorkerServiceSchema, _super);
    function WorkerServiceSchema() {
        var _this = _super.call(this) || this;
        _this.setId(WorkerServiceSchema.ID);
        _this.setName('WorkerServiceSchema');
        _this.addSchemaProperty({
            name: 'start',
            schema: _this.createStartSchema()
        });
        _this.addSchemaProperty({
            name: 'getById',
            schema: _this.createGetByIdSchema()
        });
        _this.addSchemaProperty({
            name: 'search',
            schema: _this.createSearchSchema()
        });
        return _this;
    }
    WorkerServiceSchema.prototype.createStartSchema = function () {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId(WorkerServiceSchema.ID + '.add');
        schema.addRefProperty({
            name: 'worker',
            refSchemaId: WorkerSchema_1.WorkerSchema.ID,
            required: true
        });
        return schema;
    };
    WorkerServiceSchema.prototype.createSearchSchema = function () {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId(WorkerServiceSchema.ID + '.search');
        schema.addRefProperty({
            name: 'searchParams',
            refSchemaId: SearchParamsSchema_1.SearchParamsSchema.ID
        });
        return schema;
    };
    WorkerServiceSchema.prototype.createUpdateSchema = function () {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId(WorkerServiceSchema.ID + '.updateState');
        schema.addPrimitiveProperty({
            name: 'id',
            typeName: DataTypes_1.DataTypes.STRING,
            required: true
        });
        schema.addRefProperty({
            name: 'worker',
            refSchemaId: WorkerSchema_1.WorkerSchema.ID,
            required: true
        });
        return schema;
    };
    WorkerServiceSchema.prototype.createGetByIdSchema = function () {
        var schema = new JsonSchema_1.JsonSchema();
        schema.setId(WorkerServiceSchema.ID + '.getById');
        schema.addPrimitiveProperty({
            name: 'id',
            typeName: DataTypes_1.DataTypes.STRING,
            required: true
        });
        schema.addRefProperty({
            name: 'searchOptions',
            refSchemaId: SearchOptionsSchema_1.SearchOptionsSchema.ID
        });
        return schema;
    };
    return WorkerServiceSchema;
}(JsonSchema_1.JsonSchema));
WorkerServiceSchema.ID = 'schema.application.WorkerServiceSchema';
exports.WorkerServiceSchema = WorkerServiceSchema;
