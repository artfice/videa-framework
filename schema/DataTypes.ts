export module DataTypes {
    export const STRING: string = 'string';
    export const ARRAY: string = 'array';
    export const NUMBER: string = 'number';
    export const OBJECT: string = 'object';
    export const BOOLEAN: string = 'boolean';
}
