"use strict";
var DataTypes;
(function (DataTypes) {
    DataTypes.STRING = 'string';
    DataTypes.ARRAY = 'array';
    DataTypes.NUMBER = 'number';
    DataTypes.OBJECT = 'object';
    DataTypes.BOOLEAN = 'boolean';
})(DataTypes = exports.DataTypes || (exports.DataTypes = {}));
