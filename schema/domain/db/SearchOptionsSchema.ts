import {BaseSchema} from '../../BaseSchema';
import {DataTypes} from '../../DataTypes';
import {NumericSchemaProperty, StringSchemaProperty, PrimitiveSchemaProperty} from '../../SchemaProperties';

export class SearchOptionsSchema extends BaseSchema {
    public static ID = 'schema.domain.db.SearchOptionsSchema';

    constructor() {

        super();
        this.setId(SearchOptionsSchema.ID);
        this.setName('SearchOptions');
        this.setTitle('Search Options');

        this.addPrimitiveProperty(
            <NumericSchemaProperty>{
                name: 'offset',
                typeName: DataTypes.NUMBER,
                title: 'Offset',
                help: 'Offset value.',
                minimum: 0
            });
        this.addPrimitiveProperty(
            <NumericSchemaProperty>{
                name: 'setSize',
                typeName: DataTypes.NUMBER,
                title: 'SetSize',
                help: 'Maximum number of results.',
                minimum: 0
            });
        this.addPrimitiveProperty(
            <PrimitiveSchemaProperty>{
                name: 'ascending',
                typeName: DataTypes.BOOLEAN,
                title: 'SetSize',
                help: 'Sort results by ascending order?',
                defaultValue: true
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'sort',
                typeName: DataTypes.STRING,
                title: 'Sort',
                help: 'Name of the field to be used for sorting results.'
            });
    }
}
