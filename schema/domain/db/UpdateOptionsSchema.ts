import {BaseSchema} from '../../BaseSchema';
import {DataTypes} from '../../DataTypes';
import {PrimitiveSchemaProperty} from '../../SchemaProperties';

export class UpdateOptionsSchema extends BaseSchema {
    public static ID = 'schema.domain.db.UpdateOptionsSchema';

    constructor() {

        super();
        this.setId(UpdateOptionsSchema.ID);
        this.setName('UpdateOptions');
        this.setTitle('Update Options');

        this.addPrimitiveProperty(
            <PrimitiveSchemaProperty>{
                name: 'upsert',
                typeName: DataTypes.BOOLEAN,
                title: 'Upsert',
                help: 'Update or add the object if it doesn\'t exist.'
            });
        this.addPrimitiveProperty(
            <PrimitiveSchemaProperty>{
                name: 'multi',
                typeName: DataTypes.BOOLEAN,
                title: 'Multi',
                help: 'Update all objects that match the update query.'
            });
    }
}
