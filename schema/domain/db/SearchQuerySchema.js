"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseSchema_1 = require("../../BaseSchema");
var DataTypes_1 = require("../../DataTypes");
var SearchQuerySchema = (function (_super) {
    __extends(SearchQuerySchema, _super);
    function SearchQuerySchema() {
        var _this = _super.call(this) || this;
        _this.setId(SearchQuerySchema.ID);
        _this.setName('SearchQuery');
        _this.setTitle('Search Query');
        _this.addPrimitiveProperty({
            name: 'field',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Field',
            help: 'Name of the field to be used with the search operator.'
        });
        return _this;
    }
    return SearchQuerySchema;
}(BaseSchema_1.BaseSchema));
SearchQuerySchema.ID = 'schema.domain.db.SearchQuerySchema';
exports.SearchQuerySchema = SearchQuerySchema;
