"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseSchema_1 = require("../../BaseSchema");
var DataTypes_1 = require("../../DataTypes");
var UpdateOptionsSchema = (function (_super) {
    __extends(UpdateOptionsSchema, _super);
    function UpdateOptionsSchema() {
        var _this = _super.call(this) || this;
        _this.setId(UpdateOptionsSchema.ID);
        _this.setName('UpdateOptions');
        _this.setTitle('Update Options');
        _this.addPrimitiveProperty({
            name: 'upsert',
            typeName: DataTypes_1.DataTypes.BOOLEAN,
            title: 'Upsert',
            help: 'Update or add the object if it doesn\'t exist.'
        });
        _this.addPrimitiveProperty({
            name: 'multi',
            typeName: DataTypes_1.DataTypes.BOOLEAN,
            title: 'Multi',
            help: 'Update all objects that match the update query.'
        });
        return _this;
    }
    return UpdateOptionsSchema;
}(BaseSchema_1.BaseSchema));
UpdateOptionsSchema.ID = 'schema.domain.db.UpdateOptionsSchema';
exports.UpdateOptionsSchema = UpdateOptionsSchema;
