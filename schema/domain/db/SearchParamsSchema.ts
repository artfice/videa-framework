import {RefSchemaProperty} from '../../SchemaProperties';
import {SearchOptionsSchema} from './SearchOptionsSchema';
import {SearchQuerySchema} from './SearchQuerySchema';

export class SearchParamsSchema extends SearchOptionsSchema {
    public static ID = 'schema.domain.db.SearchParamsSchema';

    constructor() {
        super();
        this.setId(SearchParamsSchema.ID);
        this.setName('SearchParams');
        this.setTitle('Search Params');

        this.addRefProperty(
            <RefSchemaProperty>{
                name: 'query',
                title: 'Search Query',
                help: 'Search query.',
                refSchemaId: SearchQuerySchema.ID
            });
    }
}
