import {BaseSchema} from '../../BaseSchema';
import {DataTypes} from '../../DataTypes';
import {StringSchemaProperty} from '../../SchemaProperties';

export class SearchQuerySchema extends BaseSchema {
    public static ID = 'schema.domain.db.SearchQuerySchema';

    constructor() {

        super();
        this.setId(SearchQuerySchema.ID);
        this.setName('SearchQuery');
        this.setTitle('Search Query');

        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'field',
                typeName: DataTypes.STRING,
                title: 'Field',
                help: 'Name of the field to be used with the search operator.'
            });
    }
}
