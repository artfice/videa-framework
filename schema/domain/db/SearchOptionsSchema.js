"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseSchema_1 = require("../../BaseSchema");
var DataTypes_1 = require("../../DataTypes");
var SearchOptionsSchema = (function (_super) {
    __extends(SearchOptionsSchema, _super);
    function SearchOptionsSchema() {
        var _this = _super.call(this) || this;
        _this.setId(SearchOptionsSchema.ID);
        _this.setName('SearchOptions');
        _this.setTitle('Search Options');
        _this.addPrimitiveProperty({
            name: 'offset',
            typeName: DataTypes_1.DataTypes.NUMBER,
            title: 'Offset',
            help: 'Offset value.',
            minimum: 0
        });
        _this.addPrimitiveProperty({
            name: 'setSize',
            typeName: DataTypes_1.DataTypes.NUMBER,
            title: 'SetSize',
            help: 'Maximum number of results.',
            minimum: 0
        });
        _this.addPrimitiveProperty({
            name: 'ascending',
            typeName: DataTypes_1.DataTypes.BOOLEAN,
            title: 'SetSize',
            help: 'Sort results by ascending order?',
            defaultValue: true
        });
        _this.addPrimitiveProperty({
            name: 'sort',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Sort',
            help: 'Name of the field to be used for sorting results.'
        });
        return _this;
    }
    return SearchOptionsSchema;
}(BaseSchema_1.BaseSchema));
SearchOptionsSchema.ID = 'schema.domain.db.SearchOptionsSchema';
exports.SearchOptionsSchema = SearchOptionsSchema;
