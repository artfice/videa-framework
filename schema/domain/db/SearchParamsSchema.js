"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var SearchOptionsSchema_1 = require("./SearchOptionsSchema");
var SearchQuerySchema_1 = require("./SearchQuerySchema");
var SearchParamsSchema = (function (_super) {
    __extends(SearchParamsSchema, _super);
    function SearchParamsSchema() {
        var _this = _super.call(this) || this;
        _this.setId(SearchParamsSchema.ID);
        _this.setName('SearchParams');
        _this.setTitle('Search Params');
        _this.addRefProperty({
            name: 'query',
            title: 'Search Query',
            help: 'Search query.',
            refSchemaId: SearchQuerySchema_1.SearchQuerySchema.ID
        });
        return _this;
    }
    return SearchParamsSchema;
}(SearchOptionsSchema_1.SearchOptionsSchema));
SearchParamsSchema.ID = 'schema.domain.db.SearchParamsSchema';
exports.SearchParamsSchema = SearchParamsSchema;
