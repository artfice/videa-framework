import {BaseSchema} from '../../BaseSchema';
import {DataTypes} from '../../DataTypes';
import {
    NumericSchemaProperty,
    StringSchemaProperty} from '../../SchemaProperties';

export class WorkerSchema extends BaseSchema {
    public static ID = 'schema.domain.worker.WorkerSchema';

    constructor() {

        super();
        this.setId(WorkerSchema.ID);
        this.setName('Worker');
        this.setTitle('Worker');

        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'taskName',
                typeName: DataTypes.STRING,
                title: 'Task Name',
                required: true
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'description',
                typeName: DataTypes.STRING,
                title: 'Description'
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'userId',
                typeName: DataTypes.STRING,
                title: 'User ID'
            });
        this.addPrimitiveProperty(
            <NumericSchemaProperty>{
                name: 'progress',
                typeName: DataTypes.NUMBER,
                title: 'Progress',
                minimum: 0
            });
    }
}
