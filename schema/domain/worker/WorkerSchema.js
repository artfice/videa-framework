"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseSchema_1 = require("../../BaseSchema");
var DataTypes_1 = require("../../DataTypes");
var WorkerSchema = (function (_super) {
    __extends(WorkerSchema, _super);
    function WorkerSchema() {
        var _this = _super.call(this) || this;
        _this.setId(WorkerSchema.ID);
        _this.setName('Worker');
        _this.setTitle('Worker');
        _this.addPrimitiveProperty({
            name: 'taskName',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Task Name',
            required: true
        });
        _this.addPrimitiveProperty({
            name: 'description',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Description'
        });
        _this.addPrimitiveProperty({
            name: 'userId',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'User ID'
        });
        _this.addPrimitiveProperty({
            name: 'progress',
            typeName: DataTypes_1.DataTypes.NUMBER,
            title: 'Progress',
            minimum: 0
        });
        return _this;
    }
    return WorkerSchema;
}(BaseSchema_1.BaseSchema));
WorkerSchema.ID = 'schema.domain.worker.WorkerSchema';
exports.WorkerSchema = WorkerSchema;
