"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseSchema_1 = require("../../BaseSchema");
var DataTypes_1 = require("../../DataTypes");
var ImageTypeSchema = (function (_super) {
    __extends(ImageTypeSchema, _super);
    function ImageTypeSchema() {
        var _this = _super.call(this) || this;
        _this.setId(ImageTypeSchema.ID);
        _this.setName('ImageType');
        _this.setTitle('Image Type');
        _this.addPrimitiveProperty({
            name: 'name',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Name',
            required: true,
            minLength: 3
        });
        _this.addPrimitiveProperty({
            name: 'width',
            typeName: DataTypes_1.DataTypes.NUMBER,
            title: 'Width',
            help: 'The width in pixels.'
        });
        _this.addPrimitiveProperty({
            name: 'height',
            typeName: DataTypes_1.DataTypes.NUMBER,
            title: 'Height',
            help: 'The height in pixels.'
        });
        return _this;
    }
    return ImageTypeSchema;
}(BaseSchema_1.BaseSchema));
ImageTypeSchema.ID = 'schema.domain.image.ImageTypeSchema';
exports.ImageTypeSchema = ImageTypeSchema;
