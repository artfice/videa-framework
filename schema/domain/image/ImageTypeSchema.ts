import {BaseSchema} from '../../BaseSchema';
import {DataTypes} from '../../DataTypes';
import {
    NumericSchemaProperty,
    StringSchemaProperty} from '../../SchemaProperties';

export class ImageTypeSchema extends BaseSchema {
    public static ID = 'schema.domain.image.ImageTypeSchema';

    constructor() {

        super();
        this.setId(ImageTypeSchema.ID);
        this.setName('ImageType');
        this.setTitle('Image Type');

        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'name',
                typeName: DataTypes.STRING,
                title: 'Name',
                required: true,
                minLength: 3
            });
        this.addPrimitiveProperty(
            <NumericSchemaProperty>{
                name: 'width',
                typeName: DataTypes.NUMBER,
                title: 'Width',
                help: 'The width in pixels.'
            });
        this.addPrimitiveProperty(
            <NumericSchemaProperty>{
                name: 'height',
                typeName: DataTypes.NUMBER,
                title: 'Height',
                help: 'The height in pixels.'
            });
    }
}
