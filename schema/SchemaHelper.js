"use strict";
var _ = require("lodash");
var jsonSchemaBuilder = require("json-schema-build");
var VideaError_1 = require("../VideaError");
var DataTypes_1 = require("./DataTypes");
var SchemaHelper;
(function (SchemaHelper) {
    function buildProperty(name, typeName, title, help) {
        return buildPropertyExtended({
            name: name,
            typeName: typeName,
            title: title,
            help: help
        });
    }
    SchemaHelper.buildProperty = buildProperty;
    function buildPropertyExtended(property, foundRefCallback) {
        var prop = jsonSchemaBuilder[property.typeName]();
        if (property.title) {
            prop.title(property.title);
        }
        if (property.help) {
            prop.help(property.help);
        }
        buildValidation(property, prop, foundRefCallback);
        return prop;
    }
    SchemaHelper.buildPropertyExtended = buildPropertyExtended;
    function buildRefProperty(property, foundRefCallback) {
        var prop = ref(property.refSchemaId);
        if (property.title) {
            prop.title(property.title);
        }
        if (property.help) {
            prop.help(property.help);
        }
        // Warn the caller that we found a reference to another schema.
        if (foundRefCallback) {
            foundRefCallback(property);
        }
        return prop;
    }
    SchemaHelper.buildRefProperty = buildRefProperty;
    function buildDynamicEnumRefProperty(property, foundRefCallback) {
        var refProp = buildRefProperty(property, foundRefCallback);
        refProp.dynamicEnum(property.dynamicEnumUrl);
        return refProp;
    }
    SchemaHelper.buildDynamicEnumRefProperty = buildDynamicEnumRefProperty;
    function buildSchemaProperty(property, foundRefCallback) {
        var schema = property.schema.getSchema();
        if (foundRefCallback) {
            var schemaRefIds = property.schema.getSchemaRefs();
            // add references
            for (var i = 0; i < schemaRefIds.length; i++) {
                foundRefCallback({ refSchemaId: schemaRefIds[i] });
            }
        }
        return schema;
    }
    SchemaHelper.buildSchemaProperty = buildSchemaProperty;
    function buildSchema(properties, required, additionalProperties) {
        var schema = jsonSchemaBuilder.schema();
        schema.property(properties);
        if (!_.isUndefined(additionalProperties)) {
            schema.additionalProperties(additionalProperties);
        }
        if (required && required.length > 0) {
            schema.required(required);
        }
        return schema.json();
    }
    SchemaHelper.buildSchema = buildSchema;
    function getSchema() {
        return jsonSchemaBuilder.schema();
    }
    SchemaHelper.getSchema = getSchema;
    function buildRefs(values, valueTitles) {
        var list = [];
        for (var i = 0; i < values.length; i++) {
            if (valueTitles.length > 0) {
                list.push(jsonSchemaBuilder.$ref(values[i]).title(valueTitles[i]));
            }
            else {
                list.push(jsonSchemaBuilder.$ref(values[i]));
            }
        }
        return list;
    }
    SchemaHelper.buildRefs = buildRefs;
    function buildSingleRef(name, value, title) {
        return SchemaHelper.buildProperty(name, 'object', title).$ref(value);
    }
    SchemaHelper.buildSingleRef = buildSingleRef;
    function ref(value) {
        return jsonSchemaBuilder.$ref(value);
    }
    SchemaHelper.ref = ref;
    function buildValidation(property, prop, foundRefCallback) {
        if (property.defaultValue !== undefined) {
            prop.default(property.defaultValue);
        }
        if (property.enumValues !== undefined) {
            prop.enum(property.enumValues);
        }
        if (property.pattern) {
            prop.pattern(property.pattern);
        }
        if (property.typeName === DataTypes_1.DataTypes.STRING) {
            buildStringValidation(property, prop, foundRefCallback);
        }
        else if (property.typeName === DataTypes_1.DataTypes.NUMBER) {
            buildNumberValidation(property, prop, foundRefCallback);
        }
        else if (property.typeName === DataTypes_1.DataTypes.ARRAY) {
            buildArrayValidation(property, prop, foundRefCallback);
        }
        else if (property.typeName === DataTypes_1.DataTypes.OBJECT) {
            buildObjectValidation(property, prop, foundRefCallback);
        }
        else if (property.typeName === DataTypes_1.DataTypes.BOOLEAN) {
            buildBooleanValidation(property, prop, foundRefCallback);
        }
        else {
            throw new VideaError_1.InvalidArgumentError('Unknown type name "' + property.typeName + '" while building schema"');
        }
        return prop;
    }
    function buildBooleanValidation(property, prop, foundRefCallback) {
        return prop;
    }
    function buildStringValidation(property, prop, foundRefCallback) {
        var stringValidation = property;
        if (stringValidation.format) {
            prop.format(stringValidation.format);
        }
        if (stringValidation.maxLength) {
            prop.maxLength(stringValidation.maxLength);
        }
        if (stringValidation.minLength) {
            prop.minLength(stringValidation.minLength);
        }
        if (stringValidation.pattern) {
            prop.pattern(stringValidation.pattern);
        }
        return prop;
    }
    function buildNumberValidation(property, prop, foundRefCallback) {
        var numericValidation = property;
        if (numericValidation.multipleOF) {
            prop.multipleOF(numericValidation.multipleOF);
        }
        if (numericValidation.maximum) {
            prop.maximum(numericValidation.maximum);
        }
        if (numericValidation.exclusiveMaximum) {
            prop.exclusiveMaximum(numericValidation.exclusiveMaximum);
        }
        if (numericValidation.minimum) {
            prop.minimum(numericValidation.minimum);
        }
        if (numericValidation.exclusiveMinimum) {
            prop.exclusiveMinimum(numericValidation.exclusiveMinimum);
        }
        return prop;
    }
    function buildArrayValidation(property, prop, foundRefCallback) {
        var arrayValidation = property;
        if (arrayValidation.items) {
            var itemsSchema = void 0;
            if (_.isArray(arrayValidation.items)) {
                itemsSchema = [];
                for (var i = 0; i < arrayValidation.items.length; i++) {
                    var itemSchema = checkIfRefAndBuild(arrayValidation.items[i], foundRefCallback);
                    if (!itemSchema) {
                        itemSchema = buildPropertyExtended(arrayValidation.items[i], foundRefCallback);
                    }
                    if (itemsSchema) {
                        itemsSchema.push(itemSchema);
                    }
                    else {
                        throw new VideaError_1.InvalidArgumentError('Unsupported schema type for array items: ' +
                            JSON.stringify(arrayValidation.items[i]));
                    }
                }
            }
            else {
                itemsSchema = checkIfRefAndBuild(arrayValidation.items, foundRefCallback);
                if (!itemsSchema) {
                    itemsSchema = buildPropertyExtended(arrayValidation.items, foundRefCallback);
                }
                if (!itemsSchema) {
                    throw new VideaError_1.InvalidArgumentError('Unsupported schema type for array items: ' + JSON.stringify(arrayValidation));
                }
            }
            prop.items(itemsSchema);
        }
        if (arrayValidation.maxItems) {
            prop.maxItems(arrayValidation.maxItems);
        }
        if (arrayValidation.minItems) {
            prop.minItems(arrayValidation.minItems);
        }
        if (arrayValidation.uniqueItems) {
            prop.uniqueItems(arrayValidation.uniqueItems);
        }
        checkForReferences(property, prop, foundRefCallback);
        return prop;
    }
    function buildObjectValidation(property, prop, foundRefCallback) {
        var objValidation = property;
        if (objValidation.schema) {
            var schema = objValidation.schema;
            schema.getSchemaRefs();
            objValidation.schema.getSchema();
            prop.properties();
        }
        if (objValidation.maxProperties) {
            prop.maxProperties(objValidation.maxProperties);
        }
        if (objValidation.minProperties) {
            prop.minProperties(objValidation.minProperties);
        }
        checkForReferences(property, prop, foundRefCallback);
        return prop;
    }
    function checkIfRefAndBuild(schema, foundRefCallback) {
        var refSchema;
        if (schema.hasOwnProperty('refSchemaId')) {
            refSchema = schema.hasOwnProperty('dynamicEnumUrl') ?
                buildDynamicEnumRefProperty(schema, foundRefCallback) :
                buildRefProperty(schema, foundRefCallback);
        }
        return refSchema;
    }
    function checkForReferences(property, prop, foundRefCallback) {
        if (property.hasOwnProperty('refSchema')) {
            var refSchema = property.refSchema;
            prop.$ref(refSchema.refSchemaId);
        }
        if (property.hasOwnProperty('oneOf')) {
            var refSchemas = property.oneOf;
            if (!_.isArray(refSchemas)) {
                throw new VideaError_1.InvalidArgumentError('oneOf schema property must be an array of schemas.');
            }
            prop.oneOf(buildReferences(refSchemas, foundRefCallback));
        }
        if (property.hasOwnProperty('anyOf')) {
            var refSchemas = property.anyOf;
            if (!_.isArray(refSchemas)) {
                throw new VideaError_1.InvalidArgumentError('anyOf schema property must be an array of schemas.');
            }
            prop.anyOf(buildReferences(refSchemas, foundRefCallback));
        }
    }
    function buildReferences(refSchemas, foundRefCallback) {
        var typeRefs = [];
        for (var i = 0; i < refSchemas.length; i++) {
            typeRefs.push(SchemaHelper.buildRefProperty(refSchemas[i], foundRefCallback));
        }
        return typeRefs;
    }
})(SchemaHelper = exports.SchemaHelper || (exports.SchemaHelper = {}));
