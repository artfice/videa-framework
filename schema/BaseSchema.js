"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var _ = require("lodash");
var VideaError_1 = require("../VideaError");
var DataTypes_1 = require("./DataTypes");
var JsonSchema_1 = require("./JsonSchema");
//TODO: Move BaseSchema to core package
var BaseSchema = (function (_super) {
    __extends(BaseSchema, _super);
    function BaseSchema() {
        var _this = _super.call(this) || this;
        _this.addPrimitiveProperty({
            name: 'id',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Id'
        });
        _this.addPrimitiveProperty({
            name: '_metadata',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Metadata'
        });
        _this.addPrimitiveProperty({
            name: 'displayField',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Display Field',
            defaultValue: _this._getDefaultDisplayField()
        });
        _this.addPrimitiveProperty({
            name: 'createdDate',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Created Date',
            format: 'date-time'
        });
        _this.addPrimitiveProperty({
            name: 'modifiedDate',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Modified Date',
            format: 'date-time'
        });
        _this.addPrimitiveProperty({
            name: 'systemObject',
            typeName: DataTypes_1.DataTypes.BOOLEAN,
            title: 'systemObject'
        });
        return _this;
    }
    BaseSchema.prototype._getTitle = function () {
        return 'Default Title';
    };
    BaseSchema.prototype._getDefaultDisplayField = function () {
        return '';
    };
    BaseSchema.prototype._addPrimitiveProperty = function (name, typeName, title, defaultValue, help) {
        this.addPrimitiveProperty({
            name: name,
            typeName: typeName,
            title: title,
            defaultValue: defaultValue,
            help: help
        });
    };
    BaseSchema.prototype._addEnumProperty = function (name, typeName, title, values, defaultValue, help) {
        this.addPrimitiveProperty({
            name: name,
            typeName: typeName,
            title: title,
            defaultValue: defaultValue,
            enumValues: values,
            help: help
        });
    };
    BaseSchema.prototype._addRefProperty = function (name, title, value, help, required) {
        this.addRefProperty({
            name: name,
            title: title,
            refSchemaId: value,
            help: help,
            required: required
        });
    };
    BaseSchema.prototype.addReferenceArrayProperty = function (name, title, referenceType, help) {
        this.addPrimitiveProperty({
            name: name,
            typeName: DataTypes_1.DataTypes.ARRAY,
            title: title,
            items: {
                typeName: DataTypes_1.DataTypes.STRING,
                title: title,
                help: help
            },
            refSchema: {
                refSchemaId: referenceType
            }
        });
    };
    BaseSchema.prototype._addDynamicRefProperty = function (name, title, value, dynamicEnumUrl, help) {
        this.addDynamicEnumRefProperty({
            name: name,
            title: title,
            help: help,
            refSchemaId: value,
            dynamicEnumUrl: dynamicEnumUrl
        });
    };
    BaseSchema.prototype.addReferenceArrayOneOfProperty = function (name, title, referenceTypes, help) {
        var typeRefs = [];
        for (var i = 0; i < referenceTypes.length; i++) {
            var typeRef = {
                refSchemaId: referenceTypes[i]
            };
            typeRefs.push(typeRef);
        }
        this.addPrimitiveProperty({
            name: name,
            typeName: DataTypes_1.DataTypes.ARRAY,
            title: title,
            help: help,
            items: {
                typeName: DataTypes_1.DataTypes.STRING
            },
            oneOf: typeRefs
        });
    };
    /**
     * This method is used for adding an array property which has specific types of items in it.
     * @param name
     * @param title
     * @param itemsType Specifies what type of items are in this array
     * @protected
     */
    BaseSchema.prototype._addArrayProperty = function (name, title, itemsType, help) {
        this.addPrimitiveProperty({
            name: name,
            typeName: DataTypes_1.DataTypes.ARRAY,
            title: title,
            help: help,
            items: {
                title: title,
                refSchemaId: itemsType
            }
        });
    };
    /**
     * This method adds an array of any of property
     * @param name array field name
     * @param title array title name
     * @param itemTitle  The title inside of item ie. items: { title: 'Item Title'}
     * @param valueType  The $ref inside of item ie. items: {  $ref: 'ref'}
     * @param values The $ref for anyof "anyOf":[{"$ref":"some ref"}]
     * @param valueTitles The title for anyOf "anyOf":[{"title":"Some title"}]
     * @param valueHelp The Help text for anyOf "anyOf": [{"help":"Some help"}]
     * @param itemHelp The Help text for item
     * @protected
     */
    BaseSchema.prototype._addArrayAnyOfProperty = function (name, title, itemTitle, valueType, values, valueTitles, valueHelp, itemHelp) {
        if (!values || values.length < 1) {
            throw new Error('BaseSchema: _addArrayAnyOfProperty, values cant be undefined or empty');
        }
        var typeRefs = [];
        for (var i = 0; i < values.length; i++) {
            var typeRef = {
                refSchemaId: values[i],
                title: valueTitles[i]
            };
            if (valueHelp) {
                typeRef.help = valueHelp[i];
            }
            typeRefs.push(typeRef);
        }
        this.addPrimitiveProperty({
            name: name,
            typeName: DataTypes_1.DataTypes.ARRAY,
            title: title,
            items: {
                title: itemTitle,
                refSchemaId: valueType,
                help: itemHelp
            },
            anyOf: typeRefs
        });
    };
    BaseSchema.prototype._addPatternProperty = function (name, typeName, title, pattern) {
        this.addPrimitiveProperty({
            name: name,
            typeName: typeName,
            title: title,
            pattern: pattern
        });
        //this._properties[name] = SchemaHelper.buildProperty(name, type, title).pattern(pattern);
    };
    /**
     *
     * @param name
     * @param title
     * @param itemTypes
     * @private
     */
    BaseSchema.prototype._addMultiTypeArrayProperty = function (name, title, itemTypes) {
        throw new VideaError_1.NotImplementedError('_addMultiTypeArrayProperty not Implemented');
    };
    BaseSchema.prototype._addOneOfRefProperty = function (name, title, values, valueTitles, ref, defaultValue) {
        if (!valueTitles) {
            valueTitles = [];
        }
        var typeRefs = [];
        if (!_.isEmpty(values)) {
            for (var i = 0; i < values.length; i++) {
                var typeRef = {
                    refSchemaId: values[i],
                    title: valueTitles[i]
                };
                typeRefs.push(typeRef);
            }
        }
        this.addPrimitiveProperty({
            name: name,
            typeName: DataTypes_1.DataTypes.OBJECT,
            title: title,
            oneOf: typeRefs,
            defaultValue: defaultValue
        });
    };
    return BaseSchema;
}(JsonSchema_1.JsonSchema));
//TODO: add ID to all other schema objects
BaseSchema.ID = 'schema.BaseSchema';
exports.BaseSchema = BaseSchema;
