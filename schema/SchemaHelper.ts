import * as _ from 'lodash';
import jsonSchemaBuilder = require('json-schema-build');

import { InvalidArgumentError } from '../VideaError';
import {DataTypes} from './DataTypes';
import {
    ArraySchemaProperty,
    DynamicEnumRefSchemaProperty,
    NumericSchemaProperty,
    ObjectSchemaProperty,
    PrimitiveSchemaProperty,
    RefSchemaProperty,
    StringSchemaProperty, JsonSchemaProperty
} from './SchemaProperties';

export module SchemaHelper {

    export function buildProperty(name: string, typeName: string, title: string, help?: string): any {
        return buildPropertyExtended(<PrimitiveSchemaProperty>{
            name: name,
            typeName: typeName,
            title: title,
            help: help
        });
    }

    export function buildPropertyExtended(
        property: PrimitiveSchemaProperty,
        foundRefCallback?: (refSchema: RefSchemaProperty) => void): any {
        const prop = jsonSchemaBuilder[property.typeName]();
        if (property.title) {
            prop.title(property.title);
        }
        if (property.help) {
            prop.help(property.help);
        }
        buildValidation(property, prop, foundRefCallback);
        return prop;
    }

    export function buildRefProperty(
        property: RefSchemaProperty,
        foundRefCallback?: (refSchema: RefSchemaProperty) => void): any {
        const prop = ref(property.refSchemaId);
        if (property.title) {
            prop.title(property.title);
        }
        if (property.help) {
            prop.help(property.help);
        }

        // Warn the caller that we found a reference to another schema.
        if (foundRefCallback) {
            foundRefCallback(property);
        }
        return prop;
    }

    export function buildDynamicEnumRefProperty(
        property: DynamicEnumRefSchemaProperty,
        foundRefCallback?: (refSchema: RefSchemaProperty) => void): any {
        const refProp = buildRefProperty(property, foundRefCallback);
        refProp.dynamicEnum(property.dynamicEnumUrl);
        return refProp;
    }

    export function buildSchemaProperty(property: JsonSchemaProperty,
                                        foundRefCallback?: (refSchema: RefSchemaProperty) => void): any {
        const schema = property.schema.getSchema();
        if (foundRefCallback) {
            const schemaRefIds = property.schema.getSchemaRefs();
            // add references
            for(let i = 0; i < schemaRefIds.length; i ++) {
                foundRefCallback(<RefSchemaProperty>{ refSchemaId: schemaRefIds[i] });
            }
        }
        return schema;
    }

    export function buildSchema(properties: any, required?: string[], additionalProperties?: boolean) {
        const schema = jsonSchemaBuilder.schema();
        schema.property(properties);

        if (!_.isUndefined(additionalProperties)) {
            schema.additionalProperties(additionalProperties);
        }

        if (required && required.length > 0) {
            schema.required(required);
        }
        return schema.json();
    }

    export function getSchema() {
        return jsonSchemaBuilder.schema();
    }

    export function buildRefs(values: string[], valueTitles?: string[]) {
        const list = [];
        for (let i = 0; i < values.length; i++) {
            if (valueTitles.length > 0) {
                list.push(jsonSchemaBuilder.$ref(values[i]).title(valueTitles[i]));
            } else {
                list.push(jsonSchemaBuilder.$ref(values[i]));
            }

        }
        return list;
    }

    export function buildSingleRef(name: string, value: string, title: string): any {
        return SchemaHelper.buildProperty(name, 'object', title).$ref(value);
    }

    export function ref(value: string) {
        return jsonSchemaBuilder.$ref(value);
    }

    function buildValidation(
        property: PrimitiveSchemaProperty,
        prop: any,
        foundRefCallback?: (refSchema: RefSchemaProperty) => void): any {

        if (property.defaultValue !== undefined) {
            prop.default(property.defaultValue);
        }

        if (property.enumValues !== undefined) {
            prop.enum(property.enumValues);
        }

        if (property.pattern) {
            prop.pattern(property.pattern);
        }

        if (property.typeName === DataTypes.STRING) {
            buildStringValidation(property, prop, foundRefCallback);
        } else if (property.typeName === DataTypes.NUMBER) {
            buildNumberValidation(property, prop, foundRefCallback);
        } else if (property.typeName === DataTypes.ARRAY) {
            buildArrayValidation(property, prop, foundRefCallback);
        } else if (property.typeName === DataTypes.OBJECT) {
            buildObjectValidation(property, prop, foundRefCallback);
        } else if (property.typeName === DataTypes.BOOLEAN) {
            buildBooleanValidation(property, prop, foundRefCallback);
        } else {
            throw new InvalidArgumentError('Unknown type name "' + property.typeName + '" while building schema"');
        }

        return prop;
    }

    function buildBooleanValidation(property: PrimitiveSchemaProperty,
                                    prop: any,
                                    foundRefCallback?: (refSchema: RefSchemaProperty) => void) : any {
        return prop;
    }

    function buildStringValidation(property: PrimitiveSchemaProperty,
                                   prop: any,
                                   foundRefCallback?: (refSchema: RefSchemaProperty) => void) : any {
        const stringValidation = <StringSchemaProperty>property;
        if (stringValidation.format) {
            prop.format(stringValidation.format);
        }
        if (stringValidation.maxLength) {
            prop.maxLength(stringValidation.maxLength);
        }
        if (stringValidation.minLength) {
            prop.minLength(stringValidation.minLength);
        }
        if (stringValidation.pattern) {
            prop.pattern(stringValidation.pattern);
        }
        return prop;
    }

    function buildNumberValidation(property: PrimitiveSchemaProperty,
                                   prop: any,
                                   foundRefCallback?: (refSchema: RefSchemaProperty) => void) : any {
        const numericValidation = <NumericSchemaProperty>property;
        if (numericValidation.multipleOF) {
            prop.multipleOF(numericValidation.multipleOF);
        }
        if (numericValidation.maximum) {
            prop.maximum(numericValidation.maximum);
        }
        if (numericValidation.exclusiveMaximum) {
            prop.exclusiveMaximum(numericValidation.exclusiveMaximum);
        }
        if (numericValidation.minimum) {
            prop.minimum(numericValidation.minimum);
        }
        if (numericValidation.exclusiveMinimum) {
            prop.exclusiveMinimum(numericValidation.exclusiveMinimum);
        }
        return prop;
    }

    function buildArrayValidation(property: PrimitiveSchemaProperty,
                                  prop: any,
                                  foundRefCallback?: (refSchema: RefSchemaProperty) => void) : any {
        const arrayValidation = <ArraySchemaProperty>property;
        if (arrayValidation.items) {
            let itemsSchema;
            if (_.isArray(arrayValidation.items)) {
                itemsSchema = [];
                for (let i = 0; i < arrayValidation.items.length; i++) {
                    let itemSchema = checkIfRefAndBuild(arrayValidation.items[i], foundRefCallback);
                    if (!itemSchema) {
                        itemSchema = buildPropertyExtended(<PrimitiveSchemaProperty>arrayValidation.items[i], foundRefCallback);
                    }
                    if (itemsSchema) {
                        itemsSchema.push(itemSchema);
                    } else {
                        throw new InvalidArgumentError('Unsupported schema type for array items: ' +
                            JSON.stringify(arrayValidation.items[i]));
                    }
                }
            } else {
                itemsSchema = checkIfRefAndBuild(arrayValidation.items, foundRefCallback);
                if (!itemsSchema) {
                    itemsSchema = buildPropertyExtended(arrayValidation.items, foundRefCallback);
                }

                if (!itemsSchema) {
                    throw new InvalidArgumentError('Unsupported schema type for array items: ' + JSON.stringify(arrayValidation));
                }
            }
            prop.items(itemsSchema);
        }
        if (arrayValidation.maxItems) {
            prop.maxItems(arrayValidation.maxItems);
        }
        if (arrayValidation.minItems) {
            prop.minItems(arrayValidation.minItems);
        }
        if (arrayValidation.uniqueItems) {
            prop.uniqueItems(arrayValidation.uniqueItems);
        }

        checkForReferences(property, prop, foundRefCallback);
        return prop;
    }

    function buildObjectValidation(property: PrimitiveSchemaProperty,
                                   prop: any,
                                   foundRefCallback?: (refSchema: RefSchemaProperty) => void) : any {
        const objValidation = <ObjectSchemaProperty>property;
        if (objValidation.schema) {
            const schema = objValidation.schema;
            schema.getSchemaRefs();
            objValidation.schema.getSchema();
            prop.properties();
        }
        if (objValidation.maxProperties) {
            prop.maxProperties(objValidation.maxProperties);
        }
        if (objValidation.minProperties) {
            prop.minProperties(objValidation.minProperties);
        }

        checkForReferences(property, prop, foundRefCallback);
        return prop;
    }

    function checkIfRefAndBuild(
        schema: any,
        foundRefCallback?: (refSchema: RefSchemaProperty) => void) : RefSchemaProperty {

        let refSchema: RefSchemaProperty;
        if (schema.hasOwnProperty('refSchemaId')) {
            refSchema = schema.hasOwnProperty('dynamicEnumUrl') ?
                buildDynamicEnumRefProperty(<DynamicEnumRefSchemaProperty>schema, foundRefCallback) :
                buildRefProperty(<RefSchemaProperty>schema, foundRefCallback);
        }
        return refSchema;
    }

    function checkForReferences(
        property: PrimitiveSchemaProperty,
        prop: any,
        foundRefCallback?: (refSchema: RefSchemaProperty) => void) : void {

        if (property.hasOwnProperty('refSchema')) {
            const refSchema : RefSchemaProperty = (<any>property).refSchema;
            prop.$ref(refSchema.refSchemaId);
        }
        if (property.hasOwnProperty('oneOf')) {
            const refSchemas = <RefSchemaProperty[]>(<any>property).oneOf;
            if (!_.isArray(refSchemas)) {
                throw new InvalidArgumentError('oneOf schema property must be an array of schemas.');
            }
            prop.oneOf(buildReferences(refSchemas, foundRefCallback));
        }
        if (property.hasOwnProperty('anyOf')) {
            const refSchemas = <RefSchemaProperty[]>(<any>property).anyOf;
            if (!_.isArray(refSchemas)) {
                throw new InvalidArgumentError('anyOf schema property must be an array of schemas.');
            }
            prop.anyOf(buildReferences(refSchemas, foundRefCallback));
        }
    }

    function buildReferences(
        refSchemas: RefSchemaProperty[],
        foundRefCallback?: (refSchema: RefSchemaProperty) => void) : any[] {

        const typeRefs = [];
        for (let i = 0; i < refSchemas.length; i++) {
            typeRefs.push(SchemaHelper.buildRefProperty(refSchemas[i], foundRefCallback));
        }
        return typeRefs;
    }
}
