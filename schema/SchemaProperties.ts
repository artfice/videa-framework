import {JsonSchema} from './JsonSchema';
export interface SchemaProperty {
    name: string;
    title: string;
    help: string;
    required: boolean;
}

export interface PrimitiveSchemaProperty extends SchemaProperty {
    typeName: string;
    defaultValue: any;
    enumValues: any[];
    pattern: string;
}

export interface NumericSchemaProperty extends PrimitiveSchemaProperty {
    multipleOF: number;
    maximum: number;
    exclusiveMaximum: boolean;
    minimum: number;
    exclusiveMinimum: boolean;
}

export interface StringSchemaProperty extends PrimitiveSchemaProperty {
    maxLength: number;
    minLength: number;
    pattern: string;
    format: string;
}

export interface ArraySchemaProperty extends PrimitiveSchemaProperty {
    items: any; // schema or RefSchemaProperty
    maxItems: number;
    minItems: number;
    uniqueItems: number;
    refSchema: RefSchemaProperty;
    anyOf: RefSchemaProperty[];
    oneOf: RefSchemaProperty[];
}

export interface ObjectSchemaProperty extends PrimitiveSchemaProperty {
    maxProperties: number;
    minProperties: number;
    refSchema: RefSchemaProperty;
    anyOf: RefSchemaProperty[];
    oneOf: RefSchemaProperty[];
    schema: JsonSchema;
}

export interface RefSchemaProperty extends SchemaProperty {
    refSchemaId: string;
}

export interface JsonSchemaProperty extends SchemaProperty {
    schema: JsonSchema;
}

/**
 * A reference to a schema which values need to be loaded from the given dynamicEnum url
 */
export interface DynamicEnumRefSchemaProperty extends RefSchemaProperty {
    dynamicEnumUrl: string; // URL ot load the object values
}
