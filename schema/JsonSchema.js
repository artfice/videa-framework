"use strict";
var _ = require("lodash");
var Digi = require("../Digi");
var SchemaHelper_1 = require("./SchemaHelper");
var VideaError_1 = require("../VideaError");
var JsonSchema = (function () {
    function JsonSchema() {
        this._properties = {};
        this._required = [];
        this._schemaRefs = [];
    }
    JsonSchema.prototype.setId = function (id) {
        this._id = id;
    };
    JsonSchema.prototype.getId = function () {
        return this._id;
    };
    JsonSchema.prototype.setName = function (name) {
        this._name = name;
    };
    JsonSchema.prototype.getName = function () {
        return this._name;
    };
    JsonSchema.prototype.setTitle = function (title) {
        this._title = title;
    };
    JsonSchema.prototype.getTitle = function () {
        return this._title;
    };
    JsonSchema.prototype.setDescription = function (description) {
        this._description = description;
    };
    JsonSchema.prototype.getDescription = function () {
        return this._description;
    };
    JsonSchema.prototype.getSchemaRefs = function () {
        return this._schemaRefs;
    };
    JsonSchema.prototype.addSchemaRefs = function (refs) {
        this._schemaRefs.concat(refs);
    };
    JsonSchema.prototype.getSchema = function () {
        if (!this._compiledSchema) {
            this._compiledSchema = SchemaHelper_1.SchemaHelper.buildSchema(this._properties, this._required, this._additionalProperties);
        }
        return this._compiledSchema;
    };
    JsonSchema.prototype.setAdditionalProperties = function (additionalProperties) {
        this._additionalProperties = additionalProperties;
    };
    JsonSchema.prototype.addPrimitiveProperty = function (property) {
        this._addProperty(property, SchemaHelper_1.SchemaHelper.buildPropertyExtended);
    };
    JsonSchema.prototype.addRefProperty = function (property) {
        this._addProperty(property, SchemaHelper_1.SchemaHelper.buildRefProperty);
    };
    JsonSchema.prototype.addDynamicEnumRefProperty = function (property) {
        this._addProperty(property, SchemaHelper_1.SchemaHelper.buildDynamicEnumRefProperty);
    };
    JsonSchema.prototype.addSchemaProperty = function (property) {
        if (_.isUndefined(property.schema)) {
            throw new VideaError_1.InvalidArgumentError('Schema must be valid');
        }
        this._addProperty(property, SchemaHelper_1.SchemaHelper.buildSchemaProperty);
    };
    JsonSchema.prototype.getProperties = function () {
        return this._properties;
    };
    JsonSchema.prototype.getInstance = function (config) {
        var instance = {};
        var schema = this.getSchema();
        var schemaProperties = schema.properties;
        var propertyNames = Object.keys(schemaProperties);
        var nProperties = propertyNames.length;
        var property;
        var propertyType;
        var propertyValue;
        var validType;
        var defaultPropertyGetter;
        for (var i = 0; i < nProperties; i++) {
            property = schemaProperties[propertyNames[i]];
            propertyType = property.type;
            propertyValue = JsonSchema.DEFAULT_TYPE_VALUES[property.type];
            if (config.hasOwnProperty(propertyNames[i]) && config[propertyNames[i]] !== undefined) {
                if (!this._isTypeValid(config[propertyNames[i]], propertyType)) {
                    throw new VideaError_1.SchemaError('invalid type ' + (typeof config[propertyNames[i]]) + ' in ' + config);
                }
                propertyValue = config[propertyNames[i]];
            }
            defaultPropertyGetter = this['getDefault' + Digi.String.capitalize(propertyNames[i])];
            if (typeof defaultPropertyGetter === 'function') {
                propertyValue = defaultPropertyGetter.apply(this, []);
                validType = this._isTypeValid(property, propertyType);
                if (!validType) {
                    throw new VideaError_1.SchemaError('Default Property Value not matching type from ' +
                        'schema. Property: ' + propertyNames[i] +
                        ' Type: ' + propertyType +
                        ' Default Value: ' + property);
                }
            }
            instance[propertyNames[i]] = propertyValue;
        }
        return instance;
    };
    JsonSchema.prototype._addProperty = function (property, createPropFunc) {
        // clean to force schema recompilation
        this._compiledSchema = null;
        if (this._properties.hasOwnProperty(property.name)) {
            throw new VideaError_1.SchemaError('Property ' + property.name + ' already exists');
        }
        var self = this;
        var prop = createPropFunc(property, function (refSchema) {
            self._cacheSchemaRef(refSchema);
        });
        this._properties[property.name] = prop;
        if (property.required) {
            this._required.push(property.name);
        }
        return prop;
    };
    JsonSchema.prototype._isTypeValid = function (propertyValue, typeName) {
        return JsonSchema.TYPESCRIPT_TYPE_MAPPING[typeof propertyValue].indexOf(typeName) > -1;
    };
    JsonSchema.prototype._cacheSchemaRef = function (property) {
        var refTypeName = property.refSchemaId;
        if (this._schemaRefs.indexOf(refTypeName) < 0) {
            this._schemaRefs.push(refTypeName);
        }
    };
    return JsonSchema;
}());
JsonSchema.ID = 'schema.JsonSchema';
JsonSchema.DEFAULT_TYPE_VALUES = {
    string: '',
    integer: 0,
    number: 0,
    boolean: false,
    array: [],
    object: null
};
JsonSchema.TYPESCRIPT_TYPE_MAPPING = {
    string: ['string'],
    number: ['number', 'integer'],
    boolean: ['boolean'],
    array: ['array'],
    object: ['object', 'null']
};
exports.JsonSchema = JsonSchema;
