"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var VideaError = (function (_super) {
    __extends(VideaError, _super);
    function VideaError(name, message, innerErrorMessage) {
        var _this = this;
        if (innerErrorMessage) {
            message = message + ' caused by ' + innerErrorMessage;
        }
        _this = _super.call(this, message) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = VideaError.prototype;
        _this.name = name;
        _this.message = message;
        _this.stack = new Error(name + ': ' + message).stack;
        return _this;
    }
    return VideaError;
}(Error));
exports.VideaError = VideaError;
var InternalServerError = (function (_super) {
    __extends(InternalServerError, _super);
    function InternalServerError(message, innerErrorMessage) {
        var _this = _super.call(this, 'InternalServerError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = InternalServerError.prototype;
        return _this;
    }
    return InternalServerError;
}(VideaError));
exports.InternalServerError = InternalServerError;
var DuplicatedItemIdError = (function (_super) {
    __extends(DuplicatedItemIdError, _super);
    function DuplicatedItemIdError(message, innerErrorMessage) {
        var _this = _super.call(this, 'DuplicatedItemIdError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = DuplicatedItemIdError.prototype;
        return _this;
    }
    return DuplicatedItemIdError;
}(VideaError));
exports.DuplicatedItemIdError = DuplicatedItemIdError;
var InvalidArgumentError = (function (_super) {
    __extends(InvalidArgumentError, _super);
    function InvalidArgumentError(message, innerErrorMessage) {
        var _this = _super.call(this, 'InvalidArgumentError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = InvalidArgumentError.prototype;
        return _this;
    }
    return InvalidArgumentError;
}(VideaError));
exports.InvalidArgumentError = InvalidArgumentError;
var ItemNotFoundError = (function (_super) {
    __extends(ItemNotFoundError, _super);
    function ItemNotFoundError(message, innerErrorMessage) {
        var _this = _super.call(this, 'ItemNotFoundError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = ItemNotFoundError.prototype;
        return _this;
    }
    return ItemNotFoundError;
}(VideaError));
exports.ItemNotFoundError = ItemNotFoundError;
var NotImplementedError = (function (_super) {
    __extends(NotImplementedError, _super);
    function NotImplementedError(message, innerErrorMessage) {
        var _this = _super.call(this, 'NotImplementedError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = NotImplementedError.prototype;
        return _this;
    }
    return NotImplementedError;
}(VideaError));
exports.NotImplementedError = NotImplementedError;
var SecurityError = (function (_super) {
    __extends(SecurityError, _super);
    function SecurityError(message, innerErrorMessage) {
        var _this = _super.call(this, 'SecurityError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = SecurityError.prototype;
        return _this;
    }
    return SecurityError;
}(VideaError));
exports.SecurityError = SecurityError;
var SchemaError = (function (_super) {
    __extends(SchemaError, _super);
    function SchemaError(message, innerErrorMessage) {
        var _this = _super.call(this, 'SchemaError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = SchemaError.prototype;
        return _this;
    }
    return SchemaError;
}(VideaError));
exports.SchemaError = SchemaError;
var LambdaError = (function (_super) {
    __extends(LambdaError, _super);
    function LambdaError(message, innerErrorMessage) {
        var _this = _super.call(this, 'LambdaError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = LambdaError.prototype;
        return _this;
    }
    return LambdaError;
}(VideaError));
exports.LambdaError = LambdaError;
var ExternalResourceError = (function (_super) {
    __extends(ExternalResourceError, _super);
    function ExternalResourceError(message, innerErrorMessage) {
        var _this = _super.call(this, 'ExternalResourceError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = ExternalResourceError.prototype;
        return _this;
    }
    return ExternalResourceError;
}(VideaError));
exports.ExternalResourceError = ExternalResourceError;
var JSONParseError = (function (_super) {
    __extends(JSONParseError, _super);
    function JSONParseError(message, innerErrorMessage) {
        var _this = _super.call(this, 'JSONParseError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = JSONParseError.prototype;
        return _this;
    }
    return JSONParseError;
}(VideaError));
exports.JSONParseError = JSONParseError;
var InvalidWorkflowStateError = (function (_super) {
    __extends(InvalidWorkflowStateError, _super);
    function InvalidWorkflowStateError(message, innerErrorMessage) {
        var _this = _super.call(this, 'InvalidWorkflowStateError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = InvalidWorkflowStateError.prototype;
        return _this;
    }
    return InvalidWorkflowStateError;
}(VideaError));
exports.InvalidWorkflowStateError = InvalidWorkflowStateError;
var TranslationError = (function (_super) {
    __extends(TranslationError, _super);
    function TranslationError(message, innerErrorMessage) {
        var _this = _super.call(this, 'TranslationError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = TranslationError.prototype;
        return _this;
    }
    return TranslationError;
}(VideaError));
exports.TranslationError = TranslationError;
var ScreenError = (function (_super) {
    __extends(ScreenError, _super);
    function ScreenError(message, innerErrorMessage) {
        var _this = _super.call(this, 'ScreenError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = ScreenError.prototype;
        return _this;
    }
    return ScreenError;
}(VideaError));
exports.ScreenError = ScreenError;
var ValidationError = (function (_super) {
    __extends(ValidationError, _super);
    function ValidationError(message, innerErrorMessage) {
        var _this = _super.call(this, 'ValidationError', message, innerErrorMessage) || this;
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        _this.__proto__ = ValidationError.prototype;
        return _this;
    }
    return ValidationError;
}(VideaError));
exports.ValidationError = ValidationError;
