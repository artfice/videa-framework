Changelog
------------

- [*MAJOR*] Backward incompatible changes, it means clients need to upgrade to work
- [*MINOR*] Backward compatible changes, usually new functionalities, new api endpoints etc, clients do not have to upgrade to work
- [*PATCH*] No new features/functionalities introduced, such as bug fixing, code refactoring etc

Example: if we make a code change which to fix a bug, before submitting a new PR, we should update this Changelog.md file and add an entry under alpha section like below:

[PATCH] Fix a string comparison bug in BaseModel.

Once we want to release a new version, we will examine all changes in the alpha section and determine the proper version number.

alpha
-----


0.6.0
-----
- [MAJOR] Fixed image service uploadByUrl function signature
- [MINOR] Added utility to require modules in runtime
- [MINOR] Generic helper for extracting search parameters from lambdas requests
- [MINOR] Added count method to repositories
- [PATCH] Fixed remote path in AWS S3 repository
- [PATCH] Add create image transformation on ImageTypeService
- [PATCH] Introduced mongodb ElementMatch query
- [PATCH] Refactor updateOnCollection in DtoMongoDbAdapter  
- [PATCH] Add pullFromElement update option to remove elements from nested arrays
- [PATCH] Add unset update option to remove properties from elements
- [PATCH] Added deepOmit to digi class
- [PATCH] Fix extract id in BaseDTOHandler
- [PATCH] Fix a compile error in digi class

0.5.0
-----
- [PATCH] Added ImageType schema
- [PATCH] Added getRepository to AccountBasedRepository
- [PATCH] Fix mongodb QueryHelper
- [MAJOR] Image system refatored to split cloudinary adpater responsability on image types and images
- [MINOR] Improved SchemaService to support the new ES6 code exports
- [MINOR] Services validation based on service schemas
- [MINOR] SearchParamsHelper will also look at the request body for extracting search parameters
- [MINOR] Added Regex queries to mongodb
- [MINOR] Updated typescript to version 2.1.6
- [MINOR] Updated tslint to 4.4.2
- [PATCH] Fix pipeline order in aggregate, now it's possible to query with the lookUpField
- [MINOR] Implemented Message repository and adapter to send messages using AWS SQS service
- [PATCH] Fix node typings
- [PATCH] Fix issue when service doesn't have a schema, should ignore the setting of the schema
- [PATCH] Fixed update validation of BasedAccountService
- [PATCH] Fixed ImageType schema
- [MINOR] Added FileManagerService to create files from streams
- [MINOR] Added AWS S3 Adapter to manage remote assets
- [PATCH] Fix aggregate sort of lookup field
- [PATCH] Fixed error message not being sent in the response
- [PATCH] Improved Videa error definition in typescript
- [PATCH] Updated version of winston npm packages
- [PATCH] Fixing missing imports on some files
- [PATCH] Fixed AccountBasedService schema
- [PATCH] Fixed ImageTypeSchema id
- [PATCH] Clean up compile errors and setup npm build tasks
- [MINOR] Structured the repository classes into modules
- [PATCH] Refactor domain folder to use module based import and export
- [PATCH] Refactor handler folder to use module based import and export
- [PATCH] Clean lint errors and Updated WorkerState and WorkerAction to be used as type instead object
- [PATCH] Refactor adapter folder to use module based import and export
- [PATCH] Refactor schema folder to use module based import and export
- [PATCH] Refactor application folder to use module based import and export
- [PATCH] Export main modules in framework index.ts

0.4.0
-----
- [MAJOR]
  
  Extended schema to support more validation options;
  
  Schema service includes global validator;
  
  AccountBasedService includes validation checks for add and update methods
  
  Added missing config files to services and repositories

  Moved cloudinary repository to cloudinary folder
  
  BaseSchemas have IDs
  
  Implemented AccountBasedHandler
  
  Added BaseDtoHandler for base CRUD operations on dto
  
  Added support on mongodb adapter to aggregate documents
  
  Migration of the latest changes on SearchOperators and UpdateOperators from the old backend
  
  Renamed AccountId to TenantId
  
  Added support for systemObjects (objects that can be modified or deleted)
  
  Added helpers for fixtures on unit tests
  
  Added AccountBasedService unit tests
  
  Moved typescript dependency to devDependency on package.json
  
  Added tslint.json
  
  Minor bug fixes and code improvements

0.3.2
-----
- [PATCH] Lockdown dependencies versions in package.json and replaced underscore with lodash

0.3.1
-----
- [PATCH] Refactored base repositories and created base service

0.3.0
-----
- [MINOR] Updated callback of handlers that wasn't retrieving results to the request.
- Updated package.json to 0.3.0

0.2.1
-----
- [PATCH] Code cleanup and changed the way the GenericParser is instantiated.

0.2.0
-----
- [MINOR] Added base64 image upload functionality to the HttpHandler in order to get around the API gateway limitation
- Updated package.json to 0.2.0

0.1.0
-----
