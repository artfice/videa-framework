import * as Promise from 'bluebird';

import {Logger} from '../Logger';
import {JsonSchema} from '../schema/JsonSchema';
import {Schema} from '../schema/Schema';
import {SchemaError} from '../VideaError';
import {SchemaApplicationServiceConfig} from './config/SchemaApplicationServiceConfig';
import {ModuleImporter} from './helper/ModuleImporter';
import {SchemaValidator} from './SchemaValidator';

export class SchemaApplicationService {

    private _schemas = {};
    private _validator: SchemaValidator;
    private _schemaImporter: ModuleImporter;

    public constructor(config: SchemaApplicationServiceConfig) {
        this._schemaImporter = new ModuleImporter(config.schemaLocations);
        this._validator = new SchemaValidator(this);
    }

    public getSchema(schemaId: string): Promise<Schema> {
        return new Promise<Schema>((resolve, reject) => {
            try {
                const jsonSchema = this.getJsonSchema(schemaId);
                resolve(jsonSchema.getSchema());
            } catch (e) {
                reject(e);
            }
        });
    }

    public getJsonSchema(schemaId: string) : JsonSchema {
        let instance = this._schemas[schemaId];
        if (instance) {
            return instance;
        }

        const Schema = this._schemaImporter.importById(schemaId);
        if (!Schema) {
            Logger.error('Schema "' + schemaId + '" not found!');
            throw new SchemaError('Schema ' + schemaId + ' not found!');
        }

        instance = new Schema();
        this._schemas[schemaId] = instance;

        return instance;
    }

    public getGlobalValidator() : SchemaValidator {
        return this._validator;
    }
}
