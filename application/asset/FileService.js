"use strict";
var Promise = require("bluebird");
var uuid = require("node-uuid");
var streamifier = require("streamifier");
var VideaError_1 = require("../../VideaError");
var FileService = (function () {
    function FileService(config) {
        this._remoteAssetService = config.remoteAssetService;
        this._basePath = config.basePath;
    }
    FileService.prototype.uploadFile = function (file) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!file.name) {
                return reject(new VideaError_1.InternalServerError('File name is required'));
            }
            if (!file.buffer) {
                return reject(new VideaError_1.InternalServerError('No content found in file "' + file.name + '"'));
            }
            var stream = streamifier.createReadStream(file.buffer);
            return _this._remoteAssetService.upload({
                assetId: _this.getAssetKey(uuid.v1(), file.name),
                containerId: _this._basePath
            }, stream).then(function (result) {
                return resolve(result);
            }).catch(function (err) {
                return reject(err);
            });
        });
    };
    FileService.prototype.getAssetKey = function (id, name) {
        var lastIndex = name.lastIndexOf('.');
        var extension = '';
        if (lastIndex > -1) {
            extension = '.' + name.substr(lastIndex + 1);
        }
        return id + extension;
    };
    FileService.prototype.getFile = function (assetKey) {
        return this._remoteAssetService.get(this._basePath, assetKey);
    };
    return FileService;
}());
exports.FileService = FileService;
