import * as Promise from 'bluebird';
import * as uuid from 'node-uuid';
import streamifier = require('streamifier');

import {FileAsset, RemoteAsset} from '../../domain';
import {IRemoteAssetRepository} from '../../repository/IRemoteAssetRepository';
import {InternalServerError} from '../../VideaError';
import {FileServiceConfig} from './config/FileServiceConfig';

export class FileService {

    private _remoteAssetService: IRemoteAssetRepository;
    private _basePath: string;

    constructor(config: FileServiceConfig) {
        this._remoteAssetService = config.remoteAssetService;
        this._basePath = config.basePath;
    }

    public uploadFile(file: FileAsset): Promise<string> {
        return new Promise<string>((resolve, reject) => {

            if (!file.name) {
                return reject(new InternalServerError('File name is required'));
            }

            if (!file.buffer) {
                return reject(new InternalServerError('No content found in file "' + file.name + '"'));
            }

            const stream = streamifier.createReadStream(file.buffer);
            return this._remoteAssetService.upload(<RemoteAsset>{
                assetId : this.getAssetKey(uuid.v1(), file.name),
                containerId : this._basePath
            }, stream).then((result) => {
                return resolve(<any>result);
            }).catch((err) => {
                return reject(err);
            });
        });
    }

    public getAssetKey(id: string, name: string): string {
        const lastIndex = name.lastIndexOf('.');
        let extension = '';

        if (lastIndex > -1) {
            extension = '.' + name.substr(lastIndex + 1);
        }

        return id + extension;
    }

    public getFile(assetKey: string) : any {
        return this._remoteAssetService.get(this._basePath, assetKey);
    }
}
