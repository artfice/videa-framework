import {IRemoteAssetRepository} from '../../../repository/IRemoteAssetRepository';

export interface FileServiceConfig {
    remoteAssetService: IRemoteAssetRepository;
    basePath: string;
}
