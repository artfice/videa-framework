"use strict";
var _ = require("lodash");
var Ajv = require("ajv");
var VideaError_1 = require("../VideaError");
var SchemaValidator = (function () {
    function SchemaValidator(schemaService) {
        this._ajv = new Ajv();
        this._schemaService = schemaService;
    }
    SchemaValidator.prototype.addSchema = function (schema) {
        var id = schema.getId();
        if (_.isUndefined(id)) {
            throw new VideaError_1.SchemaError('Schema\'s Id property is required to add a schema to the validator.');
        }
        if (this._ajv.getSchema(id)) {
            return;
        }
        var s = schema.getSchema();
        this._ajv.addSchema(s, id);
        var schemaRefs = schema.getSchemaRefs();
        if (!_.isEmpty(schemaRefs)) {
            for (var i = 0; i < schemaRefs.length; i++) {
                var refSchemaId = schemaRefs[i];
                this.addSchema(this._schemaService.getJsonSchema(refSchemaId));
            }
        }
    };
    SchemaValidator.prototype.validate = function (schemaId, obj) {
        var result = {
            valid: this._ajv.validate(schemaId, obj)
        };
        if (!result.valid) {
            result.errorsText = this._ajv.errorsText();
        }
        return result;
    };
    return SchemaValidator;
}());
exports.SchemaValidator = SchemaValidator;
