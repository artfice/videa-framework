"use strict";
var Promise = require("bluebird");
var Logger_1 = require("../Logger");
var VideaError_1 = require("../VideaError");
var ModuleImporter_1 = require("./helper/ModuleImporter");
var SchemaValidator_1 = require("./SchemaValidator");
var SchemaApplicationService = (function () {
    function SchemaApplicationService(config) {
        this._schemas = {};
        this._schemaImporter = new ModuleImporter_1.ModuleImporter(config.schemaLocations);
        this._validator = new SchemaValidator_1.SchemaValidator(this);
    }
    SchemaApplicationService.prototype.getSchema = function (schemaId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                var jsonSchema = _this.getJsonSchema(schemaId);
                resolve(jsonSchema.getSchema());
            }
            catch (e) {
                reject(e);
            }
        });
    };
    SchemaApplicationService.prototype.getJsonSchema = function (schemaId) {
        var instance = this._schemas[schemaId];
        if (instance) {
            return instance;
        }
        var Schema = this._schemaImporter.importById(schemaId);
        if (!Schema) {
            Logger_1.Logger.error('Schema "' + schemaId + '" not found!');
            throw new VideaError_1.SchemaError('Schema ' + schemaId + ' not found!');
        }
        instance = new Schema();
        this._schemas[schemaId] = instance;
        return instance;
    };
    SchemaApplicationService.prototype.getGlobalValidator = function () {
        return this._validator;
    };
    return SchemaApplicationService;
}());
exports.SchemaApplicationService = SchemaApplicationService;
