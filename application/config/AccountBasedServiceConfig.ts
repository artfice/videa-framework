import {IAccountBasedRepository} from "../../repository/IAccountBasedRepository";
import {BaseSchema} from '../../schema/BaseSchema';
import {BaseDto} from '../../domain';
import {SchemaValidator} from '../SchemaValidator';

export interface AccountBasedServiceConfig<T extends BaseDto> {
    baseSchema: BaseSchema;
    schemaValidator: SchemaValidator;
    repository: IAccountBasedRepository<T>;
}
