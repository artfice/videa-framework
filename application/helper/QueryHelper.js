"use strict";
var SearchOperators_1 = require("../../domain/db/SearchOperators");
var QueryHelper;
(function (QueryHelper) {
    function mergeQueryToParamsWithAnd(searchParams, searchQuery) {
        var updatedSearchParams = searchParams;
        var query;
        if (searchParams.query) {
            query = QueryHelper.mergeQueriesWithAnd(searchParams.query, searchQuery);
        }
        else {
            query = searchQuery;
        }
        updatedSearchParams.query = query;
        return updatedSearchParams;
    }
    QueryHelper.mergeQueryToParamsWithAnd = mergeQueryToParamsWithAnd;
    function mergeQueriesWithAnd(query1, query2) {
        var query;
        // safety checks
        if (!query1 || !query1.operator) {
            return query2;
        }
        else if (!query2 || !query2.operator) {
            return query1;
        }
        if (query1.operator === SearchOperators_1.SearchOperators.And) {
            if (!query1.value) {
                query1.value = [];
            }
            query1.value.push(query2);
            query = query1;
        }
        else {
            query = {
                operator: SearchOperators_1.SearchOperators.And,
                value: [query1, query2]
            };
        }
        return query;
    }
    QueryHelper.mergeQueriesWithAnd = mergeQueriesWithAnd;
})(QueryHelper = exports.QueryHelper || (exports.QueryHelper = {}));
