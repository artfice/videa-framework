"use strict";
var _ = require("lodash");
var Logger_1 = require("../../Logger");
var ModuleImporter = (function () {
    function ModuleImporter(locations) {
        this._locations = locations;
    }
    ModuleImporter.prototype.importById = function (id) {
        for (var i = 0; i < this._locations.length; i++) {
            var filePath = this.generatePathFromId(this._locations[i], id);
            try {
                var func = require(filePath);
                if (!_.isFunction(func) && _.isObject(func)) {
                    // try to use the default export or fallback to the class with the same name of the file
                    func = func.default ? func.default : func[this.getNameFromId(id)];
                }
                return func;
            }
            catch (e) {
                if (e.code !== 'MODULE_NOT_FOUND') {
                    // a severe problem loading the fucntion has happened
                    Logger_1.Logger.error('ERROR LOADING FUNCTION', e);
                    throw e;
                }
            }
        }
        return undefined;
    };
    ModuleImporter.prototype.generatePathFromId = function (path, schemaId) {
        var parts = schemaId.split('.').join('/');
        return path + '/' + parts;
    };
    ModuleImporter.prototype.getNameFromId = function (schemaId) {
        var parts = schemaId.split('.');
        return parts[parts.length - 1];
    };
    return ModuleImporter;
}());
exports.ModuleImporter = ModuleImporter;
