import * as _ from 'lodash';
import {Logger} from '../../Logger';

export class ModuleImporter {
    private _locations: string[];

    constructor(locations: string[]) {
        this._locations = locations;
    }

    public importById(id: string): any {
        for (let i = 0; i < this._locations.length; i++) {
            const filePath = this.generatePathFromId(this._locations[i], id);
            try {
                let func = require(filePath);

                if (!_.isFunction(func) && _.isObject(func)) {
                    // try to use the default export or fallback to the class with the same name of the file
                    func = func.default ? func.default : func[this.getNameFromId(id)];
                }
                return func;
            } catch (e) {
                if (e.code !== 'MODULE_NOT_FOUND') {
                    // a severe problem loading the fucntion has happened
                    Logger.error('ERROR LOADING FUNCTION', e);
                    throw e;
                }
                // ignore and try the next location
            }
        }
        return undefined;
    }
    private generatePathFromId(path: string, schemaId: string): string {
        const parts = schemaId.split('.').join('/');
        return path + '/' + parts;
    }

    private getNameFromId(schemaId: string): string {
        const parts = schemaId.split('.');
        return parts[parts.length - 1];
    }
}
