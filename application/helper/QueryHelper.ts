import {SearchOperators} from '../../domain/db/SearchOperators';
import {SearchQuery} from '../../domain/db/SearchQuery';

import {SearchParams} from '../../domain/db/SearchParams';

export module QueryHelper {

    export function mergeQueryToParamsWithAnd(searchParams:  SearchParams, searchQuery: SearchQuery):  SearchParams {
        const updatedSearchParams = searchParams;

        let query: SearchQuery;
        if (searchParams.query) {
            query = QueryHelper.mergeQueriesWithAnd(searchParams.query, searchQuery);
        } else {
            query = searchQuery;
        }

        updatedSearchParams.query = query;
        return updatedSearchParams;
    }

    export function mergeQueriesWithAnd(query1: SearchQuery, query2: SearchQuery): SearchQuery {
        let query: SearchQuery;

        // safety checks
        if (!query1 || !query1.operator) {
            return query2;
        } else if (!query2 || !query2.operator) {
            return query1;
        }

        if (query1.operator === SearchOperators.And) {
            if (!query1.value) {
                query1.value = [];
            }
            query1.value.push(query2);
            query = query1;

        } else {
            query = <SearchQuery> {
                operator: SearchOperators.And,
                value: [query1, query2]
            };
        }

        return query;
    }
}
