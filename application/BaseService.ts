import Promise = require('bluebird');
import {BaseDto} from '../domain';
import {ResultSet, SearchParams, UpdateResult} from '../domain/db';
import {IRepository} from '../repository/IRepository';

export class BaseService<T extends BaseDto> {
    protected _repository : IRepository<T>;

    public add(dto: T) : Promise<T> {
        return this._repository.add(dto);
    }

    public update(id: string, dto: T) : Promise<UpdateResult> {
        return this._repository.update(id, dto);
    }

    public replace(id: string, dto: T) : Promise<UpdateResult> {
        return this._repository.replace(id, dto);
    }

    public remove(id: string) : Promise<void> {
        return this._repository.remove(id);
    }

    public getById(id: string) : Promise<T> {
        return this._repository.getById(id);
    }

    public batchGet(uiConfigs: string[]): Promise<T[]> {
        return this._repository.batchGet(uiConfigs);
    }

    public bulkAdd(dtos: T[]): Promise<any> {
        return this._repository.bulkAdd(dtos);
    }

    public resetData(): Promise<void> {
        return this._repository.resetData();
    }

    public search(options: SearchParams) : Promise<ResultSet> {
       return this._repository.search(options);
    }
}
