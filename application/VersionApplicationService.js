"use strict";
var fs = require("fs");
var VersionApplicationService = (function () {
    function VersionApplicationService() {
    }
    VersionApplicationService.prototype.getPackageVersion = function () {
        var json = JSON.parse(fs.readFileSync('package.json', 'utf8'));
        return {
            version: json.version,
            build: json.build
        };
    };
    return VersionApplicationService;
}());
exports.VersionApplicationService = VersionApplicationService;
