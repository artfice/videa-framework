import fs = require('fs');

export class VersionApplicationService {

    public getPackageVersion(): any {
        const json = JSON.parse(fs.readFileSync('package.json', 'utf8'));
        return {
            version: json.version,
            build: json.build
        };
    }
}
