"use strict";
var Promise = require("bluebird");
var _ = require("lodash");
var SearchOperators_1 = require("../domain/db/SearchOperators");
var VideaError_1 = require("../VideaError");
var QueryHelper_1 = require("./helper/QueryHelper");
var application_1 = require("../schema/application");
var systemObjectQueryFilter = { field: 'systemObject', value: true, operator: SearchOperators_1.SearchOperators.NE };
var AccountBasedService = (function () {
    function AccountBasedService(config) {
        this._schemaValidator = config.schemaValidator;
        this._repository = config.repository;
        this.setBaseSchema(config.baseSchema);
    }
    AccountBasedService.prototype.setSchemaValidator = function (schemaValidator) {
        this._schemaValidator = schemaValidator;
    };
    AccountBasedService.prototype.getSchemaValidator = function () {
        return this._schemaValidator;
    };
    AccountBasedService.prototype.setBaseSchema = function (schema) {
        if (_.isUndefined(schema)) {
            return; // should ignore this if the service doesn't have schema
        }
        if (_.isUndefined(schema.getId())) {
            throw new VideaError_1.InvalidArgumentError('Schema Id\'s property is required to set as base schema on the AccountBasedService');
        }
        this._baseSchema = schema;
        this._serviceSchema = this.createServiceSchema(schema);
        if (this._schemaValidator && schema) {
            this._schemaValidator.addSchema(schema);
            this._schemaValidator.addSchema(this._serviceSchema);
        }
    };
    AccountBasedService.prototype.getRepository = function () {
        return this._repository;
    };
    AccountBasedService.prototype.add = function (tenantId, dto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.validationFilter({ add: { tenantId: tenantId, dto: dto } });
            return _this._repository.add(tenantId, dto).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.update = function (tenantId, id, dto, options, query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            query = _.isUndefined(query) ? systemObjectQueryFilter : QueryHelper_1.QueryHelper.mergeQueriesWithAnd(query, systemObjectQueryFilter);
            _this.validationFilter({ update: { tenantId: tenantId, id: id, dto: dto, updateOptions: options, searchQuery: query } });
            return _this._repository.update(tenantId, id, dto, options, query).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.replace = function (tenantId, id, dto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.validationFilter({ replace: { tenantId: tenantId, id: id, dto: dto } });
            return _this._repository.replace(tenantId, id, dto).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.remove = function (tenantId, id, query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.validationFilter({ remove: { tenantId: tenantId, id: id, searchQuery: query } });
            return _this._repository.remove(tenantId, id, query).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.getById = function (tenantId, id, options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.validationFilter({ getById: { tenantId: tenantId, id: id, searchOptions: options } });
            return _this._repository.getById(tenantId, id, options).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.batchGet = function (tenantId, ids) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.validationFilter({ batchGet: { tenantId: tenantId, ids: ids } });
            return _this._repository.batchGet(tenantId, ids).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.search = function (tenantId, options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var call = { search: { tenantId: tenantId, searchParams: options } };
            _this.validationFilter(call);
            return _this._repository.search(tenantId, options).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.bulkAdd = function (tenantId, dtos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.validationFilter({ bulkAdd: { tenantId: tenantId, dtos: dtos } });
            return _this._repository.bulkAdd(tenantId, dtos).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.resetData = function (tenantId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.validationFilter({ resetData: { tenantId: tenantId } });
            return _this._repository.resetData(tenantId).then(resolve).catch(reject);
        });
    };
    AccountBasedService.prototype.createServiceSchema = function (schema) {
        return new application_1.AccountBasedServiceSchema(schema);
    };
    AccountBasedService.prototype.getAccountError = function (tenantId, err) {
        err.message = 'Error on account ' + tenantId + ' caused by: ' + err.message;
        return err;
    };
    AccountBasedService.prototype.validationFilter = function (value) {
        if (this._baseSchema && this._schemaValidator) {
            var validatorResults = this._schemaValidator.validate(this._serviceSchema.getId(), value);
            if (!_.isUndefined(validatorResults.errorsText)) {
                throw this.getAccountError(value ? value.tenantId : undefined, new VideaError_1.ValidationError('Error validation DTO with schema ID ' + this._baseSchema.getId() +
                    ' caused by: ' + validatorResults.errorsText));
            }
        }
    };
    return AccountBasedService;
}());
exports.AccountBasedService = AccountBasedService;
