import * as Promise from 'bluebird';
import * as _ from 'lodash';

import {BaseDto} from '../domain';
import {SearchOperators} from '../domain/db/SearchOperators';
import {SearchOptions} from '../domain/db/SearchOptions';
import {SearchParams} from '../domain/db/SearchParams';
import {SearchQuery} from '../domain/db/SearchQuery';
import {UpdateOptions} from '../domain/db/UpdateOptions';
import {IAccountBasedRepository} from '../repository/IAccountBasedRepository';
import {BaseSchema} from '../schema/BaseSchema';
import {InvalidArgumentError, ValidationError, VideaError} from '../VideaError';
import {AccountBasedServiceConfig} from './config/AccountBasedServiceConfig';
import {QueryHelper} from './helper/QueryHelper';
import {SchemaValidator} from './SchemaValidator';
import {UpdateResult} from '../domain/db/UpdateResult';
import {ResultSet} from '../domain/db/ResultSet';
import {AccountBasedServiceSchema} from '../schema/application';

const systemObjectQueryFilter = <SearchQuery> {field : 'systemObject', value : true, operator : SearchOperators.NE};

export class AccountBasedService<T extends BaseDto> {
    private _baseSchema: BaseSchema;
    private _repository : IAccountBasedRepository<T>;
    private _schemaValidator: SchemaValidator;
    private _serviceSchema: AccountBasedServiceSchema;

    constructor(config: AccountBasedServiceConfig<T>) {
        this._schemaValidator = config.schemaValidator;
        this._repository = config.repository;
        this.setBaseSchema(config.baseSchema);
    }

    public setSchemaValidator(schemaValidator: SchemaValidator) : void {
        this._schemaValidator = schemaValidator;
    }

    public getSchemaValidator(): SchemaValidator {
        return this._schemaValidator;
    }

    public setBaseSchema(schema: BaseSchema) : void {
        if (_.isUndefined(schema)) {
            return; // should ignore this if the service doesn't have schema
        }

        if (_.isUndefined(schema.getId())) {
            throw new InvalidArgumentError('Schema Id\'s property is required to set as base schema on the AccountBasedService');
        }
        this._baseSchema = schema;
        this._serviceSchema = this.createServiceSchema(schema);
        if (this._schemaValidator && schema) {
            this._schemaValidator.addSchema(schema);
            this._schemaValidator.addSchema(this._serviceSchema);
        }
    }

    public getRepository(): IAccountBasedRepository<T> {
        return this._repository;
    }

    public add(tenantId: string, dto: T): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.validationFilter({ add: { tenantId: tenantId, dto: dto }});
            return this._repository.add(tenantId, dto).then(resolve).catch(reject);
        });
    }

    public update(tenantId: string, id: string, dto: T, options?: UpdateOptions, query?: SearchQuery): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            query = _.isUndefined(query) ? systemObjectQueryFilter : QueryHelper.mergeQueriesWithAnd(query, systemObjectQueryFilter);
            this.validationFilter({ update: { tenantId: tenantId, id: id, dto: dto, updateOptions: options, searchQuery: query } });
            return this._repository.update(tenantId, id, dto, options, query).then(resolve).catch(reject);
        });
    }

    public replace(tenantId: string, id: string, dto: T): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            this.validationFilter({ replace: { tenantId: tenantId, id: id, dto: dto } });
            return this._repository.replace(tenantId, id, dto).then(resolve).catch(reject);
        });
    }

    public remove(tenantId: string, id: string, query?: SearchQuery): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.validationFilter({remove: {tenantId: tenantId, id: id, searchQuery: query}});
            return this._repository.remove(tenantId, id, query).then(resolve).catch(reject);
        });
    }

    public getById(tenantId: string, id: string, options?: SearchOptions): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.validationFilter({getById: {tenantId: tenantId, id: id, searchOptions: options}});
            return this._repository.getById(tenantId, id, options).then(resolve).catch(reject);
        });
    }

    public batchGet(tenantId: string, ids: string[]): Promise<T[]> {
        return new Promise<T[]>((resolve, reject) => {
            this.validationFilter({batchGet: {tenantId: tenantId, ids: ids}});
            return this._repository.batchGet(tenantId, ids).then(resolve).catch(reject);
        });
    }
    public search(tenantId: string, options: SearchParams): Promise<ResultSet> {
        return new Promise<ResultSet>((resolve, reject) => {
            const call = {search: {tenantId: tenantId, searchParams: options}};
            this.validationFilter(call);
            return this._repository.search(tenantId, options).then(resolve).catch(reject);
        });
    }

    public bulkAdd(tenantId: string, dtos: T[]): Promise<any> {
        return new Promise<UpdateResult>((resolve, reject) => {
            this.validationFilter({bulkAdd: {tenantId: tenantId, dtos: dtos}});
            return this._repository.bulkAdd(tenantId, dtos).then(resolve).catch(reject);
        });
    }

    public resetData(tenantId: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.validationFilter({resetData: {tenantId: tenantId}});
            return this._repository.resetData(tenantId).then(resolve).catch(reject);
        });
    }

    protected createServiceSchema(schema: BaseSchema): AccountBasedServiceSchema {
        return new AccountBasedServiceSchema(schema);
    }

    protected getAccountError(tenantId: string, err: VideaError): VideaError {
        err.message = 'Error on account ' + tenantId + ' caused by: ' + err.message;
        return err;
    }

    protected validationFilter(value: any) {
        if (this._baseSchema && this._schemaValidator) {
            const validatorResults = this._schemaValidator.validate(this._serviceSchema.getId(), value);
            if (!_.isUndefined(validatorResults.errorsText)) {
                throw this.getAccountError(
                    value ? value.tenantId : undefined,
                    new ValidationError('Error validation DTO with schema ID ' + this._baseSchema.getId() +
                        ' caused by: ' + validatorResults.errorsText));
            }
        }
    }
}
