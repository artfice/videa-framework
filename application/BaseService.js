"use strict";
var BaseService = (function () {
    function BaseService() {
    }
    BaseService.prototype.add = function (dto) {
        return this._repository.add(dto);
    };
    BaseService.prototype.update = function (id, dto) {
        return this._repository.update(id, dto);
    };
    BaseService.prototype.replace = function (id, dto) {
        return this._repository.replace(id, dto);
    };
    BaseService.prototype.remove = function (id) {
        return this._repository.remove(id);
    };
    BaseService.prototype.getById = function (id) {
        return this._repository.getById(id);
    };
    BaseService.prototype.batchGet = function (uiConfigs) {
        return this._repository.batchGet(uiConfigs);
    };
    BaseService.prototype.bulkAdd = function (dtos) {
        return this._repository.bulkAdd(dtos);
    };
    BaseService.prototype.resetData = function () {
        return this._repository.resetData();
    };
    BaseService.prototype.search = function (options) {
        return this._repository.search(options);
    };
    return BaseService;
}());
exports.BaseService = BaseService;
