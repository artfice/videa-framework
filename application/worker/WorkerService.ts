import * as Promise from 'bluebird';
import * as _ from 'lodash';

import {ResultSet} from '../../domain/db/ResultSet';
import {SearchParams} from '../../domain/db/SearchParams';
import {UpdateResult} from '../../domain/db/UpdateResult';
import {Message} from '../../domain/message/Message';
import {Worker} from '../../domain/worker/Worker';
import {WorkerAction} from '../../domain/worker/WorkerAction';
import {WorkerEvent} from '../../domain/worker/WorkerEvent';
import {WorkerState} from '../../domain/worker/WorkerState';
import {IMessageRepository} from '../../repository/IMessageRepository';
import {IRepository} from '../../repository/IRepository';
import {WorkerServiceSchema} from '../../schema/application/WorkerServiceSchema';
import {ItemNotFoundError, ValidationError} from '../../VideaError';
import {SchemaValidator} from '../SchemaValidator';
import {WorkerServiceConfig} from './config/WorkerServiceConfig';

export class WorkerService {
    private _messageRepository: IMessageRepository;
    private _repository: IRepository<Worker>;
    private _schemaValidator: SchemaValidator;
    private _serviceSchema: WorkerServiceSchema;

    constructor(config: WorkerServiceConfig) {
        this.messageRepository = config.messageRepository;
        this.repository = config.repository;
        this.schemaValidator = config.schemaValidator;

        if (this._schemaValidator) {
            this._serviceSchema = new WorkerServiceSchema();
            this._schemaValidator.addSchema(this._serviceSchema);
        }
    }

    public get repository(): IRepository<Worker> {
        return this._repository;
    }
    public set repository(repository: IRepository<Worker>) {
        this._repository = repository;
    }

    public get messageRepository(): IMessageRepository {
        return this._messageRepository;
    }
    public set messageRepository(repository: IMessageRepository) {
        this._messageRepository = repository;
    }

    public get schemaValidator(): SchemaValidator {
        return this._schemaValidator;
    }
    public set schemaValidator(validator: SchemaValidator) {
        this._schemaValidator = validator;
    }

    public pushWorkerToQueue(worker: Worker): Promise<Worker> {
        return new Promise<Worker>((resolve, reject) => {
            return this.validationFilter({ start: {worker: worker} }).then(() => {
                worker.state = WorkerState.Pending;
                return this._repository.add(worker);
            }).then((workerResult) => {
                if (this._messageRepository) {
                    const message = <Message>{
                        body: JSON.stringify(<WorkerEvent>{
                            workerId: workerResult.id,
                            action: WorkerAction.Start
                        })
                    };
                    return this._messageRepository.sendMessage(message).then((msgResult) => {
                        resolve(workerResult);
                    }).catch(reject);
                } else {
                    resolve(workerResult);
                }
            }).catch(reject);
        });
    }

    public pullWorkerFromQueue(): Promise<Worker> {
        return new Promise<Worker>((resolve, reject) => {
            this._messageRepository.pullMessage().then((message) => {
                if (message) {
                    const workerId = message.body;
                    return this.getById(workerId).then(resolve).catch(reject);
                } else {
                    reject(new ItemNotFoundError('Worker not found in the queue.'));
                    return;
                }
            }).catch(reject);
        });
    }

    public getById(id: string): Promise<Worker> {
        return new Promise<Worker>((resolve, reject) => {
            return this.validationFilter({ getById: {id: id} }).then(() => {
                return this._repository.getById(id);
            }).then(resolve).catch(reject);
        });
    }

    public search(options: SearchParams): Promise<ResultSet> {
        return new Promise<ResultSet>((resolve, reject) => {
            return this.validationFilter({ search: {searchParams: options} }).then(() => {
                return this._repository.search(options);
            }).then(resolve).catch(reject);
        });
    }

    public update(id: string, worker: Worker): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            this.validationFilter({ updateState: { id: id, worker: worker } });
            return this._repository.update(id, worker).then(resolve).catch(reject);
        });
    }

    protected validationFilter(value: any): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            if (this._schemaValidator) {
                const validatorResults = this._schemaValidator.validate(this._serviceSchema.getId(), value);
                if (!_.isUndefined(validatorResults.errorsText)) {
                    reject(new ValidationError('Error validating service schema ' + this._serviceSchema.getId() +
                            ' caused by: ' + validatorResults.errorsText));
                } else {
                    resolve(undefined);
                }
            }
        });
    }
}
