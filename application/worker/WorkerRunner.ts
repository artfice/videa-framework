import * as Promise from 'bluebird';
import * as _ from 'lodash';

import {UpdateResult} from '../../domain/db/UpdateResult';
import {Worker} from '../../domain/worker/Worker';
import {WorkerFunction} from '../../domain/worker/WorkerFunction';
import {WorkerState} from '../../domain/worker/WorkerState';
import {Logger} from '../../Logger';
import {InvalidArgumentError} from '../../VideaError';
import {ModuleImporter} from '../helper/ModuleImporter';
import {WorkerRunnerConfig} from './config/WorkerRunnerConfig';
import {WorkerService} from './WorkerService';

export interface WorkerUpdateCallback { (state: Worker): Promise<UpdateResult>; }

export class WorkerRunner {

    private _workerService: WorkerService;
    private _workerHandlers: any;
    private _workerImporter: ModuleImporter;

    constructor(config: WorkerRunnerConfig) {
        this._workerImporter = new ModuleImporter(config.workerLocations);
        this._workerService = config.workerService;
        this._workerHandlers = {};
    }

    public startById(workerId: string): Promise<Promise<void>> {
        return new Promise<Promise<void>>((resolve, reject) => {
            return this._workerService.getById(workerId).then((worker) => {
                try {
                    const workerHandler = this.loadWorkerHandler(worker);
                    resolve(this.run(worker, workerHandler));
                } catch (err) {
                    reject (err);
                }
            });
        });
    }

    public run(worker: Worker, workerHandler?: WorkerFunction): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            try {
                if (!workerHandler) {
                    workerHandler = this.loadWorkerHandler(worker);
                }
                if (_.isUndefined(worker.progress)) {
                    worker.progress = 0;
                }
                return this._workerService.update(worker.id, <Worker>{ state: WorkerState.Running, progress: worker.progress })
                    .then((result) => {
                    return workerHandler(worker, (state) => {
                        if (state.progress > 1) {
                            state.progress = 1;
                        }
                        return this._workerService.update(worker.id, <Worker>{progress: state.progress});
                    });
                }).then(() => {
                    return this._workerService.update(worker.id, <Worker>{ state: WorkerState.Completed });
                }).then((result) => {
                    resolve(undefined);
                }).catch((err) => {
                    return this._workerService.update(worker.id, <Worker>{ state: WorkerState.Failed, error: err })
                        .finally(() => {
                            reject(err);
                        });
                });
            } catch (err) {
                reject (err);
            }
        });
    }

    private loadWorkerHandler(worker: Worker) : WorkerFunction {
        const workerName = worker.taskName;
        let handler = this._workerHandlers[workerName];
        if (handler) {
            return handler;
        }

        handler = <WorkerFunction>this._workerImporter.importById(workerName);
        if (!handler) {
            Logger.error('Worker "' + workerName + '" not found!');
            throw new InvalidArgumentError('Worker not found ' + workerName);
        }

        this._workerHandlers[workerName] = handler;
        return handler;
    }
}
