"use strict";
var Logger_1 = require("../../Logger");
var VideaError_1 = require("../../VideaError");
var WorkerScheduler = (function () {
    function WorkerScheduler(config) {
        this._workerService = config.workerService;
        this._workerRunner = config.workerRunner;
        this._updateRate = config.updateRate;
        this._workerQueue = {};
        this._workerQueueLimit = config.workerQueueLimit || 0;
        this._loopCount = 0;
    }
    WorkerScheduler.prototype.run = function () {
        if (this._isRunning) {
            throw new VideaError_1.InternalServerError('Worker scheduler is already running.');
        }
        this._isRunning = true;
        this.loop();
    };
    WorkerScheduler.prototype.loop = function () {
        var _this = this;
        this._loopCount++;
        if (!this._isRunning) {
            return;
        }
        var numOfWorkers = Object.keys(this._workerQueue);
        if (this._workerQueueLimit > 0 && numOfWorkers.length >= this._workerQueueLimit) {
            // keep waiting until at least one worker finishes
            this.setupNextLoop();
            return;
        }
        this._workerService.pullWorkerFromQueue().then(function (worker) {
            var workerId = worker.id;
            if (_this._workerQueue[workerId]) {
                return;
            }
            var workerPromise = _this._workerRunner.run(worker);
            _this._workerQueue[workerId] = workerPromise;
            workerPromise.then(function () {
                Logger_1.Logger.info('Completed worker with Id' + workerId);
            }).catch(function (err) {
                Logger_1.Logger.error('Failed execution of worker with Id ' + workerId, err);
            }).finally(function () {
                delete _this._workerQueue[workerId];
            });
        }).catch(function (err) {
            Logger_1.Logger.error(err);
        }).finally(function () {
            _this.setupNextLoop();
        });
    };
    WorkerScheduler.prototype.setupNextLoop = function () {
        var scheduler = this;
        setTimeout(function () { scheduler.loop(); }, this._updateRate);
    };
    return WorkerScheduler;
}());
exports.WorkerScheduler = WorkerScheduler;
