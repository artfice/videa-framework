import {Logger} from '../../Logger';
import {InternalServerError} from '../../VideaError';
import {WorkerSchedulerConfig} from './config/WorkerSchedulerConfig';
import {WorkerRunner} from './WorkerRunner';
import {WorkerService} from './WorkerService';

export class WorkerScheduler {

    private _workerService: WorkerService;
    private _workerRunner: WorkerRunner;
    private _updateRate: number;
    private _isRunning: boolean;
    private _workerQueue: any;
    private _workerQueueLimit: number;
    private _loopCount: number;

    constructor(config: WorkerSchedulerConfig) {
        this._workerService = config.workerService;
        this._workerRunner = config.workerRunner;
        this._updateRate = config.updateRate;
        this._workerQueue = {};
        this._workerQueueLimit = config.workerQueueLimit || 0;
        this._loopCount = 0;
    }

    public run() {
        if (this._isRunning) {
            throw new InternalServerError('Worker scheduler is already running.');
        }
        this._isRunning = true;
        this.loop();
    }

    private loop(): void {
        this._loopCount++;
        if (!this._isRunning) {
            return;
        }

        const numOfWorkers = Object.keys(this._workerQueue);
        if (this._workerQueueLimit > 0 && numOfWorkers.length >= this._workerQueueLimit) {
            // keep waiting until at least one worker finishes
            this.setupNextLoop();
            return;
        }

        this._workerService.pullWorkerFromQueue().then((worker) => {
            const workerId = worker.id;
            if (this._workerQueue[workerId]) {
                return;
            }
            const workerPromise = this._workerRunner.run(worker);
            this._workerQueue[workerId] = workerPromise;
            workerPromise.then(() => {
                Logger.info('Completed worker with Id' + workerId);
            }).catch((err) => {
                Logger.error('Failed execution of worker with Id ' + workerId, err);
            }).finally(() => {
                delete this._workerQueue[workerId];
            });
        }).catch((err) => {
            Logger.error(err);
        }).finally(() => {
            this.setupNextLoop();
        });
    }

    private setupNextLoop() {
        const scheduler = this;
        setTimeout(() => { scheduler.loop(); }, this._updateRate);
    }

}
