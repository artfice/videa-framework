"use strict";
var Promise = require("bluebird");
var _ = require("lodash");
var WorkerAction_1 = require("../../domain/worker/WorkerAction");
var WorkerState_1 = require("../../domain/worker/WorkerState");
var WorkerServiceSchema_1 = require("../../schema/application/WorkerServiceSchema");
var VideaError_1 = require("../../VideaError");
var WorkerService = (function () {
    function WorkerService(config) {
        this.messageRepository = config.messageRepository;
        this.repository = config.repository;
        this.schemaValidator = config.schemaValidator;
        if (this._schemaValidator) {
            this._serviceSchema = new WorkerServiceSchema_1.WorkerServiceSchema();
            this._schemaValidator.addSchema(this._serviceSchema);
        }
    }
    Object.defineProperty(WorkerService.prototype, "repository", {
        get: function () {
            return this._repository;
        },
        set: function (repository) {
            this._repository = repository;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WorkerService.prototype, "messageRepository", {
        get: function () {
            return this._messageRepository;
        },
        set: function (repository) {
            this._messageRepository = repository;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WorkerService.prototype, "schemaValidator", {
        get: function () {
            return this._schemaValidator;
        },
        set: function (validator) {
            this._schemaValidator = validator;
        },
        enumerable: true,
        configurable: true
    });
    WorkerService.prototype.pushWorkerToQueue = function (worker) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.validationFilter({ start: { worker: worker } }).then(function () {
                worker.state = WorkerState_1.WorkerState.Pending;
                return _this._repository.add(worker);
            }).then(function (workerResult) {
                if (_this._messageRepository) {
                    var message = {
                        body: JSON.stringify({
                            workerId: workerResult.id,
                            action: WorkerAction_1.WorkerAction.Start
                        })
                    };
                    return _this._messageRepository.sendMessage(message).then(function (msgResult) {
                        resolve(workerResult);
                    }).catch(reject);
                }
                else {
                    resolve(workerResult);
                }
            }).catch(reject);
        });
    };
    WorkerService.prototype.pullWorkerFromQueue = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._messageRepository.pullMessage().then(function (message) {
                if (message) {
                    var workerId = message.body;
                    return _this.getById(workerId).then(resolve).catch(reject);
                }
                else {
                    reject(new VideaError_1.ItemNotFoundError('Worker not found in the queue.'));
                    return;
                }
            }).catch(reject);
        });
    };
    WorkerService.prototype.getById = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.validationFilter({ getById: { id: id } }).then(function () {
                return _this._repository.getById(id);
            }).then(resolve).catch(reject);
        });
    };
    WorkerService.prototype.search = function (options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.validationFilter({ search: { searchParams: options } }).then(function () {
                return _this._repository.search(options);
            }).then(resolve).catch(reject);
        });
    };
    WorkerService.prototype.update = function (id, worker) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.validationFilter({ updateState: { id: id, worker: worker } });
            return _this._repository.update(id, worker).then(resolve).catch(reject);
        });
    };
    WorkerService.prototype.validationFilter = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this._schemaValidator) {
                var validatorResults = _this._schemaValidator.validate(_this._serviceSchema.getId(), value);
                if (!_.isUndefined(validatorResults.errorsText)) {
                    reject(new VideaError_1.ValidationError('Error validating service schema ' + _this._serviceSchema.getId() +
                        ' caused by: ' + validatorResults.errorsText));
                }
                else {
                    resolve(undefined);
                }
            }
        });
    };
    return WorkerService;
}());
exports.WorkerService = WorkerService;
