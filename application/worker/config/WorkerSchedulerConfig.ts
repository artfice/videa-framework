import {WorkerRunner} from '../WorkerRunner';
import {WorkerService} from '../WorkerService';

export interface WorkerSchedulerConfig {
    workerService: WorkerService;
    workerRunner: WorkerRunner;
    updateRate: number; // period in ms to check messages in the workers queue
    workerQueueLimit: number;
    workerLocations: string[];
}
