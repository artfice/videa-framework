import {Worker} from '../../../domain/worker/Worker';
import {IMessageRepository} from '../../../repository/IMessageRepository';
import {IRepository} from '../../../repository/IRepository';
import {SchemaValidator} from '../../SchemaValidator';

export interface WorkerServiceConfig {
    messageRepository: IMessageRepository;
    schemaValidator: SchemaValidator;
    repository: IRepository<Worker>;
}
