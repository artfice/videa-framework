import {WorkerService} from '../WorkerService';

export interface WorkerRunnerConfig {
    workerService: WorkerService;
    workerLocations: string[];
}
