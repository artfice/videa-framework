"use strict";
var Promise = require("bluebird");
var _ = require("lodash");
var WorkerState_1 = require("../../domain/worker/WorkerState");
var Logger_1 = require("../../Logger");
var VideaError_1 = require("../../VideaError");
var ModuleImporter_1 = require("../helper/ModuleImporter");
var WorkerRunner = (function () {
    function WorkerRunner(config) {
        this._workerImporter = new ModuleImporter_1.ModuleImporter(config.workerLocations);
        this._workerService = config.workerService;
        this._workerHandlers = {};
    }
    WorkerRunner.prototype.startById = function (workerId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this._workerService.getById(workerId).then(function (worker) {
                try {
                    var workerHandler = _this.loadWorkerHandler(worker);
                    resolve(_this.run(worker, workerHandler));
                }
                catch (err) {
                    reject(err);
                }
            });
        });
    };
    WorkerRunner.prototype.run = function (worker, workerHandler) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                if (!workerHandler) {
                    workerHandler = _this.loadWorkerHandler(worker);
                }
                if (_.isUndefined(worker.progress)) {
                    worker.progress = 0;
                }
                return _this._workerService.update(worker.id, { state: WorkerState_1.WorkerState.Running, progress: worker.progress })
                    .then(function (result) {
                    return workerHandler(worker, function (state) {
                        if (state.progress > 1) {
                            state.progress = 1;
                        }
                        return _this._workerService.update(worker.id, { progress: state.progress });
                    });
                }).then(function () {
                    return _this._workerService.update(worker.id, { state: WorkerState_1.WorkerState.Completed });
                }).then(function (result) {
                    resolve(undefined);
                }).catch(function (err) {
                    return _this._workerService.update(worker.id, { state: WorkerState_1.WorkerState.Failed, error: err })
                        .finally(function () {
                        reject(err);
                    });
                });
            }
            catch (err) {
                reject(err);
            }
        });
    };
    WorkerRunner.prototype.loadWorkerHandler = function (worker) {
        var workerName = worker.taskName;
        var handler = this._workerHandlers[workerName];
        if (handler) {
            return handler;
        }
        handler = this._workerImporter.importById(workerName);
        if (!handler) {
            Logger_1.Logger.error('Worker "' + workerName + '" not found!');
            throw new VideaError_1.InvalidArgumentError('Worker not found ' + workerName);
        }
        this._workerHandlers[workerName] = handler;
        return handler;
    };
    return WorkerRunner;
}());
exports.WorkerRunner = WorkerRunner;
