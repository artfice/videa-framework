import _ = require('lodash');
import Ajv = require('ajv');

import {SchemaApplicationService} from './SchemaApplicationService';

import {ValidatorResult} from '../domain/validation/ValidatorResult';
import {JsonSchema} from '../schema/JsonSchema';
import {SchemaError} from '../VideaError';

export class SchemaValidator {

    private _ajv: any;
    private  _schemaService: SchemaApplicationService;

    constructor(schemaService: SchemaApplicationService) {
        this._ajv = new Ajv();
        this._schemaService = schemaService;
    }

    public addSchema(schema: JsonSchema): void {
        const id = schema.getId();

        if (_.isUndefined(id)) {
            throw new SchemaError('Schema\'s Id property is required to add a schema to the validator.');
        }

        if (this._ajv.getSchema(id)) {
            return;
        }

        const s = schema.getSchema();
        this._ajv.addSchema(s, id);
        const schemaRefs = schema.getSchemaRefs();
        if (!_.isEmpty(schemaRefs)) {
            for (let i = 0; i < schemaRefs.length; i++) {
                const refSchemaId = schemaRefs[i];
                this.addSchema(this._schemaService.getJsonSchema(refSchemaId));
            }
        }
    }

    public validate(schemaId: string, obj: any) : ValidatorResult {
        const result = <ValidatorResult> {
            valid: this._ajv.validate(schemaId, obj)
        };
        if (!result.valid) {
            result.errorsText = this._ajv.errorsText();
        }
        return result;
    }
}
