"use strict";
var Promise = require("bluebird");
var VideaError_1 = require("../../VideaError");
var ImageService = (function () {
    function ImageService(config) {
        this._repository = config.repository;
        this._imageTypeService = config.imageTypeService;
    }
    ImageService.prototype.createExternalHostedImg = function (tenantId, image) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!image.imageType) {
                return reject(new VideaError_1.InvalidArgumentError('ImageType must be specified'));
            }
            if (!image.url || !image.rawUrl) {
                return reject(new VideaError_1.InvalidArgumentError('URL and RawURL must be specified'));
            }
            return _this._imageTypeService.getById(tenantId, image.imageType).then(function (imageType) {
                return _this._repository.createExternalHostedImg(tenantId, image);
            }).then(resolve).catch(reject);
        });
    };
    ImageService.prototype.upload = function (tenantId, image, stream) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!image.imageType) {
                return reject(new VideaError_1.InvalidArgumentError('ImageType must be specified'));
            }
            return _this._imageTypeService.getById(tenantId, image.imageType).then(function (imageType) {
                return _this._repository.upload(tenantId, image, stream);
            }).then(resolve).catch(reject);
        });
    };
    ImageService.prototype.uploadByUrl = function (tenantId, image, url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!image.imageType) {
                return reject(new VideaError_1.InvalidArgumentError('ImageType must be specified'));
            }
            return _this._imageTypeService.getById(tenantId, image.imageType).then(function (imageType) {
                return _this._repository.uploadByUrl(tenantId, image, url);
            }).then(resolve).catch(reject);
        });
    };
    ImageService.prototype.remove = function (tenantId, imageId) {
        return this._repository.remove(tenantId, imageId);
    };
    return ImageService;
}());
exports.ImageService = ImageService;
