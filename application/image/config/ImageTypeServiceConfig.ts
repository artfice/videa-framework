import { ImageType } from '../../../domain/image/ImageType';
import { IImageTypeRepository } from '../../../repository/IImageTypeRepository';
import { AccountBasedServiceConfig } from '../../config/AccountBasedServiceConfig';

export interface ImageTypeServiceConfig extends AccountBasedServiceConfig<ImageType> {
    secondaryRepository: IImageTypeRepository;
}
