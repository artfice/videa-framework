import {IImageRepository} from '../../../repository/IImageRepository';
import {ImageTypeService} from '../ImageTypeService';

export interface ImageServiceConfig {
    imageTypeService: ImageTypeService;
    repository: IImageRepository;
}
