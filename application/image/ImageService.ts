import Promise = require('bluebird');

import {Image} from '../../domain/image/Image';

import {IImageRepository} from '../../repository/IImageRepository';
import {InvalidArgumentError} from '../../VideaError';
import {ImageServiceConfig} from './config/ImageServiceConfig';
import {ImageTypeService} from './ImageTypeService';

export class ImageService {
    protected _imageTypeService: ImageTypeService;
    protected _repository: IImageRepository;

    constructor(config: ImageServiceConfig) {
        this._repository = config.repository;
        this._imageTypeService = config.imageTypeService;
    }

    public createExternalHostedImg(tenantId: string, image: Image): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            if (!image.imageType) {
                return reject(new InvalidArgumentError('ImageType must be specified'));
            }
            if (!image.url || !image.rawUrl) {
                return reject(new InvalidArgumentError('URL and RawURL must be specified'));
            }
            return this._imageTypeService.getById(tenantId, image.imageType).then((imageType) => {
                return this._repository.createExternalHostedImg(tenantId, image);
            }).then(resolve).catch(reject);
        });
    }

    public upload(tenantId: string, image: Image, stream: any): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            if (!image.imageType) {
                return reject(new InvalidArgumentError('ImageType must be specified'));
            }
            return this._imageTypeService.getById(tenantId, image.imageType).then((imageType) => {
                return this._repository.upload(tenantId, image, stream);
            }).then(resolve).catch(reject);
        });
    }

    public uploadByUrl(tenantId: string, image: Image, url: string): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            if (!image.imageType) {
                return reject(new InvalidArgumentError('ImageType must be specified'));
            }
            return this._imageTypeService.getById(tenantId, image.imageType).then((imageType) => {
                return this._repository.uploadByUrl(tenantId, image, url);
            }).then(resolve).catch(reject);
        });
    }

    public remove(tenantId: string, imageId: string): Promise<void> {
        return this._repository.remove(tenantId, imageId);
    }
}
