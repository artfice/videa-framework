"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require("bluebird");
var AccountBasedService_1 = require("../AccountBasedService");
var ImageTypeService = (function (_super) {
    __extends(ImageTypeService, _super);
    function ImageTypeService(config) {
        var _this = _super.call(this, config) || this;
        _this._secondaryRepository = config.secondaryRepository;
        return _this;
    }
    ImageTypeService.prototype.add = function (accountId, imageType) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _super.prototype.add.call(_this, accountId, imageType).then(function (result) {
                return _this._secondaryRepository.add(accountId, result);
            }).then(resolve).catch(reject);
        });
    };
    ImageTypeService.prototype.remove = function (accountId, id, query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _super.prototype.remove.call(_this, accountId, id, query).then(function () {
                return _this._secondaryRepository.remove(accountId, id, query);
            }).then(resolve).catch(reject);
        });
    };
    ImageTypeService.prototype.createTransformation = function (tenantId, imageTypeId, imageType) {
        return this._secondaryRepository.createTransformation(tenantId, imageTypeId, imageType);
    };
    return ImageTypeService;
}(AccountBasedService_1.AccountBasedService));
exports.ImageTypeService = ImageTypeService;
