import * as Promise from 'bluebird';

import { SearchQuery } from '../../domain/db/SearchQuery';
import { ImageType } from '../../domain/image/ImageType';
import { AccountBasedService } from '../AccountBasedService';
import { ImageTypeServiceConfig } from './config/ImageTypeServiceConfig';

import { IImageTypeRepository } from '../../repository/IImageTypeRepository';

export class ImageTypeService extends AccountBasedService<ImageType> {
    private _secondaryRepository: IImageTypeRepository;

    constructor(config: ImageTypeServiceConfig) {
        super(config);
        this._secondaryRepository = config.secondaryRepository;
    }

    public add(accountId: string, imageType: ImageType): Promise<ImageType> {
        return new Promise<ImageType>((resolve, reject) => {
            return super.add(accountId, imageType).then((result) => {
                return this._secondaryRepository.add(accountId, result);
            }).then(resolve).catch(reject);
        });
    }

    public remove(accountId: string, id: string, query?: SearchQuery): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            return super.remove(accountId, id, query).then(() => {
                return this._secondaryRepository.remove(accountId, id, query);
            }).then(resolve).catch(reject);
        });
    }

    public createTransformation(tenantId: string, imageTypeId: string, imageType: ImageType) : Promise<void> {
        return this._secondaryRepository.createTransformation(tenantId, imageTypeId, imageType);
    }
}
