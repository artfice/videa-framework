"use strict";
// import mocha for all typings
require("mocha");
var dotenv = require("dotenv");
dotenv.config();
var fixtures = require("pow-mongodb-fixtures");
var connection = fixtures.connect('testaccount00001');
var FixturesHelper_1 = require("./helper/FixturesHelper");
var data = FixturesHelper_1.FixturesHelper.loadFixturesData(module, './fixtures');
beforeEach(function (callback) {
    connection.clearAllAndLoad(data, callback);
});
after(function () {
    connection.clear(function (err) {
        console.log(err);
    });
});
