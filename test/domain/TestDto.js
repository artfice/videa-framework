"use strict";
exports.TestDtoFieldType = {
    DateTime: 'DateTime',
    Dropdown: 'Dropdown',
    File: 'File',
    Image: 'Image',
    Integer: 'Integer',
    Rating: 'Rating',
    Reference: 'Reference',
    TextArea: 'TextArea',
    Text: 'Text',
    URL: 'URL',
    VideoURL: 'VideoURL'
};
