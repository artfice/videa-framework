import {BaseDto} from '../../domain';

export interface TestDto extends BaseDto {
    name: string; // machine name
    title: string; // label that is displayed
    fieldType: string;
    size: number;
    isMandatory: boolean;
    initialValue: string;
}

export const TestDtoFieldType = {
    DateTime: 'DateTime',
    Dropdown: 'Dropdown',
    File: 'File',
    Image: 'Image',
    Integer: 'Integer',
    Rating: 'Rating',
    Reference: 'Reference',
    TextArea: 'TextArea',
    Text: 'Text',
    URL: 'URL',
    VideoURL: 'VideoURL'
};

