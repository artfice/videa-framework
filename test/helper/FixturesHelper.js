"use strict";
var requireDirectory = require("require-directory");
var FixturesHelper;
(function (FixturesHelper) {
    function loadFixturesData(m, folder) {
        var data = {};
        requireDirectory(m, folder, { visit: function (obj) {
                var keys = Object.keys(obj);
                for (var i = 0; i < keys.length; i++) {
                    data[keys[i]] = obj[keys[i]];
                }
            } });
        return data;
    }
    FixturesHelper.loadFixturesData = loadFixturesData;
})(FixturesHelper = exports.FixturesHelper || (exports.FixturesHelper = {}));
