import requireDirectory = require('require-directory');

export module FixturesHelper {
   export function loadFixturesData(m: any, folder: string): any {
       const data = {};
       requireDirectory(m, folder, { visit: (obj) => {
           const keys = Object.keys(obj);
           for (let i = 0; i < keys.length; i++) {
               data[keys[i]] = obj[keys[i]];
           }
       }});
       return data;
   }
}
