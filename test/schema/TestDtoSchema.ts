import {BaseSchema} from '../../schema/BaseSchema';
import {DataTypes} from '../../schema/DataTypes';
import {
    NumericSchemaProperty,
    PrimitiveSchemaProperty,
    StringSchemaProperty} from '../../schema/SchemaProperties';
import {TestDtoFieldType} from '../domain/TestDto';

class TestDtoSchema extends BaseSchema {
    public static ID = 'schema.TestDtoSchema';

    constructor() {

        super();
        this.setId(TestDtoSchema.ID);
        this.setName('TestDto');
        this.setTitle('Test DTO');

        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'name',
                typeName: DataTypes.STRING,
                title: 'Name',
                minLength: 3
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'title',
                typeName: DataTypes.STRING,
                title: 'Title'
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'fieldType',
                typeName: DataTypes.STRING,
                title: 'Field Type',
                enumValues: [
                    TestDtoFieldType.DateTime,
                    TestDtoFieldType.Dropdown,
                    TestDtoFieldType.File,
                    TestDtoFieldType.Image,
                    TestDtoFieldType.Integer,
                    TestDtoFieldType.Rating,
                    TestDtoFieldType.Reference,
                    TestDtoFieldType.TextArea,
                    TestDtoFieldType.Text,
                    TestDtoFieldType.URL,
                    TestDtoFieldType.VideoURL
                ]
            });
        this.addPrimitiveProperty(
            <NumericSchemaProperty>{
                name: 'size',
                typeName: DataTypes.NUMBER,
                title: 'Size',
                help: 'The size of the field using the bootstrap layout grid as reference.'
            });
        this.addPrimitiveProperty(
            <PrimitiveSchemaProperty>{
                name: 'isMandatory',
                typeName: DataTypes.BOOLEAN,
                title: 'Is mandatory'
            });
        this.addPrimitiveProperty(
            <StringSchemaProperty>{
                name: 'initialValue',
                typeName: DataTypes.STRING,
                title: 'Initial Value'
            });
    }
}

export = TestDtoSchema;
