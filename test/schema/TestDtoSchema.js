"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseSchema_1 = require("../../schema/BaseSchema");
var DataTypes_1 = require("../../schema/DataTypes");
var TestDto_1 = require("../domain/TestDto");
var TestDtoSchema = (function (_super) {
    __extends(TestDtoSchema, _super);
    function TestDtoSchema() {
        var _this = _super.call(this) || this;
        _this.setId(TestDtoSchema.ID);
        _this.setName('TestDto');
        _this.setTitle('Test DTO');
        _this.addPrimitiveProperty({
            name: 'name',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Name',
            minLength: 3
        });
        _this.addPrimitiveProperty({
            name: 'title',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Title'
        });
        _this.addPrimitiveProperty({
            name: 'fieldType',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Field Type',
            enumValues: [
                TestDto_1.TestDtoFieldType.DateTime,
                TestDto_1.TestDtoFieldType.Dropdown,
                TestDto_1.TestDtoFieldType.File,
                TestDto_1.TestDtoFieldType.Image,
                TestDto_1.TestDtoFieldType.Integer,
                TestDto_1.TestDtoFieldType.Rating,
                TestDto_1.TestDtoFieldType.Reference,
                TestDto_1.TestDtoFieldType.TextArea,
                TestDto_1.TestDtoFieldType.Text,
                TestDto_1.TestDtoFieldType.URL,
                TestDto_1.TestDtoFieldType.VideoURL
            ]
        });
        _this.addPrimitiveProperty({
            name: 'size',
            typeName: DataTypes_1.DataTypes.NUMBER,
            title: 'Size',
            help: 'The size of the field using the bootstrap layout grid as reference.'
        });
        _this.addPrimitiveProperty({
            name: 'isMandatory',
            typeName: DataTypes_1.DataTypes.BOOLEAN,
            title: 'Is mandatory'
        });
        _this.addPrimitiveProperty({
            name: 'initialValue',
            typeName: DataTypes_1.DataTypes.STRING,
            title: 'Initial Value'
        });
        return _this;
    }
    return TestDtoSchema;
}(BaseSchema_1.BaseSchema));
TestDtoSchema.ID = 'schema.TestDtoSchema';
module.exports = TestDtoSchema;
