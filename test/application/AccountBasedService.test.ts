import Promise = require('bluebird');
import * as _ from 'lodash';

import chai = require ('chai');
const expect = chai.expect;

import {AccountBasedService} from '../../application/AccountBasedService';
import {ResultSet} from '../../domain/db/ResultSet';
import {SearchOperators} from '../../domain/db/SearchOperators';
import {SearchParams} from '../../domain/db/SearchParams';
import {SearchQuery} from '../../domain/db/SearchQuery';
import {UpdateOptions} from '../../domain/db/UpdateOptions';

import {
    DuplicatedItemIdError,
    InvalidArgumentError,
    ItemNotFoundError,
    ValidationError
} from '../../VideaError';

import {MongodbAdapterConfigHelper as AdapterConfigHelper} from '../../adapter/db/mongodb';
import {AccountBasedServiceConfig} from '../../application/config/AccountBasedServiceConfig';
import {SchemaApplicationService} from '../../application/SchemaApplicationService';
import {AccountBaseDtoRepository} from '../../repository/mongodb/AccountBaseDtoRepository';

import {TestDto, TestDtoFieldType} from '../domain/TestDto';
import {TestDtos as TestDtoSampleData} from '../fixtures/TestDtos';
import TestDtoSchema = require('../schema/TestDtoSchema');
import * as TestSetup from './TestSetup';
import {SchemaApplicationServiceConfig} from '../../application/config/SchemaApplicationServiceConfig';

describe('AccountBasedService Test', () => {

    const sortByField = 'name';

    const pad = (num: number, size: number): string => {
        let s = num + '';
        while (s.length < size) {
            s = '0' + s;
        }
        return s;
    };

    const generateSampleDto = (index: number = 1): TestDto => {
        const name = 'TestContentType' + pad(index, 5);
        return <TestDto> {
            name: name,
            title: 'title' + name,
            fieldType: TestDtoFieldType.DateTime
        };
    };

    const getSampleDto = (index: number = 0): TestDto => {
        return <TestDto>TestDtoSampleData[index].document_v1;
    };

    const getSampleDtos = (total?: number): TestDto[] => {
        if (_.isUndefined(total)) {
            total = TestDtoSampleData.length;
        }
        const dtos = [];
        for (let i = 0; i < total; i++) {
            dtos.push(getSampleDto(i));
        }
        return dtos;
    };

    const getSampleFieldsToUpdate = (): TestDto => {
        return <TestDto>{
            name: 'modified name',
            title: 'modified title'
        };
    };

    const expectValidDto = (current: TestDto, expected: TestDto) => {
        expect(current).to.have.property('id');
        expect(current).to.have.property('createdDate');
        expect(current).to.have.property('modifiedDate');
    };

    const schemaService = new SchemaApplicationService(<SchemaApplicationServiceConfig>{
        schemaLocations: [
            __dirname + '/../..', // add framework schema folder
            __dirname + '/..' // add test schema folder
        ]
    });
    const baseSchema = new TestDtoSchema();
    const repoConfig = AdapterConfigHelper.getDtoMongoDbAdapterConfig(baseSchema.getName());
    const repository = new AccountBaseDtoRepository<TestDto>(repoConfig);
    const config: AccountBasedServiceConfig<TestDto> = {
        baseSchema: baseSchema,
        schemaValidator: schemaService.getGlobalValidator(),
        repository: repository
    };
    const testService = new AccountBasedService<TestDto>(config);

    describe('Creating a new DTO ', () => {
        it('should add a new DTO to the database', () => {
            const dto = generateSampleDto();
            return testService.add(TestSetup.accountId, dto).then((newDto) => {
                expectValidDto(newDto, dto);
            });
        });
    });

    describe('Creating a new DTO with an existing id', () => {
        it('should throw a DuplicatedItemIdError error', () => {
            const dto = generateSampleDto();
            const existingDto = getSampleDto();
            dto.id = existingDto.id;
            return testService.add(TestSetup.accountId, dto).then((newDto) => {
                // we shouldn't be here
                throw new Error('complete add service with success instead of Error');
            }).catch((err) => {
                expect(err).to.be.instanceof(DuplicatedItemIdError);
            });
        });
    });

    describe('Getting an existing DTO by Id', () => {
        it('should retrieve the expect DTO details', () => {
            const dto = getSampleDto();
            return testService.getById(TestSetup.accountId, dto.id).then((resultDto) => {
                expectValidDto(resultDto, dto);
            });
        });
    });

    describe('Getting a non-existing DTO by Id', () => {
        it('should retrieve an ItemNotFound error', () => {
            return testService.getById(TestSetup.accountId, 'Not Exists').then((resultDto) => {
                // we shouldn't be here
                throw new Error('complete add service with success instead of Error');
            }).catch((err) => {
                expect(err).to.be.instanceof(ItemNotFoundError);
            });
        });
    });

    describe('Deleting an existing DTO by Id', () => {
        it('should complete with success', () => {
            const dto = getSampleDto();
            return testService.remove(TestSetup.accountId, dto.id);
        });
    });

    describe('Deleting a non-existing DTO by Id', () => {
        it('should retrieve an ItemNotFound error', () => {
            return testService.remove(TestSetup.accountId, 'Not Exists').then((resultDto) => {
                // we shouldn't be here
                throw new Error('complete remove service with success instead of Error');
            }).catch((err) => {
                expect(err).to.be.instanceof(ItemNotFoundError);
            });
        });
    });

    describe('Partially updating an existing DTO by Id', () => {
        it('should complete with success', () => {
            const dto = getSampleDto();
            const updateDtoFields = getSampleFieldsToUpdate();

            return testService.update(TestSetup.accountId, dto.id, updateDtoFields).then(() => {
                // copy the modified properties to validate the dto later
                const keys = Object.keys(updateDtoFields);
                for (let i = 0; i < keys.length; i++) {
                    (<any>dto)[keys[i]] = updateDtoFields[keys[i]];
                }
                return testService.getById(TestSetup.accountId, dto.id).then((updatedDto) => {
                    expectValidDto(updatedDto, dto);
                    expect(updatedDto.name).eq(dto.name);
                });
            });
        });
    });

    describe('Trying to modify the Id of an existing DTO', () => {
        it('should throw an error', () => {
            const dto = getSampleDto();

            const updateDtoFields = <TestDto>{
                id: 'Modified Id'
            };

            return testService.update(TestSetup.accountId, dto.id, updateDtoFields).then(() => {
                // we shouldn't be here
                throw new Error('completed update service with success instead of error');
            }).catch((err) => {
                expect(err).to.be.instanceof(InvalidArgumentError);
            });
        });
    });

    describe('Update or add a DTO', () => {
        it('should complete with success', () => {
            const dto = generateSampleDto();

            const updateOptions = <UpdateOptions>{
                upsert: true
            };

            return testService.update(TestSetup.accountId, undefined, dto, updateOptions).then((updateResult) => {
                return testService.getById(TestSetup.accountId, updateResult.id).then((updatedDto) => {
                    expectValidDto(updatedDto, dto);
                });
            });
        });
    });

    describe('Replace an existing DTO by Id', () => {
        it('should complete with success', () => {
            const dto = getSampleDto();

            // HACK: we need to temporarily remove the dates because the schemas doesn't support them yet
            // and the service validator would fail
            delete dto.createdDate;
            delete dto.modifiedDate;

            const updateDtoFields = getSampleFieldsToUpdate();
            const keys = Object.keys(updateDtoFields);
            for (let i = 0; i < keys.length; i++) {
                const key = keys[i];
                (<any>dto)[key] = updateDtoFields[key]; // doesn't include the ID
            }

            // send the original dto and the id should be included
            return testService.replace(TestSetup.accountId, dto.id, dto).then(() => {
                return testService.getById(TestSetup.accountId, dto.id).then((updatedDto) => {
                    expectValidDto(updatedDto, dto);
                });
            });
        });
    });

    describe('Finding DTOs sorted by ' + sortByField + ' and limited setSize', () => {
        it('should return a result set with the right number of results', () => {
            const sampleDataSetSize = 10;
            let dtos = getSampleDtos();
            const sampleDataCount = dtos.length;
            dtos = dtos.sort((a, b) => {
                return -1 * a[sortByField].localeCompare(b[sortByField]);
            });

            const numOfBatches = Math.ceil(sampleDataCount / sampleDataSetSize);
            const batches = [];
            for (let i = 0; i < numOfBatches; i++) {
                const offset = i * sampleDataSetSize;
                const p = testService.search(TestSetup.accountId, <SearchParams>{
                    setSize: sampleDataSetSize,
                    offset: offset,
                    sort: sortByField,
                    ascending: false
                })
                    .bind({offset: offset, validateDto: expectValidDto})
                    .then(function (result: ResultSet) {
                        const expectedresults = Math.min(sampleDataCount - this.offset, sampleDataSetSize);
                        expect(result.data.length).to.be.equal(expectedresults);
                        for (let u = 0; u < expectedresults; u++) {
                            expect(result.data[u].id).to.be.equal(dtos[this.offset + u].id);
                            expectValidDto(result.data[u], dtos[this.offset + u]);
                        }
                    }).catch((err) => {
                        throw err;
                    });
                batches.push(p);
            }

            return Promise.all(batches);
        });
    });

    describe('Finding a list of given DTOs ids sorted by ' + sortByField, () => {
        it('should return a result set with the right number of results', () => {
            const dtos = getSampleDtos();
            const sampleDataCount = dtos.length;
            let dtosToFind = [];
            const idsToFind = [];
            for (let i = 0; i < sampleDataCount; i += 2) {
                dtosToFind.push(dtos[i]);
                idsToFind.push(dtos[i].id);
            }

            dtosToFind = dtosToFind.sort((a, b) => {
                return -1 * a[sortByField].localeCompare(b[sortByField]);
            });

            return testService.search(TestSetup.accountId, <SearchParams>{
                setSize: sampleDataCount,
                offset: 0,
                sort: sortByField,
                ascending: false,
                query: <SearchQuery>{
                    field: 'id',
                    operator: SearchOperators.In,
                    value: idsToFind
                }
            })
                .bind({validateDto: expectValidDto})
                .then(function (result: ResultSet) {
                const expectedresults = dtosToFind.length;
                expect(result.data.length).to.be.equal(expectedresults);
                for (let u = 0; u < expectedresults; u++) {
                    expect(result.data[u].id).to.be.equal(dtosToFind[u].id);
                    expectValidDto(result.data[u], dtosToFind[u]);
                }
            });
        });
    });

    describe('Calling the search function with an undefined account id ', () => {
        it('should return an error', () => {
            const sampleDataCount = 1;
            return testService.search(undefined, <SearchParams>{
                setSize: sampleDataCount,
                offset: 0,
                sort: sortByField,
                ascending: false
            })
                .then((result) => {
                    // we shouldn't be here
                    throw new Error('completed update service with success instead of error');
                }).catch((err) => {
                    expect(err).to.be.instanceof(ValidationError, 'should throw invalid argument error');
                    expect(err.message).to.contain('should have required property \'tenantId\'');
                });
        });
    });
});
