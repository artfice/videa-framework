import {AccountBasedService} from '../../../application/AccountBasedService';
import {BaseDto} from '../../../domain';

export interface AccountBasedServiceTestConfig<T extends BaseDto> {
    accountId: string;
    service: AccountBasedService<T>;
    sortByField: string;
}
