"use strict";
var Promise = require("bluebird");
var _ = require("lodash");
var chai = require("chai");
var expect = chai.expect;
var AccountBasedService_1 = require("../../application/AccountBasedService");
var SearchOperators_1 = require("../../domain/db/SearchOperators");
var VideaError_1 = require("../../VideaError");
var mongodb_1 = require("../../adapter/db/mongodb");
var SchemaApplicationService_1 = require("../../application/SchemaApplicationService");
var AccountBaseDtoRepository_1 = require("../../repository/mongodb/AccountBaseDtoRepository");
var TestDto_1 = require("../domain/TestDto");
var TestDtos_1 = require("../fixtures/TestDtos");
var TestDtoSchema = require("../schema/TestDtoSchema");
var TestSetup = require("./TestSetup");
describe('AccountBasedService Test', function () {
    var sortByField = 'name';
    var pad = function (num, size) {
        var s = num + '';
        while (s.length < size) {
            s = '0' + s;
        }
        return s;
    };
    var generateSampleDto = function (index) {
        if (index === void 0) { index = 1; }
        var name = 'TestContentType' + pad(index, 5);
        return {
            name: name,
            title: 'title' + name,
            fieldType: TestDto_1.TestDtoFieldType.DateTime
        };
    };
    var getSampleDto = function (index) {
        if (index === void 0) { index = 0; }
        return TestDtos_1.TestDtos[index].document_v1;
    };
    var getSampleDtos = function (total) {
        if (_.isUndefined(total)) {
            total = TestDtos_1.TestDtos.length;
        }
        var dtos = [];
        for (var i = 0; i < total; i++) {
            dtos.push(getSampleDto(i));
        }
        return dtos;
    };
    var getSampleFieldsToUpdate = function () {
        return {
            name: 'modified name',
            title: 'modified title'
        };
    };
    var expectValidDto = function (current, expected) {
        expect(current).to.have.property('id');
        expect(current).to.have.property('createdDate');
        expect(current).to.have.property('modifiedDate');
    };
    var schemaService = new SchemaApplicationService_1.SchemaApplicationService({
        schemaLocations: [
            __dirname + '/../..',
            __dirname + '/..' // add test schema folder
        ]
    });
    var baseSchema = new TestDtoSchema();
    var repoConfig = mongodb_1.MongodbAdapterConfigHelper.getDtoMongoDbAdapterConfig(baseSchema.getName());
    var repository = new AccountBaseDtoRepository_1.AccountBaseDtoRepository(repoConfig);
    var config = {
        baseSchema: baseSchema,
        schemaValidator: schemaService.getGlobalValidator(),
        repository: repository
    };
    var testService = new AccountBasedService_1.AccountBasedService(config);
    describe('Creating a new DTO ', function () {
        it('should add a new DTO to the database', function () {
            var dto = generateSampleDto();
            return testService.add(TestSetup.accountId, dto).then(function (newDto) {
                expectValidDto(newDto, dto);
            });
        });
    });
    describe('Creating a new DTO with an existing id', function () {
        it('should throw a DuplicatedItemIdError error', function () {
            var dto = generateSampleDto();
            var existingDto = getSampleDto();
            dto.id = existingDto.id;
            return testService.add(TestSetup.accountId, dto).then(function (newDto) {
                // we shouldn't be here
                throw new Error('complete add service with success instead of Error');
            }).catch(function (err) {
                expect(err).to.be.instanceof(VideaError_1.DuplicatedItemIdError);
            });
        });
    });
    describe('Getting an existing DTO by Id', function () {
        it('should retrieve the expect DTO details', function () {
            var dto = getSampleDto();
            return testService.getById(TestSetup.accountId, dto.id).then(function (resultDto) {
                expectValidDto(resultDto, dto);
            });
        });
    });
    describe('Getting a non-existing DTO by Id', function () {
        it('should retrieve an ItemNotFound error', function () {
            return testService.getById(TestSetup.accountId, 'Not Exists').then(function (resultDto) {
                // we shouldn't be here
                throw new Error('complete add service with success instead of Error');
            }).catch(function (err) {
                expect(err).to.be.instanceof(VideaError_1.ItemNotFoundError);
            });
        });
    });
    describe('Deleting an existing DTO by Id', function () {
        it('should complete with success', function () {
            var dto = getSampleDto();
            return testService.remove(TestSetup.accountId, dto.id);
        });
    });
    describe('Deleting a non-existing DTO by Id', function () {
        it('should retrieve an ItemNotFound error', function () {
            return testService.remove(TestSetup.accountId, 'Not Exists').then(function (resultDto) {
                // we shouldn't be here
                throw new Error('complete remove service with success instead of Error');
            }).catch(function (err) {
                expect(err).to.be.instanceof(VideaError_1.ItemNotFoundError);
            });
        });
    });
    describe('Partially updating an existing DTO by Id', function () {
        it('should complete with success', function () {
            var dto = getSampleDto();
            var updateDtoFields = getSampleFieldsToUpdate();
            return testService.update(TestSetup.accountId, dto.id, updateDtoFields).then(function () {
                // copy the modified properties to validate the dto later
                var keys = Object.keys(updateDtoFields);
                for (var i = 0; i < keys.length; i++) {
                    dto[keys[i]] = updateDtoFields[keys[i]];
                }
                return testService.getById(TestSetup.accountId, dto.id).then(function (updatedDto) {
                    expectValidDto(updatedDto, dto);
                    expect(updatedDto.name).eq(dto.name);
                });
            });
        });
    });
    describe('Trying to modify the Id of an existing DTO', function () {
        it('should throw an error', function () {
            var dto = getSampleDto();
            var updateDtoFields = {
                id: 'Modified Id'
            };
            return testService.update(TestSetup.accountId, dto.id, updateDtoFields).then(function () {
                // we shouldn't be here
                throw new Error('completed update service with success instead of error');
            }).catch(function (err) {
                expect(err).to.be.instanceof(VideaError_1.InvalidArgumentError);
            });
        });
    });
    describe('Update or add a DTO', function () {
        it('should complete with success', function () {
            var dto = generateSampleDto();
            var updateOptions = {
                upsert: true
            };
            return testService.update(TestSetup.accountId, undefined, dto, updateOptions).then(function (updateResult) {
                return testService.getById(TestSetup.accountId, updateResult.id).then(function (updatedDto) {
                    expectValidDto(updatedDto, dto);
                });
            });
        });
    });
    describe('Replace an existing DTO by Id', function () {
        it('should complete with success', function () {
            var dto = getSampleDto();
            // HACK: we need to temporarily remove the dates because the schemas doesn't support them yet
            // and the service validator would fail
            delete dto.createdDate;
            delete dto.modifiedDate;
            var updateDtoFields = getSampleFieldsToUpdate();
            var keys = Object.keys(updateDtoFields);
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i];
                dto[key] = updateDtoFields[key]; // doesn't include the ID
            }
            // send the original dto and the id should be included
            return testService.replace(TestSetup.accountId, dto.id, dto).then(function () {
                return testService.getById(TestSetup.accountId, dto.id).then(function (updatedDto) {
                    expectValidDto(updatedDto, dto);
                });
            });
        });
    });
    describe('Finding DTOs sorted by ' + sortByField + ' and limited setSize', function () {
        it('should return a result set with the right number of results', function () {
            var sampleDataSetSize = 10;
            var dtos = getSampleDtos();
            var sampleDataCount = dtos.length;
            dtos = dtos.sort(function (a, b) {
                return -1 * a[sortByField].localeCompare(b[sortByField]);
            });
            var numOfBatches = Math.ceil(sampleDataCount / sampleDataSetSize);
            var batches = [];
            for (var i = 0; i < numOfBatches; i++) {
                var offset = i * sampleDataSetSize;
                var p = testService.search(TestSetup.accountId, {
                    setSize: sampleDataSetSize,
                    offset: offset,
                    sort: sortByField,
                    ascending: false
                })
                    .bind({ offset: offset, validateDto: expectValidDto })
                    .then(function (result) {
                    var expectedresults = Math.min(sampleDataCount - this.offset, sampleDataSetSize);
                    expect(result.data.length).to.be.equal(expectedresults);
                    for (var u = 0; u < expectedresults; u++) {
                        expect(result.data[u].id).to.be.equal(dtos[this.offset + u].id);
                        expectValidDto(result.data[u], dtos[this.offset + u]);
                    }
                }).catch(function (err) {
                    throw err;
                });
                batches.push(p);
            }
            return Promise.all(batches);
        });
    });
    describe('Finding a list of given DTOs ids sorted by ' + sortByField, function () {
        it('should return a result set with the right number of results', function () {
            var dtos = getSampleDtos();
            var sampleDataCount = dtos.length;
            var dtosToFind = [];
            var idsToFind = [];
            for (var i = 0; i < sampleDataCount; i += 2) {
                dtosToFind.push(dtos[i]);
                idsToFind.push(dtos[i].id);
            }
            dtosToFind = dtosToFind.sort(function (a, b) {
                return -1 * a[sortByField].localeCompare(b[sortByField]);
            });
            return testService.search(TestSetup.accountId, {
                setSize: sampleDataCount,
                offset: 0,
                sort: sortByField,
                ascending: false,
                query: {
                    field: 'id',
                    operator: SearchOperators_1.SearchOperators.In,
                    value: idsToFind
                }
            })
                .bind({ validateDto: expectValidDto })
                .then(function (result) {
                var expectedresults = dtosToFind.length;
                expect(result.data.length).to.be.equal(expectedresults);
                for (var u = 0; u < expectedresults; u++) {
                    expect(result.data[u].id).to.be.equal(dtosToFind[u].id);
                    expectValidDto(result.data[u], dtosToFind[u]);
                }
            });
        });
    });
    describe('Calling the search function with an undefined account id ', function () {
        it('should return an error', function () {
            var sampleDataCount = 1;
            return testService.search(undefined, {
                setSize: sampleDataCount,
                offset: 0,
                sort: sortByField,
                ascending: false
            })
                .then(function (result) {
                // we shouldn't be here
                throw new Error('completed update service with success instead of error');
            }).catch(function (err) {
                expect(err).to.be.instanceof(VideaError_1.ValidationError, 'should throw invalid argument error');
                expect(err.message).to.contain('should have required property \'tenantId\'');
            });
        });
    });
});
