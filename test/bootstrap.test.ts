// import mocha for all typings
import 'mocha';

import dotenv = require('dotenv');
dotenv.config();

import fixtures = require('pow-mongodb-fixtures');
const connection = fixtures.connect('testaccount00001');

import {FixturesHelper} from './helper/FixturesHelper';
const data = FixturesHelper.loadFixturesData(module, './fixtures');

beforeEach((callback) => {
    connection.clearAllAndLoad(data, callback);
});

after(() => {
    connection.clear((err) => {
        console.log(err);
    });
});
