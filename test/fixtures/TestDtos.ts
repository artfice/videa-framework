export const TestDtos = [
    {
        "_id": "crew",
        "document_v1": {
            "id": "crew",
            "name": "crew",
            "title": "Crew",
            "fieldType": "Reference",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.756+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.843+0000"),
            "items": [
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d"
            ]
        }
    },
    {
        "_id": "languages",
        "document_v1": {
            "id": "languages",
            "name": "languages",
            "title": "Language(s)",
            "fieldType": "Dropdown",
            "size": 3,
            "isMandatory": false,
            "possibleValues": [
                "English",
                "French",
                "Spanish",
                "Portuguese"
            ],
            "createdDate": new Date("2016-09-30T08:13:15.791+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "rating",
        "document_v1": {
            "id": "rating",
            "name": "rating",
            "title": "Rating",
            "fieldType": "Dropdown",
            "size": 3,
            "isMandatory": false,
            "possibleValues": [
                "G",
                "PG",
                "PG-13",
                "R",
                "NC-17",
                "TV-Y",
                "TV-Y7",
                "TV-G",
                "TV-PG",
                "TV-14",
                "TV-MA"
            ],
            "createdDate": new Date("2016-09-30T08:13:15.804+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d",
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "runtime",
        "document_v1": {
            "id": "runtime",
            "name": "runtime",
            "title": "Run Time",
            "fieldType": "Integer",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.808+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.808+0000")
        }
    },
    {
        "_id": "stars",
        "document_v1": {
            "id": "stars",
            "name": "stars",
            "title": "Stars",
            "fieldType": "Rating",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.825+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.843+0000"),
            "items": [
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d"
            ]
        }
    },
    {
        "_id": "description",
        "document_v1": {
            "id": "description",
            "name": "description",
            "title": "Description",
            "fieldType": "Text",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.763+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "f75d93b3-1ba8-450b-a15a-3d1630646a99",
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d",
                "634821b9-fc5c-4600-9e65-342272f1d4ae",
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "episodeNumber",
        "document_v1": {
            "id": "episodeNumber",
            "name": "episodeNumber",
            "title": "Episode #",
            "fieldType": "Integer",
            "size": 3,
            "isMandatory": true,
            "createdDate": new Date("2016-09-30T08:13:15.778+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.838+0000"),
            "items": [
                "f75d93b3-1ba8-450b-a15a-3d1630646a99"
            ]
        }
    },
    {
        "_id": "images",
        "document_v1": {
            "id": "images",
            "name": "images",
            "title": "Images",
            "fieldType": "Image",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.787+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "f75d93b3-1ba8-450b-a15a-3d1630646a99",
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d",
                "634821b9-fc5c-4600-9e65-342272f1d4ae",
                "5455ed4f-46ea-469c-a699-307060c45174",
                "83c9a10f-cd12-412a-873f-feb06684e65e",
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "seasonNumber",
        "document_v1": {
            "id": "seasonNumber",
            "name": "seasonNumber",
            "title": "Season #",
            "fieldType": "Integer",
            "size": 3,
            "isMandatory": true,
            "createdDate": new Date("2016-09-30T08:13:15.814+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.853+0000"),
            "items": [
                "f75d93b3-1ba8-450b-a15a-3d1630646a99",
                "83c9a10f-cd12-412a-873f-feb06684e65e"
            ]
        }
    },
    {
        "_id": "series",
        "document_v1": {
            "id": "series",
            "name": "series",
            "title": "Series",
            "fieldType": "Text",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.821+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.838+0000"),
            "items": [
                "f75d93b3-1ba8-450b-a15a-3d1630646a99"
            ]
        }
    },
    {
        "_id": "videoUrl",
        "document_v1": {
            "id": "videoUrl",
            "name": "videoUrl",
            "title": "video URL",
            "fieldType": "URL",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.802+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.843+0000"),
            "items": [
                "f75d93b3-1ba8-450b-a15a-3d1630646a99",
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d"
            ]
        }
    },
    {
        "_id": "directedBy",
        "document_v1": {
            "id": "directedBy",
            "name": "directedBy",
            "title": "Directed By",
            "fieldType": "Text",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.766+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.843+0000"),
            "items": [
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d"
            ]
        }
    },
    {
        "_id": "files",
        "document_v1": {
            "id": "files",
            "name": "files",
            "title": "Files",
            "fieldType": "Reference",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.780+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.843+0000"),
            "items": [
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d"
            ]
        }
    },
    {
        "_id": "releaseDate",
        "document_v1": {
            "id": "releaseDate",
            "name": "releaseDate",
            "title": "Release Date",
            "fieldType": "DateTime",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.807+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d",
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "link",
        "document_v1": {
            "id": "link",
            "name": "link",
            "title": "Link",
            "fieldType": "URL",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.796+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.846+0000"),
            "items": [
                "f75d93b3-1ba8-450b-a15a-3d1630646a99",
                "634821b9-fc5c-4600-9e65-342272f1d4ae"
            ]
        }
    },
    {
        "_id": "episodes",
        "document_v1": {
            "id": "episodes",
            "name": "episodes",
            "title": "Episodes",
            "fieldType": "Reference",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.774+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.853+0000"),
            "items": [
                "83c9a10f-cd12-412a-873f-feb06684e65e"
            ]
        }
    },
    {
        "_id": "cast",
        "document_v1": {
            "id": "cast",
            "name": "cast",
            "title": "Cast",
            "fieldType": "Reference",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.750+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d",
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "country",
        "document_v1": {
            "id": "country",
            "name": "country",
            "title": "Country",
            "fieldType": "Text",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.755+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "dateFinished",
        "document_v1": {
            "id": "dateFinished",
            "name": "dateFinished",
            "title": "Date Finished",
            "fieldType": "DateTime",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.759+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "episodeCount",
        "document_v1": {
            "id": "episodeCount",
            "name": "episodeCount",
            "title": "Episode Count",
            "fieldType": "Integer",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.770+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "genre",
        "document_v1": {
            "id": "genre",
            "name": "genre",
            "title": "Genre",
            "fieldType": "Text",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.783+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "ea745547-8de9-41b8-b234-1c7bb53ffa2d",
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "latestEpisode",
        "document_v1": {
            "id": "latestEpisode",
            "name": "latestEpisode",
            "title": "Latest Episode",
            "fieldType": "Integer",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.794+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "longDescription",
        "document_v1": {
            "id": "longDescription",
            "name": "longDescription",
            "title": "Long Description",
            "fieldType": "TextArea",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.798+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "seasonCount",
        "document_v1": {
            "id": "seasonCount",
            "name": "seasonCount",
            "title": "Season Count",
            "fieldType": "Integer",
            "size": 3,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.811+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "seasons",
        "document_v1": {
            "id": "seasons",
            "name": "seasons",
            "title": "Seasons",
            "fieldType": "Reference",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.819+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "synopsis",
        "document_v1": {
            "id": "synopsis",
            "name": "synopsis",
            "title": "Synopsis",
            "fieldType": "TextArea",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.823+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.859+0000"),
            "items": [
                "3ac9418f-196b-4cbf-87d8-f7c6c41d0271"
            ]
        }
    },
    {
        "_id": "items",
        "document_v1": {
            "id": "items",
            "name": "items",
            "title": "Items",
            "fieldType": "Reference",
            "size": 12,
            "isMandatory": false,
            "createdDate": new Date("2016-09-30T08:13:15.817+0000"),
            "modifiedDate": new Date("2016-09-30T08:13:15.863+0000"),
            "items": [
                "1c6378da-8b1a-44f0-b326-3e386cc3b3dc"
            ]
        }
    }
];
