/**
 * Created by bardiakhosravi on 2016-10-14.
 */

declare module 'async-eventemitter' {
    var _q: any;
    export = _q;
}

declare module 'cloudinary' {
    let _q: any;
    export = _q;
}

declare module 'check-types' {
    let _q: any;
    export = _q;
}

declare module 'event-stream' {
    let _q: any;
    export = _q;
}

declare module 'json-schema-build' {
    let _q: any;
    export = _q;
}

declare module 'pmx' {
    let _q: any;
    export = _q;
}

declare module 'streamifier' {
    let _q: any;
    export = _q;
}

declare module 'uplynk-api' {
    let _q: any;
    export = _q;
}

declare module 'winston-logentries' {
    let _q: any;
    export = _q;
}

declare module 'pow-mongodb-fixtures' {
    let _q: any;
    export = _q;
}

declare module 'busboy' {
    let _q: any;
    export = _q;
}

declare module 'require-directory' {
    let _q: any;
    export = _q;
}
