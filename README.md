# videa-framework

Videa Framework for all Videa microservice projects. More info about Videa Framework, see [wiki](https://accedobroadband.jira.com/wiki/display/VIDEA/Videa+Framework)

## First time Setup

```sh
npm install
```

## Modules

1. repository
    * aws
    * cloudinary
    * mongodb

## NPM commands

### Build All Typescript

```sh
npm run build:all
```

### Run Test

```sh
npm run test
```

### Run all lints

```sh
npm run lint
```

### Clean generated js files

```sh
npm run clean
```

### Run a branch test (this will compile the code and run test)

```sh
npm run branch:test
```
