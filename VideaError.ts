
export class VideaError extends Error {

    //This is a HACK to maintain the inheritance for any subclass of built-in class Error, Array, Map
    // In typescriot 2.1, extending built-in class Error, Array and Map breaks the inheritance
    //see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    //see issue: https://github.com/Microsoft/TypeScript/issues/13965
    __proto__: Error;

    constructor(name: string, message?: string, innerErrorMessage?: string) {
        if (innerErrorMessage) {
            message = message + ' caused by ' + innerErrorMessage;
        }
        super(message);

        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = VideaError.prototype;

        this.name = name;
        this.message = message;
        this.stack = (<any> new Error(name + ': ' + message)).stack;
    }
}

export class InternalServerError extends VideaError {

    constructor(message?: string, innerErrorMessage?: string) {
        super('InternalServerError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = InternalServerError.prototype;
    }
}

export class DuplicatedItemIdError extends VideaError {

    constructor(message?: string, innerErrorMessage?: string) {
        super('DuplicatedItemIdError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = DuplicatedItemIdError.prototype;
    }
}

export class InvalidArgumentError extends VideaError {

    constructor(message?: string, innerErrorMessage?: string) {
        super('InvalidArgumentError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = InvalidArgumentError.prototype;
    }
}

export class ItemNotFoundError extends VideaError {

    constructor(message?: string, innerErrorMessage?: string) {
        super('ItemNotFoundError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = ItemNotFoundError.prototype;
    }
}

export class NotImplementedError extends VideaError {

    constructor(message?: string, innerErrorMessage?: string) {
        super('NotImplementedError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = NotImplementedError.prototype;
    }
}

export class SecurityError extends VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('SecurityError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = SecurityError.prototype;
    }
}

export class SchemaError extends VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('SchemaError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = SchemaError.prototype;
    }
}

export class LambdaError extends VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('LambdaError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = LambdaError.prototype;
    }
}

export class ExternalResourceError extends  VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('ExternalResourceError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = ExternalResourceError.prototype;
    }
}

export class JSONParseError extends VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('JSONParseError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = JSONParseError.prototype;
    }
}

export class InvalidWorkflowStateError extends VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('InvalidWorkflowStateError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = InvalidWorkflowStateError.prototype;
    }
}

export class TranslationError extends VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('TranslationError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = TranslationError.prototype;
    }
}

export class ScreenError extends VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('ScreenError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = ScreenError.prototype;
    }
}

export class ValidationError extends VideaError {
    constructor(message?: string, innerErrorMessage?: string) {
        super('ValidationError', message, innerErrorMessage);
        // manually set the prototype, see issue: issue: https://github.com/Microsoft/TypeScript/issues/13965
        this.__proto__ = ValidationError.prototype;
    }
}
