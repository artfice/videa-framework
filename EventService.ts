// TODO: We need to use a distributed PubSub mechanism for this (AWS Simple Notification Service)

import AsyncEventEmitter = require('async-eventemitter');

export const EventService = new AsyncEventEmitter();
