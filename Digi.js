"use strict";
var Promise = require("bluebird");
var check = require("check-types");
var _ = require("lodash");
var String;
(function (String) {
    function capitalize(s) {
        return s.charAt(0).toUpperCase() + s.slice(1);
    }
    String.capitalize = capitalize;
})(String = exports.String || (exports.String = {}));
var Obj;
(function (Obj) {
    function flattenObject(ob) {
        var toReturn = {};
        for (var i in ob) {
            if (!ob.hasOwnProperty(i)) {
                continue;
            }
            if (ob[i] != null && check.object(ob[i])) {
                var flatObject = Obj.flattenObject(ob[i]);
                for (var x in flatObject) {
                    if (!flatObject.hasOwnProperty(x)) {
                        continue;
                    }
                    toReturn[i + '.' + x] = flatObject[x];
                }
            }
            else {
                toReturn[i] = ob[i];
            }
        }
        return toReturn;
    }
    Obj.flattenObject = flattenObject;
    function hasKeys(obj) {
        return Object.keys(obj).length > 0;
    }
    Obj.hasKeys = hasKeys;
    function changeNestedObjectValue(properties, value, original) {
        if (!properties || properties.length === 0) {
            return;
        }
        for (var i = 1; i < properties.length; i++) {
            var property = properties[i - 1];
            if (!original[property]) {
                original[property] = {};
            }
            original = original[property];
        }
        original[properties[properties.length - 1]] = value;
    }
    Obj.changeNestedObjectValue = changeNestedObjectValue;
    function getNestedObjectValue(properties, original) {
        if (!properties || properties.length === 0) {
            return undefined;
        }
        for (var i = 1; i < properties.length; i++) {
            var property = properties[i - 1];
            if (!original[property]) {
                return undefined;
            }
            original = original[property];
        }
        return original[properties[properties.length - 1]];
    }
    Obj.getNestedObjectValue = getNestedObjectValue;
    function deepOmit(obj, keysToOmit) {
        var keysToOmitIndex = _.keyBy(Array.isArray(keysToOmit) ? keysToOmit : [keysToOmit]); // create an index object of the keys that should be omitted
        return _.transform(obj, function (result, value, key) {
            if (key in keysToOmitIndex) {
                return;
            }
            result[key] = _.isObject(value) ? deepOmit(value, keysToOmit) : value; // if the key is an object run it through the inner function - omitFromObject
        });
    }
    Obj.deepOmit = deepOmit;
})(Obj = exports.Obj || (exports.Obj = {}));
var Function;
(function (Function) {
    /**
     * This function lets execute functions with different scopes
     * @param fn the function you want to execute
     * @param args are the arguments you past into the function
     * @param  scope is the scope this function is running from
     */
    function pass(fn, args, scope) {
        return function () {
            var fnArgs = args.slice();
            fnArgs.push.apply(fnArgs, arguments);
            return fn.apply(scope || this, fnArgs);
        };
    }
    Function.pass = pass;
})(Function = exports.Function || (exports.Function = {}));
var List;
(function (List) {
    function exist(list, needle) {
        return list.indexOf(needle) > -1;
    }
    List.exist = exist;
})(List = exports.List || (exports.List = {}));
var Promises;
(function (Promises) {
    function promiseWhile(condition, action) {
        return new Promise(function (resolve, reject) {
            var loop = function () {
                if (!condition()) {
                    resolve(undefined);
                    return;
                }
                return Promise.cast(action())
                    .then(loop)
                    .catch(reject);
            };
            process.nextTick(loop);
        });
    }
    Promises.promiseWhile = promiseWhile;
})(Promises = exports.Promises || (exports.Promises = {}));
