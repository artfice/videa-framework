import {LogAdapter} from './adapter/logging/LogAdapter';

let transports: any[] = [{
    console: {
        prettyPrint: true,
        colorize: true,
        silent: false,
        timestamp: true,
        level: 'trace',
        stderrLevels: ['critical', 'error']
    }
}];

if (!process.env.IS_OFFLINE) {
    transports.push({
        logentries: {
            token: process.env.LOGENTRIES_TOKEN,
            secure: true,
            level: 'trace',
            levels: {
                trace: 0,
                debug: 1,
                info: 2,
                warn: 3,
                error: 4,
                critical: 5
            }
        }
    });
}

export const Logger = new LogAdapter(transports);
